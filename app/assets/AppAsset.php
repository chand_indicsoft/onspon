<?php
	namespace app\assets;

	class AppAsset extends \yii\web\AssetBundle
	{
		public $sourcePath = '@app/media';
		public $css = [
			'css/styles.css',
			'css/salman.css',
			'css/slick.css'
		];
		public $js = [
			'js/jquery.min.js',
			'js/slick.min.js',
			'js/jquery.mixitup.min.js',
			'js/simpleMobileMenu.js',
			'js/bootstrap-datepicker.min.js',
			'js/formden.js',
			'js/jquery.focuspoint.min.js',				
		];
		public $depends = [
			'yii\web\JqueryAsset',
			'yii\bootstrap\BootstrapAsset',
			'yii\bootstrap\BootstrapPluginAsset',
		];
	}
?>