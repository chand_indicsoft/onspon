<?php
namespace app\assets;

class LoginAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@app/media';
    public $css = [
        'css/stylelogin.css',
        'css/materialize.min.css',
        'css/materialize.css',
        'css/login.css',
        'css/salman.css',
        'css/modern-business.css',
    ];
    public $js = [
        'js/jquery.min.js',
        'js/materialize.js',
        'js/salman.js',
        'js/materialize.min.js',
        'js/jquery.mixitup.min.js',
        'js/simpleMobileMenu.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
