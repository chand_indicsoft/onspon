<?php
namespace app\assets;

class DefaultAppAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@app/media';
    public $css = [
		  'css/slick.css',
        'css/evnt-organiser.css',
      
    ];
    public $js = [
       'js/slick.min.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
