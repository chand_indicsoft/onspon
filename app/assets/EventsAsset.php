<?php
namespace app\assets;

class EventsAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@app/media';
    public $css = [
        'css/sponsor.css',
        'css/materialize.min.css',
    ];
    public $js = [
        'js/material.min.js',
	'js/simpleMobileMenu.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
