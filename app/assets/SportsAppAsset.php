<?php
namespace app\assets;

class SportsAppAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@app/media';
    public $css = [

        'css/sports.min.css',
    ];
    public $js = [
       // 'js/scripts.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
