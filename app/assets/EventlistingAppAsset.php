<?php
namespace app\assets;

class EventlistingAppAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@app/media';
    public $css = [
        'css1/style.css',
    ];
    public $js = [
        'js1/jquery.min.js',
        'js1/jquery.mixitup.min.js',
        'js1/simpleMobileMenu.js'
    ];
}