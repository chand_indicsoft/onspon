<?php
namespace app\assets;

class EventAppAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@app/media';
    public $css = [
        'css/own-style.css',
        'css/materialize.min.css',
        'css/materialize.css',
        'css/modern-business.css',
    ];
    public $js = [
        'js/materialize.js',
        'js/materialize.min.js',
        'js/jquery.mixitup.min.js',
        'js/simpleMobileMenu.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
