<?php
	namespace app\assets;

	class SponsorAsset extends \yii\web\AssetBundle
	{
		public $sourcePath = '@app/media';
		public $css = [
			'css/sponsor.css',
			'css/focuspoint.css',
			'css/slick.css',
			'css/login.css',
			'css/salman.css',
			'css/styles.css',
			'css/inbox.css'
		];
		public $js = [
			'js/jquery.min.js',
			'js/material.min.js',
			'js/slick.min.js',
			'js/jquery.focuspoint.min.js',
			'js/simpleMobileMenu.js'
		];
		public $depends = [
			'yii\web\JqueryAsset',
			'yii\bootstrap\BootstrapAsset',
			'yii\bootstrap\BootstrapPluginAsset'
		];
	}
