<?php

namespace app\controllers;

use Yii;
use yii\web\Response;
use yii\helpers\Url;
use yii\easyii\modules\page\models\Page;
use yii\web\Controller;
use yii\widgets\ActiveForm;
use app\modules\users\models\Users;
use app\modules\pet\models\Pet; 
use yii\easyii\modules\faq\api\Faq;
use yii\web\UploadedFile;
use yii\easyii\helpers\Image;
use app\models\CreateEvents;
use yii\easyii\models\Photo;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;

use app\modules\amazing\models\Amazing;
use app\models\Dashboard;
use app\models\Brandmandates;
use app\models\Commonhelper;
use app\models\Seekerprofile;
use app\models\ChangePassword;


class EventdashboardController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ], 
             
        ];
    }
    
	public function beforeAction($action) {
	$this->layout = 'dashboard';	
    $this->enableCsrfValidation = false;
    return parent::beforeAction($action);
	}

	
	public function actionIndex()
	{
		$session = Yii::$app->session;
		
		if ((!isset($session['user_id'])) && (isset($session['role'])!=2)) {
  			return $this->goHome();
  		}else{
  			return $this->render('index');
		}
	}
	
	public function actionInboxhome()
	{
		$session = Yii::$app->session;
		$inbox = Dashboard::UserInboxHome($session['user_id']);
		
		
		$list = '';
		foreach($inbox as $item){ 
		$timeage = Commonhelper::getTimeago($item['date']);
		
		$list .=' <a href="'. Url::to(['eventdashboard/inboxitem','id'=>$item['id']]).'" class="ntfction-anchor-cont '.$calss = ($item['status'] =='0') ? 'read': '';
        $list .= '"><div class="article-thumb-img">';
              if($item['logo']){ $list .='<img src="'.Yii::$app->homeUrl.$item['logo'].'">'; }
         $list .='</div>
              <div class="article-thumb-text-container">
                <h6 class="article-thumb-heading">
                 '.$item['message'].'
                </h6>
                <span class="notifi-evnt-time">
                  <i class="fa fa-clock-o" aria-hidden="true"></i>
                 '.$timeage.'
                </span></div></a>';
		}
		return $list;	
	}
	
	public function actionInbox()
	{	$session = Yii::$app->session;
			if ((!isset($session['user_id'])) && (isset($session['role'])==2)) {
				return $this->goHome();
			}else{
			$modelresult='';
			$id = '';	
			return $this->render('inbox',['modelresult'=>$modelresult,'id'=>$id]);
		}	
	}
	
	public function actionInboxdetails()
	{	$session = Yii::$app->session;
			if ((!isset($session['user_id'])) && (isset($session['role'])==2)) {
				return $this->goHome();
			}else{
		
			$id = $_POST['id'];	
			$type = $_POST['type'];	
			$list = array();
			if($type=='sender'){
				$list = Dashboard::SentDetailsByID($id);
			}
			if($type=='receiver'){
				
				$inbox = $receiverdeatils = Yii::$app->getDb()->createCommand("SELECT status FROM app_sponsor_apply WHERE id='".$id."' ")->queryone();
				if($inbox['status'] == '0')
				{
					$notice =  Yii::$app->getDb()->createCommand("SELECT total_notification FROM app_users WHERE id='".$session['user_id']."' ")->queryone();	
					$total = $notice['total_notification'] - 1;
					$count = Yii::$app->getDb()->createCommand()->update('app_users', ['total_notification' => $total],'id= "'.$session['user_id'].'"')->execute();	
					$status = Yii::$app->getDb()->createCommand()->update('app_sponsor_apply', ['status' => '1'],'id= "'.$id.'"')->execute();	
					
				}
				$list = Dashboard::ReceiveDetailsByID($id);
			}
			
			$result = '';
			foreach($list as $item){
			$date = date("d F Y", strtotime($item['date']));	
			$time = date("H:i a", strtotime($item['date']));
					
			$result .= '<div class="msgHeader"><div>
                	<ul>
                	<li><div class="delTooltip" id="del"> Delete</div>
                        	<a href="javascript:void(0)" onclick="DeleteInbox('.$item['id'].','."'".$type."'".');" id="DeltoolTip" onmouseover="deleteOver();" onmouseout="deleteOut();"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                      </li>
                    </ul>
                </div>
                <ul>
                	<li><i class="fa fa-calendar" aria-hidden="true"></i>'.$date.'</li>
                    <li><i class="fa fa-clock-o" aria-hidden="true"></i>'.$time.'</li>
                </ul>
            </div>
            <div class="msg_Innr_Contr">
           		<p>
            	'.$item['message'].'.
            </p>
            	<p>'.$item['username'].'</p>
            </div>';
       
		}
		return $result;
			
		}	
	}
	
	
	public function actionDeleteinbox()
	{
		$id = $_POST['id'];	
		$type = $_POST['type'];	
		if($type=='sender'){
			$connection = Yii::$app->getDb()->createCommand()->delete('app_sponsor_apply_outbox', ['id' => $id])->execute();
			}
		if($type=='receiver'){
			$connection = Yii::$app->getDb()->createCommand()->delete('app_sponsor_apply', ['id' => $id])->execute();
		}
		echo "Your message deleted.";
	}
	
	public function actionInboxitem($id)
	{
		$session = Yii::$app->session;
		if ((!$session['user_id']) && ($session['role'])!=2) {
			$currentUrl = Url::current();
			$session = Yii::$app->session;
			$session['referral']= $currentUrl;
			
		return $this->redirect(['site/login']);
		}else{
			
			
		$inbox = $receiverdeatils = Yii::$app->getDb()->createCommand("SELECT status FROM app_sponsor_apply WHERE id='".$id."' ")->queryone();
		if($inbox['status'] == '0')
				{
					$notice =  Yii::$app->getDb()->createCommand("SELECT total_notification FROM app_users WHERE id='".$session['user_id']."' ")->queryone();	
					$total = $notice['total_notification'] - 1;
					$count = Yii::$app->getDb()->createCommand()->update('app_users', ['total_notification' => $total],'id= "'.$session['user_id'].'"')->execute();	
					$status = Yii::$app->getDb()->createCommand()->update('app_sponsor_apply', ['status' => '1'],'id= "'.$id.'"')->execute();	
					
				}
		$modelresult = Dashboard::ReceiveDetailsByID($id);
		return $this->render('inbox',['modelresult'=>$modelresult,'id'=>$id]);
		}
	}
	
	
	public function actionContactdetails()
	{
		$id = $_POST['id'];
		$list = Yii::$app->getDb()->createCommand("SELECT * FROM app_inbox WHERE id='".$id."' ")->queryone();	
		
		$date = date("d F Y", strtotime($list['date_created']));	
		$time = date("H:i a", strtotime($list['date_created']));
				
				
			$result = '<div class="msgHeader"><div>
                	<ul>
                	<li><div class="delTooltip" id="del"> Delete</div>
                        	<a href="javascript:void(0)" onclick="DeleteContact('.$list['id'].');" id="DeltoolTip" onmouseover="deleteOver();" onmouseout="deleteOut();"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                      </li>
                    </ul>
                </div>
                <ul>
                	<li><i class="fa fa-calendar" aria-hidden="true"></i>'.$date.'</li>
                    <li><i class="fa fa-clock-o" aria-hidden="true"></i>'.$time.'</li>
                </ul>
            </div>
            <div class="msg_Innr_Contr">
           		<p>
            	'.$list['message'].'.
            </p>
            	<p>Sender Name : '.$list['sender_name'].'</p>
            	<p>Mobile Number : '.$list['sender_mobile'].'</p>
            </div>';
           
           return $result;
				
	}
	
	public function actionDeletecontact()
	{
		$id = $_POST['id'];	
			$connection = Yii::$app->getDb()->createCommand()->delete('app_inbox', ['id' => $id])->execute();
		echo "Your message deleted.";
	}
	
	public function actionEditprofile()
	{
		$session = Yii::$app->session;
			if ((!isset($session['user_id'])) && (isset($session['role'])==2)) {
				return $this->goHome();
			}else{
				
				$model = Seekerprofile::findOne($session['user_id']);
				
				if($model->load(Yii::$app->request->post())){
					
					$model->image = UploadedFile::getInstance($model, 'image');
					if($model->image && $model->validate(['image'])){
                        $model->image = Image::upload($model->image, 'sponsorprofile');
                    }
                    else{
                       $model->image = $model->oldAttributes['image'];
                    }	
				
				
				if($model->save()){
					
					Yii::$app->session->setFlash('success', 'Your profile is Updated.');
					return $this->refresh();
					}	
					
				}	
			else {
				
				return $this->render('editprofile', ['model' => $model]);
			}
		}
	}
	
	
	public function actionChangepassword()
	{
		$session = Yii::$app->session;
		$model = new ChangePassword;
		if($model->load(Yii::$app->request->post())){
			
			if($model->currentPassword()){
				
				Yii::$app->session->setFlash('success', 'Your password has been Changed.');
				return $this->refresh();
				}else{
					Yii::$app->session->setFlash('error', 'Your Current password Invalid.');
				return $this->refresh();
					}
			
			//print_r(Yii::$app->request->post());
			
		}else{
		
		return $this->render('changepass',['model' =>$model]);
		}
	}
	
	
	public function actionAmazing()
	{
		$session = Yii::$app->session;
		if ((!isset($session['user_id'])) && (isset($session['role'])==2)) {
  			return $this->goHome();
  		}else{	
			
			
		 $query = Amazing::find()->where(['status' => 1]);
		 $countQuery = clone $query;
		 $pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize'=>10]);
		 $Eventamazing = Brandmandates::AmazingEventListing($pages->limit,$pages->offset);
		return $this->render('amazing',['Eventamazing'=>$Eventamazing,'pages'=>$pages]);	
		
		}
	}
	
	public function actionEventcontact()
	{
		$rows = (new \yii\db\Query())->select(['user_id'])->from('app_events')->where(['id' => $_POST['id']])->all();
		$user_id = $rows[0]['user_id'];
		$date_created =  date('Y-m-d h:i:s');
		
		$connection = Yii::$app->getDb();
		$connection->createCommand()
		->insert('app_inbox', ['user_id' => $user_id,'event_id' => $_POST['id'],'sender_name'=>$_POST['name'],'sender_mobile'=>$_POST['phone'],'message'=>$_POST['text'],'date_created'=>$date_created])
		->execute();
		echo "success";
	}
	
	public function actionMyevents()
	{
		
		
		$currentdate = strtotime(date("d.m.Y"));
		//print_r($currentdate);
		
		$session = Yii::$app->session;
		$activelist = Dashboard::MyEventsListActive($session['user_id'],$currentdate);
		$inactivelist = Dashboard::MyEventsListInactive($session['user_id'],$currentdate);
		
		return $this->render('myevents',['activelist' =>$activelist ,'inactivelist' =>$inactivelist ]);
		
	} 
	
	public function actionBrandlist()
	{
		 
		 $query = Brandmandates::find()->where(['status' => 1]);
		 $countQuery = clone $query;
		 $pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize'=>9]);
		 $BrandMandate = Dashboard::BrandMandateDashboardlisting($pages->limit,$pages->offset);
		return $this->render('brandlisting',['BrandMandate'=>$BrandMandate,'pages'=>$pages]);
	}
	
		
	
	
	
	
				
}
