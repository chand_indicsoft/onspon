<?php

namespace app\controllers;

use Yii;
use yii\easyii\modules\page\models\Page;
use yii\web\Controller;
use app\models\SignupForm;
use app\models\LoginForm;
use app\models\SubscribeForm;
use app\modules\brand\api\Brand;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\modules\testimonials\api\Testimonials;
use app\modules\eventcategories\api\EventCategories;
use app\modules\eventcategories\models\EventCategories as EventCategory;
use app\modules\eventtestimonials\api\EventTestimonials;
use yii\easyii\modules\feedback\models\Feedback as FeedbackModel;
//use yii\easyii\modules\subscribe\models\Subscriber;

use app\models\Common;
use app\modules\boxgallery\models\BoxGallery;

class SiteController extends Controller
{
	 public function actions()
	 {
		  return [
				'error' => [
					 'class' => 'yii\web\ErrorAction',
				],

			 'auth' => [
				'class' => 'yii\authclient\AuthAction',
				'successCallback' => [$this, 'oAuthSuccess'],
			 ],
		  ];
	 }

	 public function successCallback($client)
	 {
		  $attributes = $client->getUserAttributes();
		  // user login or signup comes here
	 }
	  public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
     }

	 public function actionIndex()
	 {

		  if(!Yii::$app->getModule('admin')->installed){
				return $this->redirect(['/install/step1']);
		  }
		$brand=Brand::last(25);
		$testmonial = Testimonials::last(5);
		$eventTestimonials=EventTestimonials::last(5);
		//$eventcategoryList = EventCategories::last(10);
		$boxgalleryList = Boxgallery::find()->status(Boxgallery::STATUS_ON)->limit(10)->all();
		$eventcategoryList = EventCategory::find()->status(EventCategory::STATUS_ON)->where(['onhome' => 1])->all();


		  return $this->render('index',['brand'=>$brand,'testmonial'=>$testmonial,
		  'eventcategoryList'=>$eventcategoryList,'eventTestimonials'=>$eventTestimonials,'boxgalleryList'=>$boxgalleryList]);

	 }

	public function oAuthSuccess($client) {

		// get user data from client
		$userAttributes = $client -> getUserAttributes();
		//print_r($userAttributes);die;
		$autclient = $_GET['authclient'];
		$userAttributes['authclient'] = $_GET['authclient'];
		$model = new SignupForm();
		if($_GET['authclient']=='facebook')
		{
			$user = $model->socialSignup($userAttributes);
		 }else{
			 $user = $model->socialGoogleSignup($userAttributes);
		}

			foreach ($user as $fuser) {
				$user_id = $fuser['id'];
				$username =$fuser['username'];
				}

		$session = Yii::$app -> session;
			$session -> set('user_id', $user_id );
		 $session -> set('user_tmpid1', $username );

		// do some thing with user data. for example with $userAttributes['email']
	}

	public function actionSignup()
	{
		$model = new SignupForm();

		if ($model->load(Yii::$app->request->post()))
		{
			if ($user = $model->signup())
			{
				if (Yii::$app->getUser()->login($user))
				{
					Yii::$app->mailer->compose([
						'html' => 'activationUser-html.php',
						'text' => 'activationUser-text.php'
					], ['user' => $user])
					->setFrom([\Yii::$app->params['supportEmail'] => 'Onspon Support'])
					->setTo($user->email)
					->setSubject('Activation link for Onspon')
					->send();

					Yii::$app->session->setFlash('success', 'Successfully submitted. Please check your email and activate your account.');

					return $this->refresh();
				}
			}
		}

		return $this->render('signup', [
			'model' => $model,
		]);
	}

	public function actionLogin()
	{
		$session = Yii::$app->session;
	
		# checking if the user has already logged in or not
		if (!\Yii::$app->user->isGuest)
		{
			return $this->goHome();
		}

		# models
		$model = new LoginForm();
		
		# checking login with given data
		if ($model->load(Yii::$app->request->post()) && $model->login())
		{
			$session = Yii::$app->session;

			if ($session['user_tmpid'])
			{
				$session->set('user_id', $session['user_tmpid']);
				$session->remove('user_tmpid');
			}
			
			//abu
		   if(($session['role'])==1){ 
			  $this->layout = 'sponsor';
			  
			  $existprofile = Yii::$app->getDb()->createCommand("SELECT count(*) as total FROM app_sponsor_profile WHERE user_id='".$session['user_id']."'")->queryone();
			  if($existprofile['total']=='0'){
				  
			   return $this->redirect(['sponsor/profile']); 
			   
			  } else 
			  {
				  return $this->redirect(['sponsor/index']); 
			  }
			 }
			 //close
			if((($session['role'])==2)&&($session['type']=='thirteenth'))
			{
				
				return $this->redirect(['eventcreate/sponsorslab']);
			}

			 if($session['referral'])
			{
				$referral = $session['referral'];
				 $session -> remove('referral');
				return $this->redirect($referral);
			}
			else{
				if($session['role']==2 && $session['user_id'])
				{
						 return $this->redirect(['eventdashboard/index']); 
				}		
			}


			return $this->goHome();
		}
		else
		{
			
			# rendering view
			return $this->render('login', [
				'model' => $model
			]);
		}
	}

	public function actionRequestPasswordReset()
	{
		$model = new PasswordResetRequestForm();

		if ($model->load(Yii::$app->request->post()) && $model->validate())
		{
			if ($model->sendEmail())
			{
				Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

				return $this->goHome();
			}
			else
			{
				Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
			}
		}

		return $this->render('requestPasswordResetToken', [
			'model' => $model,
		]);
	}

	 public function actionResetPassword($token)
	 {
		  try {
				$model = new ResetPasswordForm($token);
		  } catch (InvalidParamException $e) {
				throw new BadRequestHttpException($e->getMessage());
		  }

		  if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
				Yii::$app->session->setFlash('success', 'New password was saved.');

				return $this->goHome();
		  }

		  return $this->render('resetPassword', [
				'model' => $model,
		  ]);
	 }

	 public function actionLogout()
	 {
		  Yii::$app->user->logout();
		  $session = Yii::$app->session;
		  $session -> remove('user_id');
		  $session -> remove('user_tmpid1');
		  $session -> remove('role');
		  $session -> remove('email');
		  $session -> remove('type');
		  $session -> remove('eventid');
		  return $this->goHome();
	 }

	public function actionActivation() {
		$key = $_GET['key'];
		$model = new SignupForm;
		$model -> activation($key);
		$this -> redirect(array('/site/login'));
	}


	/*
	public function actionSignup() {
		$model = new SignupForm();
		if ($model -> load(Yii::$app -> request -> post())) {
			if ($user = $model -> signup()) {
				if (Yii::$app -> getUser() -> login($user)) {
					Yii::$app -> mailer -> compose(['html' => 'activationUser-html.php', 'text' => 'activationUser-text.php'], ['user' => $user]) -> setFrom([\Yii::$app -> params['supportEmail'] => 'Petyaar Support']) -> setTo($user -> email) -> setSubject('Activation link for Petyaar') -> send();

					echo "success";
					die ;
				} else {
					echo "error";
					die ;
				}
			}
		}

	}*/




/*START subcribe*/


/*START subcribe*/

public function actionSubcribeonspon()
{

		$model = new SubscribeForm;
		if($_POST)
		{
			$alredyexist=Common::findScribeByEmail($_POST['semail'])	;
			if(!$alredyexist) {
					 $model->email=$_POST['semail'];
					  $returnval= $model->save(false);
					  if( $returnval==1)
					  {
						  return  $returnval;
					  }else{
						  return  $returnval;
					  }
			}else{
					return  $returnval=2;
			}

		}//END OF POST


}	//action Subscribe

/*END subcribe    END subcribe   END subcribe END subcribe*/	/*END subcribe*/

	/*CONTACT US*///

		public function actionContactus()
		{
			if ($_POST){
					//request is post
					$toemail='rashmee.indicsoft@gmail.com';

					$model = new FeedbackModel;
					$model -> attributes = $_POST;
					if($model->save(false))
						{
							$name = $_POST['name'];
							$email = $_POST['email'];
							$message = $_POST['text'];
							$body='Name:'.$name.'Message'.$message;
							$subject="Contact us from Onspon Website";
							$mailsend= Yii::$app->mailer->compose()
							->setTo($toemail)
							->setFrom([$email => $name])
							->setSubject($subject)
							->setTextBody($body)
							->send();
							return $mailsend =(($mailsend == 1) ? 1 : 0);
						}

			}
		  return $this->render('index');
		}


/*CONTACT US*///	/*CONTACT US*///



}
