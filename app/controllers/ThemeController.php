<?php

namespace app\controllers;
use app\common\components;
use Yii;
use yii\easyii\modules\page\models\Page;
use yii\web\Controller;




class ThemeController extends Controller
{
	public $lang='en';
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex($id)
    {
		
	  
  	   $this->layout = 'default';
		 return $this->render('index',['eventid'=>$id]);
		
	}
	
	
	
	
    
}
