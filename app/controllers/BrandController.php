<?php

namespace app\controllers;

use Yii;
use yii\web\Response;
use yii\web\Request;
use yii\data\Pagination;
use yii\easyii\modules\page\models\Page;
use yii\web\Controller;
use yii\widgets\ActiveForm;
use app\models\Common;
use app\modules\brandcategories\api\BrandCategories;
use app\modules\brandcategories\models\BrandCategories as BrandCategory;
use app\modules\brandmandates\models\BrandMandates;

class BrandController extends Controller
{
	 public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
     }

	
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
	
	public function actionBrandlisting()
    {
		$city=false;
		$brand_name= false;
		$industryid=false;
		$datetype=false;
	
      $query=BrandMandates::find()->status(BrandMandates::STATUS_ON);
      $params = Yii::$app->request->queryParams;
	  
		if(Yii::$app->getRequest()->getQueryParam('datetype'))
		{
			if(Yii::$app->getRequest()->getQueryParam('datetype')=='next3month')
			{
				$today=strtotime("now");
				$newdate = strtotime ( '+3 month' ,  $today  ) ;
				$query->andWhere(['between',  "start_date",  $today,$newdate ]) ;    
			}

			if(Yii::$app->getRequest()->getQueryParam('datetype')=='week')
			{
				$today = strtotime('today 00:00:00');
				$this_week_start = strtotime('-1 week monday 00:00:00');
				$this_week_end = strtotime('sunday 23:59:59');
				$query->andWhere(['between',  "start_date",  $this_week_start,$this_week_end ]) ;      
			}

			if(Yii::$app->getRequest()->getQueryParam('datetype')=='month')
			{ 
				$firstdayofmonth=strtotime(date('Y-m-01'));
				$lastdayofmonth =strtotime(date('Y-m-t'));
				$query->andWhere(['between',  "start_date",  $firstdayofmonth,$lastdayofmonth ]) ;
			}
		}
		
       if(Yii::$app->getRequest()->getQueryParam('date')){
		 $startdate=Yii::$app->getRequest()->getQueryParam('date');
		 $start_date = date("Y-m-d", strtotime($startdate))." 00:00:00";

	    }
	     if(Yii::$app->getRequest()->getQueryParam('date1')){
		 $enddate=Yii::$app->getRequest()->getQueryParam('date1');
		 $end_date = date("Y-m-d", strtotime($enddate))." 24:59:59";

	    }
	    if(Yii::$app->getRequest()->getQueryParam('date') && Yii::$app->getRequest()->getQueryParam('date1')   ){
		$to=strtotime( $start_date);
		$from=strtotime( $end_date);
		$query->andWhere(['and',  "start_date>=$to","end_date<=$from "]) ;    
		}
  
		if(Yii::$app->getRequest()->getQueryParam('city'))
		{
			$city=Yii::$app->getRequest()->getQueryParam('city');
			$query->andWhere(['LIKE' ,'city_id',strtr($city,['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']).'%', false]);
		}
		
		if(Yii::$app->getRequest()->getQueryParam('brand_name')){
		 $brand_name= Yii::$app->getRequest()->getQueryParam('brand_name');
		 $brand_name = filter_var($brand_name, FILTER_SANITIZE_STRING);
		 $brand_name= trim($brand_name);
		 $query->andWhere(['LIKE' ,'brand_name',strtr($brand_name,['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']).'%', false]);
		}

		if(Yii::$app->getRequest()->getQueryParam('industryid')){
		$industryid= Yii::$app->getRequest()->getQueryParam('industryid');
		$query->andWhere(['industry_id'=>$industryid]);
		}
	 
		$count = $query->count();
		$pagination = new Pagination(['totalCount' => $count]);
		$countQuery = clone $query;
		$pages = new Pagination(['totalCount' => $countQuery->count()]);
		
		
		if(Yii::$app->getRequest()->getQueryParam('sortby'))
		{
			if(Yii::$app->getRequest()->getQueryParam('sortby')=='start_date')
			{
				$query=$query->orderBy(['start_date'=>SORT_ASC,]); 
			}
		}

		else
		{
			$query=$query->orderBy(['start_date' => SORT_DESC,]);
		}
		
		
		
		$query_result = $query->offset($pages->offset)
			 ->limit($pages->limit)
			->all();
			return $this->render('brandlisting', [
		   'brand_name'=>$brand_name,
			'industryid'=>$industryid,
			 'branddetail' => $query_result,
			 'pages' => $pages,
        ]); 
	}
	
	
	
	public function actionBrandapply()
	{
		
	    $brandmandateid		= $_REQUEST['brandid'];
	    $receiver_id		= $_REQUEST['receiver'];
        $sender_id          = Yii::$app->session['user_id'];
        $date_created       =  date('Y-m-d h:i:s');
		$exist = Yii::$app->getDb()->createCommand("SELECT count(1) as total FROM app_sponsor_apply WHERE brandmandates_id='".$brandmandateid."' AND sender_id='".$sender_id."' ")->queryone();
		$receiverdeatils = Yii::$app->getDb()->createCommand("SELECT username,email,total_notification 
		                                                      FROM app_users WHERE id='".$receiver_id."' ")->queryone();
			
        $senderemail = Yii::$app->getDb()->createCommand("SELECT email FROM app_users WHERE id='".$sender_id."' ")->queryone();		
		$connection = Yii::$app->getDb();
		$brandname=Yii::$app->getDb()->createCommand("SELECT brand_name FROM app_brandmandates WHERE id='".$brandmandateid."' ")->queryone();		
		//print_r($brandname);die;
       if($exist['total']=='0'){
				$message = 'Hi '.$receiverdeatils['username'].', I am applying for this  '.$brandname['brand_name'].' ';
				
				$insert=$connection->createCommand()
				->insert('app_sponsor_apply', ['receiver_id' => $receiver_id,'sender_id'=>$sender_id,
				'date'=>$date_created,'message'=>$message,
				'brandmandates_id'=>$brandmandateid])
				->execute();
				
				$insertOutbox=$connection->createCommand()
				->insert('app_sponsor_apply_outbox', ['receiver_id' => $receiver_id,'sender_id'=>$sender_id,
				'date'=>$date_created,'message'=>$message,
				'brandmandates_id'=>$brandmandateid])
				->execute();
				
				$email = $receiverdeatils['email'];
				$subject = 'Apply BrandMandates';
				$headers = "From: '".$senderemail['email']."'" . "\r\n";
				$headers .= "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
															
				 mail($email,$subject, $message, $headers);
				
				
				$notification = $receiverdeatils['total_notification'] + 1;
				$count = Yii::$app->getDb()->createCommand()->update('app_users', ['total_notification' => $notification],'id= "'. $receiver_id.'"')->execute();	
				echo "Thank you to applying for this brand.";
			} 
			else {
				echo "You have already applied this brand.";
			}
		
		
		
		
		
	}
	
	
	
	
	
	
}
