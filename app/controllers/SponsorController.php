<?php

namespace app\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Response;
use yii\easyii\modules\page\models\Page;
use yii\web\Controller;
use yii\widgets\ActiveForm;
use app\modules\users\models\Users;
use yii\web\UploadedFile;
use yii\easyii\helpers\Image;
use yii\easyii\models\Photo;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;

use app\modules\lastminute\models\LastMinute;
use app\modules\events\models\Events;
use app\models\Brandmandates;
use app\models\Commonhelper;
use app\models\Sponsorprofile;
use app\models\ChangePassword;
use app\modules\amazing\models\Amazing;
use app\modules\trending\models\Trending;

class SponsorController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ], 
             
        ];
    }
    
    public function beforeAction($action) {
	$this->layout = 'sponsor';	
    $this->enableCsrfValidation = false;
    return parent::beforeAction($action);
	}
    
    
	public function actionIndex()
	{
		$session = Yii::$app->session;
		if ((!isset($session['user_id'])) && (isset($session['role'])!=1)) {
  			return $this->goHome();
  		}else{	
				return $this->render('index');
		}
	}

	public function actionCreatebrand()
	{
		$session = Yii::$app->session;
		if ((!isset($session['user_id'])) && (isset($session['role'])!=1)) {
  			return $this->goHome();
  		}else{	
				$model = new Brandmandates;
				return $this->render('form', ['model' => $model]);
		}
	}
	
	
	public function actionCreate()
	{
		$session = Yii::$app->session;
		if ((!isset($session['user_id'])) && (isset($session['role'])!=1)) {
  			return $this->goHome();
  		}else{	
			$list = $_POST['Brandmandates'];
			$id =  $_POST['Brandmandates']['id'];
			if(!$id)
			{
				$model = new Brandmandates;
				$model->date_created = date('Y-m-d h:i:s');
				$model->user_id = $session['user_id'];
				$model->user_type = $session['role'];
			}
			if($id)
			{
				$model = Brandmandates::findOne($id);
			}
			$model->attributes = $list;
			
				$uploadedFile = UploadedFile::getInstance($model, 'logo');
				if($uploadedFile)
				{
				 $timeStamp = time(); 
				 $fileName = "{$timeStamp}-{$uploadedFile}";
				 $fileName = str_replace(" ","_",$fileName) ;
				 $upload_path = Yii::getAlias('@webroot') . '/uploads/brandmandates/';	 
				 $uploadedFile->saveAs($upload_path . $fileName);
				 $model->logo = 'uploads/brandmandates/'.$fileName;
				}
				if($model->save()){
					echo $model->id;
				}
			}
		}
	
	public function actionUpdateallstep()
	{

		$session = Yii::$app->session;
		if ((!isset($session['user_id'])) && (isset($session['role'])!=1)) {
  			return $this->goHome();
  		}else{	
			$list = $_POST['Brandmandates'];
			$step = $_POST['step'];	
			$model = Brandmandates::findOne($list['id']);
			$model->attributes = $list;
			
			
			if($model->timeline == 'Open timeline' OR $model->timeline == '')
			{
				$model->start_date ="";
				$model->end_date ="";
			}
			
			$model->date_updated = date('Y-m-d h:i:s');
				
			if($model->save()){
				   switch($step)
				   {  
					   case 'step3':
							echo "step3";
					   break;
					   case 'step4':
							echo "step4";
					   break;
					   case 'step5':
							echo "success";
					   break;
					}	
				}
			}
	}
		
	
	
	public function actionUpdate()
	{
		$session = Yii::$app->session;
		if ((!isset($session['user_id'])) && (isset($session['role'])!=1)) {
  			return $this->goHome();
  		}else{	
			$list = $_POST['Brandmandates'];
			$step = $_POST['step'];
			$model = Brandmandates::findOne($list['id']);
			$model->attributes = $list;
			$model->date_updated = date('Y-m-d h:i:s');
			$model->event_category_id = $list['event_category_id'] ? implode(",",$list['event_category_id']):'';
            $model->specific_subjective = $list['specific_subjective'] ? implode(",",$list['specific_subjective']):'';
            	
			/**************** close  logo uplaod *******************/
			if($model->save()){
				$update = Brandmandates::UpdateBrandmandates($model->id,$model->specific_subjective,$model->event_category_id);
				echo "step2";	
				}
			}
	}
	
	public function actionEditbrand($id)
	{
		$session = Yii::$app->session;
		if ((!isset($session['user_id'])) && (isset($session['role'])!=1)) {
  			return $this->goHome();
  		}else{	
				$model = Brandmandates::findOne($id);
				return $this->render('form', ['model' => $model]);
		}
	}
	
	public function actionDeletebrand()
	{
		$session = Yii::$app->session;
		if ((!isset($session['user_id'])) && (isset($session['role'])!=1)) {
  			return $this->goHome();
  		}else{	
			$id = $_POST['id'];
			$delete = Brandmandates::BrandDeleteById($id);
			echo 'Your brand deleted.';
		}	
	}
	
	public function actionEventcontact()
	{
		$rows = (new \yii\db\Query())->select(['user_id'])->from('app_events')->where(['id' => $_POST['id']])->all();
		$user_id = $rows[0]['user_id'];
		$date_created =  date('Y-m-d h:i:s');
		
		$connection = Yii::$app->getDb();
		$connection->createCommand()
		->insert('app_inbox', ['user_id' => $user_id,'event_id' => $_POST['id'],'sender_name'=>$_POST['name'],'sender_mobile'=>$_POST['phone'],'message'=>$_POST['text'],'date_created'=>$date_created])
		->execute();
		echo "success";
	}	
	
	public function actionRecommend()
	{
		$session = Yii::$app->session;
		if ((!isset($session['user_id'])) && (isset($session['role'])!=1)) {
  			return $this->goHome();
  		}else{
			$currentdate = strtotime(date("d.m.Y"));
			$result = Brandmandates::Select_by_user($session['user_id']);
			$cat = Brandmandates::Selcetcategory_City($session['user_id']);
			$catid = '';
			$cityid = '';
			foreach($cat as $cat1)
			{
				$catid .= ','.$cat1['event_category_id'];
				$cityid .= ','.$cat1['city_id'];
			}
			$eventcat= array_filter(array_unique(explode(",",$catid)));
			$city= array_filter(array_unique(explode(",",$cityid)));

			$query=Events::find()->status(Events::STATUS_ON);
			
			if($eventcat){ $query->andWhere(['event_category_id'=>$eventcat]);}
			if($city){ $query->orWhere(['city_id'=>$city]);}
			$query->andWhere(['>=', 'event_end_date', $currentdate]);
			
		 $countQuery = clone $query;
		 $pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize'=>10]);
		 $eventRecomende = $query->offset($pages->offset)->limit($pages->limit)->all();
		 return $this->render('recmended',['eventRecomende'=>$eventRecomende,'pages'=>$pages]);
		
		}
	}
	
	public function actionShortlist()
	{
		$session = Yii::$app->session;
		if ((!isset($session['user_id'])) && (isset($session['role'])!=1)) {
  			return $this->goHome();
  		}else{	
		
		$total = Yii::$app->getDb()->createCommand("SELECT count(*) as total FROM app_user_shortlist_event WHERE user_id='".$session['user_id']."'")->queryone();
		 $pages = new Pagination(['totalCount' => $total['total'], 'defaultPageSize'=>10]);
		 $shoertlist = Brandmandates::ShortlisteventAll($session['user_id'],$pages->limit,$pages->offset);
		return $this->render('shortlist',['shoertlist'=>$shoertlist,'pages'=>$pages]);	
		}
	}
	
	public function actionAmazing()
	{
		$session = Yii::$app->session;
		if ((!isset($session['user_id'])) && (isset($session['role'])!=1)) {
  			return $this->goHome();
  		}else{
		$currentdate = strtotime(date("d.m.Y"));	
		 $query = Amazing::find()->where(['status' => 1]);
		 $countQuery = clone $query;
		 $pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize'=>10]);
		 $Eventamazing = Brandmandates::AmazingEventListing($pages->limit,$pages->offset,$currentdate);
		return $this->render('amazing',['Eventamazing'=>$Eventamazing,'pages'=>$pages]);		
		
		}
	}
	
	public function actionTrending()
	{
		$session = Yii::$app->session;
		if ((!isset($session['user_id'])) && (isset($session['role'])!=1)) {
  			return $this->goHome();
  		}else{	
			
		$currentdate = strtotime(date("d.m.Y"));
		 $query = Trending::find()->where(['status' => 1]);
		 $countQuery = clone $query;
		 $pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize'=>10]);
		 $Eventtrending = Brandmandates::trendingEventListing($pages->limit,$pages->offset,$currentdate);
		return $this->render('trending',['Eventtrending'=>$Eventtrending,'pages'=>$pages]);
		
		}
	}
	
	public function actionRequesttoapply()
	{
		$session = Yii::$app->session;
		$sender = $session['user_id'];
		$event_id = $_POST['id'];
		
		
		$exist = Yii::$app->getDb()->createCommand("SELECT count(*) as total FROM app_sponsor_apply WHERE event_id='".$event_id."' AND sender_id='".$sender."' ")->queryone();
		
		if($exist['total']=='0'){
			
			$brandlist = Brandmandates::brandmandateslist_for_request($sender);
			$result = Yii::$app->getDb()->createCommand("SELECT event_name,user_id FROM app_events WHERE id='".$event_id."' ")->queryone();
			
			$receiverdeatils = Yii::$app->getDb()->createCommand("SELECT username,email,total_notification FROM app_users WHERE id='".$result['user_id']."' ")->queryone();
			$senderemail = Yii::$app->getDb()->createCommand("SELECT email FROM app_users WHERE id='".$sender."' ")->queryone();		
			
			$message = 'Hi '.$receiverdeatils['username'].', You can apply these brands mandate "'.$brandlist.'" for your event';
			
			$notification = $receiverdeatils['total_notification'] + 1;
			$count = Yii::$app->getDb()->createCommand()->update('app_users', ['total_notification' => $notification],'id= "'.$result['user_id'].'"')->execute();	
			$result = Yii::$app->getDb()->createCommand("SELECT event_name,user_id FROM app_events WHERE id='".$event_id."' ")->queryone();
			$date_created =  date('Y-m-d h:i:s');
			$inserteventid = Yii::$app->getDb()->createCommand()
			->insert('app_sponsor_apply', ['event_id' => $event_id,'message'=>$message,'sender_id'=>$sender,'event_name'=>$result['event_name'],'receiver_id'=>$result['user_id'],'date'=>$date_created])
			->execute();
			$sponsorid = Yii::$app->db->getLastInsertID();
			
			//$inboxUrl = Yii::$app->homeUrl.'eventdashboard/inboxitem/'.$sponsorid;
			$inboxUrl = Yii::$app->urlManager->createAbsoluteUrl(['eventdashboard/inboxitem/'.$sponsorid]);
			$username =  $receiverdeatils['username'];
			
			$text = 'Hi '.$receiverdeatils['username'].', You have a sponsor request for your event'.'<br> click here to see '.$inboxUrl;
			$email = $receiverdeatils['email'];
			
			Yii::$app->mailer->compose(['html' => 'requestToApply.php', 'text' => 'requestToApply.php'], ['username' => $username,'url' =>$inboxUrl])->setFrom([\Yii::$app->params['supportEmail'] => 'Onspon Support'])->setTo($email)->setSubject('Apply BrandMandates')->send();
														
			$outbox = Yii::$app->getDb()->createCommand()
			->insert('app_sponsor_apply_outbox', ['event_id' => $event_id,'message'=>$message,'sender_id'=>$sender,'event_name'=>$result['event_name'],'receiver_id'=>$result['user_id'],'date'=>$date_created])
			->execute();
			
				echo "Thank you to applying for this event.";
			} 
			else {
				echo "You have already applied this event.";
			}
		
	}
	
	
	public function actionProfile(){
		
			$session = Yii::$app->session;
			if ((!isset($session['user_id'])) && (isset($session['role'])!=1)) {
				return $this->goHome();
			}else{	
				$model = new Sponsorprofile;	
				
				if($model->load(Yii::$app->request->post())){
					
					$model->user_id = $session['user_id'];
					$model->intrested_area = ($model->intrested_area) ? implode(",",$model->intrested_area):'';
					
					$user = Yii::$app->getDb()->createCommand("SELECT username,email FROM app_users WHERE id='".$session['user_id']."' ")->queryone();
					$model->email = $user['email'];
					$model->name = $user['username'];
					$model->date_created =  date('Y-m-d h:i:s');
					if($model->save()){
						return $this->render('index');
					}
				}
				else { 
				
				return $this->render('profile', ['model' => $model]);
			}
			}
	}
	
	public function actionInboxhome()
	{
		$session = Yii::$app->session;
		$inbox = Brandmandates::UserInboxHome($session['user_id']);
		return $this->renderpartial('notification',['inbox' =>$inbox]);	
	}
	
	public function actionEditprofile()
	{
		$session = Yii::$app->session;
			if ((!isset($session['user_id'])) && (isset($session['role'])!=1)) {
				return $this->goHome();
			}else{
				$profileid = Yii::$app->getDb()->createCommand("SELECT id FROM app_sponsor_profile WHERE user_id='".$session['user_id']."' ")->queryone();
				$model = Sponsorprofile::findOne($profileid['id']);
				
				if($model->load(Yii::$app->request->post())){
					
					$model->image = UploadedFile::getInstance($model, 'image');
					if($model->image && $model->validate(['image'])){
                        $model->image = Image::upload($model->image, 'sponsorprofile');
                    }
                    else{
                       $model->image = $model->oldAttributes['image'];
                    }	
				$model->intrested_area = ($model->intrested_area) ? implode(",",$model->intrested_area):'';
				
				if($model->save()){
					
					Yii::$app->session->setFlash('success', 'Your profile is Updated.');
					return $this->refresh();
					}	
					
				}	
			else {
				
				return $this->render('editprofile', ['model' => $model]);
			}
		}
	}
	
	public function actionInbox()
	{	$session = Yii::$app->session;
			if ((!isset($session['user_id'])) && (isset($session['role'])!=1)) {
				return $this->goHome();
			}else{
			$modelresult='';
			$id = '';	
			return $this->render('inbox',['modelresult'=>$modelresult,'id'=>$id]);
		}	
	}
	
	public function actionInboxdetails()
	{	$session = Yii::$app->session;
			if ((!isset($session['user_id'])) && (isset($session['role'])!=1)) {
				return $this->goHome();
			}else{
		
			$id = $_POST['id'];	
			$type = $_POST['type'];	
			$list = array();
			if($type=='sender'){
				$list = Brandmandates::SentDetailsByID($id);
			}
			if($type=='receiver'){
				
				$inbox = $receiverdeatils = Yii::$app->getDb()->createCommand("SELECT status FROM app_sponsor_apply WHERE id='".$id."' ")->queryone();
				if($inbox['status'] == '0')
				{
					$notice =  Yii::$app->getDb()->createCommand("SELECT total_notification FROM app_users WHERE id='".$session['user_id']."' ")->queryone();	
					$total = $notice['total_notification'] - 1;
					$count = Yii::$app->getDb()->createCommand()->update('app_users', ['total_notification' => $total],'id= "'.$session['user_id'].'"')->execute();	
					$status = Yii::$app->getDb()->createCommand()->update('app_sponsor_apply', ['status' => '1'],'id= "'.$id.'"')->execute();	
					
				}
				$list = Brandmandates::ReceiveDetailsByID($id);
			}
			
		return $this->renderPartial('messagedeatails',['list' => $list,'type' => $type]);
			
		}	
	}
	
	public function actionDeleteinbox()
	{
		$id = $_POST['id'];	
		$type = $_POST['type'];	
		if($type=='sender'){
			$connection = Yii::$app->getDb()->createCommand()->delete('app_sponsor_apply_outbox', ['id' => $id])->execute();
			}
		if($type=='receiver'){
			$connection = Yii::$app->getDb()->createCommand()->delete('app_sponsor_apply', ['id' => $id])->execute();
		}
		echo "Your message deleted.";
	}
	
	public function actionInboxitem($id)
	{
		$session = Yii::$app->session;
		$inbox = $receiverdeatils = Yii::$app->getDb()->createCommand("SELECT status FROM app_sponsor_apply WHERE id='".$id."' ")->queryone();
		if($inbox['status'] == '0')
				{
					$notice =  Yii::$app->getDb()->createCommand("SELECT total_notification FROM app_users WHERE id='".$session['user_id']."' ")->queryone();	
					$total = $notice['total_notification'] - 1;
					$count = Yii::$app->getDb()->createCommand()->update('app_users', ['total_notification' => $total],'id= "'.$session['user_id'].'"')->execute();	
					$status = Yii::$app->getDb()->createCommand()->update('app_sponsor_apply', ['status' => '1'],'id= "'.$id.'"')->execute();	
					
				}
		$modelresult = Brandmandates::ReceiveDetailsByID($id);
		return $this->render('inbox',['modelresult'=>$modelresult,'id'=>$id]);
	}
	
	public function actionChangepassword()
	{
		$session = Yii::$app->session;
		$model = new ChangePassword;
		if($model->load(Yii::$app->request->post())){
			
			if($model->currentPassword()){
				
				Yii::$app->session->setFlash('success', 'your password has changed.');
				return $this->refresh();
				}else{
					Yii::$app->session->setFlash('error', 'Your Current password Invalid.');
				return $this->refresh();
					}
		}else{
		
		return $this->render('changepass',['model' =>$model]);
		}
	}
	
	public function actionBrandlist()
	{
		return $this->render('editbrand');
	}
	
	public function actionLastminutesdeals()
	{
		$currentdate = strtotime(date("d.m.Y"));
		$totallastminute = Brandmandates::TotalLastminutes($currentdate);
		$pages = new Pagination(['totalCount' => $totallastminute, 'defaultPageSize'=>10]);
		$lastminute = Brandmandates::lastMinutesDeal($pages->limit,$pages->offset,$currentdate);
		return $this->render('lastminutesdeals',['lastminute'=>$lastminute,'pages'=>$pages]);	
		
	}
	
	public function actionViewedbtother()
	{
		$session = Yii::$app->session;
		$industryid = Yii::$app->getDb()->createCommand("SELECT industry FROM app_sponsor_profile WHERE user_id='".$session['user_id']."' ")->queryone();
		$eventviewd = Brandmandates::eventvewedByOtherBrandListing($industryid['industry']);
		
		return $this->render('viewedbybrand',['eventviewd'=>$eventviewd]);
	}
	
	 public function actionClearprofileimage($id)
    {
        $model = Sponsorprofile::findOne($id);

        if($model === null){
            Yii::$app->session->setFlash('error', 'Not found.');
        }
        else{
            $model->image = '';
            if($model->update()){
                @unlink(Yii::getAlias('@webroot').$model->image);
                Yii::$app->session->setFlash('success', 'Image cleared.');
            } else {
				Yii::$app->session->setFlash('error', 'Not found.');
               
            }
        }
        return $this->render('editprofile',['model' =>$model]);
    }
	
	
		
}
