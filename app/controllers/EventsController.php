<?php

namespace app\controllers;

use Yii;
use yii\web\Response;
use yii\web\Request;
use yii\data\Pagination;
use yii\data\Sort;
use yii\easyii\modules\page\models\Page;
use yii\web\Controller;
use yii\widgets\ActiveForm;
use app\models\Common;
use app\modules\eventcategories\api\EventCategories;
use app\modules\eventcategories\models\EventCategories as EventCategory;
use app\modules\events\models\Events;
use app\modules\trending\models\Trending;
use app\models\Brandmandates;

class EventsController extends Controller
{
	public function beforeAction($action)
	{
		$this->enableCsrfValidation = false;
		return parent::beforeAction($action);
	}
	
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
	
public function actionEventlisting()
{
    $city=false;$event_name= false;$catid=false;$datetype=false;$tagid=false;
    $currentdate = strtotime(date("d.m.Y"));
    $eventcategoryList = EventCategory::find()->status(EventCategory::STATUS_ON)->all();

    $query=Events::find()->status(Events::STATUS_ON)
            ->andWhere(['>=', 'event_end_date', $currentdate]);
            //->andWhere(['=', 'verified',1]);
    $params = Yii::$app->request->queryParams;
    
		if(@$_REQUEST['boxid']!='')
		{
		$connection = Yii::$app->getDb();
		$boxid=$_REQUEST['boxid'];
		//get tag_id related to box
		$query_tag=$connection->createCommand("SELECT tag_id FROM app_box_tag where boxid='".$boxid."'")->queryAll();

		   if(count($query_tag)>=2)
		   {
			$tagid=implode (',', array_map(function ($entry) {return "".$entry['tag_id']."";}, $query_tag));
			$tagid= explode(',',$tagid);
			}else{
			$tagid= $query_tag[0]['tag_id'];
			}
		}
		//print_r($tagid);
		if($tagid!="" && Yii::$app->getRequest()->getQueryParam('boxid'))
		{
			$query->join('LEFT JOIN ','app_event_tag','app_event_tag.event_id=app_events.id');
			$query->andWhere(['app_event_tag.tag_id'=>$tagid]);
		}	  
      	
		 if(Yii::$app->getRequest()->getQueryParam('datetype')){
           
		 if(Yii::$app->getRequest()->getQueryParam('datetype')=='next3month')
                 { 
                   $today=strtotime("now");
                   $newdate = strtotime ( '+3 month' ,  $today  ) ;
                   $query->andWhere(['between',  "event_strt_date",  $today,$newdate ]) ;    
		
		 }

                 if(Yii::$app->getRequest()->getQueryParam('datetype')=='week')
                 { 
				 
					$today = strtotime('today 00:00:00');

					$this_week_start = strtotime('-1 week monday 00:00:00');
					$this_week_end = strtotime('sunday 23:59:59');
                   //$today=strtotime("now");
                   //$day7=strtotime(date('Y-m-d', strtotime("this week")));
		    $query->andWhere(['between',  "event_strt_date",  $this_week_start,$this_week_end ]) ;      
		 }
                 
                  if(Yii::$app->getRequest()->getQueryParam('datetype')=='month')
                 { 
                      $firstdayofmonth=strtotime(date('Y-m-01'));
                      $lastdayofmonth =strtotime(date('Y-m-t'));
                      $query->andWhere(['between',  "event_strt_date",  $firstdayofmonth,$lastdayofmonth ]) ;    
		
		  }
                 
                 
	    }
           if(Yii::$app->getRequest()->getQueryParam('date')){
		 $startdate=Yii::$app->getRequest()->getQueryParam('date');
		 $start_date = date("Y-m-d", strtotime($startdate))." 00:00:00";

	    }
	     if(Yii::$app->getRequest()->getQueryParam('date1')){
		 $enddate=Yii::$app->getRequest()->getQueryParam('date1');
		 $end_date = date("Y-m-d", strtotime($enddate))." 24:59:59";

	    }
	    if(Yii::$app->getRequest()->getQueryParam('date') && Yii::$app->getRequest()->getQueryParam('date1')   ){
		$to=strtotime( $start_date);
		$from=strtotime( $end_date);
		$query->andWhere(['and',  "event_strt_date<=$to","event_end_date>=$from "]) ;    
		}
	    
    
     if(Yii::$app->getRequest()->getQueryParam('city')){
		 $city=Yii::$app->getRequest()->getQueryParam('city');
		 $query->andWhere(['LIKE' ,'city_id',strtr($city,['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']).'%', false]);
	  	 
		 }  
     if(Yii::$app->getRequest()->getQueryParam('event_name')){
		 $event_name= Yii::$app->getRequest()->getQueryParam('event_name');
		 $event_name = filter_var($event_name, FILTER_SANITIZE_STRING);
		 $event_name= trim($event_name);
		 $query->andWhere(['LIKE' ,'event_name',strtr($event_name,['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']).'%', false]);
	  	   
		}	
	if(Yii::$app->getRequest()->getQueryParam('catid')){
		$catid= Yii::$app->getRequest()->getQueryParam('catid');
		$query->andWhere(['event_category_id'=>$catid]);
		
	 }
	 
	 // get the total number of articles (but do not fetch the article data yet)
         $count = $query->count();

       // create a pagination object with the total count
       $pagination = new Pagination(['totalCount' => $count]);

	
     
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
         if(Yii::$app->getRequest()->getQueryParam('sortby')){
       
          if(Yii::$app->getRequest()->getQueryParam('sortby')=='no_of_attendees')
          {
              $query=$query->orderBy([
                    'no_of_attendees'=>SORT_DESC,
                                      
                ]); 
          }
          
          if(Yii::$app->getRequest()->getQueryParam('sortby')=='premium')
          {
             $query=$query->orderBy([
                    'premium'=>SORT_DESC,
                                      
                ]); 
          }
           if(Yii::$app->getRequest()->getQueryParam('sortby')=='event_strt_date')
          {
             $query=$query->orderBy([
                    'event_strt_date'=>SORT_DESC,
                                      
                ]); 
          }
      }else{
          
          $query=$query->orderBy([
                    'premium'=>SORT_ASC,
                    'event_strt_date' => SORT_DESC,
                    
                ]);
     
      }
        $query_result = $query->offset($pages->offset)
         ->limit($pages->limit)->all();
      // var_dump($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);exit();
      
		
		return $this->render('eventlisting', [
                'event_name'=>$event_name,
                 'city'=>$city,
                 'catid'=>$catid,
                  'eventdetail' => $query_result,
                   'eventcategoryList'=>$eventcategoryList,
                  'pages' => $pages,
                 ]); 
	}
	
	
	public function actionEventgallerylist()
    {
		$connection = Yii::$app->getDb();
		$params = Yii::$app->request->queryParams;
		if(@$_REQUEST['id']!='')
		{
			$boxid=$_REQUEST['id'];
			 //get tag_id related to box
		    $query_tag=$connection->createCommand("SELECT tag_id FROM app_box_tag where boxid='".$boxid."'")->queryAll();
		      if(count($query_tag)>=2){
					$tagid= implode(', ', array_map(function ($entry) {return "'".$entry['tag_id']."'";}, $query_tag));
			  }else{
				   $tagid= $query_tag[0]['tag_id'];
			  }
		  
		}else{
			$query_tag=$connection->createCommand("SELECT distinct tag_id FROM onspon_DB.app_box_tag;")->queryAll();
			 if(count($query_tag)>=2){
					$tagid= implode(', ', array_map(function ($entry) {return "'".$entry['tag_id']."'";}, $query_tag));
			  }else{
				   $tagid= $query_tag[0]['tag_id'];
			  }
			
		}
		if($tagid){
		$Where=" AND et.tag_id IN(".$tagid.")";

		}
			$query=$connection->createCommand("select distinct et.event_id,e.*  from app_event_tag as et 
                                            LEFT JOIN app_events as e ON e.id=et.event_id
                                            where status=1 ".$Where." ")->queryAll();
//print_r( $query);die;

		/*$countQuery = clone $query;
		$pages = new Pagination(['totalCount' => $countQuery->count()]);
		$query_result = $query->offset($pages->offset)
		->limit($pages->limit)
		->all();*/
		
		return $this->render('eventgallerylist', [
		'eventdetail' => $query,
		//'pages' => $pages,
		]); 
	}
	
	public function actionEventcontact()
	{
		if( $_POST['id'])
		{
			$rows = (new \yii\db\Query())->select(['user_id'])->from('app_events')->where(['id' => $_POST['id']])->all();
			$user_id = $rows[0]['user_id'];
			$date_created =  date('Y-m-d h:i:s');
			
				   
			$sender_id =(Yii::$app->session['user_id']!='' )? Yii::$app->session['user_id']: 0;
		   
			$connection = Yii::$app->getDb();
			$connection->createCommand()
			->insert('app_inbox', ['user_id' => $user_id,'event_id' => $_POST['id'],'sender_name'=>$_POST['name'],'sender_mobile'=>$_POST['phone'],'message'=>$_POST['text'],'date_created'=>$date_created,'sender_id'=>$sender_id])
			->execute();
			echo "success";
	  }else{
		    echo "error";
	  }
	}
	
	
	
	
	
	public function actionShortlistfolder()
	{
		
		$user_id = Yii::$app->session['user_id'];
		$date_created =  date('Y-m-d h:i:s');
		
		$connection = Yii::$app->getDb();
		$insertFolder=$connection->createCommand()
		->insert('app_user_folder', ['user_id' => $user_id,'foldername'=>$_POST['flodername'],'date_created'=>$date_created])
		->execute();
			if($insertFolder)
			{
			echo "success";
			}
	}
	
	public function actionShortlistevent()
	{
		
		$user_id = Yii::$app->session['user_id'];$connection = Yii::$app->getDb();
		$date_created =  date('Y-m-d h:i:s');
		$_folderid=@explode(',',$_POST['checkboxValues']);
		$eventid=$_POST['eventid'];
		if($_folderid)
		{  
			 $connection->createCommand()
				->delete('app_user_shortlist_event', ['user_id' => $user_id,'event_id' =>$eventid])
				->execute();
		
			foreach($_folderid as $fid)
			{
				$insertF=$connection->createCommand()
				->insert('app_user_shortlist_event', ['user_id' => $user_id,'folderid'=>$fid,'event_id'=>$eventid,'date_created'=>$date_created])
				->execute();
				
			}
			echo "success";
		}
		
	}
	
	
	public function actionTrending()
	{
		 $query = Trending::find()->where(['status' => 1]);
		 $countQuery = clone $query;
		 $pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize'=>10]);
		 $Eventtrending = Brandmandates::trendingEventListing($pages->limit,$pages->offset);
		return $this->render('trending',['Eventtrending'=>$Eventtrending,'pages'=>$pages]);
		
	}
	
		
}
