<?php

namespace app\controllers;

use Yii;
use yii\web\Response;

use yii\easyii\modules\page\models\Page;
use yii\web\Controller;
use yii\widgets\ActiveForm;
use app\modules\users\models\Users;
use app\modules\pet\models\Pet;
use yii\easyii\modules\faq\api\Faq;
use yii\web\UploadedFile;
use yii\easyii\helpers\Image;
use app\models\CreateEvents;
use app\models\Organization;
use app\models\SignupForm;
use app\models\User;
use yii\easyii\models\Photo;
use yii\data\ActiveDataProvider;
use app\modules\package\models\Package; 


class EventcreateController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ], 
             
        ];
    }
    
	public function beforeAction($action) {
	$this->layout = 'events';	
    $this->enableCsrfValidation = false;
    return parent::beforeAction($action);
	}

	public function actionCreate()
	{
		$type = '';
		
		$orgmodel = new Organization;		
		$model = new CreateEvents;
		if($_POST){
			$list = $_POST['CreateEvents'];
			if($_POST['type']){
				$type = $_POST['type'];
			}
			
			if(!$list['id']){
				$model = new CreateEvents;
				}
			if($list['id']){
				$model = CreateEvents::findOne($list['id']);
				$model->url = 'http://' . Yii::$app -> getRequest() -> serverName.'/'.$model->slug;
				
				}
			$model->attributes = $list;
			
			if($type =='second'){
				$session = Yii::$app->session;
				if($session['user_id']){
						$model->user_id = $session['user_id'];
					}
				$model->date_created = date('Y-m-d h:i:s');
				$model->user_type = '2';
				
			}
			else
			{
			$model->date_updated = date('Y-m-d h:i:s');	
			}
			if($model->save())
			{
				switch($type){
					case 'second':
						$result = array('id' => $model->id,'type' => 'second');
						$reslut1 = json_encode($result);
						echo $reslut1;
					break;
					case 'third':
						$result = array('id' => $model->id,'type' => 'third');
						$reslut1 = json_encode($result);
						echo $reslut1;
					break;
					case 'fivth':
						$result = array('id' => $model->id,'type' => 'fivth');
						$reslut1 = json_encode($result);
						echo $reslut1;
					break;
					case 'seventh':
						$result = array('id' => $model->id,'type' => 'seventh');
						$reslut1 = json_encode($result);
						echo $reslut1;
					break;
					case 'eighth':
						$result = array('id' => $model->id,'type' => 'eighth');
						$reslut1 = json_encode($result);
						echo $reslut1;
					break;
					case 'tenth':
						$result = array('id' => $model->id,'type' => 'tenth');
						$reslut1 = json_encode($result);
						echo $reslut1;
					break;
					case 'eleventh':
						$result = array('id' => $model->id,'type' => 'eleventh');
						$reslut1 = json_encode($result);
						echo $reslut1;
					break;
					case 'twelveth':
						$result = array('id' => $model->id,'type' => 'twelveth');
						$reslut1 = json_encode($result);
						echo $reslut1;
					break;
					case 'thirteenth':
						$result = array('id' => $model->id,'type' => 'thirteenth');
						$reslut1 = json_encode($result);
						echo $reslut1;
					break;
				}
			}
			
		}
		else
		{
    	return $this->render('eventcreate', ['model' => $model,'orgmodel' =>$orgmodel,]);	
		}	
	}
	
	public function actionUpdatefourthstep()
	{
		$list = $_POST['CreateEvents'];
		$type = $_POST['type'];
		$model = CreateEvents::findOne($list['id']);		
		$model->attributes = $list;
		$model->event_genre_id = $list['event_genre_id'] ? implode(",",$list['event_genre_id']):'';	
			if($model->save())
			{
				$result = array('id' => $model->id,'type' => 'fourth');
				$reslut1 = json_encode($result);
				echo $reslut1;
			}
	}
	public function actionUpdateseventhstep()
	{
		$list = $_POST['CreateEvents'];
		$type = $_POST['type'];
		$model = CreateEvents::findOne($list['id']);		
		$model->attributes = $list;
		$model->audience_profile = $list['audience_profile'] ? implode(",",$list['audience_profile']):'';	
			if($model->save())
			{
				$result = array('id' => $model->id,'type' => 'seventh');
				$reslut1 = json_encode($result);
				echo $reslut1;
			}
	}
	
	public function actionCreateorganization()
	{
		$user_id= '';
		$session = Yii::$app->session;
		$list = $_POST['CreateEvents'];
		$org = $_POST['Organization'];
		$type = $_POST['type'];
		$eventmodel = CreateEvents::findOne($list['id']);
		if(isset($session['user_id'])){
			$user_id = $session['user_id'];
		}
		
		if($org['org_id']){
			$model = Organization::findOne($org['org_id']);	
		}else{
			$model = new Organization;
		}	
		$model->attributes = $org;
			
			if($model->save())
			{
				$eventmodel->org_id = $model->org_id;
				$eventmodel->save();
				$exist = Organization::orgExist($list['id'],$model->org_id);
				if(!$exist){
					$connection = Yii::$app->getDb();
					$connection->createCommand()
							->insert('app_event_organization_asoc', [
							'user_id' => $user_id,
							'event_id' => $list['id'],
							'organization_id' => $model->org_id,])
				            ->execute();
				}		
							
				echo "Success";
			}
	}
	
	public function actionCreateform()
	{
		$session = Yii::$app->session;
		
		/*if ((!isset($session['user_id'])) && (isset($session['role'])!=2)) {
  			return $this->goHome();
  		}else{*/
  			$model = new CreateEvents;
    		return $this->renderPartial('eventcreate_form', ['model' => $model]);	
		//}
	}
	
	public function actionEditevent($id)
	{
		$session = Yii::$app->session;
		if ((!isset($session['user_id'])) && (isset($session['role'])!=2)) {
  			return $this->goHome();
  		}else{	
				$model = CreateEvents::findOne($id);
				$orgmodel = Organization :: findOne($model->org_id);
				return $this->render('eventcreate', ['model' => $model,'orgmodel'=>$orgmodel]);
		}
	}
	
	
	public function actionVenueauto()
    {
        $key= $_GET['key'];
		$data = CreateEvents::selectvenueauto($key);
		foreach ($data as $item)
			{
				$suggestresult[] = array(
										'value'=>$item['title'],
										'id'=>$item['venue_id'],
										);
			}
		echo json_encode($suggestresult);
    }

	
	public function actionSlidepage()
	{
				
		$model = CreateEvents::findOne($_POST['id']);
		
		$type  = $_POST['pre'];
		if($type=='first'){
			return $this->renderPartial('eventcreate_form', ['model' => $model]);
		}
		
	}

	public function actionSponsorslab()
	{
		$session = Yii::$app->session;
		$list = $_POST;
		if($list){
			$model = CreateEvents::findOne($list['id']);
			$model->attributes = $list;
		}
		/*elseif($session['eventid']){
			$model = CreateEvents::findOne($session['eventid']);
		}
		if($session['user_id']){
			$model->verified = '1';
			}else{
				$model->verified = '0';
			}*/
		
		if($model->save())
			{
				
				$result = array('id' => $model->id,'type' => 'twelveth');
				$reslut1 = json_encode($result);
				echo $reslut1;
			/*	if($session['user_id']){
					$connection = Yii::$app -> db;
					$command = $connection -> createCommand("UPDATE app_events SET user_id ='".$session['user_id']."' WHERE id= '" . $model -> id . "'");
					$command -> execute();
					$session -> set('email', $model -> email);
					$session -> set('eventid', $model -> id);
					return $this->redirect(['eventcreate/packages']);
					
				}else{
					$exist = User::findByEmailandrole($model->email,2);
						if(!$exist){
							$user = new User;
							$user->activatekey = sha1(mt_rand(10000, 99999) . time() . $model->email);
							$user->email = $model->email;
							$user->role = '2';
							$user->contactnumber = $model->contact;
							$password = SignupForm::randomPassword();
							$user -> setPassword($password);
							$user -> generateAuthKey();
							if($user->save()){
								Yii::$app -> mailer -> compose(['html' => 'activationEventanduser-html.php', 'text' => 'activationEventanduser-text.php'], ['user' => $user,'model'=>$model,'password'=>$password]) -> setFrom([\Yii::$app -> params['supportEmail'] => 'Onspon Support']) -> setTo($user->email) -> setSubject('Account Activation and Publication link for Your Event') -> send();
								$connection = Yii::$app -> db;
								$command = $connection -> createCommand("UPDATE app_events SET user_id ='".$user->id."' WHERE id= '" . $model -> id . "'");
								$command -> execute();
								echo "Your event is successfully submited and user account is created. Please check your mail and activate your account.";
							}
						}else{
							
							$connection = Yii::$app -> db;
							$command = $connection -> createCommand("UPDATE app_events SET user_id ='".$exist->id."' WHERE id= '" . $model -> id . "'");
							$command -> execute();
							$session = Yii::$app -> session;
							$session -> set('type', $list['type']);
							$session -> set('email', $model->email);
							$session -> set('eventid', $model->id);
							echo $list['type'];
							
						}
				}*/
				
			}
	}
	
	public function actionEventactivation() {
		$key = $_GET['key'];
		$eventid = $_GET['id'];
		$password = $_GET['random'];
		$model = new CreateEvents;
		$model -> activation($key,$eventid,$password);
		$this -> redirect(array('/eventcreate/packages'));
	}
	
	
	public function actionPackages()
	{
		$session = Yii::$app->session;
		$eventid = $session['eventid'];
		$model = Package::packageDetail();
		
		if ((!isset($session['user_id'])) && (isset($session['role'])!=2)) {
  			return $this->goHome();
  		}else{
  			
    		return $this->render('package_form', ['model' => $model]);	
		}
	}
	
	public function actionEventgallery()
	{
		
			
		$model = new Photo;
		
		if($_POST){
			$list = $_POST['CreateEvents'];
		}
		
		if($list['id']){
        $model->class ='app\modules\events\models\Events';
        $model->item_id = $list['id'];
		
		}
		$model->attributes = $list;
		
		$uploadedFile = UploadedFile::getInstance($model, 'image');
				if($uploadedFile)
				{
				 $timeStamp = time(); 
				 $fileName = "{$timeStamp}-{$uploadedFile}";
				 $fileName = str_replace(" ","_",$fileName) ;
				 $upload_path = Yii::getAlias('@webroot') . '/uploads/photos/';	 
				 $uploadedFile->saveAs($upload_path . $fileName);
				 $model->image = 'uploads/photos/'.$fileName;
				
				}
            
                if($model->save()){
                	echo 'success';
                   /* $success = [
                        'message' => Yii::t('easyii', 'Photo uploaded'),
                        'photo' => [
                            'id' => $photo->primaryKey,
                            'image' => $photo->image,
                            'thumb' => Image::thumb($photo->image, Photo::PHOTO_THUMB_WIDTH, Photo::PHOTO_THUMB_HEIGHT),
                            'description' => ''
                        ]
                    ];
					 */
                }
                else{
                    @unlink(Yii::getAlias('@webroot') . str_replace(Url::base(true), '', $photo->image));
					
                }
            
        
		
	
	}

				
}
