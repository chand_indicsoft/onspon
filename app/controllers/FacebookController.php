<?php
	namespace app\controllers;

	use Yii;

	use yii\web\Controller;
	use yii\helpers\Json;

	class FacebookController extends \yii\web\Controller
	{
		public $layout = "main";

		/**
		 * @Author         : Parminder Singh
		 * @Last modified  : 10, October 2016
		 *
		 * @Project        : OnSpon
		 * @Function name  : actionIndex
		 * @Description    : function to fetch the events from facebook account of the logged in user
		 * @Parameters     : none
		 *
		 * @Method         : GET
		 * @Returns        :
		 * @Return type    :
		 */
		public function actionIndex()
		{
			return $this->render('index');
		}

		/**
		 * @Author         : Parminder Singh
		 * @Last modified  : 12, October 2016
		 *
		 * @Project        : ObSpon
		 * @Function name  : get_events
		 * @Description    : function to convert the given json string to html view
		 * @Parameters     : none
		 *
		 * @Method         : POST
		 * @Returns        : raw data
		 * @Return type    : html
		 */
		public function actionEvents()
		{
			return $this->renderAjax('events', [
				"events_json" => Yii::$app->request->post('data')
			]);
		}

		/**
		 * @Author         : Parminder Singh
		 * @Last modified  : 13, October
		 *
		 * @Project        : OnSpon
		 * @Function name  : actionSaveEvents
		 * @Description    : function to save the facebook events data of the selected user in the database table
		 * @Parameters     :
		 *
		 * @Method         : POST
		 * @Returns        : true/false
		 * @Return type    : boolean
		 */
		public function actionSaveEvents()
		{
			# variables
			$array_insert = [];
			$facebook_events = Json::decode(Yii::$app->request->post('data'));

			# inserting data into database
			foreach($facebook_events['data'] as $key => $event)
			{
				# models
				$model_facebook_events = new \app\models\AppFacebookEvent;

				# inserting data
				$model_facebook_events->name			= $event["name"];
				$model_facebook_events->start_time	= date("Y-m-d H:i:s", strtotime($event['start_time'] . ' +330minute'));
				$model_facebook_events->description	= $event["description"];
				$model_facebook_events->location		= $event["place"]["name"];
				$model_facebook_events->v_city		= $event["place"]["location"]["city"];
				$model_facebook_events->v_country	= $event["place"]["location"]["country"];
				$model_facebook_events->v_latitude	= $event["place"]["location"]["latitude"];
				$model_facebook_events->v_longitude	= $event["place"]["location"]["longitude"];
				$model_facebook_events->v_id			= $event["place"]["id"];
				$model_facebook_events->status		= $event["rsvp_status"];

				if(!empty($event["end_time"]))
				{
					$model_facebook_events->end_time = date("Y-m-d H:i:s", strtotime($event['end_time'] . ' +330minute'));
				}

				# saving data
				if(!$model_facebook_events->save())
				{
					echo "<pre>";
					print_r($model_facebook_events->getErrors());
					echo "</pre>";
					exit();
				}
			}

			return Json::encode([
				"success" => true
			]);
		}
	}
?>