<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
 
?>
<div class="password-reset">
    <p>Hello <?= Html::encode($username) ?>,</p>

    <p>You have a sponsor request for your event Click here to see.</p>

    <p><?= Html::a(Html::encode($url), $url) ?></p>
</div>
