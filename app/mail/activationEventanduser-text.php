<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
 

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['eventcreate/eventactivation', 'key' => $user->activatekey, 'id'=>$model->id, 'random'=>$password]);
?>
<div class="password-reset">
    <p>Hello <!--<?= Html::encode($user->username) ?>-->,</p>

    <p>Follow the link below to activate your Account and Your event will be published automatically:</p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>
