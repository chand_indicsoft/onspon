<?php
namespace app\models;


use Yii;
use yii\easyii\behaviors\Taggable;
use yii\easyii\models\Photo;
use yii\helpers\StringHelper;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;


class Seekerprofile extends \yii\easyii\components\ActiveRecord
{
    const STATUS_OFF = 0;
    const STATUS_ON = 1;
	
	
    public static function tableName()
    {
        return 'app_users';
    }
    
    

	public function rules()
    { 
        return [
            [['username','email','address','sex','contactnumber','location'], 'required'],
            [['age','contactnumber'], 'integer'],
            ['image', 'image'],
            [['text','image','age'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            
        ];
    }
    
    
}
