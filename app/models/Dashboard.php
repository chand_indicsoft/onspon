<?php
namespace app\models;


use Yii;
use yii\behaviors\SluggableBehavior;
use yii\easyii\behaviors\SeoBehavior;
use yii\easyii\behaviors\Taggable;
use yii\easyii\models\Photo;
use yii\helpers\StringHelper;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;
use app\modules\events\models\Events;

class Dashboard extends \yii\easyii\components\ActiveRecord
{
    const STATUS_OFF = 0;
    const STATUS_ON = 1;
	
	   
	  public static function AmazingEventDashboard()
	   {
			$query = (new \yii\db\Query())->select(['app_amazing.event_id','app_events.*'])->from('app_amazing')->limit('3');
			$command = $query->join('LEFT JOIN', 'app_events', 'app_events.id = app_amazing.event_id');
			$result = $query->createCommand()->queryAll();
			return $result ;
	   }
	   
	  public static function AmazingEventListing()
	   {
			$query = (new \yii\db\Query())->select(['app_amazing.event_id','app_events.*'])->from('app_amazing');
			$command = $query->join('LEFT JOIN', 'app_events', 'app_events.id = app_amazing.event_id');
			$result = $query->createCommand()->queryAll();
			return $result ;
	   }
	   
	   public static function UserInboxHome($id)
	   {
			$query = (new \yii\db\Query())->select(['app_sponsor_apply.*','app_brandmandates.logo'])->from('app_sponsor_apply')
			->where(['app_sponsor_apply.receiver_id'=>$id])->orderBy('app_sponsor_apply.date');
			$command = $query->join('LEFT JOIN', 'app_brandmandates', 'app_brandmandates.id = app_sponsor_apply.brandmandates_id');
			$result = $query->createCommand()->queryAll();
			return $result ;	
	   }
	   
	    public static function SentDetailsByID($id)
	   {
			$query = (new \yii\db\Query())->select(['app_sponsor_apply_outbox.*','app_users.username'])->from('app_sponsor_apply_outbox')
			->where(['app_sponsor_apply_outbox.id'=>$id]);
			$command = $query->join('LEFT JOIN', 'app_users', 'app_users.id = app_sponsor_apply_outbox.receiver_id');
			$result = $query->createCommand()->queryAll();
			return $result ;	
	   }
	   
	    public static function ReceiveDetailsByID($id)
	   {
			$query = (new \yii\db\Query())->select(['app_sponsor_apply.*','app_users.username'])->from('app_sponsor_apply')
			->where(['app_sponsor_apply.id'=>$id]);
			$command = $query->join('LEFT JOIN', 'app_users', 'app_users.id = app_sponsor_apply.sender_id');
			$result = $query->createCommand()->queryAll();
			return $result ;	
	   }
	   
	    public static function ContactList($id)
	   {
			$query = (new \yii\db\Query())->select(['app_inbox.*','app_events.logo'])->from('app_inbox')
			->where(['app_inbox.user_id'=>$id]);
			$command = $query->join('LEFT JOIN', 'app_events', 'app_events.id = app_inbox.event_id');
			$result = $query->createCommand()->queryAll();
			return $result ;	
	   }
	   
	    public static function MyEventsList($id)
	   {
			$query = (new \yii\db\Query())->select(['events.*','count(Distinct viewslist.userid) as totalview','count(Distinct shorlist.user_id) as toatlshort'])
			->from('app_events as events')->where(['events.user_id'=>$id])->groupBy(['events.id'])->limit('10');
			$command = $query->join('LEFT JOIN', 'app_event_viewby_brand as viewslist', 'viewslist.event_id = events.id');
			$command = $query->join('LEFT JOIN', 'app_user_shortlist_event as shorlist', 'shorlist.event_id = events.id');
			return $result = $command->createCommand()->queryAll();
	   }
	   
	    public static function MyEventsListActive($id,$daterange)
	   {
			$query = (new \yii\db\Query())->select(['events.*','count(Distinct viewslist.userid) as totalview','count(Distinct shorlist.user_id) as toatlshort'])
			->from('app_events as events')->where(['events.user_id'=>$id])
			 ->andWhere(['>=', 'event_end_date', $daterange])->groupBy(['events.id']);
			$command = $query->join('LEFT JOIN', 'app_event_viewby_brand as viewslist', 'viewslist.event_id = events.id');
			$command = $query->join('LEFT JOIN', 'app_user_shortlist_event as shorlist', 'shorlist.event_id = events.id');
			return $result = $command->createCommand()->queryAll();
	   }
	   
	    public static function MyEventsListInactive($id,$daterange)
	   {
			$query = (new \yii\db\Query())->select(['events.*','count(Distinct viewslist.userid) as totalview','count(Distinct shorlist.user_id) as toatlshort'])
			->from('app_events as events')->where(['events.user_id'=>$id,'verified'=>'0'])
			 ->andWhere(['<=', 'event_end_date', $daterange])->groupBy(['events.id']);
			$command = $query->join('LEFT JOIN', 'app_event_viewby_brand as viewslist', 'viewslist.event_id = events.id');
			$command = $query->join('LEFT JOIN', 'app_user_shortlist_event as shorlist', 'shorlist.event_id = events.id');
			return $result = $command->createCommand()->queryAll();
	   }
	   
	   public static function BrandMandateHome()
	   { 
		   $query = (new \yii\db\Query())->select(['mandates.*','age.title as agegroup','educat.title as educationname','incom.title as totalimcome'])
		   ->from('app_brandmandates as mandates')
			->where(['mandates.status'=>'1'])->limit('6');
			$command = $query->join('LEFT JOIN', 'app_agegroup as age', 'age.agegroup_id = mandates.age_group');
			$command = $query->join('LEFT JOIN', 'app_education as educat', 'educat.education_id = mandates.education');
			$command = $query->join('LEFT JOIN', 'app_income as incom', 'incom.income_id = mandates.income');
			return $result = $query->createCommand()->queryAll();
		}
		
		 public static function BrandMandateDashboardlisting($limit,$offset)
		{ 
		   $query = (new \yii\db\Query())->select(['mandates.*','age.title as agegroup','educat.title as educationname','incom.title as totalimcome'])
		   ->from('app_brandmandates as mandates')
			->where(['mandates.status'=>'1'])->offset($offset)->limit($limit);
			$command = $query->join('LEFT JOIN', 'app_agegroup as age', 'age.agegroup_id = mandates.age_group');
			$command = $query->join('LEFT JOIN', 'app_education as educat', 'educat.education_id = mandates.education');
			$command = $query->join('LEFT JOIN', 'app_income as incom', 'incom.income_id = mandates.income');
			return $result = $query->createCommand()->queryAll();
		}
	   
	   
	  
	   
	  
    
}
