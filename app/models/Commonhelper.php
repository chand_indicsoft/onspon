<?php

namespace app\models;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class Commonhelper extends Model {
	
	public static function selectautocomplete($name)
		{
			$connection = Yii::$app->getDb();
			$command = $connection->createCommand('
	        SELECT id,email FROM app_users Where username LIKE "%'.$name.'%"  AND role="1"');
			$result = $command->queryAll();
			return $result;
		}
		
		public static function selectautocompleteForEvent($name)
		{
			$connection = Yii::$app->getDb();
			$command = $connection->createCommand('
	        SELECT id,email FROM app_users Where username LIKE "%'.$name.'%"  AND role="2"');
			$result = $command->queryAll();
			return $result;
		}
		
		 public static function select_user_name($id)
		{
			$connection = Yii::$app->getDb();
			$command = $connection->createCommand('
	        SELECT email FROM app_users Where id = "'.$id.'" ');
			$result = $command->queryScalar();
			return $result;
		}
		
		 public static function select_event_name($id)
		{
			$connection = Yii::$app->getDb();
			$command = $connection->createCommand('
	        SELECT event_name FROM app_events Where id = "'.$id.'" ');
			$result = $command->queryScalar();
			return $result;
		}
		
		public static function selectEventautocomplete($name)
		{
			$connection = Yii::$app->getDb();
			$command = $connection->createCommand('
	        SELECT id,event_name FROM app_events Where event_name LIKE "%'.$name.'%"');
			$result = $command->queryAll();
			return $result;
		}
		
		public static function getTimeago($time)
		{
			$time = strtotime($time);
			$time = time() - $time; 
			$tokens = array (31536000 => 'year',2592000 => 'month',604800 => 'week',86400 => 'day',3600 => 'hour',60 => 'minute',1 => 'second');
			
			foreach ($tokens as $unit => $text) {

				if ($time < $unit) continue;
				$numberOfUnits = floor($time / $unit);
				return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'').' ago';
			}
		}
	
}
