<?php
namespace app\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\easyii\behaviors\SeoBehavior;
use yii\easyii\behaviors\Taggable;
use yii\easyii\models\Photo;
use yii\helpers\StringHelper;
use yii\helpers\ArrayHelper;
use app\modules\tagmanagement\models\TagManagemnet;

class CreateEvents extends \yii\easyii\components\ActiveRecord
{
    const STATUS_OFF = 0;
    const STATUS_ON = 1;
    public $type;
	

    public static function tableName()
    {
        return 'app_events';
    }

    public function rules()
    { 
        return [
         //   [['event_name','user_id'], 'required'],
            [['description'], 'trim'],
            //['title', 'string', 'max' => 128],
            ['image', 'image'],
            ['banner', 'image'],
            ['logo', 'image'],
            ['verified','boolean'],
            [['views', 'status','user_id','event_strt_date','event_end_date','fb_likes','premium','user_type','incorporation_yr','event_category_id','ref_no','age_group_most','age_group_rest','education_most','education_rest','income_most','income_rest'], 'integer'],
            ['time', 'default', 'value' => time()],
            ['slug', 'match', 'pattern' => self::$SLUG_PATTERN, 'message' => Yii::t('easyii', 'Slug can contain only 0-9, a-z and "-" characters (max: 128).')],
            ['slug', 'default', 'value' => null],
            [['every_yr','televised','paid_media','multicity_event','social_media_sharing','celebrity_tweets','alchohol_licence','sampling','speaking_opportunities','ticketed','completed','basic_visibility','experience_zone','sales_counter','wifi_enable','automobile_display','parking_space','common_areas','vip_box','post_event_party'], 'boolean'],
            ['status', 'default', 'value' => self::STATUS_ON],
            [['no_of_attendees','digital_outreach','audience_profile','event_passes_ticket','database_collecttion','pre_event_buzz','ticket_branding','slabname','sponsorprice','deliverables','in_premise_posters','emailers','television','radio','ooh_media','print_media','event_name','user_id','contact','email','tagNames','website','pitch_pdf','zipcode','address1','ticket_text','tag_id','url','fb_page','city_id','country_id','state_id','address','date_created','date_updated','sprice_from','sprice_to','event_date','venue_name','address','lat','lng','event_strt_time','event_end_time','event_genre_id'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            //'title' => Yii::t('easyii', 'Title'),
            'user_id' => Yii::t('easyii', 'User'),
            'event_category_id' => Yii::t('easyii', 'Event Category'),
            'ref_no' => Yii::t('easyii', 'Reference Number'),
            'sprice_from' => Yii::t('easyii', 'Sponsership Price From'),
            'sprice_to' => Yii::t('easyii', 'Sponsership Price To'),
            'incorporation_yr' => Yii::t('easyii', 'Incorporation Year'),
            'city_id' => Yii::t('easyii', 'City'),
            'state_id' => Yii::t('easyii', 'State'),
            'country_id' => Yii::t('easyii', 'Country'),
            'lat' => Yii::t('easyii', 'Latitude'),
            'lng' => Yii::t('easyii', 'Longitude'),
            'event_strt_date' => Yii::t('easyii', 'Event Start Date'),
            'event_end_date' => Yii::t('easyii', 'Event End Date'),
            'event_strt_time' => Yii::t('easyii', 'Event Start Time'),
            'event_end_time' => Yii::t('easyii', 'Event End Time'),
            'fb_page' => Yii::t('easyii', 'Facebook Page'),
            'fb_likes' => Yii::t('easyii', 'Facebook Likes'),
            'no_of_attendees' => Yii::t('easyii', 'Number of Attendees'),
            'every_yr' => Yii::t('easyii', ''),
            'televised' => Yii::t('easyii', ''),
            'ticketed' => Yii::t('easyii', ''),
            'multicity_event' => Yii::t('easyii', ''),
            'earlier_sponser' => Yii::t('easyii', 'Earlier Sponser(s)?'),
            'image' => Yii::t('easyii', 'Image'),
            'time' => Yii::t('easyii', 'Date'),
            'slug' => Yii::t('easyii', 'Slug'),
            'tagNames' => Yii::t('easyii', 'Tags'),
            'ticket_branding' => Yii::t('easyii', ''),
            'pre_event_buzz' => Yii::t('easyii', ''),
            'event_passes_ticket' => Yii::t('easyii', ''),
            'database_collecttion' => Yii::t('easyii', ''),
            'paid_media' => Yii::t('easyii', ''),
            'social_media_sharing' => Yii::t('easyii', ''),
            'celebrity_tweets' => Yii::t('easyii', ''),
            'alchohol_licence' => Yii::t('easyii', ''),
            'sampling' => Yii::t('easyii', ''),
            'speaking_opportunities' => Yii::t('easyii', ''),
            'basic_visibility' => Yii::t('easyii', ''),
            'experience_zone' => Yii::t('easyii', ''),
            'sales_counter' => Yii::t('easyii', ''),
            'wifi_enable' => Yii::t('easyii', ''),
            'automobile_display' => Yii::t('easyii', ''),
            'parking_space' => Yii::t('easyii', ''),
            'common_areas' => Yii::t('easyii', ''),
            'vip_box' => Yii::t('easyii', ''),
            'post_event_party' => Yii::t('easyii', ''),
        ];
    }

    public function behaviors()
    {
        return [
            'seoBehavior' => SeoBehavior::className(),
            'taggabble' => Taggable::className(),
            'sluggable' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'event_name',
                'ensureUnique' => true
            ],
        ];
    }

    public function getPhotos()
    {
        return $this->hasMany(Photo::className(), ['item_id' => 'events_id'])->where(['class' => self::className()])->sort();
    }



    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $settings = Yii::$app->getModule('admin')->activeModules['events']->settings;
            $this->short = StringHelper::truncate($settings['enableShort'] ? $this->short : strip_tags($this->text), $settings['shortMaxLength']);

            if(!$insert && $this->image != $this->oldAttributes['image'] && $this->oldAttributes['image']){
                @unlink(Yii::getAlias('@webroot').$this->oldAttributes['image']);
            }
            return true;
        } else {
            return false;
        }
    }
	
	public function eventGallery($id){
		
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand('
        SELECT * FROM easyii_photos Where item_id ='.$id.'');

   return $result = $command->queryAll();
	
	}
	
	public function getCategoryDropdown() 
	{
        $listCategory   = TagManagemnet::find()->select('tagmanagemnet_id,title')
            ->where(['status' => '1'])
            //->andWhere(['status' => 'active','approved' => 'active'])
            ->all();
        $list   = ArrayHelper::map( $listCategory,'tagmanagemnet_id','title');

        return $list;
	}
	
	public static function selectvenueauto($name)
		{
			$connection = Yii::$app->getDb();
			$command = $connection->createCommand('
	        SELECT venue_id,title FROM app_venue Where title LIKE "%'.$name.'%" ');
			$result = $command->queryAll();
			return $result;
		}

	public static function selectvenue_by_id($id)
		{
			$connection = Yii::$app->getDb();
			$command = $connection->createCommand('
	                SELECT title FROM app_venue Where venue_id = "'.$id.'"');
			$result = $command->queryScalar();
			return $result;
		}

    public function afterDelete()
    {
        parent::afterDelete();

        if($this->image){
            @unlink(Yii::getAlias('@webroot').$this->image);
        }

        foreach($this->getPhotos()->all() as $photo){
            $photo->delete();
        }
    }
     public static function Updateeventtag($id,$tag)
		 {
			if($tag){
				$connection = Yii::$app->getDb();
				$connection->createCommand()
				->delete('app_event_tag', ['event_id' => $id])
				->execute();	
			
			$tag = explode(",",$tag);	
			$len = sizeof($tag);
			
			for($i = 0;$i<$len;$i++){
				$connection = Yii::$app->getDb();
				$connection->createCommand()
				->insert('app_event_tag', ['event_id' => $id,'tag_id' => $tag[$i]])
				->execute();
				}
			}
		}
		 
	public function activation($key,$eventid,$password) {

		$user = new User();
		$sql = "select * from app_users where activatekey = '" . $key . "'";
		$data = $user -> findBySql($sql) -> all();

		if (!empty($data)) {
			foreach ($data as $row) :

				if ($row -> status == '1') {
					$session = Yii::$app->session;
					$session -> set('user_id', $row -> id);
					$session -> set('email', $row -> email);
					$session -> set('eventid', $eventid);
					Yii::$app -> getSession() -> setFlash('success', Yii::t('app', 'Your account is already  active and your event is published'));

				} else {

					$connection = Yii::$app -> db;
					$command = $connection -> createCommand("UPDATE app_users SET status=1 WHERE id= '" . $row -> id . "'");
					$command -> execute();
					 
					$result = $connection -> createCommand("UPDATE app_events SET verified=1 WHERE id= '" . $eventid . "'");
					$result -> execute();
					
					
					Yii::$app -> mailer -> compose(['html' => 'eventuserCredential-html.php', 'text' => 'eventuserCredential-text.php'], ['user' => $row, 'password'=>$password]) -> setFrom([\Yii::$app -> params['supportEmail'] => 'Onspon Support']) -> setTo($row -> email) -> setSubject('User Credential for OnSpon') -> send();
					
					
					$session = Yii::$app->session;
					$session -> set('user_id', $row -> id);
					$session -> set('email', $user -> email);
					$session -> set('eventid', $eventid);
					
					Yii::$app -> getSession() -> setFlash('success', Yii::t('app', 'Successfully logged in and your events is published. Check your mail for login credential'));
									
				}
			endforeach;
		} else {
			throw new CHttpException(403, Yii::t('common', 'User not exists'));
		}

	}	 
}
