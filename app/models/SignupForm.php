<?php
	namespace app\models;

	use Yii;
	use yii\base\Model;
	use app\models\User;

	/**
	 * Signup form
	 */
	class SignupForm extends Model
	{
		public $username;
		public $email;
		public $password;
		public $email_update;
		public $current_url;
		public $role;
		public $contactnumber;

		/**
		 * @inheritdoc
		 */
		public function rules()
		{
			return [
				['username', 'filter', 'filter' => 'trim'],
			 	[['username', 'role','contactnumber', 'password', 'email'], 'required'],
			 	['email', 'unique', 'targetClass' => 'app\models\User', 'message' => 'This username has already been taken.'],
			 	['username', 'string', 'min' => 2, 'max' => 255],
			 	['email', 'filter', 'filter' => 'trim'],
			 	['email', 'email'],
			 	['email_update', 'boolean'],
			 	['email', 'string', 'max' => 255]
			];
		}

		/**
		 * Signs user up.
		 *
		 * @return User|null the saved model or null if saving fails
		 */
		public function signup()
		{
			if (!$this->validate())
			{
				return null;
			}

			$user = new User();
			$user->username      = $this->username;
			$user->email         = $this->email;
			$user->role          = $this->role;
			$user->contactnumber = $this->contactnumber;
			$user->email_update  = $this->email_update;
			$user->activatekey   = sha1(mt_rand(10000, 99999) . time() . $this->email);
			$user->setPassword($this->password);
			$user->generateAuthKey();

			return $user->save() ? $user : null;
		}

		public function apisignup($attribute) {
		/*
			if (!$this->validate()) {
				return null;
			}
	*/
			$user = new User();
			$user->username = $attribute['username'];
			$user->email = $attribute['email'];
			$user->contactnumber = $attribute['contact'];
			$user->email_update = $attribute['email_update'];
			$user->setPassword($attribute['password']);
			$user->generateAuthKey();
			$user->activatekey = sha1(mt_rand(10000, 99999) . time() . $attribute['email']);

			return $user->save() ? $user : null;
		}


		// generate username
		function generateUserName($l, $c = 'abcdefghijklmnopqrstuvwxyz1234567890') {

			for ($s = '', $cl = strlen($c) - 1, $i = 0; $i < $l; $s .= $c[mt_rand(0, $cl)], ++$i);

			return $s;
		}

		// generate password
		function randomPassword() {
			$password = "";
			$charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			for ($i = 0; $i < 8; $i++) {
				$random_int = mt_rand();
				$password .= $charset[$random_int % strlen($charset)];
			}
			return $password;

		}

		/*public function socialSignup($attrubute) {

			$password = $this->randomPassword();
			$auth_client='facebook';
			$user = new User();
			$data = $user->find()->where(['email' =>$attrubute['email'],'auth_client'=>$auth_client])->count();

			if ($data) {
				return $user->find()->where(['email' =>$attrubute['email'],'auth_client'=>$auth_client])->all();
			} else {
				//$user->username = $this->generateUserName(10, $attrubute['name'] . $attrubute['id']);
				$user->username =$attrubute['name'] ;
				$user->email = $attrubute['email'];
				$user->setPassword($password);
				$user->generateAuthKey();
				$user->auth_client=$auth_client;
				$user->save() ? $user : null;
				$useridname[]= array(
					'id'=>$user->id,
					'username'=>$user->username
				) ;
				return $useridname;

			}

		}*/

		public function socialSignup($attrubute)
		{
			$user				= new User();
			$password		= $this->randomPassword();
			$auth_client	= 'facebook';

			if (!empty($attrubute['email']))
			{
				$data = $user->find()->where([
					'email'			=> $attrubute['email'],
					'auth_client'	=> $auth_client
				])->count();
			}
			else
			{
				$data = $user->find()->where([
					'username'		=> $attrubute['name'],
					'auth_client'	=> $auth_client
				])->count();
			}

			if ($data)
			{
				if (!empty($attrubute['email']))
				{
					$data1 = $user->find()->where('email = :email', [
						':email' => $attrubute['email']
					])->orWhere('username = :username', [
						':username' => $attrubute['name']
					])->andWhere('auth_client = :auth_client', [
						':auth_client' => $auth_client
					])->all();

					$useridname[] = [
						'id'			=> $data1[0]->id,
						'username'	=> $data1[0]->username,
						'image'		=> $data1[0]->image
					];

					return $useridname;
				}
				else
				{
					$data1 = $user->find()->where('username = :username', [
						':username' => $attrubute['name']
					])->andWhere('auth_client = :auth_client', [
						':auth_client' => $auth_client
					])->all();

					$useridname[] = [
						'id'       => $data1[0]->id,
						'username' => $data1[0]->username,
						'image'    => $data1[0]->image
					];

					Yii::$app->session->set('facebook', $attrubute);

					return $useridname;
				}
			}
			else
			{
				//$user->username = $this->generateUserName(10, $attrubute['name'] . $attrubute['id']);
				$user->username = $attrubute['name'];

				if (!empty($attrubute['email']))
				{
					$user->email = $attrubute['email'];
				}

				$user->status			= 1 ;
				$user->role				= \Yii::$app->session->get('role');
				$user->auth_client	= $auth_client;
				$user->facebook_id	= $attrubute['id'];

				$user->setPassword($password);
				$user->generateAuthKey();
				$user->save() ? $user : null;

				$useridname[] = [
					'id'			=> $user->id,
					'username'	=> $user->username,
					'image'		=> $user->image
				];

				Yii::$app->session->set('facebook', $attrubute);

				return $useridname;
			}
		}

		public function socialGoogleSignup($attrubute)
		{
			# google auth
			if($attrubute['authclient'] == 'google')
			{
				$email = $attrubute['emails'][0]['value'];
				$name  = $attrubute['displayName'];
				$image = $attrubute['image']['url'];
			}
			# linkedin auth
			else
			{
				$email = $attrubute['email'];
				$name  = "{$attrubute['first-name']} {$attrubute['last-name']}";
			}

			$user = new User();
			$data = $user->find()->where([
				'email'       => $email,
				'auth_client' => $attrubute['authclient']
			])->count();

			if ($data)
			{
				$data1 = $user->find()->where([
					'email'       => $email,
					'auth_client' => $attrubute['authclient']
				])->all();

				$useridname[] = array(
					'id'       => $data1[0]->id,
					'username' => $data1[0]->username
				);

				return $useridname;
			}
			else
			{
				$user->username		= $name;
				$user->email			= $email;
				$user->role				= \Yii::$app->session->get('role');
				$user->auth_client	= $attrubute['authclient'];
				$user->status			= 1;

				$user->generateAuthKey();
				$user->setPassword($this->randomPassword());
				$user->generateAuthKey();

				if(!empty($image))
				{
					$user->image = $image;
				}

				# google auth
				if($attrubute['authclient'] == 'google')
				{
					$user->google_id = $attrubute['id'];
				}
				# linkedin auth
				else
				{
					$user->linkedin_id = $attrubute['id'];
				}

				$user->save() ? $user : null;

				$useridname[]= array(
					'id'=>$user->id,
					'username'=>$user->username
				) ;

				return $useridname;
			}
		}

		public function socialGoogleSignupapi($attrubute)
		{

			$email    		= $attrubute['email'];
			$name     		= $attrubute['name'];
			$auth_client  	= $attrubute['authclient'];
			$id       		= $attrubute['id'];
			//$image          =$attrubute['image']['url'];
			$password 		= $this->randomPassword();
			//print_r($attrubute['image']['url'] );print_r($attrubute );die;
			$user = new User();
			$data = $user->find()->where(['email' =>$email,'auth_client'=>$auth_client])->count();

			if ($data) {
				$data1 = $user->find()->where(['email' => $email,'auth_client'=>$auth_client])->all();
				$useridname[]= array(
					'id'=>$data1[0]->id,
					'username'=>$data1[0]->username,
					'image'=>$data1[0]->image
						) ;
					return $useridname;
			} else {
				$user->username = $name;
				$user->email = $email ;
				//$user->image = $image ;
				$user->google_id = $id ;
				$user->status = 1 ;
				$user->setPassword($password);
				$user->generateAuthKey();
				$user->auth_client=$auth_client;

				$user->save() ? $user : null;
				$useridname[]= array(
					'id'=>$user->id,
					'username'=>$user->username,
					'image'=>$user->image
				) ;
				return $useridname;

			}



		}

		public function activation($key) {

			$user = new User();
			//$user = $user->find('activatekey = :activatekey', array(':activatekey' => $key));  //saving key into :activatekey and comparing  it to activatekey
			// $data = $user->find()->where(['activatekey' => $key]);
			$sql = "select * from app_users where activatekey = '" . $key . "'";
			$data = $user->findBySql($sql)->all();

			if (!empty($data)) {
				foreach ($data as $row) :

					if ($row->status == '1') {
						Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Your account is already  active'));

					} else {

						$connection = Yii::$app->db;
						$command = $connection->createCommand("UPDATE app_users SET status=1 WHERE id= '" . $row->id . "'");
						$command->execute();

						//Yii::$app->mailer->compose(['html' => 'welcomeUser-html.php', 'text' => 'welcomeUser-text.php'], ['user' => $row])->setFrom([\Yii::$app->params['supportEmail'] => 'Petyaar Support'])->setTo($row->email)->setSubject('Welcome to Petyaar')->send();

						Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Your account successfully activated'));
					}
				endforeach;
			} else {
				throw new CHttpException(403, Yii::t('common', 'User not exists'));
			}

		}

	}
?>