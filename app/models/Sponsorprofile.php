<?php
namespace app\models;


use Yii;
use yii\easyii\behaviors\Taggable;
use yii\easyii\models\Photo;
use yii\helpers\StringHelper;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;


class Sponsorprofile extends \yii\easyii\components\ActiveRecord
{
    const STATUS_OFF = 0;
    const STATUS_ON = 1;
	
	
    public static function tableName()
    {
        return 'app_sponsor_profile';
    }
    
    

	public function rules()
    { 
        return [
            [['user_id','name','email','industry','brand_name','intrested_area','location'], 'required'],
             [['mobile','pincode'], 'integer'],
            ['image', 'image'],
            [['address','pincode','landmark','date_created','location_specific'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            
        ];
    }
    
    
}
