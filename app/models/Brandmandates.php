<?php
namespace app\models;


use Yii;
use yii\db\Expression;
use yii\behaviors\SluggableBehavior;
use yii\easyii\behaviors\SeoBehavior;
use yii\easyii\behaviors\Taggable;
use yii\easyii\models\Photo;
use yii\helpers\StringHelper;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;
use app\modules\events\models\Events;
use app\modules\amazing\models\Amazing;
use app\modules\trending\models\Trending;
use app\modules\lastminute\models\LastMinute;

class Brandmandates extends \yii\easyii\components\ActiveRecord
{
    const STATUS_OFF = 0;
    const STATUS_ON = 1;
	public $type;
	public $pre;
	public $userImage;
	
	
    public static function tableName()
    {
        return 'app_brandmandates';
    }
    
    public function behaviors()
    {
        return [
            'sluggable' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'brand_name',
                'ensureUnique' => true
            ],
        ];
    }

	public function rules()
    { 
        return [
            [['brand_name','industry_id','about_brand','i_am_looking'], 'required'],
            [['about_brand'], 'trim'],
            ['logo', 'image'],
            [['status','user_id','user_type'], 'integer'],
            
            ['slug', 'match', 'pattern' => self::$SLUG_PATTERN, 'message' => Yii::t('easyii', 'Slug can contain only 0-9, a-z and "-" characters (max: 128).')],
            ['slug', 'default', 'value' => null],
            ['status', 'default', 'value' => self::STATUS_ON],
            [['timeline','budget_range','address','start_date','end_date','visible_to_audience','user_type','user_id','visible_to_all_avenues','event_category_id','specific_subjective','question1','question2','age_group','education','income','gender','city_id','country_id','state_id','date_created','date_updated','lat','lng'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'brand_name' => Yii::t('easyii', 'Brand Name'),
            'user_id' => Yii::t('easyii', 'User'),
            'about_brand' => Yii::t('easyii', 'A little about your brand'),
            'i_am_looking' => Yii::t('easyii', 'I am looking to....'),
            'industry_id' => Yii::t('easyii', 'Select Industry'),
            'logo' => Yii::t('easyii', 'Brand Image/Logo'),
            'event_category_id' => Yii::t('easyii', 'Any Specfic category of Avenue in your mind'),
            'country_id' => Yii::t('easyii', 'Country'),
            'state_id' => Yii::t('easyii', 'State'),
            'city_id' => Yii::t('easyii', 'City'),
            'specific_subjective' => Yii::t('easyii', 'Partnership / Sponsorship Objective'),
            'age_group' => Yii::t('easyii', 'Age Group'),
            'education' => Yii::t('easyii', 'Education'),
            'income' => Yii::t('easyii', 'Income'),
            'gender' => Yii::t('easyii', 'Gender'),
            'visible_to_all_avenues' => Yii::t('easyii', 'Click here for your brand mandates to be visible to all avenues fitting the description. '),
            'slug' => Yii::t('easyii', 'Slug'),
             'timeline' => Yii::t('easyii', 'Timelines'),
            'visible_to_audience' => Yii::t('easyii', 'Click here to hide it from audience'),
        ];
    }
    
     public static function UpdateBrandmandates($id,$objective,$category)
	   {
				if($objective){
					$query = (new \yii\db\Query())->select(['*'])->from('app_brand_sponsorship_objective')->where(['brand_mandates_id'=>$id]);
					$result = $query->createCommand();
					$data = $result->queryAll();
					if($data){
						$connection = Yii::$app->getDb();
						$connection->createCommand()
						->delete('app_brand_category_specific', ['brand_mandates_id' => $id])
						->execute();	
					}
					$objective = explode(",",$objective);	
					$len = sizeof($objective);
					
					for($i = 0;$i<$len;$i++){
						$connection = Yii::$app->getDb();
						$connection->createCommand()
						->insert('app_brand_category_specific', ['brand_mandates_id' => $id,'category_id' => $objective[$i]])
						->execute();
						}
					}
				
				if($category){
					$query1 = (new \yii\db\Query())->select(['*'])->from('app_brand_category_specific')->where(['brand_mandates_id'=>$id]);
					$result1 = $query1->createCommand();
					$data1 = $result1->queryAll();
					if($data1){
						$connection = Yii::$app->getDb();
						$connection->createCommand()
						->delete('app_brand_sponsorship_objective', ['brand_mandates_id' => $id])
						->execute();	
					}
				$category = explode(",",$category);	
				$len = sizeof($category);
				
				for($i = 0;$i<$len;$i++){
					$connection = Yii::$app->getDb();
					$connection->createCommand()
					->insert('app_brand_sponsorship_objective', ['brand_mandates_id' => $id,'sponsor_objective_id' => $category[$i]])
					->execute();
					}
				}
	   }
	   
	   
	   public static function Select_by_user($userid)
	   {
		    $query = (new \yii\db\Query())
		    ->select(['app_brandmandates.*','app_industry.title as iname'])
		    ->from('app_brandmandates')->where(['user_id'=>$userid]);
		    $command = $query->join('LEFT JOIN', 'app_industry', 'app_industry.id = app_brandmandates.industry_id');
			$result = $command->createCommand()->queryAll();
			return $result;
	   }
	   
	    public static function Selcetcategory_City($userid)
	   {
		    $query = (new \yii\db\Query())
		    ->select(['city_id','event_category_id'])->from('app_brandmandates')->where(['user_id'=>$userid]);
			$result = $query->createCommand()->queryAll();
			return $result;
	   }
	   
	   public static function BrandDeleteById($id)
	   {
		  //   sponsorship_objective
		    $query = (new \yii\db\Query())->select(['*'])->from('app_brand_sponsorship_objective')->where(['brand_mandates_id'=>$id]);
			$result = $query->createCommand();
			$data = $result->queryAll();
			if($data){
				$connection = Yii::$app->getDb();
				$connection->createCommand()
				->delete('app_brand_sponsorship_objective', ['brand_mandates_id' => $id])
					->execute();		
				}
		    
		     //   event_category_specific
		    $query1 = (new \yii\db\Query())->select(['*'])->from('app_brand_category_specific')->where(['brand_mandates_id'=>$id]);
			$result1 = $query1->createCommand();
			$data1 = $result1->queryAll();
			if($data1){
				$connection = Yii::$app->getDb();
				$connection->createCommand()
				->delete('app_brand_category_specific', ['brand_mandates_id' => $id])
				->execute();	
				}
			
			 //   event_category_specific
			 if($id){
			 $connection = Yii::$app->getDb();
			 $connection->createCommand()
			->delete('app_brandmandates', ['id' => $id])
			->execute();
			}	
			
	   }
	   
	    public static function RecomendedEvents($cat,$city,$daterange)
	   {
			$query=Events::find()->status(Events::STATUS_ON);
			
			if($cat){ $query->andWhere(['event_category_id'=>$cat]);}
			if($city){ $query->orWhere(['city_id'=>$city]);}
			$query->andWhere(['>=', 'event_end_date', $daterange]);
			return $query_result = $query->limit('3')->orderBy(new Expression('rand()'))->all();
	   }
	   
	    public static function RecomendedEventsAll($cat,$city)
	   {
			$query=Events::find()->status(Events::STATUS_ON);
			
			if($cat){ $query->andWhere(['event_category_id'=>$cat]);}
			if($city){ $query->orWhere(['city_id'=>$city]);}
			
			return $query_result = $query->all();
	   }
	   
	    public static function Shortlistevent($userid)
	   {
		    $query = (new \yii\db\Query())
		    ->select(['app_user_shortlist_event.event_id','app_user_shortlist_event.folderid','app_user_folder.foldername','app_events.*'])
		    ->from('app_user_shortlist_event')->where(['app_user_shortlist_event.user_id'=>$userid])->limit('3')->orderBy(new Expression('rand()'));
			$command = $query->join('LEFT JOIN', 'app_user_folder', 'app_user_folder.id = app_user_shortlist_event.folderid');
			$command = $query->join('LEFT JOIN', 'app_events', 'app_events.id = app_user_shortlist_event.event_id');
			$result = $command->createCommand()->queryAll();
			
			return $result;
	   }
	   
	   public static function ShortlisteventAll($userid,$limit,$offset)
	   {
		    $query = (new \yii\db\Query())
		    ->select(['app_user_shortlist_event.event_id','app_user_shortlist_event.folderid','app_user_folder.foldername','app_events.*'])
		    ->from('app_user_shortlist_event')->where(['app_user_shortlist_event.user_id'=>$userid])->offset($offset)->limit($limit);
			$command = $query->join('LEFT JOIN', 'app_user_folder', 'app_user_folder.id = app_user_shortlist_event.folderid');
			$command = $query->join('LEFT JOIN', 'app_events', 'app_events.id = app_user_shortlist_event.event_id');
			$result = $command->createCommand()->queryAll();
			
			return $result;
	   }
	   
	    public static function brandmandateslist_for_request($id)
	   {
			 $query = (new \yii\db\Query())->select(['brand_name'])->from('app_brandmandates')->where(['user_id'=>$id]);
			$result = $query->createCommand()->queryAll();
			$i=0;	foreach($result as $item){
				if($i==0){
				$row = $item['brand_name'];$i++;
				}else{
					$row .= ', '.$item['brand_name'];$i++;
					}
			}
			
			return $row ;
	   }
	   
	   public static function trendingEventDashboard($daterange)
	   {
			$query = (new \yii\db\Query())->select(['app_trending.event_id','app_events.*'])->from('app_trending')
			->where(['app_events.status'=>'1'])->andWhere(['>=', 'event_end_date', $daterange])
			->limit('3')->orderBy(new Expression('rand()'));
			$command = $query->join('LEFT JOIN', 'app_events', 'app_events.id = app_trending.event_id');
			$result = $query->createCommand()->queryAll();
			return $result ;
	   }
	   
	    public static function trendingEventListing($limit,$offset,$daterange)
	   {
			$query = (new \yii\db\Query())->select(['app_trending.event_id','app_events.*'])->from('app_trending')
			->where(['app_events.status'=>'1'])->andWhere(['>=', 'event_end_date', $daterange])->offset($offset)->limit($limit);
			$command = $query->join('LEFT JOIN', 'app_events', 'app_events.id = app_trending.event_id');
			$result = $query->createCommand()->queryAll();
			return $result ;
	   }
	   
	    public static function AmazingEventDashboard($daterange)
	   {
			$query = (new \yii\db\Query())->select(['app_amazing.event_id','app_events.*'])->from('app_amazing')
			->where(['app_events.status'=>'1'])->andWhere(['>=', 'event_end_date', $daterange])
			->limit('3')->orderBy(new Expression('rand()'));
			$command = $query->join('LEFT JOIN', 'app_events', 'app_events.id = app_amazing.event_id');
			$result = $query->createCommand()->queryAll();
			return $result ;
	   }
	   
	    public static function AmazingEventListing($limit,$offset,$daterange)
	   {
			$query = (new \yii\db\Query())->select(['app_amazing.event_id','app_events.*'])->from('app_amazing')
			->where(['app_events.status'=>'1'])->andWhere(['>=', 'event_end_date', $daterange])->offset($offset)->limit($limit);
			$command = $query->join('LEFT JOIN', 'app_events', 'app_events.id = app_amazing.event_id');
			$result = $query->createCommand()->queryAll();
			return $result ;
	   }
	   
	   public static function UserTotalNotification($id)
	   {
			return $result = Yii::$app->db->createCommand('SELECT total_notification FROM app_users WHERE id=:id')
					->bindValue(':id', $id)->queryOne();
			
	   }
	   
	   public static function UserInboxHome($id)
	   {
			$query = (new \yii\db\Query())->select(['app_sponsor_apply.*','app_brandmandates.logo'])->from('app_sponsor_apply')
			->where(['app_sponsor_apply.receiver_id'=>$id])->orderBy(['app_sponsor_apply.date' => SORT_DESC]);
			$command = $query->join('LEFT JOIN', 'app_brandmandates', 'app_brandmandates.id = app_sponsor_apply.brandmandates_id');
			$result = $query->createCommand()->queryAll();
			return $result ;	
	   }
	   
	   public static function Inboxlist($id)
	   {
			$query = (new \yii\db\Query())->select(['app_sponsor_apply.*','app_brandmandates.logo'])->from('app_sponsor_apply')
			->where(['app_sponsor_apply.receiver_id'=>$id])->orderBy(['app_sponsor_apply.date' => SORT_DESC]);
			$command = $query->join('LEFT JOIN', 'app_brandmandates', 'app_brandmandates.id = app_sponsor_apply.brandmandates_id');
			$result = $query->createCommand()->queryAll();
			return $result ;	
	   }
	   
	    public static function Sentlist($id)
	   {
			$query = (new \yii\db\Query())->select(['app_sponsor_apply_outbox.*','app_events.logo'])->from('app_sponsor_apply_outbox')
			->where(['app_sponsor_apply_outbox.sender_id'=>$id])->orderBy(['app_sponsor_apply_outbox.date' => SORT_DESC]);
			$command = $query->join('LEFT JOIN', 'app_events', 'app_events.id = app_sponsor_apply_outbox.event_id');
			$result = $query->createCommand()->queryAll();
			return $result ;	
	   }
	   
	   public static function SentDetailsByID($id)
	   {
			$query = (new \yii\db\Query())->select(['app_sponsor_apply_outbox.*','app_users.username'])->from('app_sponsor_apply_outbox')
			->where(['app_sponsor_apply_outbox.id'=>$id]);
			$command = $query->join('LEFT JOIN', 'app_users', 'app_users.id = app_sponsor_apply_outbox.receiver_id');
			$result = $query->createCommand()->queryAll();
			return $result ;	
	   }
	   
	    public static function ReceiveDetailsByID($id)
	   {
			$query = (new \yii\db\Query())->select(['app_sponsor_apply.*','app_users.username'])->from('app_sponsor_apply')
			->where(['app_sponsor_apply.id'=>$id]);
			$command = $query->join('LEFT JOIN', 'app_users', 'app_users.id = app_sponsor_apply.sender_id');
			$result = $query->createCommand()->queryAll();
			return $result ;	
	   }
	   
	    public static function lastMinutesDeal($limit,$offset,$daterange)
	   {
			$query = (new \yii\db\Query())->select(['app_lastminute.event_id','app_events.*'])->from('app_lastminute')
			->where(['>=', 'event_end_date', $daterange])->offset($offset)->limit($limit);
			$command = $query->join('LEFT JOIN', 'app_events', 'app_events.id = app_lastminute.event_id');
			$result = $query->createCommand()->queryAll();
			return $result ;
	   }
	   
	   public static function eventvewedByOtherBrand($industry)
	   {
			$query = (new \yii\db\Query())->select(['event.*'])
			->from('app_sponsor_profile as spon')->where(['spon.industry'=>$industry])->limit('3');
			$command = $query->join('LEFT JOIN', 'app_event_viewby_brand as vewed', 'vewed.userid = spon.user_id');
			$command = $query->join('LEFT JOIN', 'app_events as event', 'event.id = vewed.event_id');
			$result = $query->createCommand()->queryAll();
			return $result ;	
			
	   }
	   
	   public static function eventvewedByOtherBrandListing($industry)
	   {
			$query = (new \yii\db\Query())->select(['event.*'])
			->from('app_sponsor_profile as spon')->where(['spon.industry'=>$industry]);
			$command = $query->join('LEFT JOIN', 'app_event_viewby_brand as vewed', 'vewed.userid = spon.user_id');
			$command = $query->join('LEFT JOIN', 'app_events as event', 'event.id = vewed.event_id');
			$result = $query->createCommand()->queryAll();
			return $result ;	
			
	   }
	   
	   
	   /******** Brand dashboard boxes count list **********/
	   public static function TotalInboxlist($id)
	   {
			$existevent = Yii::$app->getDb()->createCommand("SELECT count(*) as total FROM app_sponsor_apply WHERE receiver_id='".$id."'")->queryone();
			return $existevent['total'];
	   }
	   
	   public static function TotalRecmendation($cat,$city,$daterange)
	   {
			$query=Events::find()->status(Events::STATUS_ON);
			if($cat){ $query->andWhere(['event_category_id'=>$cat]);}
			if($city){ $query->orWhere(['city_id'=>$city]);}
			$query->andWhere(['>=', 'event_end_date', $daterange]);
			 $countQuery = clone $query;
			 return $countQuery->count();
		}
		
	   public static function TotalTrending($daterange)
	   {
			$query = (new \yii\db\Query())->select(['count(*) as total'])->from('app_trending')
			->where(['app_events.status'=>'1'])->andWhere(['>=', 'event_end_date', $daterange]);
			$command = $query->join('LEFT JOIN', 'app_events', 'app_events.id = app_trending.event_id');
			$result = $query->createCommand()->queryAll();
			foreach($result as $item){  return $item['total'];}
			
	   }
	   
	   public static function TotalLastminutes($daterange)
	   {
			$query = (new \yii\db\Query())->select(['count(*) as total'])->from('app_lastminute')
			->where(['app_events.status'=>'1'])->andWhere(['>=', 'event_end_date', $daterange]);
			$command = $query->join('LEFT JOIN', 'app_events', 'app_events.id = app_lastminute.event_id');
			$result = $query->createCommand()->queryAll();
			foreach($result as $item){  return $item['total'];}
			
		}
		
	   public static function totalShortlist($user)
	   {
			$existevent = Yii::$app->getDb()->createCommand("SELECT count(*) as total FROM app_user_shortlist_event WHERE user_id='".$user."'")->queryone();
			return $existevent['total'];
	   }
	   
	   /******** end Brand dashboard boxes count list **********/
	   
    
}
