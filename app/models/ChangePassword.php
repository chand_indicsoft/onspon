<?php

namespace app\models;
use Yii;
use yii\base\Model;
use app\models\User;

/**
 * Login form
 */
class ChangePassword extends Model
{
    
	public $repeat_password;
	public $confirm_password;
	public $password;
	public $_user;
	
	
    public function rules()
    {
        return [
        	[['repeat_password', 'password','confirm_password'], 'required'],
            
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
            ['repeat_password', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ],
            
        ];
    }
	
	
	
	

    public function validatePassword($attribute, $params)
    {
        
            $user = $this->getUser();
            if (!$user->validatePassword($this->confirm_password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        
    }


	
   public function currentPassword()
   {
	   
		if ($this->validate()) {
		$session = Yii::$app->session;	
		$user = User::findOne($session['user_id']);
        $user->password_hash = Yii::$app->security->generatePasswordHash($this->password);
        return $user->save() ? $user : null;
	}
	else {
		return false;
		
		}
		
		
		 
	   
	}
     
   
     protected function getUser()
    {
		$session = Yii::$app->session;	
		$user = User::findById($session['user_id']);
		 return $user;
    }
    
    
}
