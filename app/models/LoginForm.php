<?php

namespace app\models;
use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $email;
    public $password;
	public $role;
    public $rememberMe = true;

    private $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        	[['email', 'password','role'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            ['email', 'email'],
           // ['role', 'safe'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
            
        ];
    }
	
	public function attributeLabels()
    {
        return [
            'email' => Yii::t('easyii', 'Email'),
            'role' => Yii::t('easyii', 'Who you are?'),
           
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
   
     
    public function login()
    {
    	
        if ($this->validate()) {
            
            $userdata = $this->getUser();

		if($userdata){
           
            if($userdata->status == 1){
                if($userdata->role == $this->role){
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
				}else{
					return 3;
				}		
            }else{
                return 2;
            }
	}else{
		return false;
	}
        } else {
            return false;
        }
    }
	
	

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
   /* protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByEmail($this->email);
        }
	  $session = Yii::$app->session;

          $session->set('user_id', $this->_user->id);


        return $this->_user;
    * 
    */ 
     protected function getUser()
    {
		if ($this->_user === null) {
			$this->_user = User::findByEmail($this->email);
        }
		if(isset($this->_user->status) && ($this->_user->status == 1) && ($this-> _user->role == $this->role)){
      		$session = Yii::$app->session;

          	$session->set('user_tmpid', $this->_user->id);
			$session->set('user_tmpid1', $this->_user->username);
			$session->set('role', $this->_user->role);  

		 return $this->_user;
		}
       
    }
    
    /*
    protected function getUser()
    {
     
       $this->_user = User::findByEmail($this->email);
		if ($this->_user === null) {
			$this->_user = User::findByUsername($this->email);
        }
		if(isset($this->_user->status) && ($this->_user->status == 1)){
      		$session = Yii::$app->session;

          	$session->set('user_tmpid', $this->_user->id);
			$session->set('user_tmpid1', $this->_user->username); 

		}
        return $this->_user;
    }
    
    */
}
