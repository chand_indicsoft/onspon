<?php
	namespace app\models;

	use Yii;

	/**
	 * This is the model class for table "app_facebook_event".
	 *
	 * @property integer $id
	 * @property string $eid
	 * @property string $name
	 * @property string $image
	 * @property string $image1
	 * @property string $start_time
	 * @property string $end_time
	 * @property string $location
	 * @property string $creator
	 * @property string $creator_name
	 * @property string $description
	 * @property string $cover_image
	 * @property string $is_date_only
	 * @property string $parent_group_id
	 * @property string $privacy
	 * @property string $ticket_uri
	 * @property string $timezone
	 * @property string $v_latitude
	 * @property string $v_longitude
	 * @property string $v_city
	 * @property string $v_state
	 * @property string $v_country
	 * @property string $v_id
	 * @property string $v_name
	 * @property string $v_street
	 * @property string $v_zip
	 * @property string $category
	 * @property string $event_cat_id
	 * @property integer $status
	 */
	class AppFacebookEvent extends \yii\db\ActiveRecord
	{
		/**
		 * @inheritdoc
		 */
		public static function tableName()
		{
			return 'app_facebook_event';
		}

		/**
		 * @inheritdoc
		 */
		public function rules()
		{
			return [
				[['description'], 'string'],
				[['status'], 'string', 'max' => 255],
				[['eid', 'name', 'image', 'start_time', 'end_time', 'location', 'creator', 'cover_image', 'is_date_only', 'parent_group_id', 'privacy', 'ticket_uri', 'timezone', 'v_city', 'v_state', 'v_country', 'event_cat_id'], 'string', 'max' => 100],
				[['image1'], 'string', 'max' => 5000],
				[['creator_name', 'v_id', 'v_name', 'v_street', 'v_zip', 'category'], 'string', 'max' => 200]
			];
		}

		/**
		 * @inheritdoc
		 */
		public function attributeLabels()
		{
			return [
				'id' => 'ID',
				'eid' => 'Eid',
				'name' => 'Name',
				'image' => 'Image',
				'image1' => 'Image1',
				'start_time' => 'Start Time',
				'end_time' => 'End Time',
				'location' => 'Location',
				'creator' => 'Creator',
				'creator_name' => 'Creator Name',
				'description' => 'Description',
				'cover_image' => 'Cover Image',
				'is_date_only' => 'Is Date Only',
				'parent_group_id' => 'Parent Group ID',
				'privacy' => 'Privacy',
				'ticket_uri' => 'Ticket Uri',
				'timezone' => 'Timezone',
				'v_latitude' => 'V Latitude',
				'v_longitude' => 'V Longitude',
				'v_city' => 'V City',
				'v_state' => 'V State',
				'v_country' => 'V Country',
				'v_id' => 'V ID',
				'v_name' => 'V Name',
				'v_street' => 'V Street',
				'v_zip' => 'V Zip',
				'category' => 'Category',
				'event_cat_id' => 'Event Cat ID',
				'status' => 'Status',
			];
		}
	}
