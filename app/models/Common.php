<?php

namespace app\models;
use Yii;
use yii\base\Model;

/**
 * Common
 */
class Common extends Model
{
	
	
	public function getEventCategoryImg($categoryid)
	{
		  $connection = Yii::$app->getDb();
        $command = $connection->createCommand("
        SELECT id,event_category_id,event_name,image FROM app_events where event_category_id='".$categoryid."' LIMIT 0,10 ");

   	return $result = $command->queryAll();
	}
	
	 public static function select_user_name($id)
	{
			$connection = Yii::$app->getDb();
			$command = $connection->createCommand('
	                           SELECT username FROM app_users Where id = "'.$id.'" ');
			$result = $command->queryScalar();
			return $result;
	}
	public function getEventtheme($catid)
	{
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand("SELECT themename FROM app_theme where category_id='".$catid."' ");
	 	$result = $command->queryone();
	 	//print_r($result);die;
			switch ($result['themename']) {
			case 'sports':
			  $themename="sportstheme";
			break;

			default:
			  $themename="index";
			}
      return  $themename;
   	    
	}//GETEVENTTHEME
	
	
	public function getFeaturedBrands()
	{
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand("SELECT featuredlogo FROM app_brand where IsFeatured=1");
	 	return $result = $command->queryAll();
	}
	
	public function getEventDetails($eventid)
	{
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand("SELECT * FROM app_events where id='".$eventid."'");
	 	return $result = $command->queryAll();
	
	}
	
	public function getEventCategorylist($catid)
	{
		$connection = Yii::$app->getDb();
		if($catid!=0)
			{
				$command = $connection->createCommand("SELECT * FROM app_events where event_category_id='".$catid."'");
			}
		else
			{
				$command = $connection->createCommand("SELECT * FROM app_events ");
			}
		return $result = $command->queryAll();
	}
	
	public function getCategoryById($catid)
	{
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand("SELECT title FROM app_eventcategories where eventcategories_id='".$catid."'");
		$result = $command->queryone();
		return $result['title'];
	}
	
	public function getBrandDetails($industryid)
	{
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand("SELECT * FROM app_brandmandates where industry_id='".$industryid."'");
	 	return $result = $command->queryAll();
	}
	
	public function findScribeByEmail($email)
	{
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand("SELECT email FROM easyii_subscribe_subscribers where email='".$email."'");
		$result = $command->queryone();
		return $result['email'];
	}
	
	public function UserShortlistfolder($userid)
	{
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand("SELECT foldername,id FROM app_user_folder where user_id='".$userid."'");
		return $result = $command->queryAll();
	}
	public function checkUserEventShorlist($eventid,$userid)
	{
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand("SELECT count(*) as countevent FROM app_user_shortlist_event where event_id='".$eventid."' AND user_id='".$userid."' ");
		$result = $command->queryone();
		return ($result['countevent']);
		//return $result['title'];
	}
		
	public function getTagname($tagid)
	{
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand("SELECT title FROM app_tagmanagement where tagmanagemnet_id='".$tagid."' ");
		$result = $command->queryone();
		return ($result['title']);
	}
	
	public function getCuratedEvents()
	{
		$connection = Yii::$app->getDb();
		$query=$connection->createCommand("SELECT * FROM app_boxgallery;;")->queryAll();
		return $query;
	}
	
	public function eventViewbyBrand($eventid,$userid,$role)
	{
		$connection = Yii::$app->getDb();
			$command = $connection->createCommand("SELECT count(*) as ecount FROM app_event_viewby_brand
															WHERE event_id='".$eventid."' 
															AND   userid='".$userid."' 
															AND usertype='".$role."' ");
		$result = $command->queryone();
		//echo $result['ecount'];die;
		if(!$result['ecount'])
		{		$date_created =  date('Y-m-d h:i:s');
		        $inserteventid=$connection->createCommand()
		->insert('app_event_viewby_brand', ['userid' => $userid,'event_id'=>$eventid,'usertype'=>$role,'date_created'=>$date_created])
		->execute();
		}
	}
	
	public function getIncome($income_id)
	{
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand("SELECT title FROM app_income where income_id='".$income_id."' ");
		$result = $command->queryone();
		return ($result['title']);
	}
	
	public function getEducation($education)
	{
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand("SELECT title FROM app_education where education_id='".$education."' ");
		$result = $command->queryone();
		return ($result['title']);
	}

	public function getAgegroup($agegroup)
	{
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand("SELECT title FROM app_agegroup where agegroup_id='".$agegroup."' ");
		$result = $command->queryone();
		return ($result['title']);
	}
	
	public function getObjective($_objective)
	{
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand("SELECT title FROM app_sponsorshipobjective where id='".$_objective."' ");
		$result = $command->queryone();
		return ($result['title']);
	}
	
	public static function trendingEventDashboard()
	{
		$query = (new \yii\db\Query())->select(['app_trending.event_id','app_events.*'])->from('app_trending')->limit('3');
		$command = $query->join('LEFT JOIN', 'app_events', 'app_events.id = app_trending.event_id');
		$result = $query->createCommand()->queryAll();
		return $result ;
   }
   
	public static function trendingEventListing()
	{
		$query = (new \yii\db\Query())->select(['app_trending.event_id','app_events.*'])->from('app_trending');
		$command = $query->join('LEFT JOIN', 'app_events', 'app_events.id = app_trending.event_id');
		$result = $query->createCommand()->queryAll();
		return $result ;
	}
}
