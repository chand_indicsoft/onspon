<?php
namespace app\models;
use Yii;
use yii\base\Model;use yii\helpers\Url;
use yii\db\ActiveRecord;
use yii\easyii\models\Setting;

class SubscribeForm extends ActiveRecord
	{
		public static function tableName()
			{
				return 'easyii_subscribe_subscribers';
			}

		public function rules()
			{
				return [
					['email', 'required'],
					['email', 'trim'],
					['email', 'email'],
					// ['email', 'unique'],
				   
				];
			}

		public function beforeSave($insert)
			{
				if (parent::beforeSave($insert))
					{
						if($insert)
							{
								$this->ip = Yii::$app->request->userIP;
								$this->time = time();
							}
						return true;
					}
				else
					{
						return false;
					}
			}

		public static function findByEmail($email)
			{
				return static::findOne(['email' => $email]);
			}

        public function sendEmail($email)
        {


                $subject="Thanks for subsribing with us";
                $text = "Thanks for subsribing with us
                <br><br>".
                "--------------------------------------------------------------------------------";

                $unsubscribeLink = '<br><a href="' . Url::to(['/admin/subscribe/send/unsubscribe', 'email' => $email], true) . '" target="_blank">Unsubscribe</a>';
                //echo $unsubscribeLink;die;
                $mailsend= Yii::$app->mailer->compose()
                   ->setFrom(Setting::get('robot_email'))
                   ->setTo($email)
                   ->setSubject($subject)
                   ->setHtmlBody($text.$unsubscribeLink)
                   ->setReplyTo(Setting::get('robot_email'))
                   ->send();

               return $mailsend = (($mailsend == 1) ? 'mailsend' : 'error');


        }
                
                
	}
?>
