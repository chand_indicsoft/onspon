<?php
namespace app\models;

use Yii;


use yii\helpers\StringHelper;
use yii\helpers\ArrayHelper;


class Organization extends \yii\easyii\components\ActiveRecord
{
    const STATUS_OFF = 0;
    const STATUS_ON = 1;
    public $type;
	

    public static function tableName()
    {
        return 'app_organization_detail';
    }

    public function rules()
    { 
        return [
            [['org_about'], 'trim'],
            [['established_date'], 'integer'],
            [['oraganization_name','org_location','fb_page','twitter_page','org_city','org_about'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            //'title' => Yii::t('easyii', 'Title'),
            'org_location' => Yii::t('easyii', 'Location'),
            'fb_page' => Yii::t('easyii', 'Facebook Page'),
            'twitter_page' => Yii::t('easyii', 'Twitter Page'),
            'org_city' => Yii::t('easyii', 'City'),
            'org_about' => Yii::t('easyii', 'About'),
            'established_date' => Yii::t('easyii', 'Establishment Date'),
            
           
        ];
    }

	
	public function orgExist($event_id,$org_id){
		
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand('
        SELECT * FROM app_event_organization_asoc Where event_id = '.$event_id.' AND organization_id ='.$org_id.' ');

   		return $result = $command->queryAll();
	
	}
	
    public function afterDelete()
    {
        parent::afterDelete();

        if($this->image){
            @unlink(Yii::getAlias('@webroot').$this->image);
        }

        foreach($this->getPhotos()->all() as $photo){
            $photo->delete();
        }
    }
     
	
}
