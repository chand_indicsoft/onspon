<?php
$this->title = Yii::t('easyii/agegroup', 'Create Age Group');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>