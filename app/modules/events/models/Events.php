<?php
namespace app\modules\events\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\easyii\behaviors\SeoBehavior;
use yii\easyii\behaviors\Taggable;
use yii\easyii\models\Photo;
use yii\helpers\StringHelper;
use yii\helpers\ArrayHelper;
use app\modules\tagmanagement\models\TagManagemnet;

class Events extends \yii\easyii\components\ActiveRecord
{
    const STATUS_OFF = 0;
    const STATUS_ON = 1;

    public static function tableName()
    {
        return 'app_events';
    }

    public function rules()
    { 
        return [
            [['event_name','user_id'], 'required'],
            [['description'], 'trim'],
            //['title', 'string', 'max' => 128],
            ['image', 'image'],
            ['banner', 'image'],
            ['logo', 'image'],
            [['views', 'status','user_id','event_strt_date','event_end_date','fb_likes','premium','user_type','incorporation_yr','event_category_id','ref_no','age_group_most','age_group_rest','education_most','education_rest','income_most','income_rest'], 'integer'],
            ['time', 'default', 'value' => time()],
            ['slug', 'match', 'pattern' => self::$SLUG_PATTERN, 'message' => Yii::t('easyii', 'Slug can contain only 0-9, a-z and "-" characters (max: 128).')],
            ['slug', 'default', 'value' => null],
            [['every_yr','televised','ticketed','completed'], 'boolean'],
            ['status', 'default', 'value' => self::STATUS_ON],
            [['tagNames','digital_outreach','in_premise_posters','verified','emailers','television','radio','ooh_media','print_media','website','why_sponsor','earlier_sponsor','pitch_pdf','no_of_attendees','ticket_text','tag_id','url','fb_page','city_id','country_id','state_id','date_created','date_updated','sprice_from','sprice_to','event_date','venue_name','address','lat','lng','event_strt_time','event_end_time','gender_most'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            //'title' => Yii::t('easyii', 'Title'),
            'user_id' => Yii::t('easyii/events', 'User'),
            'event_category_id' => Yii::t('easyii/events', 'Event Category'),
            'ref_no' => Yii::t('easyii/events', 'Reference Number'),
            'sprice_from' => Yii::t('easyii', 'Sponsership Price From'),
            'sprice_to' => Yii::t('easyii', 'Sponsership Price To'),
            'incorporation_yr' => Yii::t('easyii', 'Incorporation Year'),
            'city_id' => Yii::t('easyii', 'City'),
            'state_id' => Yii::t('easyii', 'State'),
            'country_id' => Yii::t('easyii', 'Country'),
            'lat' => Yii::t('easyii', 'Latitude'),
            'lng' => Yii::t('easyii', 'Longitude'),
            'event_strt_date' => Yii::t('easyii', 'Event Start Date'),
            'event_end_date' => Yii::t('easyii', 'Event End Date'),
            'event_strt_time' => Yii::t('easyii', 'Event Start Time'),
            'event_end_time' => Yii::t('easyii', 'Event End Time'),
            'fb_page' => Yii::t('easyii', 'Facebook Page'),
            'fb_likes' => Yii::t('easyii', 'Facebook Likes'),
            'no_of_attendees' => Yii::t('easyii', 'Number of Attendees'),
            'every_yr' => Yii::t('easyii', 'Does it happen every year?'),
            'televised' => Yii::t('easyii', 'Is it televised?'),
            'ticketed' => Yii::t('easyii', 'Is it ticketed?'),
            'earlier_sponsor' => Yii::t('easyii', 'Earlier Sponsor(s)?'),
            'image' => Yii::t('easyii', 'Image'),
            'time' => Yii::t('easyii', 'Date'),
            'slug' => Yii::t('easyii', 'Slug'),
            'tagNames' => Yii::t('easyii', 'Tags'),
            'why_sponsor' => Yii::t('easyii', 'Why a Sponsor should sponsor us?'),
			'print_media' => Yii::t('easyii', 'Audience reach through Print'),
            'ooh_media' => Yii::t('easyii', 'Audience reach through OOH (Out of Home Media)'),
            'radio' => Yii::t('easyii', 'Audience reach through Radio'),
            'television' => Yii::t('easyii', 'Audience reach through Television'),
            'emailers' => Yii::t('easyii', 'Audience reach through Emailers'),
            'in_premise_posters' => Yii::t('easyii', 'Audience reach through In Premise Posters'),
            'digital_outreach' => Yii::t('easyii', 'Audience reach through Digital Outreach'),
        ];
    }

    public function behaviors()
    {
        return [
            'seoBehavior' => SeoBehavior::className(),
            'taggabble' => Taggable::className(),
            'sluggable' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'event_name',
                'ensureUnique' => true
            ],
        ];
    }

    public function getPhotos()
    {
        return $this->hasMany(Photo::className(), ['item_id' => 'id'])->where(['class' => self::className()])->sort();
    }

public function getEventImg($eventid)
	{
		$photo=new Photo;
		return $data=$photo->getImageView('app\\modules\\events\\models\\Events',$eventid);
		
	}


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $settings = Yii::$app->getModule('admin')->activeModules['events']->settings;
            $this->short = StringHelper::truncate($settings['enableShort'] ? $this->short : strip_tags($this->text), $settings['shortMaxLength']);

            if(!$insert && $this->image != $this->oldAttributes['image'] && $this->oldAttributes['image']){
                @unlink(Yii::getAlias('@webroot').$this->oldAttributes['image']);
            }
            return true;
        } else {
            return false;
        }
    }
	
	
	public function getCategoryDropdown()
	{
        $listCategory   = TagManagemnet::find()->select('tagmanagemnet_id,title')
            ->where(['status' => '1'])
            //->andWhere(['status' => 'active','approved' => 'active'])
            ->all();
        $list   = ArrayHelper::map( $listCategory,'tagmanagemnet_id','title');

        return $list;
	}
	
	
	public static function selectautocomplete($name)
		{
			$connection = Yii::$app->getDb();
			$command = $connection->createCommand('
	        SELECT id,username FROM app_users Where username LIKE "%'.$name.'%" ');
			$result = $command->queryAll();
			return $result;
		}
	
	public static function selectvenueauto($name)
		{
			$connection = Yii::$app->getDb();
			$command = $connection->createCommand('
	        SELECT venue_id,title FROM app_venue Where title LIKE "%'.$name.'%" ');
			$result = $command->queryAll();
			return $result;
		}

    public function afterDelete()
    {
        parent::afterDelete();

        if($this->image){
            @unlink(Yii::getAlias('@webroot').$this->image);
        }

        foreach($this->getPhotos()->all() as $photo){
            $photo->delete();
        }
    }

	 public static function Updateeventtag($id,$tag)
		 {
			if($tag){
				$connection = Yii::$app->getDb();
				$connection->createCommand()
				->delete('app_event_tag', ['event_id' => $id])
				->execute();	
			
			$tag = explode(",",$tag);	
			$len = sizeof($tag);
			
			for($i = 0;$i<$len;$i++){
				$connection = Yii::$app->getDb();
				$connection->createCommand()
				->insert('app_event_tag', ['event_id' => $id,'tag_id' => $tag[$i]])
				->execute();
				}
			}
		}
		
		 public static function eventCompletionPercentage($eventid)
         {
                    
                        $model=new Events;
                        $event=Events::find()->andWhere(['id'=>$eventid])->all();
                        //var_dump ( $event->createCommand()->rawSql);		
                        //print_r($event[0]);
                        //print_r($model->getAttributes());
                        $totalfield=0;$nullfield=0;
                        foreach($model->getAttributes()as $key => $val )
                        {
                        // echo '<br>'.$event[0][$key];
                        if($event[0][$key]=="")
                        {
                        $nullfield++;
                        }
                        $totalfield++;
                        }
                    $total_completion_percentage=round( (($totalfield-$nullfield)/$totalfield)*100 );
                     return $total_completion_percentage;  
                    //echo "totalf".$totalfield.'null'.$nullfield.'%'.(round( (($totalfield-$nullfield)/$totalfield)*100 ));

         }
               
		
		
}
