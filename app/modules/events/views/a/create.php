<?php
$this->title = Yii::t('easyii/events', 'Create Events');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>