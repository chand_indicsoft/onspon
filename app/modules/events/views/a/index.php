<?php
use app\modules\events\models\Events;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('easyii/events', 'Events');

$module = $this->context->module->id;
$text=isset( $text)? $text: "";
$status=( @$_REQUEST['status']==1) ?@$_REQUEST['status'] : "";

$this->registerJs("
var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : \"body\";
$('input[name=\"date1\"], input[name=\"date1\"]').datepicker({
	format: 'mm/dd/yyyy',
	container: container,
	todayHighlight: true,
	autoclose: true,
})

var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : \"body\";
$('input[name=\"date\"], input[name=\"date\"]').datepicker({
	format: 'mm/dd/yyyy',
	container: container,
	todayHighlight: true,
	autoclose: true,
})

");


$model=new Events;
$event=Events::find()->andWhere(['id'=>2])->all();
//var_dump ( $event->createCommand()->rawSql);		
//print_r($event[0]);
//print_r($model->getAttributes());
$totalfield=0;$nullfield=0;
foreach($model->getAttributes()as $key => $val )
{
   // echo '<br>'.$event[0][$key];
     if($event[0][$key]=="")
     {
         $nullfield++;
     }
    $totalfield++;
}

//echo "totalf".$totalfield.'null'.$nullfield.'%'.(round( (($totalfield-$nullfield)/$totalfield)*100 ));
?>

<?= $this->render('_menu') ?>

<div id="searchadmin" class="">
	<?= Html::beginForm(Url::to(['/admin/events/a/search']), 'get', ['class' => 'form-inline','id'=>'asearchForm']) ?>
		<div class="input-group">
			<div class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span></div>
			<input class="form-control border-right" name="date" placeholder="From Date(mm/dd/yyyy)" type="text" 
				value="<?=(@$_REQUEST['date']!="")? @$_REQUEST['date'] : ""; ?>"/>
			<input type="hidden" name="search_param" value="name" />
			<div class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span></div>
			<input class="form-control" name="date1"  
					value="<?=(@$_REQUEST['date1']!="")? @$_REQUEST['date1'] : ""; ?>" placeholder=" To Date(mm/dd/yyyy)" type="text"/>
			<input type="hidden" name="search_param" value="name" />
		</div>

		<div class="input-group"> 
			<i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>

			<?= Html::textInput('event_name', $text, ['class' => '', 'placeholder' => 'Type Event Name']) ?>
<?php 
$radioval = ['verified' => 'Verified', 'premium' => 'Premium'];
if(@$_REQUEST['checkval']=='verified')
{
    $radiovalue='verified';

}else if(@$_REQUEST['checkval']=='premium'){
    $radiovalue='premium';
}
?>
     <?php  echo Html::radioList('checkval',@$radiovalue, $radioval, [
                                'class' => 'radio filter-radio-div',
                                'itemOptions' => ['class' => 'filter-radio'],
                                ]); ?> 	
 <?= Html::checkbox('status', $status, ['label' => 'Status']);?>

                
                </div>

		<div class="input-group search-last">
			<input type="submit" name="search" value="Go"/>
			<input type="button" onclick="window.location.href ='<?= Url::to(['/admin/events']) ; ?>';" value="Reset" />
		</div>
	<?= Html::endForm() ?>

	
</div>


    <table class="table table-hover">
        <thead>
            <tr>
                <?php if(IS_ROOT) : ?>
                    <th width="50">#</th>
                <?php endif; ?>
                <th><?= Yii::t('easyii', 'Event Name') ?></th>
                 <th width="100"><?= Yii::t('easyii', 'Event Completion') ?></th>
                <th width="90"><?= Yii::t('easyii', 'ContactNo.') ?></th>
                <th width="100"><?= Yii::t('easyii', 'Status') ?></th>
                <th width="100"></th>
            </tr>
        </thead>
        <tbody>
    <?php foreach($data->models as $item) : ?>
            <tr data-id="<?= $item->primaryKey ?>">
                <?php if(IS_ROOT) : ?>
                    <td><?= $item->primaryKey ?></td>
                <?php endif; ?>
                <td><a href="<?= Url::to(['/admin/'.$module.'/a/edit/', 'id' => $item->primaryKey]) ?>"><?= $item->event_name ?></a></td>
               
                 <td><?php
                 $modele=new Events;
                 echo $modele->eventCompletionPercentage($item->primaryKey).'%';
                  ?></td>
                <td><?= $item->contact ?></td>
                <td class="status">
                    <?= Html::checkbox('', $item->status == Events::STATUS_ON, [
                        'class' => 'switch',
                        'data-id' => $item->primaryKey,
                        'data-link' => Url::to(['/admin/'.$module.'/a']),
                    ]) ?>
                </td>
                <td>
                    <div class="btn-group btn-group-sm" role="group">
                        <a href="<?= Url::to(['/admin/'.$module.'/a/up', 'id' => $item->primaryKey]) ?>" class="btn btn-default move-up" event_name="<?= Yii::t('easyii', 'Move up') ?>"><span class="glyphicon glyphicon-arrow-up"></span></a>
                        <a href="<?= Url::to(['/admin/'.$module.'/a/down', 'id' => $item->primaryKey]) ?>" class="btn btn-default move-down" event_name="<?= Yii::t('easyii', 'Move down') ?>"><span class="glyphicon glyphicon-arrow-down"></span></a>
                        <a href="<?= Url::to(['/admin/'.$module.'/a/delete', 'id' => $item->primaryKey]) ?>" class="btn btn-default confirm-delete" event_name="<?= Yii::t('easyii', 'Delete item') ?>"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                </td>
            </tr>
    <?php endforeach; ?>
        </tbody>
    </table>
    <?= yii\widgets\LinkPager::widget([
        'pagination' => $data->pagination
    ]) ?>
