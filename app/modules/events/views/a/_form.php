<?php
use yii\easyii\widgets\DateTimePicker;
use yii\easyii\helpers\Image;
use yii\easyii\widgets\TagsInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\easyii\widgets\Redactor;
use yii\easyii\widgets\SeoForm;
use app\modules\eventcategories\api\EventCategories;
use app\modules\agegroup\api\AgeGroup;
use app\modules\education\api\Education;
use app\modules\income\api\Income;
use app\modules\city\api\City;
use app\modules\state\api\State;
use app\modules\country\api\Country;
use app\modules\role\api\Role;
use kartik\time\TimePicker;
use app\models\Commonhelper;
use app\modules\events\models\Events;

$module = $this->context->module->id;

if($model->tag_id){
	$model->tag_id = explode(",",$model->tag_id);
	
}
?>

<script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places" type="text/javascript"></script>
<script>
	function initialize() {
			  var input = document.getElementById('events-address');
			  var autocomplete = new google.maps.places.Autocomplete(input);
			  
			  google.maps.event.addListener(autocomplete, 'place_changed', function() {
				$( "#events-address" ).trigger( "click" );
				$('#edited').val('1');
						var data = $('#events-address').val();		
							geocoder = new google.maps.Geocoder();	
							var myarr = data.split(',');
							var len = myarr.length;
							
							geocoder.geocode( { 'address': data}, function(results, status) {	
							if (status == google.maps.GeocoderStatus.OK) {
								source=results[0].geometry.location.lat();
								lng=results[0].geometry.location.lng();	
								$('#events-city_id').val(myarr[len-3]);
								$('#events-country_id').val(myarr[len-1]);
								$('#events-state_id').val(myarr[len-2]);
								$('#events-lat').val(source);
								$('#events-lng').val(lng);
								
							}else
							{
								alert('Latitude and longitude not found for your input please try different(near by) location');
								$('#events-address').val('');		
							}
							});		
					});
			  
	  }
	  
	  google.maps.event.addDomListener(window, 'load', initialize);
</script>



<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
]); ?>

<?= $form->field($model, 'event_name') ?>

<label class="control-label" for="events-user_id">User</label>
<input type="text" name="userautocom" id="userautocom" class="form-control" autocomplete="off"
value="<?php if($model->user_id) { echo Commonhelper::select_user_name($model->user_id); } ?>"/>

<?= $form->field($model, 'user_id')->hiddenInput()->label(false); ?>
<div id="keywords" style="display:none;"></div>

<?= $form->field($model, 'user_type')->dropDownList(Role::roleName(), ['prompt'=>'Choose User Type']);  ?>

<?= $form->field($model, 'event_category_id')->dropDownList(EventCategories::eventcategoriesName(), ['prompt'=>'Choose Event Category']);  ?>

<?= $form->field($model, 'url')->hiddenInput()->label(''); ?>

<?= $form->field($model, 'description')->widget(Redactor::className(),[
    'options' => [
		'minHeight' => 200,
		'imageUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'events']),
		'fileUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'events']),
		'plugins' => ['fullscreen']
    ]
]) ?>

<?= $form->field($model, 'ref_no') ?>


<?php if($this->context->module->settings['enableThumb']) : ?>
    <?php if($model->logo) : ?>
		<?php if(Yii::$app->homeUrl){?>
        <img src="<?= Yii::$app->homeUrl.Image::thumb($model->logo, 240) ?>">
     <?php }else{ ?>
     	<img src="<?= Image::thumb($model->logo, 240) ?>">
     <?php } ?>
        <a href="<?= Url::to(['/admin/'.$module.'/a/clear-image', 'id' => $model->id,'key' =>'logo']) ?>" class="text-danger confirm-delete" title="<?= Yii::t('easyii', 'Clear Logo')?>"><?= Yii::t('easyii', 'Clear Logo')?></a>
    <?php endif; ?>
    <?= $form->field($model, 'logo')->fileInput() ?>
<?php endif; ?>

<?php if($this->context->module->settings['enableThumb']) : ?>
    <?php if($model->image) : ?>
		<?php if(Yii::$app->homeUrl){?>
        <img src="<?= Yii::$app->homeUrl.Image::thumb($model->image, 240) ?>">
     <?php }else{ ?>
     	<img src="<?= Image::thumb($model->image, 240) ?>">
     <?php } ?>
        <a href="<?= Url::to(['/admin/'.$module.'/a/clear-image', 'id' => $model->id,'key' =>'image']) ?>" class="text-danger confirm-delete" title="<?= Yii::t('easyii', 'Clear Image')?>"><?= Yii::t('easyii', 'Clear Image')?></a>
    <?php endif; ?>
    <?= $form->field($model, 'image')->fileInput() ?>
<?php endif; ?>

<?php if($this->context->module->settings['enableThumb']) : ?>
    <?php if($model->banner) : ?>
		<?php if(Yii::$app->homeUrl){?>
        <img src="<?= Yii::$app->homeUrl.Image::thumb($model->banner, 240) ?>">
     <?php }else{ ?>
     	<img src="<?= Image::thumb($model->banner, 240) ?>">
     <?php } ?>
        <a href="<?= Url::to(['/admin/'.$module.'/a/clear-image', 'id' => $model->id,'key' =>'banner']) ?>" class="text-danger confirm-delete" title="<?= Yii::t('easyii', 'Clear banner')?>"><?= Yii::t('easyii', 'Clear banner')?></a>
    <?php endif; ?>
    <?= $form->field($model, 'banner')->fileInput() ?>
<?php endif; ?>
<?= $form->field($model, 'event_strt_date')->widget(DateTimePicker::className(), ['options'=>['format'=>Yii::$app->params['dateFormat']]]); ?>

<?= $form->field($model, 'event_end_date')->widget(DateTimePicker::className(), ['options'=>['format'=>Yii::$app->params['dateFormat']]]); ?>

<?= $form->field($model, 'event_strt_time')->widget(TimePicker::className(),['pluginOptions' => [
        'showSeconds' => true
    ]]); ?>

<?= $form->field($model, 'event_end_time')->widget(TimePicker::className(),['pluginOptions' => [
        'showSeconds' => true
    ]]); ?>

<?= $form->field($model, 'sprice_from'); ?>

<?= $form->field($model, 'sprice_to'); ?>

<?= $form->field($model, 'no_of_attendees'); ?>

<?= $form->field($model, 'incorporation_yr'); ?>

<?= $form->field($model, 'tag_id')            
         ->dropDownList($model->CategoryDropdown,
         [
          'multiple'=>'multiple',
          'class'=>'chosen-select input-md required', 
          'size'=>'4',             
         ]             
        )->label("Add Tag"); ?>


<?php if($this->context->module->settings['enableTags']) : ?>
    <?= $form->field($model, 'tagNames')->widget(TagsInput::className()); ?>
<?php endif; ?>

<h3>Venue Details</h3>

<label class="control-label" for="events-venue_name">Venue</label>
<input type="text" name="venueautocom" id="venueautocom" class="form-control" autocomplete="off" 
value="<?php if($model->venue_name) { echo Events::selectvenue_by_id($model->venue_name); } ?>"/>
<?= $form->field($model, 'venue_name')->hiddenInput()->label(false); ?>
<div id="venue" style="display:none;"></div>

<?= $form->field($model, 'address'); ?>

<?= $form->field($model, 'city_id'); ?>

<?= $form->field($model, 'state_id'); ?>

<?= $form->field($model, 'country_id'); ?>

<?= $form->field($model, 'lat')->hiddenInput()->label(''); ?>

<?= $form->field($model, 'lng')->hiddenInput()->label(''); ?>

<h3>Further Details : More about your listing</h3>
<?= $form->field($model, 'every_yr')->checkbox(); ?>

<?= $form->field($model, 'televised')->checkbox(); ?>

<?= $form->field($model, 'ticketed')->checkbox(); ?>

<?= $form->field($model, 'ticket_type')->dropDownList(["Free"=>"Free Entry", "Invite"=>"Entry By Invite"], ['prompt'=>'Select..']); ?>

<?= $form->field($model, 'ticket_text'); ?>

<?= $form->field($model, 'website'); ?>

<?= $form->field($model, 'fb_page'); ?>

<?= $form->field($model, 'fb_likes'); ?>

<?= $form->field($model, 'pitch_pdf'); ?>

<?= $form->field($model, 'videos_link'); ?>

<?= $form->field($model, 'earlier_sponsor'); ?>

<?= $form->field($model, 'views') ?>

<h3>Audience Profile</h3>
<?= $form->field($model, 'age_group_most')->dropDownList(AgeGroup::ageName(), ['prompt'=>'Choose Age Group']); ?>

<?= $form->field($model, 'age_group_rest')->dropDownList(AgeGroup::ageName(), ['prompt'=>'Choose Age Group']); ?>

<?= $form->field($model, 'education_most')->dropDownList(Education::educationName(), ['prompt'=>'Choose Education']); ?>

<?= $form->field($model, 'education_rest')->dropDownList(Education::educationName(), ['prompt'=>'Choose Education']); ?>

<?= $form->field($model, 'income_most')->dropDownList(Income::incomeName(), ['prompt'=>'Choose Income Group']); ?>

<?= $form->field($model, 'income_rest')->dropDownList(Income::incomeName(), ['prompt'=>'Choose Income Group']); ?>

<?= $form->field($model, 'gender_most')->dropDownList(["Male"=>"Male", "Female"=>"Female"], ['prompt'=>'Choose Gender']);  ?>

<h3>Media Reach</h3>
<?= $form->field($model, 'print_media')->dropDownList(["All"=>"All", "0-10000"=>"0-10000", "10001-25000"=>"10001-25000", "25001-100000"=>"25001-100000", "100000+"=>"100000+"], ['prompt'=>'Choose']); ?>

<?= $form->field($model, 'ooh_media')->dropDownList(["All"=>"All", "0-10000"=>"0-10000", "10001-25000"=>"10001-25000", "25001-100000"=>"25001-100000", "100000+"=>"100000+"], ['prompt'=>'Choose']); ?>

<?= $form->field($model, 'radio')->dropDownList(["All"=>"All", "0-10000"=>"0-10000", "10001-25000"=>"10001-25000", "25001-100000"=>"25001-100000", "100000+"=>"100000+"], ['prompt'=>'Choose']); ?>

<?= $form->field($model, 'television')->dropDownList(["All"=>"All", "0-10000"=>"0-10000", "10001-25000"=>"10001-25000", "25001-100000"=>"25001-100000", "100000+"=>"100000+"], ['prompt'=>'Choose']); ?>

<?= $form->field($model, 'emailers')->dropDownList(["All"=>"All", "0-10000"=>"0-10000", "10001-25000"=>"10001-25000", "25001-100000"=>"25001-100000", "100000+"=>"100000+"], ['prompt'=>'Choose']); ?>

<?= $form->field($model, 'in_premise_posters')->dropDownList(["All"=>"All", "0-10000"=>"0-10000", "10001-25000"=>"10001-25000", "25001-100000"=>"25001-100000", "100000+"=>"100000+"], ['prompt'=>'Choose']); ?>

<?= $form->field($model, 'digital_outreach')->dropDownList(["All"=>"All", "0-10000"=>"0-10000", "10001-25000"=>"10001-25000", "25001-100000"=>"25001-100000", "100000+"=>"100000+"], ['prompt'=>'Choose']); ?>


<?= $form->field($model, 'premium')->checkbox(); ?>

<?= $form->field($model, 'completed')->checkbox(); ?>

<?= $form->field($model, 'why_sponsor')->textarea() ?>

<?php if($this->context->module->settings['enableShort']) : ?>
    <?= $form->field($model, 'short')->textarea() ?>
<?php endif; ?>

<?php if(IS_ROOT) : ?>
    <?= $form->field($model, 'slug') ?>
    <?= SeoForm::widget(['model' => $model]) ?>
<?php endif; ?>

<?= Html::submitButton(Yii::t('easyii', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>

<script>
$(document).ready(function()
	{
		$('#userautocom').keyup(function()
			{
				var key = $('#userautocom').val();
				$.get("<?= Url::to(['a/autocomplete']) ; ?>",{key:key})
				.done(function (data)
					{
						$('#keywords').html('');
						$('#keywords').show();
						var results = jQuery.parseJSON(data);
						
						$(results).each(function (key, value)
							{
								$('#keywords').append('<div class="item" id="' + value.id + '">' + value.value + '</div>');
							})
						$('#keywords .item').click(function ()
							{
								var text = $(this).text();
								var id = $(this).attr('id');
								$('#events-user_id').val(id);
								$('#userautocom').val(text);
								$('#keywords').hide();
							});
					});
			});
	});
</script>

<script>
$(document).ready(function()
	{
		$('#venueautocom').keyup(function()
			{
				var key = $('#venueautocom').val();
				$.get("<?= Url::to(['a/venueauto']) ; ?>",{key:key})
				.done(function (data)
					{
						$('#venue').html('');
						$('#venue').show();
						var results = jQuery.parseJSON(data);
						
						$(results).each(function (key, value)
							{
								$('#venue').append('<div class="item" id="' + value.id + '">' + value.value + '</div>');
							})
						$('#venue .item').click(function ()
							{
								var text = $(this).text();
								var id = $(this).attr('venue_id');
								$('#events-venue_name').val(id);
								$('#venueautocom').val(text);
								$('#venue').hide();
							});
					});
			});
	});
</script>
