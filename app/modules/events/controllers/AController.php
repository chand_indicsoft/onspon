<?php
namespace app\modules\events\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\easyii\behaviors\SortableDateController;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;
use yii\easyii\components\Controller;
use app\modules\events\models\Events;
use yii\easyii\helpers\Image;
use yii\easyii\behaviors\StatusController;
use app\models\Commonhelper;

class AController extends Controller
{
    public function behaviors()
    {
        return [
            [
                'class' => SortableDateController::className(),
                'model' => Events::className(),
            ],
            [
                'class' => StatusController::className(),
                'model' => Events::className()
            ]
        ];
    }

    public function actionIndex()
    {
       $data = new ActiveDataProvider([
            'query' => Events::find(),
        ]);

        return $this->render('index', [
            'data' => $data
        ]);
        
        
    }

    public function actionCreate()
    {
        $model = new Events;
        $model->date_created = date('Y-m-d h:i:s');
		
        if ($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else{
                if(isset($_FILES) && $this->module->settings['enableThumb']){
                    $model->image = UploadedFile::getInstance($model, 'image');
					$model->logo = UploadedFile::getInstance($model, 'logo');
					$model->banner = UploadedFile::getInstance($model, 'banner');
                    if($model->image && $model->validate(['image'])){
                        $model->image = Image::upload($model->image, 'events');
                    }
                    else{
                        $model->image = '';
                    }
					if($model->logo && $model->validate(['logo'])){
                        $model->logo = Image::upload($model->logo, 'events');
                    }
                    else{
                        $model->logo = '';
                    }
					
					
					if($model->banner && $model->validate(['banner']))
					{
                        $model->banner = Image::upload($model->banner, 'events');
                    }
                    else
					{
                        $model->banner = '';
                    }
                }
				
				$model->tag_id = $_POST['Events']['tag_id'] ? implode(",",$_POST['Events']['tag_id']):'';
				$model->url = 'http://' . Yii::$app -> getRequest() -> serverName.'/'.$_POST['Events']['slug'];
                if($model->save()){
					$update = Events::Updateeventtag($model->id,$model->tag_id);
                    $this->flash('success', Yii::t('easyii/events', 'Event created'));
                    return $this->redirect(['/admin/'.$this->module->id]);
                }
                else{
                    $this->flash('error', Yii::t('easyii', 'Create error. {0}', $model->formatErrors()));
                    return $this->refresh();
                }
            }
        }
        else {
            return $this->render('create', [
                'model' => $model
            ]);
        }
    }

    public function actionEdit($id)
    {
        $model = Events::findOne($id);
		$model->date_updated = date('Y-m-d h:i:s');
		
        if($model === null){
            $this->flash('error', Yii::t('easyii', 'Not found'));
            return $this->redirect(['/admin/'.$this->module->id]);
        }

        if ($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else{
                if(isset($_FILES) && $this->module->settings['enableThumb']){
                    $model->image = UploadedFile::getInstance($model, 'image');
					$model->logo = UploadedFile::getInstance($model, 'logo');
					$model->banner = UploadedFile::getInstance($model, 'banner');
                    if($model->image && $model->validate(['image'])){
                        $model->image = Image::upload($model->image, 'events');
                    }
                    else{
                        $model->image = $model->oldAttributes['image'];
                    }
					if($model->logo && $model->validate(['logo'])){
                        $model->logo = Image::upload($model->logo, 'events');
                    }
                    else{
                        $model->logo = $model->oldAttributes['logo'];
                    }
					
					if($model->banner && $model->validate(['banner'])){
                        $model->banner = Image::upload($model->banner, 'events');
                    }
                    else{
                        $model->banner = $model->oldAttributes['banner'];
                    }
                }
					
					$model->tag_id = $_POST['Events']['tag_id'] ? implode(",",$_POST['Events']['tag_id']):''; 
					$model->url = 'http://' . Yii::$app -> getRequest() -> serverName.'/'.$_POST['Events']['slug'];
					
                if($model->save()){
					$update = Events::Updateeventtag($model->id,$model->tag_id);
                    $this->flash('success', Yii::t('easyii/events', 'Event updated'));
                }
                else{
                    $this->flash('error', Yii::t('easyii', 'Update error. {0}', $model->formatErrors()));
                }
                return $this->refresh();
            }
        }
        else {
            return $this->render('edit', [
                'model' => $model
            ]);
        }
    }

    public function actionPhotos($id)
    {
        if(!($model = Events::findOne($id))){
            return $this->redirect(['/admin/'.$this->module->id]);
        }

        return $this->render('photos', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        if($id){ $connection = Yii::$app->getDb();
            $connection->createCommand()
				->delete('app_event_tag', ['event_id' => $id])
				->execute();
		} 
        
        if(($model = Events::findOne($id))){
            $model->delete();
        } else {
            $this->error = Yii::t('easyii', 'Not found');
        }
        return $this->formatResponse(Yii::t('easyii/events', 'Event deleted'));
    }

    public function actionClearImage($id,$key)
    {
        $model = Events::findOne($id);

        if($model === null){
            $this->flash('error', Yii::t('easyii', 'Not found'));
        }
        else{
		
		switch($key)
		{
			case 'image':
				$model->image = '';
				@unlink(Yii::getAlias('@webroot').$model->image);
			break;
			
			case 'logo':
				$model->logo = '';
				@unlink(Yii::getAlias('@webroot').$model->logo);
			break;
			
			case 'banner':
				$model->banner = '';
				@unlink(Yii::getAlias('@webroot').$model->banner);
			break;
		}
            
            if($model->update()){
               // @unlink(Yii::getAlias('@webroot').$model->image);
                $this->flash('success', Yii::t('easyii', 'Image cleared'));
            } else {
                $this->flash('error', Yii::t('easyii', 'Update error. {0}', $model->formatErrors()));
            }
        }
        return $this->back();
    }

    public function actionUp($id)
    {
        return $this->move($id, 'up');
    }

    public function actionDown($id)
    {
        return $this->move($id, 'down');
    }

    public function actionOn($id)
    {
        return $this->changeStatus($id, Events::STATUS_ON);
    }

    public function actionOff($id)
    {
        return $this->changeStatus($id, Events::STATUS_OFF);
    }
	
	public function actionAutocomplete()
    {
        $key= $_GET['key'];
		$data = Commonhelper::selectautocomplete($key);
		foreach ($data as $item)
			{
				$suggestresult[] = array(
										'value'=>$item['email'],
										'id'=>$item['id'],
										);
			}
		echo json_encode($suggestresult);
    }
	
	public function actionVenueauto()
    {
        $key= $_GET['key'];
		$data = Events::selectvenueauto($key);
		foreach ($data as $item)
			{
				$suggestresult[] = array(
										'value'=>$item['title'],
										'id'=>$item['venue_id'],
										);
			}
		echo json_encode($suggestresult);
    }
	public function actionSearch($text="")
    {
	$text = filter_var(Yii::$app->getRequest()->getQueryParam('event_name'), FILTER_SANITIZE_STRING);
	$query=Events::find();
         if(Yii::$app->getRequest()->getQueryParam('event_name')){
	      $query->Where('event_name LIKE "%' . $text . '%" ' );
         }
           if(Yii::$app->getRequest()->getQueryParam('status')==1){
	
               $query->andWhere(['status'=>1 ]);
         }
          if(Yii::$app->getRequest()->getQueryParam('checkval')=='verified'){
	
               $query->andWhere(['verified'=>1 ]);
         }
          if(Yii::$app->getRequest()->getQueryParam('checkval')=='premium'){
	
               $query->andWhere(['premium'=>1 ]);
         }
            if(Yii::$app->getRequest()->getQueryParam('date')){
		 $startdate=Yii::$app->getRequest()->getQueryParam('date');
		 $start_date = date("Y-m-d", strtotime($startdate))." 00:00:00";

	    }
	     if(Yii::$app->getRequest()->getQueryParam('date1')){
		 $enddate=Yii::$app->getRequest()->getQueryParam('date1');
		 $end_date = date("Y-m-d", strtotime($enddate))." 24:59:59";

	    }
	    if(Yii::$app->getRequest()->getQueryParam('date') && Yii::$app->getRequest()->getQueryParam('date1')   ){
		$to=strtotime( $start_date);
		$from=strtotime( $end_date);
		$query->andWhere(['between', "date_created", $start_date , $end_date ]) ;    
		}
            $query->orderBy('id DESC');
	     //  var_dump($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);
     
                $data = new ActiveDataProvider([
		'query' => $query,
		]);
        
                //print_r($data);
                
		return $this->render('index', [
		'text'=>$text,
		'data'=>$data,
		]);
	}
}
