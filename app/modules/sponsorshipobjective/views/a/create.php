<?php
$this->title = Yii::t('easyii/sponsorshipobjective', 'Create Sponsorship Objective');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>
