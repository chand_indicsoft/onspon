<?php
$this->title = Yii::t('easyii/eventgenre', 'Create Event Genre');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>