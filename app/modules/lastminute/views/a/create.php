<?php
$this->title = Yii::t('easyii/lastminute', 'Create Last Minute Deals');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>