<?php
use yii\easyii\widgets\DateTimePicker;
use yii\easyii\helpers\Image;
use yii\easyii\widgets\TagsInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\easyii\widgets\Redactor;
use yii\easyii\widgets\SeoForm;
use app\models\Commonhelper;


$module = $this->context->module->id;
?>
<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
]); ?>


<label class="control-label" for="events-user_id">Event Name</label>
<input type="text" name="event_name" id="event_name" class="form-control" autocomplete="off" value="
<?php if($model->event_id){echo Commonhelper::select_event_name($model->event_id);} ?>"/>
<?= $form->field($model, 'event_id')->hiddenInput()->label(false); ?>
<div id="keywords" style="display:none;"></div>


<?= $form->field($model, 'title')->hiddenInput()->label(false); ?>

<?= Html::submitButton(Yii::t('easyii', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>


<script>
$(document).ready(function()
	{
		$('#event_name').keyup(function()
			{
				var key = $('#event_name').val();
				$.get("<?= Url::to(['a/eventautocomplete']) ; ?>",{key:key})
				.done(function (data)
					{
						$('#keywords').html('');
						$('#keywords').show();
						var results = jQuery.parseJSON(data);
						
						$(results).each(function (key, value)
							{
								$('#keywords').append('<div class="item" id="' + value.id + '">' + value.value + '</div>');
								
							})
						$('#keywords .item').click(function ()
							{
								var text = $(this).text();
								var id = $(this).attr('id');
								$('#trending-event_id').val(id);
								$('#event_name').val(text);
								$('#trending-title').val(text);
								$('#keywords').hide();
							});
					});
			});
	});
</script>