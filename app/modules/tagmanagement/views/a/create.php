<?php
$this->title = Yii::t('easyii/tagmanagement', 'Create Tag');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>