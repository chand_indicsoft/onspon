<?php
$this->title = Yii::t('easyii/income', 'Create Income');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>