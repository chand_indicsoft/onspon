<?php
namespace app\modules\package\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\easyii\behaviors\SeoBehavior;
use yii\easyii\behaviors\Taggable;
use yii\easyii\models\Photo;
use yii\helpers\StringHelper;

class Package extends \yii\easyii\components\ActiveRecord
{
    const STATUS_OFF = 0;
    const STATUS_ON = 1;

    public static function tableName()
    {
        return 'app_package';
    }

    public function rules()
    {
        return [
            [['package_name'], 'required'],
            [['package_name'], 'trim'],
            ['package_name', 'string', 'max' => 128],
            [['status'], 'integer'],
            ['status', 'default', 'value' => self::STATUS_ON],
            [['basic','silver','gold','platinum','enterprise','category'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'package_name' => Yii::t('easyii', 'Package Name'),
         
        ];
    }

    

	public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $settings = Yii::$app->getModule('admin')->activeModules['package']->settings;
            
            return true;
        } else {
            return false;
        }
    }
	public function packageDetail(){
		
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand('
        SELECT * FROM app_package WHERE status = 1');

  	 return $result = $command->queryAll();
	
	}
	
}