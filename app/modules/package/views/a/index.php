<?php
use app\modules\package\models\Package;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('easyii/package', 'Package');

$module = $this->context->module->id;
?>

<?= $this->render('_menu') ?>

<?php if($data->count > 0) : ?>
    <table class="table table-hover">
        <thead>
            <tr>
                <?php if(IS_ROOT) : ?>
                    <th width="50">#</th>
                <?php endif; ?>
                <th><?= Yii::t('easyii', 'Package') ?></th>
                <th><?= Yii::t('easyii', 'Category') ?></th>
                <th><?= Yii::t('easyii', 'Basic') ?></th>
                <th><?= Yii::t('easyii', 'Silver') ?></th>
                <th><?= Yii::t('easyii', 'Gold') ?></th>
                <th><?= Yii::t('easyii', 'Platinum') ?></th>
                <th><?= Yii::t('easyii', 'Enterprise') ?></th>
                <th width="100"><?= Yii::t('easyii', 'Status') ?></th>
                <th width="120"></th>
            </tr>
        </thead>
        <tbody>
    <?php foreach($data->models as $item) : ?>
            <tr data-id="<?= $item->primaryKey ?>">
                <?php if(IS_ROOT) : ?>
                    <td><?= $item->primaryKey ?></td>
                <?php endif; ?>
                <td><a href="<?= Url::to(['/admin/'.$module.'/a/edit/', 'id' => $item->primaryKey]) ?>"><?= $item->package_name ?></a></td>
                <td><?= $item->category ?></td>
                <td><?= $item->basic ?></td>
                <td><?= $item->silver ?></td>
                <td><?= $item->gold ?></td>
                <td><?= $item->platinum ?></td>
                <td><?= $item->enterprise ?></td>
                <td class="status">
                    <?= Html::checkbox('', $item->status == Package::STATUS_ON, [
                        'class' => 'switch',
                        'data-id' => $item->primaryKey,
                        'data-link' => Url::to(['/admin/'.$module.'/a']),
                    ]) ?>
                </td>
                <td>
                    <div class="btn-group btn-group-sm" role="group">
                        <a href="<?= Url::to(['/admin/'.$module.'/a/delete', 'id' => $item->primaryKey]) ?>" class="btn btn-default confirm-delete" title="<?= Yii::t('easyii', 'Delete item') ?>"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                </td>
            </tr>
    <?php endforeach; ?>
        </tbody>
    </table>
    <?= yii\widgets\LinkPager::widget([
        'pagination' => $data->pagination
    ]) ?>
<?php else : ?>
    <p><?= Yii::t('easyii', 'No records found') ?></p>
<?php endif; ?>