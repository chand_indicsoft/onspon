<?php
$this->title = Yii::t('easyii/package', 'Create Package');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>