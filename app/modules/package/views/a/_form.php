<?php
use yii\easyii\widgets\DateTimePicker;
use yii\easyii\helpers\Image;
use yii\easyii\widgets\TagsInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\easyii\widgets\Redactor;
use yii\easyii\widgets\SeoForm;


$module = $this->context->module->id;
?>
<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
]); ?>


<?= $form->field($model, 'category')->dropDownList(["Default"=>"Default", "Sponsorship Support"=>"Sponsorship Support","Visibility to brands and audience"=>"Visibility to brands and audience"], ['prompt'=>'Select..']); ?>
<?= $form->field($model, 'package_name'); ?>
<?= $form->field($model, 'basic'); ?>
<?= $form->field($model, 'silver'); ?>
<?= $form->field($model, 'gold'); ?>
<?= $form->field($model, 'platinum'); ?>
<?= $form->field($model, 'enterprise'); ?>



<?= Html::submitButton(Yii::t('easyii', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>
