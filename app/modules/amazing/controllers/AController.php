<?php
namespace app\modules\amazing\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\easyii\behaviors\SortableDateController;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;
use app\models\Commonhelper;
use yii\easyii\components\Controller;
use app\modules\amazing\models\Amazing;
use yii\easyii\helpers\Image;
use yii\easyii\behaviors\StatusController;

class AController extends Controller
{
    public function behaviors()
    {
        return [
            [
                'class' => StatusController::className(),
                'model' => Amazing::className()
            ]
        ];
    }

    public function actionIndex()
    {
        $data = new ActiveDataProvider([
            'query' => Amazing::find(),
        ]);

        return $this->render('index', [
            'data' => $data
        ]);
    }

    public function actionCreate()
    {
        $model = new Amazing;
		$model->date_created = date('Y-m-d h:i:s');

        if ($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else{
                if($model->save()){
                    $this->flash('success', Yii::t('easyii/amazing', 'Amazing Event Created'));
                    return $this->redirect(['/admin/'.$this->module->id]);
                }
                else{
                    $this->flash('error', Yii::t('easyii', 'Create error. {0}', $model->formatErrors()));
                    return $this->refresh();
                }
            }
        }
        else {
            return $this->render('create', [
                'model' => $model
            ]);
        }
    }

    public function actionEdit($id)
    {
        $model = Amazing::findOne($id);
		$model->date_updated = date('Y-m-d h:i:s');

        if($model === null){
            $this->flash('error', Yii::t('easyii', 'Not found'));
            return $this->redirect(['/admin/'.$this->module->id]);
        }

        if ($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else{
               
                if($model->save()){
                    $this->flash('success', Yii::t('easyii/amazing', 'Amazing Event Updated'));
                }
                else{
                    $this->flash('error', Yii::t('easyii', 'Update error. {0}', $model->formatErrors()));
                }
                return $this->refresh();
            }
        }
        else {
            return $this->render('edit', [
                'model' => $model
            ]);
        }
    }

   
    public function actionDelete($id)
    {
        if(($model = Amazing::findOne($id))){
            $model->delete();
        } else {
            $this->error = Yii::t('easyii', 'Not found');
        }
        return $this->formatResponse(Yii::t('easyii/amazing', 'Amazing Event Deleted'));
    }

    
    public function actionUp($id)
    {
        return $this->move($id, 'up');
    }

    public function actionDown($id)
    {
        return $this->move($id, 'down');
    }

    public function actionOn($id)
    {
        return $this->changeStatus($id, Amazing::STATUS_ON);
    }

    public function actionOff($id)
    {
        return $this->changeStatus($id, Amazing::STATUS_OFF);
    }
	
	 public function actionEventautocomplete()
    {
        $key= $_GET['key'];
        
		$data = Commonhelper::selectEventautocomplete($key);
		foreach ($data as $item)
			{
				$suggestresult[] = array(
										'value'=>$item['event_name'],
										'id'=>$item['id'],
										);
			}
		echo json_encode($suggestresult);
    }
}