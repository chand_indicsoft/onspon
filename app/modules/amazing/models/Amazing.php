<?php
namespace app\modules\amazing\models;

use Yii;
use yii\easyii\behaviors\Taggable;
use yii\easyii\models\Photo;
use yii\helpers\StringHelper;

class Amazing extends \yii\easyii\components\ActiveRecord
{
    const STATUS_OFF = 0;
    const STATUS_ON = 1;

    public static function tableName()
    {
        return 'app_amazing';
    }

    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'trim'],
            ['title', 'string', 'max' => 128],
            [['status'], 'integer'],
            ['status', 'default', 'value' => self::STATUS_ON],
            [['date_updated','date_created','event_id'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'event_id' => Yii::t('easyii', 'Event Name'),
            
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $settings = Yii::$app->getModule('admin')->activeModules['amazing']->settings;
            
            return true;
        } else {
            return false;
        }
    }

    public function afterDelete()
    {
        parent::afterDelete();
    }
}