<?php
$this->title = Yii::t('easyii/amazing', 'Create Amazing Events');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>