<?php
$this->title = Yii::t('easyii/role', 'Create Role');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>