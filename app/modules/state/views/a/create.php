<?php
$this->title = Yii::t('easyii/state', 'Create State');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>