<?php
$this->title = Yii::t('easyii/country', 'Create Country');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>