<?php
namespace app\modules\brandmandates\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\easyii\behaviors\SortableDateController;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;

use yii\easyii\components\Controller;
use app\modules\brandmandates\models\BrandMandates;
use yii\easyii\helpers\Image;
use yii\easyii\behaviors\StatusController;
use app\models\Commonhelper;

class AController extends Controller
{
    public function behaviors()
    {
        return [
            
            [
                'class' => StatusController::className(),
                'model' => BrandMandates::className()
            ]
        ];
    }

    public function actionIndex()
    {
        $query=BrandMandates::find()->select('app_users.username,app_brandmandates.*');
       
        $query->leftJoin( 'app_users', 'app_users.id =app_brandmandates.user_id');

        $data = new ActiveDataProvider([
            'query' =>  $query,
        ]);

        return $this->render('index', [
            'data' => $data
        ]);
    }

    public function actionCreate()
    {
        $model = new BrandMandates;
        $model->date_created = date('Y-m-d h:i:s');
		

        if ($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else{
                if(isset($_FILES) && $this->module->settings['enableThumb']){
					$model->logo = UploadedFile::getInstance($model, 'logo');
					if($model->logo && $model->validate(['logo'])){
                        $model->logo = Image::upload($model->logo, 'brandmandates');
                    }
                    else{
                        $model->logo = '';
                    }
                }
					$model->user_type = '1';
					$model->event_category_id = $model->event_category_id ? implode(",",$model->event_category_id):'';
                	$model->specific_subjective = $model->specific_subjective ? implode(",",$model->specific_subjective):'';
                	
                	if($model->timeline == 'Open timeline' OR $model->timeline == '')
					{
						$model->start_date ="";
						$model->end_date ="";
					}
                	
                if($model->save()){
					
                    $update = BrandMandates::UpdateBrandmandates($model->id,$model->specific_subjective,$model->event_category_id);
                    $this->flash('success', Yii::t('easyii/brandmandates', 'Brand created'));
                    return $this->redirect(['/admin/'.$this->module->id]);
                }
                else{
                    $this->flash('error', Yii::t('easyii', 'Create error. {0}', $model->formatErrors()));
                    return $this->refresh();
                }
            }
        }
        else {
            return $this->render('create', [
                'model' => $model
            ]);
        }
    }

    public function actionEdit($id)
    {
        $model = BrandMandates::findOne($id);
		$model->date_updated = date('Y-m-d h:i:s');
		
        if($model === null){
            $this->flash('error', Yii::t('easyii', 'Not found'));
            return $this->redirect(['/admin/'.$this->module->id]);
        }

        if ($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else{
                if(isset($_FILES) && $this->module->settings['enableThumb']){
					$model->logo = UploadedFile::getInstance($model, 'logo');
                    
					if($model->logo && $model->validate(['logo'])){
                        $model->logo = Image::upload($model->logo, 'brandmandates');
                    }
                    else{
                        $model->logo = $model->oldAttributes['logo'];
                    }
                }
                	
                	$model->event_category_id = $model->event_category_id ? implode(",",$model->event_category_id):'';
                	$model->specific_subjective = $model->specific_subjective ? implode(",",$model->specific_subjective):'';
                	
                	if($model->timeline == 'Open timeline' OR $model->timeline == '')
					{
						$model->start_date ="";
						$model->end_date ="";
					}
                	
                if($model->save()){
					
					$update = BrandMandates::UpdateBrandmandates($model->id,$model->specific_subjective,$model->event_category_id);
                    $this->flash('success', Yii::t('easyii/brandmandates', 'Brand updated'));
                }
                else{
                    $this->flash('error', Yii::t('easyii', 'Update error. {0}', $model->formatErrors()));
                }
                return $this->refresh();
            }
        }
        else {
            return $this->render('edit', [
                'model' => $model
            ]);
        }
    }

    public function actionPhotos($id)
    {
        if(!($model = BrandMandates::findOne($id))){
            return $this->redirect(['/admin/'.$this->module->id]);
        }

        return $this->render('photos', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {

       if($id){ $connection = Yii::$app->getDb();
            $connection->createCommand()
				->delete('app_brand_category_specific', ['brand_mandates_id' => $id])
				->execute();
            
            $connection->createCommand()
				->delete('app_brand_sponsorship_objective', ['brand_mandates_id' => $id])
				->execute();
		} 
        if(($model = BrandMandates::findOne($id))){
            $model->delete();
          
        } else {
            $this->error = Yii::t('easyii', 'Not found');
        }
        return $this->formatResponse(Yii::t('easyii/brandmandates', 'Brand deleted'));
    }

    public function actionClearImage($id)
    {
        $model = BrandMandates::findOne($id);

        if($model === null){
            $this->flash('error', Yii::t('easyii', 'Not found'));
        }
        else{
            $model->logo = '';
            if($model->update()){
                @unlink(Yii::getAlias('@webroot').$model->logo);
                $this->flash('success', Yii::t('easyii', 'Image cleared'));
            } else {
                $this->flash('error', Yii::t('easyii', 'Update error. {0}', $model->formatErrors()));
            }
        }
        return $this->back();
    }

    public function actionUp($id)
    {
        return $this->move($id, 'up');
    }

    public function actionDown($id)
    {
        return $this->move($id, 'down');
    }

    public function actionOn($id)
    {
        return $this->changeStatus($id, BrandMandates::STATUS_ON);
    }

    public function actionOff($id)
    {
        return $this->changeStatus($id, BrandMandates::STATUS_OFF);
    }
	
	 public function actionAutocomplete()
    {
        $key= $_GET['key'];
        
		$data = Commonhelper::selectautocomplete($key);
		foreach ($data as $item)
			{
				$suggestresult[] = array(
										'value'=>$item['email'],
										'id'=>$item['id'],
										);
			}
		echo json_encode($suggestresult);
    }

public function actionSearch($text="")
    {
        
	$text = filter_var(Yii::$app->getRequest()->getQueryParam('text'), FILTER_SANITIZE_STRING);
	$query=BrandMandates::find()->select('app_users.username,app_brandmandates.*');
        
        if(Yii::$app->getRequest()->getQueryParam('text')){
	
                $query->andFilterWhere([
                'or',
                ['like', 'brand_name',trim($text)],
                ['like', 'app_users.username',trim($text)],
                ]);
         }
         
          if(Yii::$app->getRequest()->getQueryParam('status')==1){
	
               $query->andWhere(['status'=>1 ]);
         }
            if(Yii::$app->getRequest()->getQueryParam('date')){
		 $startdate=Yii::$app->getRequest()->getQueryParam('date');
		 $start_date = date("Y-m-d", strtotime($startdate))." 00:00:00";

	    }
	     if(Yii::$app->getRequest()->getQueryParam('date1')){
		 $enddate=Yii::$app->getRequest()->getQueryParam('date1');
		 $end_date = date("Y-m-d", strtotime($enddate))." 00:59:59";

	    }
	    if(Yii::$app->getRequest()->getQueryParam('date') && Yii::$app->getRequest()->getQueryParam('date1')   ){
			$query->andWhere(['between', "date_created", $start_date ,$end_date ]) ;    
            }
                
            $query->leftJoin( 'app_users', 'app_users.id =app_brandmandates.user_id');

            //$query->orderBy('id DESC');
	   // var_dump($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);
     
                $data = new ActiveDataProvider([
		'query' => $query,
		]);
        
               
		return $this->render('index', [
		'text'=>$text,
		'data'=>$data,
		]);
	}

	
}
