<style>
select#brandmandates-event_category_id {
    width: 100%;
}
select#brandmandates-specific_subjective {
    width: 100%;
}
</style>

<?php
use yii\easyii\widgets\DateTimePicker;
use yii\easyii\helpers\Image;
use yii\easyii\widgets\TagsInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\easyii\widgets\Redactor;
use yii\easyii\widgets\SeoForm;
use app\modules\eventcategories\api\EventCategories;
use app\modules\agegroup\api\AgeGroup;
use app\modules\education\api\Education;
use app\modules\income\api\Income;
use app\modules\role\api\Role;
use app\modules\sponsorshipobjective\api\SponsorshipObjective;
use app\modules\industry\api\Industry;
use app\modules\sponsorprice\api\SponsorPrice;
use app\models\Commonhelper;
use app\modules\city\api\City;
use kartik\time\TimePicker;

$module = $this->context->module->id;

if($model->event_category_id){
	$model->event_category_id = explode(",",$model->event_category_id);
	
}

if($model->specific_subjective){
	$model->specific_subjective = explode(",",$model->specific_subjective);
	
}

$this->registerJs('$(window).load(function() {
		$("#brandmandates-timeline label:eq(1)").click(function() {
			$("#specificrangedate").show(500);
		});
		$("#brandmandates-timeline label:eq(0)").click(function() {
			$("#specificrangedate").hide(500);
		});
		});');

?>


<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => false,
    'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
]); ?>

<?= $form->field($model, 'brand_name') ?>

<?= $form->field($model, 'visible_to_audience')->checkbox(); ?>

<label class="control-label" for="events-user_id">User</label>
<input type="text" name="userautocom" id="userautocom" class="form-control" autocomplete="off" value="
<?php if($model->user_id){echo Commonhelper::select_user_name($model->user_id);} ?>"/>
<?= $form->field($model, 'user_id')->hiddenInput()->label(false); ?>
<div id="keywords" style="display:none;"></div>


 <?= $form->field($model, 'event_category_id')->label("Any Specfic category of Avenue in your mind")            
	->checkboxList(EventCategories::eventcategoriesName(),['class'=>'chosen-select input-md required']); ?>



<?= $form->field($model, 'about_brand')->textarea() ?>

<?= $form->field($model, 'industry_id')->dropDownList(Industry::industryName(), ['prompt'=>'Choose Country']);  ?>

<?php if($this->context->module->settings['enableThumb']) : ?>
    <?php if($model->logo) : ?>
        <img src="<?= Yii::$app->homeUrl.Image::thumb($model->logo, 240) ?>">
        <a href="<?= Url::to(['/admin/'.$module.'/a/clear-image', 'id' => $model->id]) ?>" class="text-danger confirm-delete" title="<?= Yii::t('easyii', 'Clear image')?>"><?= Yii::t('easyii', 'Clear image')?></a>
    <?php endif; ?>
    <?= $form->field($model, 'logo')->fileInput() ?>
<?php endif; ?>

<?= $form->field($model, 'i_am_looking')->textarea() ?>

<?= $form->field($model, 'address'); ?>

<?= $form->field($model, 'city_id'); ?>

<?= $form->field($model, 'state_id'); ?>

<?= $form->field($model, 'country_id'); ?>

<?= $form->field($model, 'lat')->hiddenInput()->label(''); ?>

<?= $form->field($model, 'lng')->hiddenInput()->label(''); ?>


<?= $form->field($model, 'specific_subjective')->label("Partnership / Sponsorship Objective")            
	->checkboxList(SponsorshipObjective::objectiveName(),['class'=>'chosen-select input-md required']); ?>


<?= $form->field($model, 'budget_range')->dropDownList(SponsorPrice::sponsorpriceName(), ['prompt'=>'Choose Budget Range']);?>

<?= $form->field($model, 'timeline')->radioList(array('Open timeline'=>'Open timeline','specific duration'=>'specific duration')); ?>

<div id="specificrangedate" style="<?php if($model->timeline=='specific duration'){ echo "";} else { echo "display:none;";}?>">
		
				<?= $form->field($model, 'start_date')->widget(DateTimePicker::className(), ['options'=>['format'=>'MM/DD/YYYY']]); ?>
			
				<?= $form->field($model, 'end_date')->widget(DateTimePicker::className(), ['options'=>['format'=>'MM/DD/YYYY']]); ?>
			
</div>


<div class="page-title"><b>Audience Details</b></div></br/>

<?= $form->field($model, 'age_group')->dropDownList(AgeGroup::ageName(), ['prompt'=>'Choose Age']);  ?>

<?= $form->field($model, 'education')->dropDownList(Education::educationName(), ['prompt'=>'Choose cation']);  ?>

<?= $form->field($model, 'income')->dropDownList(Income::incomeName(), ['prompt'=>'Choose Income']);  ?>

<?= $form->field($model, 'gender')->dropDownList(["Male"=>"Male", "Female"=>"Female"], ['prompt'=>'Choose Gender']);  ?>

<div class="page-title"><b>Questions for events owner</b></div></br/>

<?= $form->field($model, 'question1') ?>

<?= $form->field($model, 'question2') ?>

<?= $form->field($model, 'visible_to_all_avenues')->checkbox(); ?>

<?php if(IS_ROOT) : ?>
    <?= $form->field($model, 'slug')->hiddenInput()->label(''); ?>
    <?= SeoForm::widget(['model' => $model]) ?>
<?php endif; ?>

<?= Html::submitButton(Yii::t('easyii', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>

<script>
$(document).ready(function()
	{
		$('#userautocom').keyup(function()
			{
				var key = $('#userautocom').val();
				$.get("<?= Url::to(['a/autocomplete']) ; ?>",{key:key})
				.done(function (data)
					{
						$('#keywords').html('');
						$('#keywords').show();
						var results = jQuery.parseJSON(data);
						
						$(results).each(function (key, value)
							{
								$('#keywords').append('<div class="item" id="' + value.id + '">' + value.value + '</div>');
								
							})
						$('#keywords .item').click(function ()
							{
								var text = $(this).text();
								var id = $(this).attr('id');
								$('#brandmandates-user_id').val(id);
								$('#userautocom').val(text);
								$('#keywords').hide();
							});
					});
			});
	});
</script>
