<?php
use app\modules\brandmandates\models\BrandMandates;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('easyii/brandmandates', 'Add Brands Mandates');

$module = $this->context->module->id;
$text=isset( $text)? $text: "";

$this->registerJs("
var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : \"body\";
$('input[name=\"date1\"], input[name=\"date1\"]').datepicker({
	format: 'mm/dd/yyyy',
	container: container,
	todayHighlight: true,
	autoclose: true,
})

var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : \"body\";
$('input[name=\"date\"], input[name=\"date\"]').datepicker({
	format: 'mm/dd/yyyy',
	container: container,
	todayHighlight: true,
	autoclose: true,
})

");
$date=(@$_REQUEST['date']!="")? @$_REQUEST['date'] : date('m/01/Y');
$date1=(@$_REQUEST['date1']!="")? @$_REQUEST['date1'] : date('m/d/Y');
$status=( @$_REQUEST['status']==1) ?@$_REQUEST['status'] : "";
$text=(@$_REQUEST['text']!="")? @$_REQUEST['text'] :"";


?>

<?= $this->render('_menu') ?>

<?= Html::beginForm(Url::to(['/admin/brandmandates/a/search/']),'get', ['class' => 'form-inline']) ?>
<div class="input-group">
<div class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span></div>
<input class="form-control border-right" name="date" placeholder="From Date(mm/dd/yyyy)" type="text" 
        value="<?=(@$_REQUEST['date']!="")? @$_REQUEST['date'] : date('m/01/Y'); ?>"/>
<input type="hidden" name="search_param" value="name" />
<div class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span></div>
<input class="form-control" name="date1"  
                value="<?=(@$_REQUEST['date1']!="")? @$_REQUEST['date1'] : date('m/d/Y'); ?>" placeholder=" To Date(mm/dd/yyyy)" type="text"/>
<input type="hidden" name="search_param" value="name" />
</div>    

<div class="form-group">
        <?= Html::textInput('text', $text, ['class' => 'form-control', 'placeholder' => 'Search by brandname or username']) ?>
    </div>
   <div class="input-group search-last">
       <input type="submit" name="search" value="Go"/>
       <input type="button" onclick="window.location.href ='<?= Url::to(['/admin/brandmandates']) ; ?>';" value="Reset" />
   </div>				
 <?= Html::endForm() ?>

<?php if($data->count > 0) : ?>
    <table class="table table-hover">
        <thead>
            <tr>
                <?php if(IS_ROOT) : ?>
                    <th width="50">#</th>
                <?php endif; ?>
                <th><?= Yii::t('easyii', 'Brand Name') ?></th>
                <th><?= Yii::t('easyii', 'User Name') ?></th>
                <th width="100"><?= Yii::t('easyii', 'Status') ?></th>
                <th width="120"></th>
            </tr>
        </thead>
        <tbody>
    <?php 
    //echo '<pre>';print_r($data);    echo '</pre>';
    foreach($data->models as $item) : ?>
            <tr data-id="<?= $item->primaryKey ?>">
                <?php if(IS_ROOT) : ?>
                    <td><?= $item->primaryKey ?></td>
                <?php endif; ?>
                <td><a href="<?= Url::to(['/admin/'.$module.'/a/edit/', 'id' => $item->primaryKey]) ?>">
                        <?= $item->brand_name ?></a></td>
                <td><?php echo $item->username;?></td>
                <td class="status">
                    <?= Html::checkbox('', $item->status == BrandMandates::STATUS_ON, [
                        'class' => 'switch',
                        'data-id' => $item->primaryKey,
                        'data-link' => Url::to(['/admin/'.$module.'/a']),
                    ]) ?>
                </td>
                <td>
                    <div class="btn-group btn-group-sm" role="group">
                            <a href="<?= Url::to(['/admin/'.$module.'/a/delete', 'id' => $item->primaryKey]) ?>" class="btn btn-default confirm-delete" event_name="<?= Yii::t('easyii', 'Delete item') ?>"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                </td>
            </tr>
    <?php endforeach; ?>
        </tbody>
    </table>
    <?= yii\widgets\LinkPager::widget([
        'pagination' => $data->pagination
    ]) ?>
<?php else : ?>
    <p><?= Yii::t('easyii', 'No records found') ?></p>
<?php endif; ?>
