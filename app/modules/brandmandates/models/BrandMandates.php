<?php
namespace app\modules\brandmandates\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\easyii\behaviors\SeoBehavior;
use yii\easyii\behaviors\Taggable;
use yii\easyii\models\Photo;
use yii\helpers\StringHelper;
use yii\helpers\ArrayHelper;
use app\modules\tagmanagement\models\TagManagemnet;

class BrandMandates extends \yii\easyii\components\ActiveRecord
{
    const STATUS_OFF = 0;
    const STATUS_ON = 1;
  
    public $username;
    
    public static function tableName()
    {
        return 'app_brandmandates';
    }

    public function rules()
    { 
        return [
            [['brand_name','industry_id','about_brand','i_am_looking','user_type','user_id'], 'required'],
            [['about_brand'], 'trim'],
            ['logo', 'image'],
            [['status','user_id','user_type'], 'integer'],
            
            ['slug', 'match', 'pattern' => self::$SLUG_PATTERN, 'message' => Yii::t('easyii', 'Slug can contain only 0-9, a-z and "-" characters (max: 128).')],
            ['slug', 'default', 'value' => null],
            ['status', 'default', 'value' => self::STATUS_ON],
            [['timeline','budget_range','address','visible_to_audience','start_date','end_date','visible_to_all_avenues','event_category_id','specific_subjective','question1','question2','age_group','education','income','gender','city_id','country_id','state_id','date_created','date_updated','lat','lng'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'brand_name' => Yii::t('easyii', 'Brand Name'),
            'user_id' => Yii::t('easyii/brandmandates', 'User'),
            'about_brand' => Yii::t('easyii/brandmandates', 'A little about your brand'),
            'i_am_looking' => Yii::t('easyii/brandmandates', 'I am looking to....'),
            'industry_id' => Yii::t('easyii', 'Select Industry'),
            'logo' => Yii::t('easyii', 'Brand Logo'),
            'event_category_id' => Yii::t('easyii', 'Any Specfic category of Avenue in your mind'),
            'country_id' => Yii::t('easyii', 'Country'),
            'state_id' => Yii::t('easyii', 'State'),
            'city_id' => Yii::t('easyii', 'City'),
            'specific_subjective' => Yii::t('easyii', 'Partnership / Sponsorship Objective'),
            'age_group' => Yii::t('easyii', 'Age Group'),
            'education' => Yii::t('easyii', 'Education'),
            'income' => Yii::t('easyii', 'Income'),
            'gender' => Yii::t('easyii', 'Gender'),
            'visible_to_all_avenues' => Yii::t('easyii', 'Click here for your brand mandates to be visible to all avenues fitting the description. '),
            'slug' => Yii::t('easyii', 'Slug'),
            'visible_to_audience' => Yii::t('easyii', 'Click here to hide it from audience'),
            'start_date' => Yii::t('easyii', 'Start Date'),
            'end_date' => Yii::t('easyii', 'End Date'),
            //'tagNames' => Yii::t('easyii', 'Tags'),
        ];
    }

    public function behaviors()
    {
        return [
            'seoBehavior' => SeoBehavior::className(),
            'taggabble' => Taggable::className(),
            'sluggable' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'brand_name',
                'ensureUnique' => true
            ],
        ];
    }

  /*  public function getPhotos()
    {
        return $this->hasMany(Photo::className(), ['item_id' => 'events_id'])->where(['class' => self::className()])->sort();
    }*/



    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $settings = Yii::$app->getModule('admin')->activeModules['brandmandates']->settings;
           
            return true;
        } else {
            return false;
        }
    }
	
	
	public function getCategoryDropdown()
	{
        $listCategory   = TagManagemnet::find()->select('tagmanagemnet_id,title')
            ->where(['status' => '1'])
            //->andWhere(['status' => 'active','approved' => 'active'])
            ->all();
        $list   = ArrayHelper::map( $listCategory,'tagmanagemnet_id','title');

        return $list;
	}

    public function afterDelete()
    {
        parent::afterDelete();

        if($this->logo){
            @unlink(Yii::getAlias('@webroot').$this->logo);
        }

      /*  foreach($this->getPhotos()->all() as $photo){
            $photo->delete();
        }*/
        
    }
    
   public static function UpdateBrandmandates($id,$objective,$category)
   {
			if($objective){
				$connection = Yii::$app->getDb();
				$connection->createCommand()
				->delete('app_brand_category_specific', ['brand_mandates_id' => $id])
				->execute();	
			
			$objective = explode(",",$objective);	
			$len = sizeof($objective);
			
			for($i = 0;$i<$len;$i++){
				$connection = Yii::$app->getDb();
				$connection->createCommand()
				->insert('app_brand_category_specific', ['brand_mandates_id' => $id,'category_id' => $objective[$i]])
				->execute();
				}
			}
			
			if($category){
			$connection = Yii::$app->getDb();
				$connection->createCommand()
				->delete('app_brand_sponsorship_objective', ['brand_mandates_id' => $id])
				->execute();	
			
			$category = explode(",",$category);	
			$len = sizeof($category);
			
			for($i = 0;$i<$len;$i++){
				$connection = Yii::$app->getDb();
				$connection->createCommand()
				->insert('app_brand_sponsorship_objective', ['brand_mandates_id' => $id,'sponsor_objective_id' => $category[$i]])
				->execute();
				}
			}
   }
    
    
}
