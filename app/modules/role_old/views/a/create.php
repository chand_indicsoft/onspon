<?php
$this->title = Yii::t('easyii/role', 'Create Income');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>