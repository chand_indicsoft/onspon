<?php
namespace app\modules\eventtestimonials\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\easyii\behaviors\SeoBehavior;
use yii\easyii\behaviors\Taggable;
use yii\easyii\models\Photo;
use yii\helpers\StringHelper;

class EventTestimonials extends \yii\easyii\components\ActiveRecord
{
    const STATUS_OFF = 0;
    const STATUS_ON = 1;

    public static function tableName()
    {
        return 'app_eventtestimonials';
    }

    public function rules()
    {
        return [
            [['text', 'title', 'designation', 'event'], 'required'],
            [['title',  'text'], 'trim'],
            ['title', 'string', 'max' => 128],
            ['image', 'image'],
            [['time', 'status'], 'integer'],
           // ['time', 'default', 'value' => time()],
            ['slug', 'match', 'pattern' => self::$SLUG_PATTERN, 'message' => Yii::t('easyii', 'Slug can contain only 0-9, a-z and "-" characters (max: 128).')],
            ['slug', 'default', 'value' => null],
            ['status', 'default', 'value' => self::STATUS_ON],
            ['video', 'safe']
           // ['tagNames', 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'event' => Yii::t('easyii', 'Event Name'),
            'title' => Yii::t('easyii', 'Name'),
            'designation' => Yii::t('easyii', 'Designation'),
            'text' => Yii::t('easyii', 'Message'),
         //   'short' => Yii::t('easyii/eventtestimonials', 'Short'),
            'image' => Yii::t('easyii', 'Image'),
         //   'time' => Yii::t('easyii', 'Date'),
            'slug' => Yii::t('easyii', 'Slug'),
        //    'tagNames' => Yii::t('easyii', 'Tags'),
        ];
    }

    public function behaviors()
    {
        return [
            'seoBehavior' => SeoBehavior::className(),
            'taggabble' => Taggable::className(),
            'sluggable' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'ensureUnique' => true
            ],
        ];
    }

    public function getPhotos()
    {
        return $this->hasMany(Photo::className(), ['item_id' => 'eventtestimonials_id'])->where(['class' => self::className()])->sort();
    }



    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $settings = Yii::$app->getModule('admin')->activeModules['eventtestimonials']->settings;
            
            if(!$insert && $this->image != $this->oldAttributes['image'] && $this->oldAttributes['image']){
                @unlink(Yii::getAlias('@webroot').$this->oldAttributes['image']);
            }
            return true;
        } else {
            return false;
        }
    }

    public function afterDelete()
    {
        parent::afterDelete();

        if($this->image){
            @unlink(Yii::getAlias('@webroot').$this->image);
        }

        foreach($this->getPhotos()->all() as $photo){
            $photo->delete();
        }
    }
}