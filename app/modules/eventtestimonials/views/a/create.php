<?php
$this->title = Yii::t('easyii/eventtestimonials', 'Create Event Testimonials');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>