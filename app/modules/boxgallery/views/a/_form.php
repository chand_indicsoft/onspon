<?php
use yii\easyii\widgets\DateTimePicker;
use yii\easyii\helpers\Image;
use yii\easyii\widgets\TagsInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\easyii\widgets\Redactor;
use yii\easyii\widgets\SeoForm;

$module = $this->context->module->id;
if($model->tag_id){
	$model->tag_id = explode(",",$model->tag_id);
}
?>
<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
]); ?>
<?= $form->field($model, 'title') ?>

<?php if($this->context->module->settings['enableThumb']) : ?>
    <?php if($model->image) : ?>
    	<?php if(Yii::$app->homeUrl){?>
        <img src="<?= Yii::$app->homeUrl.Image::thumb($model->image, 240) ?>">
     <?php }else{ ?>
     	<img src="<?= Image::thumb($model->image, 240) ?>">
     <?php } ?>	
        <a href="<?= Url::to(['/admin/'.$module.'/a/clear-image', 'id' => $model->id]) ?>" class="text-danger confirm-delete" title="<?= Yii::t('easyii', 'Clear Image')?>"><?= Yii::t('easyii', 'Clear Image')?></a>
    <?php endif; ?>
    <?= $form->field($model, 'image')->fileInput() ?>
<?php endif; ?>

<?php if($this->context->module->settings['enableThumb']) : ?>
    <?php if($model->thumb_image) : ?>
		<?php if(Yii::$app->homeUrl){?>
        <img src="<?= Yii::$app->homeUrl.Image::thumb($model->thumb_image, 240) ?>">
     <?php }else{ ?>
     	<img src="<?= Image::thumb($model->thumb_image, 240) ?>">
     <?php } ?>
        <a href="<?= Url::to(['/admin/'.$module.'/a/clear-image', 'id' => $model->id,'key' =>'thumb_image']) ?>" class="text-danger confirm-delete" title="<?= Yii::t('easyii', 'Clear Thumb Image')?>"><?= Yii::t('easyii', 'Clear Thumb Image')?></a>
    <?php endif; ?>
    <?= $form->field($model, 'thumb_image')->fileInput() ?>
<?php endif; ?>

<?php echo $form->field($model, 'boxname')->dropDownList(['box1' => 'Box 1', 'box2' => 'Box  2', 'box3' => 'Box 3','box4' => 'Box 4','box5' => 'Box 5'
,'box6' => 'Box 6', 'box7' => 'Box  7', 'box8' => 'Box 8']); ?>
<?= $form->field($model, 'tag_id')            
         ->dropDownList($model->CategoryDropdown,
         [
          'multiple'=>'multiple',
          'class'=>'chosen-select input-md required', 
          'size'=>'4',             
         ]             
        )->label("Add Tag"); ?>

<?= $form->field($model, 'activate_url')->checkbox() ?>

<?= $form->field($model, 'url') ?>

<?php if(IS_ROOT) : ?>
    <?= $form->field($model, 'slug') ?>
    <?= SeoForm::widget(['model' => $model]) ?>
<?php endif; ?>

<?= Html::submitButton(Yii::t('easyii', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>
