<?php
$this->title = Yii::t('easyii/boxgallery', 'Create Box Gallery');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>
