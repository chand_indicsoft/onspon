<?php
namespace app\modules\boxgallery\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\easyii\behaviors\SortableDateController;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;

use yii\easyii\components\Controller;
use app\modules\boxgallery\models\BoxGallery;
use yii\easyii\helpers\Image;
use yii\easyii\behaviors\StatusController;

class AController extends Controller
{
    public function behaviors()
    {
        return [
            
            [
                'class' => StatusController::className(),
                'model' => BoxGallery::className()
            ]
        ];
    }

    public function actionIndex()
    {
        $data = new ActiveDataProvider([
            'query' => BoxGallery::find(),
        ]);

        return $this->render('index', [
            'data' => $data
        ]);
    }

    public function actionCreate()
    {
        $model = new BoxGallery;
		$model->date_created = date('Y-m-d h:i:s');

        if ($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
             else{
                if(isset($_FILES) && $this->module->settings['enableThumb']){
                    $model->image = UploadedFile::getInstance($model, 'image');
					$model->thumb_image = UploadedFile::getInstance($model, 'thumb_image');
                    if($model->image && $model->validate(['image'])){
                        $model->image = Image::upload($model->image, 'boxgallery');
                    }
                    else{
                        $model->image = '';
                    }
					if($model->thumb_image && $model->validate(['thumb_image'])){
                        $model->thumb_image = Image::upload($model->thumb_image, 'boxgallery');
                    }
                    else{
                        $model->thumb_image = '';
                    }
                }
                
                $model->tag_id = $_POST['BoxGallery']['tag_id'] ? implode(",",$_POST['BoxGallery']['tag_id']):'';
                $boxname_exist= BoxGallery::Checkboxname($model->boxname);
                if($boxname_exist!=1)
                {
					if($model->save()){
						$update = BoxGallery::Updateboxtag($model->id,$model->tag_id);
					   
						$this->flash('success', Yii::t('easyii/boxgallery', 'BoxGallery created'));
						return $this->redirect(['/admin/'.$this->module->id]);
					}
					else{
						$this->flash('error', Yii::t('easyii', 'Create error. {0}', $model->formatErrors()));
						return $this->refresh();
					}
				}else{
					$this->flash('error', Yii::t('easyii', 'Box name already exist'));
					return $this->refresh();
				}
            }
        }
        else {
            return $this->render('create', [
                'model' => $model
            ]);
        }
    }

    public function actionEdit($id)
    {
        $model = BoxGallery::findOne($id);
		$model->date_updated = date('Y-m-d h:i:s');

        if($model === null){
            $this->flash('error', Yii::t('easyii', 'Not found'));
            return $this->redirect(['/admin/'.$this->module->id]);
        }

        if ($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else{
                if(isset($_FILES) && $this->module->settings['enableThumb']){
                    $model->image = UploadedFile::getInstance($model, 'image');
					$model->thumb_image = UploadedFile::getInstance($model, 'thumb_image');
                    if($model->image && $model->validate(['image'])){
                        $model->image = Image::upload($model->image, 'boxgallery');
                    }
                    else{
                        $model->image = $model->oldAttributes['image'];
                    }
					
					if($model->thumb_image && $model->validate(['thumb_image'])){
                        $model->thumb_image = Image::upload($model->thumb_image, 'boxgallery');
                    }
                    else{
                        $model->thumb_image = $model->oldAttributes['thumb_image'];
                    }
                }
               $model->tag_id = $_POST['BoxGallery']['tag_id'] ? implode(",",$_POST['BoxGallery']['tag_id']):'';
               
                if($model->save()){
					$update = BoxGallery::Updateboxtag($model->id,$model->tag_id);
					   
                    $this->flash('success', Yii::t('easyii/boxgallery', 'BoxGallery updated'));
                }
                else{
                    $this->flash('error', Yii::t('easyii', 'Update error. {0}', $model->formatErrors()));
                }
                return $this->refresh();
            }
        }
        else {
            return $this->render('edit', [
                'model' => $model
            ]);
        }
    }

    public function actionPhotos($id)
    {
        if(!($model = BoxGallery::findOne($id))){
            return $this->redirect(['/admin/'.$this->module->id]);
        }

        return $this->render('photos', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        if(($model = BoxGallery::findOne($id))){
            $model->delete();
        } else {
            $this->error = Yii::t('easyii', 'Not found');
        }
        return $this->formatResponse(Yii::t('easyii/boxgallery', 'Box gallery deleted'));
    }

    public function actionClearImage($id,$key)
    {
        $model = BoxGallery::findOne($id);

        if($model === null){
            $this->flash('error', Yii::t('easyii', 'Not found'));
        }
        else{

		switch($key)
		{
			case 'image':
				$model->image = '';
				@unlink(Yii::getAlias('@webroot').$model->image);
			break;
			
			case 'thumb_image':
				$model->thumb_image = '';
				@unlink(Yii::getAlias('@webroot').$model->thumb_image);
			break;
		}
            if($model->update()){
                @unlink(Yii::getAlias('@webroot').$model->image);
                $this->flash('success', Yii::t('easyii', 'Image cleared'));
            } else {
                $this->flash('error', Yii::t('easyii', 'Update error. {0}', $model->formatErrors()));
            }
        }
        return $this->back();
    }

    public function actionUp($id)
    {
        return $this->move($id, 'up');
    }

    public function actionDown($id)
    {
        return $this->move($id, 'down');
    }

    public function actionOn($id)
    {
        return $this->changeStatus($id, BoxGallery::STATUS_ON);
    }

    public function actionOff($id)
    {
        return $this->changeStatus($id, BoxGallery::STATUS_OFF);
    }
}
