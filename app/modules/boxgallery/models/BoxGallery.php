<?php
namespace app\modules\boxgallery\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\easyii\behaviors\SeoBehavior;
use yii\easyii\behaviors\Taggable;
use yii\easyii\models\Photo;
use yii\helpers\StringHelper;
use yii\helpers\ArrayHelper;
use app\modules\tagmanagement\models\TagManagemnet;
class BoxGallery extends \yii\easyii\components\ActiveRecord
{
    const STATUS_OFF = 0;
    const STATUS_ON = 1;

    public static function tableName()
    {
        return 'app_boxgallery';
    }

    public function rules()
    {
        return [
            [['title','boxname'], 'required'],
            [['title'], 'trim'],
            ['title', 'string', 'max' => 128],
            ['image', 'image'],
            ['thumb_image', 'image'],
            [['views','status'], 'integer'],
			[['activate_url'], 'boolean'],
            ['slug', 'match', 'pattern' => self::$SLUG_PATTERN, 'message' => Yii::t('easyii', 'Slug can contain only 0-9, a-z and "-" characters (max: 128).')],
            ['slug', 'default', 'value' => null],
            ['status', 'default', 'value' => self::STATUS_ON],
            [['date_updated','tagNames','date_created','url'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => Yii::t('easyii', 'Title'),
            'short' => Yii::t('easyii/boxgallery', 'Short'),
            'image' => Yii::t('easyii', 'Image'),
            'thumb_image' => Yii::t('easyii', 'Thumb Image'),
            'slug' => Yii::t('easyii', 'Slug'),
            'tagNames' => Yii::t('easyii', 'Tags'),
            'activate_url' => Yii::t('easyii', 'Activate URL'),
            'url' => Yii::t('easyii', 'URL'),
        ];
    }

    public function behaviors()
    {
        return [
            'seoBehavior' => SeoBehavior::className(),
            'taggabble' => Taggable::className(),
            'sluggable' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'ensureUnique' => true
            ],
        ];
    }

    public function getPhotos()
    {
        return $this->hasMany(Photo::className(), ['item_id' => 'id'])->where(['class' => self::className()])->sort();
    }

public function getCategoryDropdown()
	{
        $listCategory   = TagManagemnet::find()->select('tagmanagemnet_id,title')
            ->where(['status' => '1'])
            //->andWhere(['status' => 'active','approved' => 'active'])
            ->all();
        $list   = ArrayHelper::map( $listCategory,'tagmanagemnet_id','title');

        return $list;
	}

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $settings = Yii::$app->getModule('admin')->activeModules['boxgallery']->settings;
           // $this->short = StringHelper::truncate($settings['enableShort'] ? $this->short : strip_tags($this->text), $settings['shortMaxLength']);

            if(!$insert && $this->image != $this->oldAttributes['image'] && $this->oldAttributes['image']){
                @unlink(Yii::getAlias('@webroot').$this->oldAttributes['image']);
            }
            return true;
        } else {
            return false;
        }
    }

    public function afterDelete()
    {
        parent::afterDelete();

        if($this->image){
            @unlink(Yii::getAlias('@webroot').$this->image);
        }

        foreach($this->getPhotos()->all() as $photo){
            $photo->delete();
        }
    }
    
    public function Checkboxname($boxname)
    {
                $connection = Yii::$app->getDb();
				$boxnameexist=BoxGallery::find()->where( [ 'boxname' =>$boxname ] ) ->exists(); 
				return  $boxnameexist;
	}
    
    
    
    public static function Updateboxtag($id,$tag)
		 {
			if($tag){
				$connection = Yii::$app->getDb();
				$connection->createCommand()
				->delete('app_box_tag', ['boxid' => $id])
				->execute();	
			
			$tag = explode(",",$tag);	
			$len = sizeof($tag);
			
			for($i = 0;$i<$len;$i++){
				$connection = Yii::$app->getDb();
				$connection->createCommand()
				->insert('app_box_tag', ['boxid' => $id,'tag_id' => $tag[$i]])
				->execute();
				}
			}
		}
    
    
    
    
}
