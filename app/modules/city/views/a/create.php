<?php
$this->title = Yii::t('easyii/city', 'Create City');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>