<?php
$this->title = Yii::t('easyii/users', 'Create user');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>