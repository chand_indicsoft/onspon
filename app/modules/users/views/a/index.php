<?php
use app\modules\users\models\Users;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('easyii/users', 'Users');

$module = $this->context->module->id;
$text=isset( $text)? $text: "";
//echo $_REQUEST['checkval'];

$this->registerJs("
var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : \"body\";
$('input[name=\"date1\"], input[name=\"date1\"]').datepicker({
	format: 'mm/dd/yyyy',
	container: container,
	todayHighlight: true,
	autoclose: true,
})

var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : \"body\";
$('input[name=\"date\"], input[name=\"date\"]').datepicker({
	format: 'mm/dd/yyyy',
	container: container,
	todayHighlight: true,
	autoclose: true,
})

");

?>

<?= $this->render('_menu') ?>
<div id="searchadmin" class="">
	<?= Html::beginForm(Url::to(['/admin/users/a/search']), 'get', ['class' => 'form-inline','id'=>'asearchForm']) ?>
		<div class="input-group">
                <div class="input-group-addon"> 
                    <span class="glyphicon glyphicon-calendar"></span></div>
                <input class="form-control border-right" name="date" placeholder="From Date(mm/dd/yyyy)" type="text" 
                        value="<?=(@$_REQUEST['date']!="")? @$_REQUEST['date'] : date('m/01/Y'); ?>"/>
                <input type="hidden" name="search_param" value="name" />
                <div class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span></div>
                <input class="form-control" name="date1"  
                                value="<?=(@$_REQUEST['date1']!="")? @$_REQUEST['date1'] : date('m/d/Y'); ?>" placeholder=" To Date(mm/dd/yyyy)" type="text"/>
                </div>

		<div class="input-group"> 
			<i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>
			<?= Html::textInput('username', $text, ['class' => '', 'placeholder' => 'Type User Name']) ?>
<?php 
$radioval = ['2' => 'EventOrganiser', '1' => 'Sponsors'];
if(@$_REQUEST['checkval']==2)
{
    $radiovalue=2;

}else if(@$_REQUEST['checkval']==1){
    $radiovalue=1;
}
$status=( @$_REQUEST['status']==1) ?@$_REQUEST['status'] : "";

?>
     <?php  echo Html::radioList('checkval',@$radiovalue, $radioval, [
                                'class' => 'radio filter-radio-div',
                                'itemOptions' => ['class' => 'filter-radio'],
                                ]); ?> 	
                        
                        <?= Html::checkbox('status', $status, ['label' => 'Status']);?>


                
                </div>

		<div class="input-group search-last">
			<input type="submit" name="search" value="Go"/>
			<input type="button" onclick="window.location.href ='<?= Url::to(['/admin/users']) ; ?>';" value="Reset" />
		</div>
	<?= Html::endForm() ?>

	
</div>


<?php if($data->count > 0) : ?>
    <table class="table table-hover">
        <thead>
            <tr>
                <?php if(IS_ROOT) : ?>
                    <th width="50">#</th>
                <?php endif; ?>
                <th><?= Yii::t('easyii', 'Username') ?></th>
                <th><?= Yii::t('easyii', 'Email') ?></th>
                <th><?= Yii::t('easyii', 'Role') ?></th>
                <th width="100"><?= Yii::t('easyii', 'Status') ?></th>
                <th width="120"></th>
            </tr>
        </thead>
        <tbody>
    <?php foreach($data->models as $item) : ?>
            <tr data-id="<?= $item->primaryKey ?>">
                <?php if(IS_ROOT) : ?>
                    <td><?= $item->primaryKey ?></td>
                <?php endif; ?>
                <td><a href="<?= Url::to(['/admin/'.$module.'/a/edit/', 'id' => $item->primaryKey]) ?>"><?= $item->username ?></a></td>
                <td><?= $item->email ?></td>
                <td><?= $item['usertype']['title'] ?></td>
               
                <td class="status">
                    <?= Html::checkbox('', $item->status == Users::STATUS_ON, [
                        'class' => 'switch',
                        'data-id' => $item->primaryKey,
                        'data-link' => Url::to(['/admin/'.$module.'/a']),
                    ]) ?>
                </td>
                <td>
                    <div class="btn-group btn-group-sm" role="group">
                        <a href="<?= Url::to(['/admin/'.$module.'/a/delete', 'id' => $item->primaryKey]) ?>" class="btn btn-default confirm-delete" title="<?= Yii::t('easyii', 'Delete item') ?>"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                </td>
            </tr>
    <?php endforeach; ?>
        </tbody>
    </table>
    <?= yii\widgets\LinkPager::widget([
        'pagination' => $data->pagination
    ]) ?>
<?php else : ?>
    <p><?= Yii::t('easyii', 'No records found') ?></p>
<?php endif; ?>