<?php
use yii\easyii\widgets\DateTimePicker;
use yii\easyii\helpers\Image;
use yii\easyii\widgets\TagsInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\easyii\widgets\Redactor;
use yii\easyii\widgets\SeoForm;
use app\modules\role\api\Role;

$module = $this->context->module->id;
?>
<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
]); ?>

<?= $form->field($model, 'role')->dropDownList(Role::roleName(), ['prompt'=>'Choose User Type']);  ?>
<?= $form->field($model, 'username') ?>
<?= $form->field($model, 'contactnumber') ?>
<?= $form->field($model, 'email') ?>
<?= $form->field($model, 'password_hash')->passwordInput(['placeholder'=>'Password']) ?>







<?= Html::submitButton(Yii::t('easyii', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>
