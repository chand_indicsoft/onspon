<?php
namespace app\modules\users\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\easyii\behaviors\SeoBehavior;
use yii\easyii\behaviors\Taggable;
use yii\easyii\models\Photo;
use yii\helpers\StringHelper;
use app\modules\role\models\Role;

class Users extends \yii\easyii\components\ActiveRecord
{
    const STATUS_OFF = 0;
    const STATUS_ON = 1;

    public static function tableName()
    {
        return 'app_users';
    }

    public function rules()
    {
        return [
            [['password_hash', 'username','email','contactnumber','role'], 'required'],
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['email', 'string', 'max' => 255],
            ['email', 'email'],
            ['email', 'unique'],
            [['status'], 'integer'],
            ['slug', 'match', 'pattern' => self::$SLUG_PATTERN, 'message' => Yii::t('easyii', 'Slug can contain only 0-9, a-z and "-" characters (max: 128).')],
            ['slug', 'default', 'value' => null],
            ['status', 'default', 'value' => self::STATUS_ON],
        ];
    }

    public function attributeLabels()
    {
        return [
            'contactnumber' => Yii::t('easyii', 'Contact Number'),
            'password_hash' => Yii::t('easyii', 'Password'),
            
        ];
    }

   		public function setPassword($password)
    {
        return Yii::$app->security->generatePasswordHash($password);
    }
	
	public function generateAuthKey()
    {
        return Yii::$app->security->generateRandomString();
    }
   
		function generateUserName($l, $c = 'abcdefghijklmnopqrstuvwxyz1234567890') {

		for ($s = '', $cl = strlen($c) - 1, $i = 0; $i < $l; $s .= $c[mt_rand(0, $cl)], ++$i);

		return $s;
	}

	// generate password
	function randomPassword() {
		$password = "";
		$charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		for ($i = 0; $i < 8; $i++) {
			$random_int = mt_rand();
			$password .= $charset[$random_int % strlen($charset)];
		}
		return $password;

	}


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $settings = Yii::$app->getModule('admin')->activeModules['users']->settings;
            
            return true;
        } else {
            return false;
        }
    }
	
	public function getRole($roleid){
	
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand('SELECT * FROM app_role Where role_id ="'.$roleid.'" AND status = 1 ');

   		return $result = $command->queryOne();
	
	}
	
	public function getUsertype()
    {
        return $this->hasOne(Role::className(), ['role_id' => 'role']);
    }
	
    public function afterDelete()
    {
        parent::afterDelete();

       
    }
}