<?php
namespace app\modules\users\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;

use yii\easyii\components\Controller;
use app\modules\users\models\Users;
use yii\easyii\helpers\Image;
use yii\easyii\behaviors\StatusController;

class AController extends Controller
{
    public function behaviors()
    {
        return [
            
            [
                'class' => StatusController::className(),
                'model' => Users::className()
            ]
        ];
    }

    public function actionIndex()
    {
        $data = new ActiveDataProvider([
            'query' => Users::find()->with('usertype'),
        ]);

        return $this->render('index', [
            'data' => $data
        ]);
    }
	
	public function actionCreate()
    {
        $model = new Users;

        if ($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else{
            	
            	$password=$_POST['Users']['password_hash'];
				$model->password_hash = Users::setPassword($password);
				$model->auth_key = Users::generateAuthKey();
				
                
                if($model->save()){
                	$usertype = $model->getRole($model->role);
                	
					Yii::$app -> mailer -> compose(['html' => 'userCredential-html.php', 'text' => 'userCredential-text.php'], ['model' => $model,'password' => $password,'usertype'=>$usertype]) -> setFrom([\Yii::$app -> params['supportEmail'] => 'Onspon Support']) -> setTo($model -> email) -> setSubject('Onspon Login Detail') -> send();
					
					
                    $this->flash('success', Yii::t('easyii/users', 'User created'));
                    return $this->redirect(['/admin/'.$this->module->id]);
                }
                else{
                    $this->flash('error', Yii::t('easyii', 'Create error. {0}', $model->formatErrors()));
                    return $this->refresh();
                }
            }
        }
        else {
            return $this->render('create', [
                'model' => $model
            ]);
        }
    }

    public function actionEdit($id)
    {
        $model = Users::findOne($id);

        if($model === null){
            $this->flash('error', Yii::t('easyii', 'Not found'));
            return $this->redirect(['/admin/'.$this->module->id]);
        }

        if ($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else{
                $password=$_POST['Users']['password_hash'];
				$model->password_hash = Users::setPassword($password);
				$model->auth_key = Users::generateAuthKey();

                if($model->save()){
                    $this->flash('success', Yii::t('easyii/users', 'User updated'));
                }
                else{
                    $this->flash('error', Yii::t('easyii', 'Update error. {0}', $model->formatErrors()));
                }
                return $this->refresh();
            }
        }
        else {
            return $this->render('edit', [
                'model' => $model
            ]);
        }
    }
	
	/*
	public function actionCreate()
    {
        $model = new Users();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                	$this->flash('success', Yii::t('easyii/users', 'User created'));
					return $this->redirect(['/admin/'.$this->module->id]);
                }
            }
        }else{
        	return $this->render('create', [
                'model' => $model
            ]);
        }
    }
	
    
    public function actionEdit($id)
    {
        $model = Users::findOne($id);

        if($model === null){
            $this->flash('error', Yii::t('easyii', 'Not found'));
            return $this->redirect(['/admin/'.$this->module->id]);
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                	$this->flash('success', Yii::t('easyii/users', 'User created'));
					return $this->redirect(['/admin/'.$this->module->id]);
                }
            }
        }else{
        	return $this->render('edit', [
                'model' => $model
            ]);
        }
    }

*/
    public function actionDelete($id)
    {
        if(($model = Users::findOne($id))){
            $model->delete();
        } else {
            $this->error = Yii::t('easyii', 'Not found');
        }
        return $this->formatResponse(Yii::t('easyii/users', 'User deleted'));
    }

    public function actionUp($id)
    {
        return $this->move($id, 'up');
    }

    public function actionDown($id)
    {
        return $this->move($id, 'down');
    }

    public function actionOn($id)
    {
        return $this->changeStatus($id, Users::STATUS_ON);
    }

    public function actionOff($id)
    {
        return $this->changeStatus($id, Users::STATUS_OFF);
    }
    
    
    public function actionSearch($text="")
    {
        
	$text = filter_var(Yii::$app->getRequest()->getQueryParam('username'), FILTER_SANITIZE_STRING);
	$query=Users::find();
        if(Yii::$app->getRequest()->getQueryParam('username')){
	
             $query->Where('username LIKE "' . $text . '%" ' );
         }
         
          if(Yii::$app->getRequest()->getQueryParam('status')==1){
	
               $query->andWhere(['status'=>1 ]);
         }
        if(Yii::$app->getRequest()->getQueryParam('checkval')==1){
	
               $query->andWhere(['role'=>1 ]);
         }
          if(Yii::$app->getRequest()->getQueryParam('checkval')==2){
	
               $query->andWhere(['role'=>2 ]);
         }
            if(Yii::$app->getRequest()->getQueryParam('date')){
		 $startdate=Yii::$app->getRequest()->getQueryParam('date');
		 $start_date = date("Y-m-d", strtotime($startdate))." 00:00:00";

	    }
	     if(Yii::$app->getRequest()->getQueryParam('date1')){
		 $enddate=Yii::$app->getRequest()->getQueryParam('date1');
		 $end_date = date("Y-m-d", strtotime($enddate))." 24:59:59";

	    }
	    if(Yii::$app->getRequest()->getQueryParam('date') && Yii::$app->getRequest()->getQueryParam('date1')   ){
		$to=strtotime( $start_date);
		$from=strtotime( $end_date);
		$query->andWhere(['between', "created_at", $to ,$from ]) ;    
		}
            $query->orderBy('id DESC');
	     //  var_dump($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);
     
                $data = new ActiveDataProvider([
		'query' => $query,
		]);
        
                //print_r($data);
                
		return $this->render('index', [
		'text'=>$text,
		'data'=>$data,
		]);
	}

    
    
    
    
    
    
}