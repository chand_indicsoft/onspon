<?php
$this->title = Yii::t('easyii/industry', 'Create Sponsorship Objective');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>
