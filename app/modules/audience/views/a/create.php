<?php
$this->title = Yii::t('easyii/audience', 'Audience');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>
