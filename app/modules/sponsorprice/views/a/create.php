<?php
$this->title = Yii::t('easyii/sponsorprice', 'Create Sponsor Range');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>