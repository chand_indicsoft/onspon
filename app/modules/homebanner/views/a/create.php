<?php
$this->title = Yii::t('easyii/homebanner', 'Create Home Banner');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>