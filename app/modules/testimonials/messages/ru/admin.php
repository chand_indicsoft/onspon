<?php
return [
    'Testimonials' => 'Новости',
    'Create testimonials' => 'Создать новость',
    'Edit testimonials' => 'Редактировать новость',
    'Testimonials created' => 'Новость успешно создана',
    'Testimonials updated' => 'Новость обновлена',
    'Testimonials deleted' => 'Новость удалена',
    'Short' => 'Коротко',
];