<?php
$this->title = Yii::t('easyii/testimonials', 'Create testimonials');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>