<?php
$this->title = Yii::t('easyii/education', 'Create Education');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>