<?php
use yii\easyii\widgets\DateTimePicker;
use yii\easyii\helpers\Image;
use yii\easyii\widgets\TagsInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\easyii\widgets\Redactor;
use yii\easyii\widgets\SeoForm;
use app\modules\country\api\Country;
use app\modules\state\api\State;
use app\modules\city\api\City;

$module = $this->context->module->id;
?>
<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
]); ?>
<?= $form->field($model, 'title') ?>
<?= $form->field($model, 'address') ?>
<?= $form->field($model, 'country_id')->dropDownList(Country::countryName(), ['prompt'=>'Choose Country']); ?>
<?= $form->field($model, 'state_id')->dropDownList(State::stateName(), ['prompt'=>'Choose State']); ?>
<?= $form->field($model, 'city_id')->dropDownList(City::cityName(), ['prompt'=>'Choose City']); ?>

<?php if(IS_ROOT) : ?>
    <?= $form->field($model, 'slug') ?>
    <?= SeoForm::widget(['model' => $model]) ?>
<?php endif; ?>

<?= Html::submitButton(Yii::t('easyii', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>
