<?php
$this->title = Yii::t('easyii/venue', 'Create Venue');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>