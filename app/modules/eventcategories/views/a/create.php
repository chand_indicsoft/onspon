<?php
$this->title = Yii::t('easyii/eventcategories', 'Create Event Categories');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>