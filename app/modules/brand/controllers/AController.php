<?php
namespace app\modules\brand\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\easyii\behaviors\SortableDateController;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;
use yii\easyii\components\Controller;
use app\modules\brand\models\Brand;
use yii\easyii\helpers\Image;
use yii\easyii\behaviors\StatusController;

class AController extends Controller
{
    public function behaviors()
    {
        return [
            [
                'class' => StatusController::className(),
                'model' => Brand::className()
            ]
        ];
    }

    public function actionIndex()
    {
        $data = new ActiveDataProvider([
            'query' => Brand::find(),
        ]);

        return $this->render('index', [
            'data' => $data
        ]);
    }

    public function actionCreate()
    {
        $model = new Brand;
		$model->date_created = date('Y-m-d h:i:s');

        if ($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else{
                if(isset($_FILES) && $this->module->settings['enableThumb']){
                    $model->image = UploadedFile::getInstance($model, 'image');
					$model->featuredlogo = UploadedFile::getInstance($model, 'featuredlogo');
                    if($model->image && $model->validate(['image'])){
                        $model->image = Image::upload($model->image, 'brand');
                    }
                    else{
                        $model->image = '';
                    }
					
					
					if($model->featuredlogo && $model->validate(['featuredlogo']))
					{
                        $model->featuredlogo = Image::upload($model->featuredlogo, 'brand');
                    }
                    else
					{
                        $model->featuredlogo = '';
                    }
					
                }
                if($model->save()){
                    $this->flash('success', Yii::t('easyii/brand', 'Brand created'));
                    return $this->redirect(['/admin/'.$this->module->id]);
                }
                else{
                    $this->flash('error', Yii::t('easyii', 'Create error. {0}', $model->formatErrors()));
                    return $this->refresh();
                }
            }
        }
        else {
            return $this->render('create', [
                'model' => $model
            ]);
        }
    }

    public function actionEdit($id)
    {
        $model = Brand::findOne($id);
		$model->date_updated = date('Y-m-d h:i:s');

        if($model === null){
            $this->flash('error', Yii::t('easyii', 'Not found'));
            return $this->redirect(['/admin/'.$this->module->id]);
        }

        if ($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else{
                if(isset($_FILES) && $this->module->settings['enableThumb']){
                    $model->image = UploadedFile::getInstance($model, 'image');
					$model->featuredlogo = UploadedFile::getInstance($model, 'featuredlogo');
                    if($model->image && $model->validate(['image'])){
                        $model->image = Image::upload($model->image, 'brand');
                    }
                    else{
                        $model->image = $model->oldAttributes['image'];
                    }
					
					if($model->featuredlogo && $model->validate(['featuredlogo'])){
                        $model->featuredlogo = Image::upload($model->featuredlogo, 'brand');
                    }
                    else{
                        $model->featuredlogo = $model->oldAttributes['featuredlogo'];
                    }
                }

                if($model->save()){
                    $this->flash('success', Yii::t('easyii/brand', 'Brand updated'));
                }
                else{
                    $this->flash('error', Yii::t('easyii', 'Update error. {0}', $model->formatErrors()));
                }
                return $this->refresh();
            }
        }
        else {
            return $this->render('edit', [
                'model' => $model
            ]);
        }
    }

    public function actionPhotos($id)
    {
        if(!($model = Brand::findOne($id))){
            return $this->redirect(['/admin/'.$this->module->id]);
        }

        return $this->render('photos', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        if(($model = Brand::findOne($id))){
            $model->delete();
        } else {
            $this->error = Yii::t('easyii', 'Not found');
        }
        return $this->formatResponse(Yii::t('easyii/brand', 'Brand deleted'));
    }

    public function actionClearImage($id,$key)
    {
        $model = Brand::findOne($id);

        if($model === null){
            $this->flash('error', Yii::t('easyii', 'Not found'));
        }
        else{
		
		switch($key)
		{
			case 'image':
				$model->image = '';
				@unlink(Yii::getAlias('@webroot').$model->image);
			break;

			case 'featuredlogo':
				$model->featuredlogo = '';
				@unlink(Yii::getAlias('@webroot').$model->featuredlogo);
			break;
		}
            if($model->update()){
               // @unlink(Yii::getAlias('@webroot').$model->image);
                $this->flash('success', Yii::t('easyii', 'Image cleared'));
            } else {
                $this->flash('error', Yii::t('easyii', 'Update error. {0}', $model->formatErrors()));
            }
        }
        return $this->back();
    }

    public function actionUp($id)
    {
        return $this->move($id, 'up');
    }

    public function actionDown($id)
    {
        return $this->move($id, 'down');
    }

    public function actionOn($id)
    {
        return $this->changeStatus($id, Brand::STATUS_ON);
    }

    public function actionOff($id)
    {
        return $this->changeStatus($id, Brand::STATUS_OFF);
    }
}