<?php
use yii\easyii\widgets\DateTimePicker;
use yii\easyii\helpers\Image;
use yii\easyii\widgets\TagsInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\easyii\widgets\Redactor;
use yii\easyii\widgets\SeoForm;
use app\modules\brandcategories\api\BrandCategories;

$module = $this->context->module->id;
?>
<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
]); ?>

<?= $form->field($model, 'brandcategories_id')->dropDownList(BrandCategories::brandcategoriesName(), ['prompt'=>'Choose Brand Category']); ?>

<?= $form->field($model, 'title') ?>

<?php if($this->context->module->settings['enableThumb']) : ?>
    <?php if($model->image) : ?>
    	<?php if(Yii::$app->homeUrl){?>
        <img src="<?= Yii::$app->homeUrl.Image::thumb($model->image, 240) ?>">
     <?php }else{ ?>
     	<img src="<?= Image::thumb($model->image, 240) ?>">
     <?php } ?>
        <a href="<?= Url::to(['/admin/'.$module.'/a/clear-image', 'id' => $model->brand_id, 'key'=>'image']) ?>" class="text-danger confirm-delete" title="<?= Yii::t('easyii', 'Clear image')?>"><?= Yii::t('easyii', 'Clear image')?></a>
    <?php endif; ?>
    <?= $form->field($model, 'image')->fileInput() ?>
<?php endif; ?>

<?= $form->field($model, 'description')->widget(Redactor::className(),[
    'options' => [
		'minHeight' => 200,
		'imageUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'events']),
		'fileUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'events']),
		'plugins' => ['fullscreen']
    ]
]) ?>

<?= $form->field($model, 'IsFeatured')->checkbox(); ?>



<?php if($this->context->module->settings['enableThumb']) : ?>
    <?php if($model->featuredlogo) : ?>
		<?php if(Yii::$app->homeUrl){?>
        <img src="<?= Yii::$app->homeUrl.Image::thumb($model->featuredlogo, 240) ?>">
     <?php }else{ ?>
     	<img src="<?= Image::thumb($model->featuredlogo, 240) ?>">
     <?php } ?>
         <a href="<?= Url::to(['/admin/'.$module.'/a/clear-image', 'id' => $model->brand_id,'key' =>'featuredlogo']) ?>" class="text-danger confirm-delete" title="<?= Yii::t('easyii', 'Clear Featured Logo')?>"><?= Yii::t('easyii', 'Clear Featured Logo')?></a>
    <?php endif; ?>
    <?= $form->field($model, 'featuredlogo')->fileInput() ?>
<?php endif; ?>

<?php if(IS_ROOT) : ?>
    <?= $form->field($model, 'slug') ?>
    <?= SeoForm::widget(['model' => $model]) ?>
<?php endif; ?>

<?= Html::submitButton(Yii::t('easyii', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>
