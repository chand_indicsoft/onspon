<?php
$this->title = Yii::t('easyii/brand', 'Create Brand');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>