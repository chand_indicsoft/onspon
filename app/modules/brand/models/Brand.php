<?php
namespace app\modules\brand\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\easyii\behaviors\SeoBehavior;
use yii\easyii\behaviors\Taggable;
use yii\easyii\models\Photo;
use yii\helpers\StringHelper;

class Brand extends \yii\easyii\components\ActiveRecord
	{
		const STATUS_OFF = 0;
		const STATUS_ON = 1;

		public static function tableName()
			{
				return 'app_brand';
			}

		public function rules()
			{
				return [
					[['title'], 'required'],
					[['title','description'], 'trim'],
					['title', 'string', 'max' => 128],
					['image', 'image'],
					['featuredlogo', 'image'],
					[['status','brandcategories_id'], 'integer'],
					['slug', 'match', 'pattern' => self::$SLUG_PATTERN, 'message' => Yii::t('easyii', 'Slug can contain only 0-9, a-z and "-" characters (max: 128).')],
					['slug', 'default', 'value' => null],
					[['IsFeatured'], 'boolean'],
					['status', 'default', 'value' => self::STATUS_ON],
					[['date_created','date_updated'], 'safe']
				];
			}

		public function attributeLabels()
			{
				return [
					'title' => Yii::t('easyii', 'Brand'),
					'brandcategories_id' => Yii::t('easyii', 'Brand Category'),
					'image' => Yii::t('easyii', 'Logo'),
					'featuredlogo' => Yii::t('easyii', 'Featured Logo'),
					'time' => Yii::t('easyii', 'Date'),
					'slug' => Yii::t('easyii', 'Slug'),
					'IsFeatured' => Yii::t('easyii', 'Is Featured ?'),
					'tagNames' => Yii::t('easyii', 'Tags'),
				];
			}

		public function behaviors()
			{
				return [
					'seoBehavior' => SeoBehavior::className(),
					'taggabble' => Taggable::className(),
					'sluggable' => [
						'class' => SluggableBehavior::className(),
						'attribute' => 'title',
						'ensureUnique' => true
					],
				];
			}

		public function getPhotos()
			{
				return $this->hasMany(Photo::className(), ['item_id' => 'brand_id'])->where(['class' => self::className()]);
			}

		public function beforeSave($insert)
			{
				if (parent::beforeSave($insert))
					{
						$settings = Yii::$app->getModule('admin')->activeModules['brand']->settings;
					  //  $this->short = StringHelper::truncate($settings['enableShort'] ? $this->short : strip_tags($this->text), $settings['shortMaxLength']);

						if(!$insert && $this->image != $this->oldAttributes['image'] && $this->oldAttributes['image'])
							{
								@unlink(Yii::getAlias('@webroot').$this->oldAttributes['image']);
							}
						return true;
					}
				else
					{
						return false;
					}
			}

		public function afterDelete()
			{
				parent::afterDelete();

				if($this->image)
					{
						@unlink(Yii::getAlias('@webroot').$this->image);
					}

				foreach($this->getPhotos()->all() as $photo)
					{
						$photo->delete();
					}
			}
	}