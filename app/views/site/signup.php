<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use app\models\SignupForm;
use app\modules\role\api\Role;


$asset = \app\assets\LoginAsset::register($this);
?>

<section class="section-text-cont">
  
  <div class="container">
  	
    <div class="row">

	<div>
   
     <?php if(Yii::$app->session->hasFlash('error')): ?>
       <div class="alert alert-error">
       <?php echo Yii::$app->session->getFlash('error'); ?>
       </div>
       <?php endif; ?>

       <?php if(Yii::$app->session->hasFlash('success')): ?>
       <div class="alert alert-success">
       <?php echo Yii::$app->session->getFlash('success'); ?>
       </div>
       <?php endif; ?>
       </div>
  	
	
      <div class="login-box-container">
        <div class="container">
          <div class="login-main-container">
            <div class="login-form-container">
            <div class="login-spnsr-btn"> <button type="button" class="active">Event Organizer</button><button type="button">Sponsor</button></div>
                <a href="<?= Url::to(['/site/login']) ; ?>" class="back-arrow"></a>
                <div class="login-heading-text">
                  Register / Sign up
                </div>


               <div class="login-sm">
                  <a href="#" class="login-sm-icons facebook"></a>
                  <a href="#" class="login-sm-icons google-plus"></a>
                 
              </div>
                <div class="login-or-sm loginWithEmail">
               
                  <span class="login-or">
                    <p>OR</p>
                  </span>
                  <span class="login-with">
                    login with
                  </span>
                  
                </div>




               <!--  <div class="who_r_u">
                  <span class="login-icons user-icon"></span>
                  Who are you..?
                </div> -->
                
                <?php   $model = new SignupForm;?>
           		<?php $form = ActiveForm::begin([
		                'id' => 'signup-form',
		                'action' => Url::to(['/site/signup']),
		                'enableAjaxValidation' => FALSE,
                 
                ]); ?>
               
                   <option><?= $form->field($model, 'role')->dropDownList(Role::roleName(), ['prompt'=>'Choose User Type'])->label(false);  ?></option>
                
                <p class="alert-msg alert-msg2">
                  Please Ensure that Popup(s) not Blocked in your Browser
                </p>
                <div class="login-input-cont email">
                  <span class="input-icons"></span>
                  <?= $form->field($model, 'email')->textInput(['class'=>'login-input','placeholder'=>'Email ID','autofocus' => true])->label(false)  ?>
                </div>
                <div class="login-input-cont password password2">
                  <span class="input-icons"></span>
                  <?= $form->field($model, 'password')->passwordInput(['class'=>'login-input','placeholder'=>'Password','autofocus' => true])->label(false)  ?>
                </div>
                <div class="login-input-cont user">
                  <span class="input-icons"></span>
                  <?= $form->field($model, 'username')->textInput(['class'=>'login-input','placeholder'=>'Full name...','autofocus' => true])->label(false) ?>
                        
                </div>
                <div class="login-input-cont phone">
                  <span class="input-icons"></span>
                  <?= $form->field($model, 'contactnumber')->textInput(['class'=>'login-input','placeholder'=>'Phone No...','autofocus' => true])->label(false) ?>
						
                </div>
                
                <?= Html::submitButton('Register', ['class' => 'login-submit-btn register-btn', 'name' => 'signup-button']) ?>
		 <?php ActiveForm::end(); ?>
                
            
            </div>
            <div class="login-text-area">
              <a href="<?= Url::to(['/site/login']) ; ?>" class="login-register-btn">Login</a>
              <div class="login-onspon-logo"></div>
              <p class="login-text">
                ONSPON is India's biggest platform which helps event managers reach out to multiple sponsors and event audience at the click of a button. More than 10,000 event managers, 350+ brands and millions of event goers have made it their platform of choice.
              </p>
            </div>
          </div>

        </div>
      </div>


    </div>
  </div>

</section>


<script>
	$(document).ready(function(){
		$('body').addClass('header_bg');
		
	});
</script>


