<?php
use yii\easyii\modules\faq\models\Faq;
use yii\easyii\modules\page\api\Page;

$page = Page::get('page-faq');

$this->title = $page->seo('title', $page->model->title);
$this->params['breadcrumbs'][] = $page->model->title;

$asset = \app\assets\AppAsset::register($this);
	
?>


<section class="section-banner-cont section-banner ">
  <div class="section-img ocuspoint" data-focus-x="0" data-focus-y="0">
    <img src="<?= $asset->baseUrl ?>/images/section-banner1.jpg">
  </div>

  <div class="section-caption-cont">
    <div class="container">
      <div class="section-caption">
        <h1 class="section-lead-text">
          Welcome to 
          <span class="red_font list-inline">onspon.com</span>
        </h1>
        <h3 class="section-sub-lead-text">
          Where best "Events" are discovered
        </h3>
       
      </div>
    </div>
  </div>
</section>

<div class="clearfix"></div>
<!--banner end here-->

<section class="section-text-cont">
  <div class="container">
    <div class="row">

    <div class="section-heading">
            <h1>FAQ <span class="red_font">s</span></h1>
          </div>

<?php 
$category=array('1'=>'I need sponsor','2'=>'I am Sponsor/Brand Agency');
            
?>        <div class="faqs-tb-cont">
              <ul class="nav nav-tabs test2">
                  <?php $i=1;
                  for($i=1;$i<=count($category);$i++){
                      $class=($i==1)? 'active':'';
                  ?>
                  <li class="<?php echo  $class;?>">
                      <a data-toggle="tab" href="#menu<?=$i?>" aria-expanded="false">
                          <?=$category[$i]?></a></li>
                 
                  <?php }?>
              </ul>
              <div class="tab-content">   
                 <?php $z=1; $y=1;
                  for($z=1;$z<=count($category);$z++){
                      
                  ?> 
                        <div id="menu<?=$z?>" class="tab-pane fade active in">
                        <div class="acordian">
                            <ul class="accordion css-accordion">
                              <?php
                             
                              $faq=Faq::find()->status(FAQ::STATUS_ON)->andWhere(['=', 'category', $z])->all();
                               //print_r( $faq);
                              foreach($faq as $_faq)
                              {
                                  
                              ?>
                               <li class="accordion-item">
                                  <input class="accordion-item-input" name="accordion" id="item<?=$y?>" type="checkbox">
                                  <label for="item<?=$y?>" class="accordion-item-hd">
                                   <span>Q.</span><?=$_faq['question']?>
                                    <span class="accordion-item-hd-cta"></span>
                                  </label>
                                  <div class="accordion-item-bd">
                                      <?=$_faq['answer']?>
                                  </div>       
                               </li>
                              <?php
                               $y++;
                              
                              }?>
                            </ul>
                      </div>
                     </div>
                 <?php }?>


              
              </div>
        </div>
    </div>
  </div>

</section>

