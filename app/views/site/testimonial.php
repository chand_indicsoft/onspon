<?php
$asset = \app\assets\AppAsset::register($this);
	
?>
<section class="section-banner-cont section-banner sec-bnr-300">
  <div class="section-img ocuspoint" data-focus-x="0" data-focus-y="0">
    <img src="<?= $asset->baseUrl ?>/images/section-banner1.jpg">
  </div>

  <div class="section-caption-cont">
    <div class="container">
      <div class="section-caption">
        <h1 class="section-lead-text">
          Welcome to 
          <span class="red_font list-inline">onspon.com</span>
        </h1>
        <h3 class="section-sub-lead-text">
          Where best "Events" are discovered
        </h3>
       
      </div>
    </div>
  </div>
</section>

<div class="clearfix"></div>
<!--banner end here-->

<section class="section-text-cont amazing-deal">
  
  <div class="container">
    <div class="row">

      <div class="section-heading">
        <h1>
          What people say
          <span class="red_font">About Us</span>
        </h1>
      </div>

      <div class="lead-testimonial-cont">
        <div class="lead-tstmnl-slider">
          <div class="lead-tstmnl-slide">
            <div class="lead-tstmnl-img">
              <img src="<?= $asset->baseUrl ?>/images/salman.jpg">
            </div>
            <div class="lead-tstmnl-text-cont">
              <span class="lead-tstmnl-comment">
                Sponsorship has become a very critical tool for us as we want to reach customers directly through an efficient and uncluttered communication channel. I firmly believe Onspon has created a great service offering partnering events and sponsors. The dashboard is very effective , well structured and articulated. It would definitely be my go-to resource
              </span>
              <ul class="lead-tstmnl-nm-dsg">
                <li>Salman Khan</li>
                <li>Bollywood Actor</li>
              </ul>
            </div>
          </div>
           <div class="lead-tstmnl-slide">
            <div class="lead-tstmnl-img">
              <img src="<?= $asset->baseUrl ?>/images/salman.jpg">
            </div>
            <div class="lead-tstmnl-text-cont">
              <span class="lead-tstmnl-comment">
                Sponsorship has become a very critical tool for us as we want to reach customers directly through an efficient and uncluttered communication channel. I firmly believe Onspon has created a great service offering partnering events and sponsors. The dashboard is very effective , well structured and articulated. It would definitely be my go-to resource
              </span>
              <ul class="lead-tstmnl-nm-dsg">
                <li>Salman Khan</li>
                <li>Bollywood Actor</li>
              </ul>
            </div>
          </div>
           <div class="lead-tstmnl-slide">
            <div class="lead-tstmnl-img">
              <img src="<?= $asset->baseUrl ?>/images/salman.jpg">
            </div>
            <div class="lead-tstmnl-text-cont">
              <span class="lead-tstmnl-comment">
                Sponsorship has become a very critical tool for us as we want to reach customers directly through an efficient and uncluttered communication channel. I firmly believe Onspon has created a great service offering partnering events and sponsors. The dashboard is very effective , well structured and articulated. It would definitely be my go-to resource
              </span>
              <ul class="lead-tstmnl-nm-dsg">
                <li>Salman Khan</li>
                <li>Bollywood Actor</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      
      
      
      <div class="clearfix"></div>
      
    <div class="testimonial-second-cont">
        <div class="row">  
            <div class="col-md-6">
                <div class="testimonial-task-cont">
                    <div class="task-content">
                   <div class="testimonial-para"> Sponsorship has become a very critical tool for us as we want to reach customers directly through an efficient and uncluttered communication channel. I firmly believe Onspon has created a great service offering partnering events and sponsors. The dashboard is very effective , well structured and articulated. It would definitely be my go-to resource
                    </div></div>
                    
                    <div class="testimonial-profile">
                        <div class="media">
                        <a class="media-left" href="#">
                        <img class="media-object" src="<?= $asset->baseUrl ?>/images/tstmnl-img1.jpg">
                        </a>
                        <div class="media-body">
                        <h4 class="media-heading">Gaurav Malik </h4>
                       <p> General Manager, Director - Daikin Airconditioning 
India Pvt. Ltd.        </p> </div>
                        </div>
                    </div>
                </div>
            </div>  
            
            <div class="col-md-6">
                <div class="testimonial-task-cont">
                    <div class="task-content">
                   <div class="testimonial-para"> Sponsorship has become a very critical tool for us as we want to reach customers directly through an efficient and uncluttered communication channel. I firmly believe Onspon has created a great service offering partnering events and sponsors. The dashboard is very effective , well structured and articulated. It would definitely be my go-to resource
                    </div></div>
                    
                    <div class="testimonial-profile">
                        <div class="media">
                        <a class="media-left" href="#">
                        <img class="media-object" src="<?= $asset->baseUrl ?>/images/tstmnl-img2.jpg">
                        </a>
                        <div class="media-body">
                        <h4 class="media-heading">Gaurav Malik </h4>
                       <p> General Manager, Director - Daikin Airconditioning 
India Pvt. Ltd.        </p> </div>
                        </div>
                    </div>
                </div>
            </div>  
            
            <div class="col-md-6">
                <div class="testimonial-task-cont">
                    <div class="task-content">
                   <div class="testimonial-para"> Sponsorship has become a very critical tool for us as we want to reach customers directly through an efficient and uncluttered communication channel. I firmly believe Onspon has created a great service offering partnering events and sponsors. The dashboard is very effective , well structured and articulated. It would definitely be my go-to resource
                    </div></div>
                    
                    <div class="testimonial-profile">
                        <div class="media">
                        <a class="media-left" href="#">
                        <img class="media-object" src="<?= $asset->baseUrl ?>/images/tstmnl-img3.jpg">
                        </a>
                        <div class="media-body">
                        <h4 class="media-heading">Gaurav Malik </h4>
                       <p> General Manager, Director - Daikin Airconditioning 
India Pvt. Ltd.        </p> </div>
                        </div>
                    </div>
                </div>
            </div>  
            
            <div class="col-md-6">
                <div class="testimonial-task-cont">
                    <div class="task-content">
                   <div class="testimonial-para"> Sponsorship has become a very critical tool for us as we want to reach customers directly through an efficient and uncluttered communication channel. I firmly believe Onspon has created a great service offering partnering events and sponsors. The dashboard is very effective , well structured and articulated. It would definitely be my go-to resource
                    </div></div>
                    
                    <div class="testimonial-profile">
                        <div class="media">
                        <a class="media-left" href="#">
                        <img class="media-object" src="<?= $asset->baseUrl ?>/images/tstmnl-img4.jpg">
                        </a>
                        <div class="media-body">
                        <h4 class="media-heading">Gaurav Malik </h4>
                       <p> General Manager, Director - Daikin Airconditioning 
India Pvt. Ltd.        </p> </div>
                        </div>
                    </div>
                </div>
            </div>  
            
        </div> 
    
    </div>
         
          
          
      
      
      
      
      
      
      
    </div>
  </div>

</section>

              

<script type="text/javascript">
	$(function () {
		
		
    // Home slider
    $('.lead-tstmnl-slider').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      // variableWidth: true,
      autoplay: true,
      autoplaySpeed: 4000,
      dots: true,
      arrows: false
    });



		
	});	
  </script> 
  