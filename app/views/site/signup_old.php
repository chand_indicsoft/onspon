<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use app\models\SignupForm;
use app\modules\role\api\Role;
//$assetHead = \app\assets\AppHeadAsset::register($this);
$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="mask"></div>

<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1><?= Html::encode($this->title) ?></h1>
            </div>
            <div class="panel-heading">
                  
				 <?= yii\authclient\widgets\AuthChoice::widget([
     'baseAuthUrl' => ['site/auth']
]) ?>

            </div>
            
             <?php   $model = new SignupForm;?>
    
            <?php $form = ActiveForm::begin([
                'id' => 'signup-form',
                'action' => Url::to(['/site/signup']),
                'enableAjaxValidation' => FALSE,
                 
                ]); ?>
            <div class="site-signup">
                 

                <!--<p>Please fill out the following fields to signup:</p>-->

              
                        <?= $form->field($model, 'role')->dropDownList(Role::roleName(), ['prompt'=>'Choose User Type']);  ?>
                        
                        <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label('Name') ?>
                        
                        <?= $form->field($model, 'contactnumber')->textInput(['autofocus' => true])->label('Mobile Number') ?>
						
                        <?= $form->field($model, 'email')->textInput(['autofocus' => true])  ?>

                        <?= $form->field($model, 'password')->passwordInput() ?>

                        <div class="form-group">
                            <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                            
                            <span style="float:right;margin-right: 40px;"> <?= Html::a('sign in', ['site/login']) ?><span>
                            
                        </div>  

                        <?php ActiveForm::end(); ?>
                  
            </div>
        </div>
    </div>
</div>
</div>
