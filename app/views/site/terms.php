<?php

$asset = \app\assets\AppAsset::register($this);
	
?>
<section class="section-banner-cont section-banner sec-bnr-300">
  <div class="section-img focuspoint" data-focus-x="0" data-focus-y="0">
    <img src="<?= $asset->baseUrl ?>/images/message-banner.jpg">
  </div>

  <div class="section-caption-cont">
    <div class="container">
      <div class="section-caption">
        <h1 class="section-lead-text">
          Welcome to 
          <span class="red_font list-inline">onspon.com</span>
        </h1>
        <h3 class="section-sub-lead-text">
          Where best "Events" are discovered
        </h3>
       
      </div>
    </div>
  </div>

 
</section>

<div class="clearfix"></div>
<!--banner end here-->

<section class="section-text-cont">
  
  <div class="container">
    <div class="row">
      <div class="section-heading">
        <h1>
         Terms &
          <span class="red_font"> Conditions</span>
        </h1>
      </div>
   <div class="clearfix"></div>
     
     <div class="terms">
     <p class="para-25">Welcome to OnSpon, an online service that helps sponsors and avenues to connect with one another. OnSpon (hereinafter "OnSpon," "the Site," or "the Service") is operated by OnSpon Services Pvt. Ltd. and its corporate affiliates (collectively "us," "we," or "the Company"). By accessing, or otherwise using, our web site at www.OnSpon.com, you represent and warrant that you have read, understand and agree to be bound by these Terms of Service (“Terms of Service" or "Agreement"), including our Privacy Policy, which is incorporated herein by reference. These representations and warranties fully apply to you irrespective of whether or not you ultimately register as a member of OnSpon.</p>
     
      <p class="para-25">The Company, at its sole discretion, reserves the right to change, modify, add, or delete any portion, or portions, of these Terms of Service, at any time, without further notice. If we do so, we will post these changes to the Terms of Service on this page and will indicate at the top of this page the date that the Terms of Service were last revised. Your continued access and/or use of the Service or the Site after any such changes have been made constitutes your affirmative acceptance of the new Terms of Service. If you do not agree to abide by these Terms of Service, or our rules regarding any future changes to the Terms of Service, you are prohibited from using or accessing (or continue to use or access) both the Service and the Site. It is your responsibility to check the Site regularly to determine if there have been changes to these Terms of Service, and, if so, to review those changes. If you are unsure of the meaning of any change, it is your obligation to contact us before continuing use of the Service or Site. </p>
     
     
     <div class="terms-alert">PLEASE READ THESE TERMS OF SERVICE CAREFULLY, AS THEY CONTAIN IMPORTANT INFORMATION REGARDING YOUR LEGAL RIGHTS, REMEDIES AND OBLIGATIONS. THESE TERMS OF SERVICE INCLUDE VARIOUS LIMITATIONS AND EXCLUSIONS, AS WELL AS A DISPUTE RESOLUTION CLAUSE THAT GOVERNS HOW ANY DISPUTE WILL BE RESOLVED.</div>
     
     <h4 class="terms-heading">Use of this Site Through your use of the site, you agree not to use the Service or the Site to:</h4>
     
       <ul class="terms-list-one">
     
            <li>collect email addresses or other contact information of other users from the Service or the Site, by electronic or other means, for the purposes of sending unsolicited emails or other unsolicited communications;</li>
            <li>Use the Service or the Site in any unlawful manner or in any other manner that could damage, disable, overburden or impair the Site;</li>
            <li>use automated scripts to collect information from, or otherwise interact with, the Service or the Site;</li>
            <li>Upload, post, transmit, share, store or otherwise make available any content that either (a) violates the law of any state or jurisdiction, or (b) even if legal, would be deemed harmful, threatening, unlawful, defamatory, infringing, abusive, inflammatory, harassing, vulgar, obscene, fraudulent, invasive of privacy or publicity rights, hateful, or racially, ethnically or otherwise objectionable;</li>
            <li>Upload, post, transmit, share, store or otherwise make available any videos other than those which you have the full rights to distribute.</li>
            <li>Register for more than one user account, register for a User account on behalf of an individual other than yourself, register for a user account by providing false of misleading information, or register for a User account on behalf of any group or entity;</li>
            <li>impersonate any person or entity, or falsely state or otherwise misrepresent yourself or your affiliation with any person or entity;</li>
            <li>Use the website in any matter that intended to commit identity fraud, or assist, enable, or facilitate any other person to do so;</li>
            <li>upload, post, transmit, share, store or otherwise make publicly available on the Site any private information of any third party, including, without limitation, names, nicknames, addresses, phone numbers, email addresses, Social Security numbers, bank account numbers, and credit card numbers;</li>
            <li>solicit personal information from any user under 13;</li>
            <li>solicit passwords or personally identifying information for commercial or unlawful purposes;</li>
            <li>upload, post, transmit, share or otherwise make available any material that contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer software or hardware or telecommunications equipment;</li>
            <li>intimidate or harass another;</li>
            <li>upload, post, transmit, share, store or otherwise make available content that would constitute, encourage or provide instructions for a criminal offense, violate the rights of any party, or that would otherwise create liability or violate any local, state, national or international law;</li>
            <li>use or attempt to use another's account, service or system without authorization from the Company, or create a false identity on the Service or the Site.</li>
            <li>upload, post, transmit, share, store or otherwise make available content that, in the sole judgment of Company, is objectionable or which restricts or inhibits any other person from using or enjoying the Site, or which may expose Company or its users to any harm or liability of any type.</li>
            <li>upload, post, transmit, share, store or otherwise make available content that, in the sole judgment of Company, is intended for the purpose of competing, or attempting to compete, against the Site.</li>
            <li>copy or distribute any part of the Website in any medium without OnSpon's prior written authorization</li>

       </ul>
     
     <p class="para-35">Further, you represent, warrant and agree that you will not alter or modify any part of the Website other than as may be reasonably necessary to use the Website for its intended purpose, as well as that no materials of any kind submitted through your account or otherwise posted, transmitted, or shared by you on or through the Service will violate or infringe upon the rights of any third party, including copyright, trademark, privacy, publicity or other personal or proprietary rights; or contain libelous, defamatory or otherwise unlawful material. User Content </p>
     
     

    
    <ol class="ordered-list">
<li>
In order to access some features of the Website, you will have to create an account. You must create, and all times use, your own account. You may never use another's account without permission. When creating your account, you must provide accurate and complete information as prompted by the Website's registration form and maintain and promptly update the registration data to keep it true, accurate, current and complete. If you provide any information that is untrue, inaccurate, not current or incomplete, or OnSpon has reasonable grounds to suspect that such information is untrue, inaccurate, not current or incomplete, OnSpon has the right to suspend or terminate your account and refuse any and all current or future use of the Website (or any portion thereof). You are solely responsible for the activity that occurs on your account, and you must keep your account password secure. You must notify OnSpon immediately of any breach of security or unauthorized use of your account.
</li>
<li>You are solely responsible for the photos, profiles, messages, notes, text, information, music, video, advertisements, listings, and other content that you upload, publish or display (hereinafter, "post") on or through the Service or the Site, or transmit to or share with other users (collectively the "User Content"). You may not post, transmit, or share User Content on the Site or Service that you did not create or that you do not have permission to post. You understand and agree that the Company may, but is not obligated to, review the Site and may delete or remove (without notice) any Site Content or User Content in its sole discretion, for any reason or no reason, including without limitation User Content that in the sole judgment of the Company violates this agreement, or which might be offensive, illegal, or that might violate the rights, or harm or threaten the safety of users or others. You are solely responsible at your sole cost and expense for creating backup copies and replacing any User Content you post or store on the Site or provide to the Company.</li>

<li>When you post User Content to the Site, you authorize and direct us to make such copies thereof as we deem necessary in order to facilitate the posting and storage of the User Content on the Site. By posting User Content to any part of the Site, you automatically grant (and represent and warrant that you have the right to grant) to the Company an irrevocable, perpetual, non-exclusive, transferable, fully paid, worldwide license (with the right to sublicense) to use, copy, publicly perform, publicly display, reformat, translate, excerpt (in whole or in part) and distribute such User Content for any purpose on or in connection with the Site or the promotion thereof, to prepare derivative works of, or incorporate into other works, such User Content, and to grant and authorize sublicenses of the foregoing. You may remove your User Content from the Site at any time. If you choose to remove your User Content, the license granted above will automatically expire; however you acknowledge that the Company may retain archived copies of your User Content. OnSpon does not accept responsibility for protecting the confidentiality of any confidential or proprietary information you submit on The Website, nor do we accept any responsibility for its potential misuse or disclosure.</li>
<li>You agree that you will not post on or transmit any advertising or commercial solicitation, other than presenting your company through the video upload, without OnSpon's express prior written approval and, if then, solely in accordance with the terms and conditions imposed by OnSpon with respect thereto. You further agree not to use the Website, or any element or portion thereof (including, without limitation, names, addresses, and e-mail addresses of users), for any purpose, whether commercial or otherwise, other than presenting your company as otherwise permitted under the Terms of Service. If you are unsure whether the presentation of your company complies with the Terms of Service, you will direct any inquiries in advance to us.</li>
<li>You agree that you will not post, upload or transmit to the Website, any communications, text, graphics or other information or Materials that:
<ol type="i">
<li>is unlawful, obscene, fraudulent, indecent or that defames, abuses, harasses, or threatens others, or is hateful, or racially, ethnically or otherwise objectionable;</li>
<li>contains any software viruses, Trojan horses, worms, bombs, or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer software or hardware or telecommunications equipment or that may damage, detrimentally interfere with, surreptitiously intercept, or expropriate any system, data, or personal information;</li>
<li>advocates or encourages any illegal activity;</li>
<li>infringes on the copyright, patent, trademark, trade secret, right of publicity or other intellectual property, proprietary, contracted, personal or other right of any third party;</li>
<li>violates either the privacy rights or publicity rights of individuals, including other users of the Website; or </li>
<li>violates any applicable local, state, national or international law. You agree not to use bots, spiders or intelligent agent software (or other methods) for any purpose other than accessing publicly posted portions of the Website and then only for the purposes consistent with the limited license hereunder and these Terms of Service. You agree not to, or attempt to, circumvent any access or use restrictions, data encryption or content protection related to the Website; not to data mine the Website and not to in any way cause harm to or burden the Website.</li>
</ol>

</li>

<li>You understand that when using the Website, you will be exposed to User Submissions from a variety of sources, and that OnSpon is not responsible for the accuracy, usefulness, safety, or intellectual property rights of or relating to such Submissions. You further understand and acknowledge that you may be exposed to User Submissions that are inaccurate, offensive, indecent, or objectionable, and you agree to waive, and hereby do waive, any legal or equitable rights or remedies you have or may have against OnSpon with respect thereto, and agree to indemnify and hold OnSpon, its Owners/Operators, affiliates, and/or licensors, harmless to the fullest extent allowed by law regarding all matters related to your use of the site. OnSpon does not endorse any User Submission or any opinion, recommendation, or advice expressed therein, and OnSpon expressly disclaims any and all liability in connection with User Submissions.</li>
</ol>
    
    <h4 class="terms-heading">Governing Law; Venue and Jurisdiction</h4>
     
  <p class="para-25">By visiting or using the Website, you agree that the laws of India, without regard to principles of conflict of laws, will govern these Terms of Service, as well as any dispute of any sort that might arise between you and OnSpon or any of our affiliates, including, but not limited to, disputes based on tort law, personal privacy rights, and data privacy
   rights. With respect to any disputes or claims not subject to arbitration (as set forth below), you agree not to commence or prosecute any action in connection therewith other than in either in New Delhi or in any Indian court, and you hereby consent to, and waive all defenses of lack of personal jurisdiction and forum non conveniens with respect to, venue and jurisdiction in
    the aforementioned state and federal courts. Intellectual Property and Limited License.</p>
     <p class="para-25">You acknowledge that all content, except for User Submissions, available through the Website, including, without limitation, content in the form of text, graphics, software, music, sound, photographs, and videos, and content provided by suppliers, sponsors or third-party advertisers (the “Other Content”), is protected by copyrights, trademarks, patents, or other proprietary rights and laws. Except as expressly authorized by herein, you agree not to copy, modify, rent, lease, loan, sell, assign, distribute, license, reverse engineer, or create derivative works based on the Website or any Other Content (including, without limitation, any software) available through the Website. You are hereby granted a nonexclusive, nontransferable, revocable, limited license to view, copy, print and distribute Other Content retrieved from the Website for your personal, noncommercial purposes, provided that you do not remove or obscure the copyright notice or other notices displayed on the Other Content. You may not copy, reprint, modify, distribute, or sell Other Content retrieved from the Website in any way, for any commercial use, or provide it to any commercial source, including other websites, regardless of whether you receive compensation, without the prior written permission of OnSpon. You may not frame any trademark, logo, or other proprietary information on this Website without the express written consent of OnSpon. Except as expressly provided in the Agreement, nothing contained in the Agreement or on the Website shall be construed as
      conferring any other license or right, expressly, by implication, by estoppel, or otherwise under any of OnSpon's content, under any Other Content or under any associated intellectual property rights. Any rights not expressly granted herein are reserved.</p>
     
     
     
         <h4 class="terms-heading">Governing Law; Venue and Jurisdiction</h4>
     
  <p class="para-25">This Website is intended for and directed to adults in India only. This Website is not intended for anybody under the age of 18. Any posted, uploaded or otherwise transmitted contents on this Website, however, must be appropriate, and legal, for the viewing of minors in all Indian states.</p>
     
     
             <h4 class="terms-heading">User Disputes</h4>
     
  <p class="para-25">You are solely responsible for your interactions with other users. We reserve the right, but have no obligation, to monitor disputes between you and other users.
</p>
  
  
  
  <h4 class="terms-heading">Privacy Policy</h4>
  
  <p class="para-25">The information that we obtain through your use of the Website, whether through the registration process or otherwise, is subject to the OnSpon privacy policy. By agreeing to the Terms of Service, you represent, warrant, and agree to be bound by our Privacy Policy, including changes to that policy, which from time to time may occur. If you are unwilling to accept the terms and conditions of the Privacy Policy, you are forbidden from accessing or using our Website, as well as from posting or submitting any materials on it.
</p>
  
  
  
  
    <h4 class="terms-heading">Links; Inbound and Outbound</h4>
  <p class="para-25">Through the use of The Website, users may be exposed to other sites, through links, that may offer products, services or other resources. OnSpon has no control over such sites and resources, and you acknowledge and agree that OnSpon has no responsibility for the accuracy of information provided by or availability via other sites. Links to external sites do not constitute an endorsement of the sponsors of such sites or the content, products, advertising or other materials presented on those sites. OnSpon does not author, edit or monitor these pages or links. You further acknowledge and agree that OnSpon is not responsible or liable, directly or indirectly, for any damage or loss caused, or alleged to be caused, by or in connection with use of or reliance on any such content, goods or services available on these other sites or resources.</p>
  
    <p class="para-25">OnSpon hereby grants you a non-exclusive, limited license, revocable at OnSpon's discretion, for you to link to the OnSpon home page from any site you own or control that is not commercially competitive and does not criticize or otherwise injure OnSpon, so long as the site where the link resides, and all other locations to which such site links, comply with all applicable laws and do not in any way abuse, defame, stalk, threaten or violate the rights of privacy, publicity, intellectual property or other legal rights of others or, in any way, post, publish, distribute, disseminate or facilitate any inappropriate, infringing, defamatory, profane, indecent, obscene or illegal/unlawful information, topic, name or other material. All of OnSpon's rights and remedies are expressly reserved.</p>
  
  
  
     <h4 class="terms-heading">Copyright Infringement - Digital Millennium Copyright Act Notice</h4>
  <p class="para-25">OnSpon respects intellectual property rights and will make every effort to secure appropriate clearances for all proprietary intellectual properties that OnSpon directly makes available on its Website. Likewise, we ask our users to respect intellectual property rights. OnSpon, at its sole discretion, may disable and/or terminate the accounts of users who it suspects to be undertaking copyright infringement. If you believe any material on The Website, either posted by OnSpon, our users, or any other party, is infringing, please contact OnSpon at the address below. You may notify OnSpon of alleged intellectual property rights infringement by contacting:
  </p>
  
          <address>
        OnSpon Services Pvt. Ltd.<br>
        38/1, Moti Kunj,<br>
        Agra 282002. India<br>
        E-mail: <a href="mailto:xyz@example.com"> info@onspon.com</a>
  
        </address>
  
  <p class="para-25">OnSpon has designated an agent to receive notices of claimed copyright infringement. If you believe in good faith that your work has been copied in a way that constitutes copyright infringement, please provide OnSpon's Copyright Agent the following information:</p>
    
    
    <ul class="terms-list-one">
<li>A physical or electronic signature of the copyright holder or a person authorized to act on behalf of the copyright owner;</li>
<li>A description of the copyrighted work claimed to have been infringed, or, if multiple copyrighted works at a single online site are covered by a single notification, a representative list of such works at that site;</li>
<li>A description of the material that is claimed to be infringing or to be the subject of infringing activity, and information reasonably sufficient to permit the service provider to locate the material;</li>
<li>Information reasonably sufficient to permit the service provider to contact you, such as an address, telephone number, and, if available, an electronic mail address;</li>
<li>A statement that you have a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent, or the law; and</li>
<li>A statement that the information in the notification is accurate, and under penalty of perjury, that you are authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.</li>
</ul>
   <p class="para-25">Upon receipt of the written notification containing the information as outlined above:</p>  
     
     <ol type="1" class="ordered-list">
<li>Onspon shall remove or disable access to the material that is alleged to be infringing;</li>
<li>OnSpon shall forward the written notification to such alleged infringer; and</li>
<li>OnSpon shall take reasonable steps to promptly notify the Subscriber that it has removed or disabled access to the material.</li>

</ol>
    
   <h4 class="terms-heading">Counter-Notification:</h4> 
    
       <p class="para-25">If the alleged infringer believes that a notice of copyright infringement has been wrongly filed against it, and it would like to submit a counter-notification, it may file a Counter-Notification in Response to Claim of Copyright Infringement by providing the following information:</p>  

    <ol class="ordered-list" type="1"><li>A physical or electronic signature;</li>
<li>Identification of the material that has been removed or to which access has been disabled and the location at which the material appeared before it was removed or access to it was disabled;</li>
<li>A statement under penalty of perjury that he or she has a good faith belief that the material was removed or disabled as a result of mistake or misidentification of the material to be removed or disabled; and</li>
<li>The alleged infringer's name, address, and telephone number, and a statement that the alleged infringer consents to the jurisdiction of Federal District Court for the judicial district in which the address is located, or if his or her address is outside of the India, for any judicial district in which OnSpon may be found, and that this entity will accept service of process from the person who provided notification or an agent of such person.</li>

</ol>
    
    
    <h4 class="terms-heading">Counter-Notification claims should be sent to: OnSpon Services Pvt. Ltd.</h4> 
    
    
    <address>
        OnSpon Services Pvt. Ltd.<br>
        38/1, Moti Kunj,<br>
        Agra 282002. India<br>
        E-mail: <a href="mailto:xyz@example.com"> info@onspon.com</a>
  
        </address>
    
   
      <h4 class="terms-heading"> Limitation of Liability</h4> 
    
     <p class="para-25">YOU EXPRESSLY UNDERSTAND AND AGREE THAT UNDER NO CIRCUMSTANCES WILL ONSPON, ITS PARENT, AFFILIATES OR SUBSIDIARIES, THEIR SPONSORS, CONTRACTORS, ADVERTISERS, VENDORS OR OTHER PARTNERS, ANY OF THEIR SUCCESSORS, ASSIGNS OR LICENSEES, OR ANY OF THEIR RESPECTIVE OFFICERS, DIRECTORS, AGENTS, EMPLOYEES (COLLECTIVELY "RELEASED PARTIES") BE RESPONSIBLE OR LIABLE TO YOU OR ANY OTHER PERSON OR ENTITY FOR ANY DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL, EXEMPLARY, PUNITIVE OR OTHER DAMAGES, INCLUDING BUT NOT LIMITED TO, DAMAGES FOR LOSS OF PROFITS, GOODWILL, USE, DATA OR OTHER INTANGIBLE LOSSES, ARISING OUT OF OR RELATING IN ANY WAY TO THE SITE, THE USE OR THE INABILITY TO USE THE SITE, UNAUTHORIZED ACCESS TO OR ALTERATION OF YOUR TRANSMISSIONS OR DATA, THE MATERIALS AVAILABLE ON THE SITE, STATEMENTS OR CONDUCT OF ANY THIRD PARTY ON OR IN CONNECTION WITH THE SITE OR ANY OTHER MATTER RELATING TO THE SITE INFORMATION CONTAINED WITHIN THE SITE, EVEN IF ANY OF THESE PARTIES HAVE BEEN ADVISED OF THE POSSIBILITY OF ANY DAMAGES. YOUR SOLE REMEDY FOR DISSATISFACTION WITH THE SITE AND/OR SITE-RELATED MATERIALS IS TO STOP USING THE SITE.</p>
    
     
     <h4 class="terms-heading"> Indemnity</h4> 
     
     <p class="para-25">You agree to indemnify and hold harmless OnSpon, its subsidiaries and affiliates, and each of their directors, officers, agents, contractors, partners and employees, from and against any loss, liability, claim, demand, damages, costs and expenses, including reasonable attorney's fees, arising out of or in connection with any User Content, any Third Party Applications, Software or Content you post or share on or through the Site (including without limitation through the Share Service), your use of the Website, your conduct in connection with The Website or with other users of the Website, or any violation of this Agreement or of any law or the rights of any third party. OnSpon Services Pvt. Ltd. The user represents, warrants and agrees that OnSpon Services Pvt. Ltd. may terminate the user's right to use the site or service for breach of any of the terms of service and user agree to waive and relinquish any rights to a refund of any fees that have been paid to OnSpon for the service.</p>
    
      <h4 class="terms-heading"> Questions</h4>
     
     
         <p class="para-25">If you have additional questions regarding the use of OnSpon, please visit our FAQ page or contact us.</p>
     
     
     
     
     </div><!--terms end here-->
     
    </div>
  </div>

</section>

