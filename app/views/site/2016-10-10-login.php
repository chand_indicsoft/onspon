<?php
	use yii\helpers\Html;
	use yii\bootstrap\ActiveForm;
	use yii\helpers\Url;
	use app\models\LoginForm;
	use app\modules\role\api\Role;

	$asset = \app\assets\LoginAsset::register($this);
	
	$session = Yii::$app->session;
	
	
?>
<section class="section-text-cont">
	<div class="container">
		<div class="row">
			<div>
				<?php if(Yii::$app->session->hasFlash('error')): ?>
					<div class="alert alert-error">
						<?php echo Yii::$app->session->getFlash('error'); ?>
					</div>
				<?php endif; ?>

				<?php if(Yii::$app->session->hasFlash('success')): ?>
					<div class="alert alert-success">
						<?php echo Yii::$app->session->getFlash('success'); ?>
					</div>
				<?php endif; ?>
			</div>

			<div class="login-box-container">
				<div class="container">
					<div class="login-main-container">
						<div class="login-form-container">
							<div class="login-heading-text">login / sign in</div>

							<?php $model = new LoginForm; 
							if($session['email']){
								$model->email = $session['email'];	
							}?>
							<?php
								$form = ActiveForm::begin([
									'id' => 'login-form',
									'action' => Url::to(['/site/login']),
									'enableAjaxValidation' => FALSE
								]);
							?>
								<div class="who_r_u">
									<span class="login-icons user-icon"></span>
									Who are you..?
								</div>

								<?php
									echo $form->field($model, 'role')
										->dropDownList(Role::roleName(), ['prompt'=>'Choose User Type'])
										->label(false);
								?>

								<p class="alert-msg alert-msg2">Please Ensure that Popup(s) not Blocked in your Browser</p>
								<div class="login-input-cont email">
									<span class="input-icons"></span>
									<?php
										echo $form->field($model, 'email')
											->textInput([
												'class' => 'login-input',
												'placeholder' => 'Email',
												'autofocus' => true
											])
											->label(false);
									?>
								</div>
								<div class="login-input-cont password">
									<span class="input-icons"></span>
									<?php
										echo $form->field($model, 'password')
											->passwordInput([
												'class' => 'login-input',
												'placeholder' => 'Password',
												'autofocus' => true
											])
											->label(false);
									?>
								</div>

								<?php
									echo Html::submitButton('login', [
										'class' => 'login-submit-btn',
										'name' => 'login-button'
									])
								?>
							<?php ActiveForm::end(); ?>
							<a href="<?= Url::to(['/site/request-password-reset']) ; ?>" class="forgot-ps">Forgot Your Password?</a>

							<div class="login-or-sm loginWithEmail">
								<span class="login-or">
									<p>OR</p>
								</span>
								<span class="login-with">
									login with
								</span>
								<div class="login-sm">
									<?= yii\authclient\widgets\AuthChoice::widget([
									     'baseAuthUrl' => ['site/auth']
									]) ?>
								</div>
							</div>
						</div>

						<div class="login-text-area">
							<a href="<?= Url::to(['/site/signup']) ; ?>" class="login-register-btn">Register</a>
							<div class="login-onspon-logo"></div>
							<p class="login-text">
								ONSPON is India's biggest platform which helps event managers reach out to multiple sponsors and event audience at the click of a button. More than 10,000 event managers, 350+ brands and millions of event goers have made it their platform of choice.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
$(document).ready(function(){
	$('body').addClass('header_bg');
});
$(document).on('click', '.login-sm', function() {
	if(!check_role()) return false;

});


function check_role() {
	if($('#loginform-role').val() == '')
	{
		alert("Please select your user type");
		return false;
	}

	return true;
}
</script>