<?php
	use yii\helpers\Html;
	use yii\widgets\ActiveForm;
	use yii\helpers\Url;
	use app\models\LoginForm;
	use app\modules\role\api\Role;

	$asset = \app\assets\LoginAsset::register($this);
	$session = Yii::$app->session;

	# models
	$model_login = new LoginForm;

	# registering javascript
	$this->registerJs("$('body').addClass('header_bg');

$(document).on('click', '.login-sm', function() {
	if($('#loginform-role').val() == '')
	{
		alert('Please select your user type');
	}
});

$(document).on('change', '.js-ajax-set-role', function(e) {
	if($(this).val() != '')
	{
		$.ajax({
			url: '" . Url::to(['/site/ajax-set-role']) . "?role_id=' + $(this).val(),
			type: 'GET',
			success: function() {
				$('.login-sm').removeClass('disabled');
			}
		});
	}
	else
	{
		$('.login-sm').addClass('disabled');
	}
});

$('.auth-link').on('click', function() {
	if($('.js-ajax-set-roll').val() == '') return false;
});");

	$this->registerCss(".login-sm.disabled { position: relative;} .login-sm.disabled:after { position: absolute; content: ''; z-index: 1; top: 0; left: 0; right: 0; bottom: 0; background-color: rgba(255,255,255,.05); cursor: pointer;}");
?>
<section class="section-text-cont">
	<div class="container">
		<div class="row">
			<div>
				<?php if(Yii::$app->session->hasFlash('error')): ?>
					<div class="alert alert-error">
						<?php echo Yii::$app->session->getFlash('error'); ?>
					</div>
				<?php endif; ?>

				<?php if(Yii::$app->session->hasFlash('success')): ?>
					<div class="alert alert-success">
						<?php echo Yii::$app->session->getFlash('success'); ?>
					</div>
				<?php endif; ?>
			</div>

			<div class="login-box-container">
				<div class="container">
					<div class="login-main-container">
						<div class="login-form-container">
						<div class="login-spnsr-btn"><button type="button">Sponsor</button> <button type="button">Event Organizer</button></div>

						<div class="login-heading-text">login / sign in</div>
						<?php
								if($session['email'])
								{
									$model_login->email = $session['email'];
								}

								$form = ActiveForm::begin([
									'id'                   => 'login-form',
									'action'               => Url::to(['/site/login']),
									'enableAjaxValidation' => FALSE
								]);
							?>
							<!-- <div class="who_r_u">
								<span class="login-icons user-icon"></span>
								Who are you..?
							</div> -->

							<?php
								echo $form->field($model_login, 'role')
									->dropDownList(Role::roleName(), [
										'prompt'=> 'Choose User Type',
										'class' => 'form-control js-ajax-set-role'
									])
									->label(false);
							?>
							<p class="alert-msg alert-msg2">Please Ensure that Popup(s) not Blocked in your Browser</p>

							<div class="login-input-cont email">
								<span class="input-icons"></span>
								<?php
									echo $form->field($model_login, 'email')
										->textInput([
											'class'       => 'login-input',
											'placeholder' => 'Email',
											'autofocus'   => true
										])
										->label(false);
								?>
							</div>
							<div class="login-input-cont password">
								<span class="input-icons"></span>
								<?php
									echo $form->field($model_login, 'password')
										->passwordInput([
											'class' => 'login-input',
											'placeholder' => 'Password',
											'autofocus' => true
										])
										->label(false);
								?>
							</div>

							<?php
								echo Html::submitButton('login', [
									'class' => 'login-submit-btn',
									'name' => 'login-button'
								]);

								ActiveForm::end();
							?>
							<a href="<?= Url::to(['/site/request-password-reset']); ?>" class="forgot-ps">Forgot Your Password?</a>

							<div class="login-or-sm loginWithEmail">
								<span class="login-or">
									<p>OR</p>
								</span>

								<span class="login-with">login with</span>

								<div class="login-sm disabled">
									<?=
										yii\authclient\widgets\AuthChoice::widget([
											'baseAuthUrl' => ['site/auth']
										]);
									?>
								</div>
							</div>
						</div>

						<div class="login-text-area">
							<a href="<?= Url::to(['/site/signup']); ?>" class="login-register-btn">Register</a>
							<div class="login-onspon-logo"></div>
							<p class="login-text">ONSPON is India's biggest platform which helps event managers reach out to multiple sponsors and event audience at the click of a button. More than 10,000 event managers, 350+ brands and millions of event goers have made it their platform of choice.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
