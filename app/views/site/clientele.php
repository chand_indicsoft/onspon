<?php
$asset = \app\assets\AppAsset::register($this);
	
?>
<section class="section-banner-cont section-banner sec-bnr-300">
  <div class="section-img focuspoint" data-focus-x="0" data-focus-y="0">
    <img src="<?= $asset->baseUrl ?>/images/message-banner.jpg">
  </div>

  <div class="section-caption-cont">
    <div class="container">
      <div class="section-caption">
        <h1 class="section-lead-text">
          Welcome to 
          <span class="red_font list-inline">onspon.com</span>
        </h1>
        <h3 class="section-sub-lead-text">
          Where best "Events" are discovered
        </h3>
       
      </div>
    </div>
  </div>

 
</section>

<div class="clearfix"></div>
<!--banner end here-->

<section class="section-text-cont">
  
  <div class="container">
    <div class="row">
      <div class="section-heading">
        <h1>
        Few of the brands that 
          <span class="red_font"> engage with Us</span>
        </h1>
      </div>
   <div class="clearfix"></div>
     
     
     <div class="clientele">
     <ul>
        <?php foreach($brand as $_brand){?>
        <li><img src="<?php echo Yii::$app->homeUrl.$_brand->image;?>"></li>
        <?php }?>  
     </ul>
     
     </div><!--clientele end here-->
     
    </div>
  </div>

</section>

