<?php
use yii\helpers\Html;
use yii\helpers\Url;
//use yii\captcha\Captcha;
use yii\easyii\widgets\ReCaptcha;
$asset = \app\assets\AppAsset::register($this);
	

$this->registerJsFile('https://www.google.com/recaptcha/api.js', ['position' => \yii\web\View::POS_END]);
?>
<section class="section-banner-cont section-banner sec-bnr-300">
	<div class="section-img focuspoint" data-focus-x="0" data-focus-y="0">
		<img src="<?= $asset->baseUrl ?>/images/message-banner.jpg">
	</div>

	<div class="section-caption-cont">
		<div class="container">
			<div class="section-caption">
				<h1 class="section-lead-text">
					Welcome to 
					<span class="red_font list-inline">onspon.com</span>
				</h1>
				<h3 class="section-sub-lead-text">
					Where best "Events" are discovered
				</h3>
				
			</div>
		</div>
	</div>
</section>

<!--banner end here-->


<section class="section-text-cont">
	
	<div class="container">
		<div class="row">

			<div class="section-heading">
				<h1>Contact <span class="red_font">Us</span></h1>
			</div>
			
			<div class="clearfix"></div>

			
			<section class="contct-us-cont">
				<div class="col-md-8 contct1-left">
					<h4 class="terms-heading">Use of this Site Through your use of the site, you agree not to use the Service or the Site to:</h4>
					<p class="para-25">Whether you have a question about your listing, someone else’s listing, have a complaint, need assistance using our proprietary tools, recommend some really cool event, schedule a meeting with us or just wanted to pass a hi, we want to know what’s on your mind. Our community is important to us!</p>
				</div>
				<div class="col-md-4">
					<div class="contct1-right">
						<div class="contact-icon"><img src="<?= $asset->baseUrl ?>/images/contact-icon.png"></div>
						<div class="contct-numbr">
							Call Us now...
							<span> +91 76660 03344</span>
						</div>              
					</div>
				</div>
			</section>
			<section class="how-wehelp-u">
				<div class="contct-heading">How can we help you ?</div>
				<ul>
					<li><h4 class="terms-heading">If you need sponsorship</h4>
						<p class="para-25">If you represent an interesting avenue which needs sponsorship; create a listing in just 2 minutes. Have any questions / query / clarification etc;
						</p>
						<div class="contct-connect">connect with us<a href="#" class="red_font "> brands@onspon.com</a></div>
					</li>

					<li><h4 class="terms-heading">If you want to schedule a meeting / demo with us</h4>
						<p class="para-25">If you represent an interesting avenue which needs sponsorship; create a listing in just 2 minutes. Have any questions / query / clarification etc;
						</p>
						<div class="contct-connect">connect with us<a href="#" class="red_font "> events@onspon.com</a></div>
					</li>

					<li><h4 class="terms-heading">Work with us</h4>
						<p class="para-25">We strive to deliver the best in sponsorship space and if you share the same passion; write to us at

						</p>
						<div class="contct-connect">connect with us<a href="#" class="red_font "> info@onspon.com</a></div>
						<div class="contct-connect">with Subject:<a href="#" class="red_font ">  Career@Onspon</a></div>

					</li>


					<li><h4 class="terms-heading">Want to recommend an event ?</h4>
						<p class="para-25">If have a really cool event which is organized by your friends, in your neighbourhood / college / organization and want us to list it. Please mail us the details of the

						</p>
						<div class="contct-connect">connect with us<a href="#" class="red_font "> info@onspon.com</a></div>
					</li>


					<li><h4 class="terms-heading">Media / PR ?</h4>
						<p class="para-25">IThere is so much happening.  If you have a Media / PR query ; kindly mail us at</p>
						<div class="contct-connect"><a href="#" class="red_font "> media@onspon.com</a></div>
					</li>

					<li><h4 class="terms-heading">Have a feedback?</h4>
						<p class="para-25">IThat’s awesome. We have constantly worked towards improvement of the user experience @Onspon. Just tweet us @onspon with your feedback – we are sure it would help us get better at what we do (and plan to do). Dont like to tweet?

						</p>
						<div class="contct-connect">Mail us at <a href="#" class="red_font ">info@onspon.com</a></div>
					</li>
				</ul> 
			</section>


			<section class="contc-from">
				<div class="col-md-7 contctform-left">
					<h4 class="terms-heading">For anything else; connect with us by filling the fields below; 
						we shall revert to you at the earliest :</h4>

						<form name="frmcontactus"  method="post" id="frm_reachid" action="">
				
						<div class="contct-inpt">
							<label class="contct-inpt-lbl">
								Name
								<span class="red_font">*</span>
							</label>
							<input type="text" name="name" id="contact-name" class="contct-frm-input-txt" placeholder="Enter your full name ...">
						<div id="err-msg-reachus-name" style="color:red; "></div>
					
                                                </div>

						<div class="contct-inpt-email">
							<label class="contct-inpt-lbl">
								Email
								<span class="red_font">*</span>
							</label>
							<input type="text" name="email"  id="contact-email" class="contct-frm-input-txt" placeholder="Enter your Eamil ID...">
						<div id="err-msg-reachus-email" style="color:red; "></div>
					
                                                </div>

						<div class="contct-inpt-phone">
							<label class="contct-inpt-lbl">
								Phone
								<span class="red_font">*</span>
							</label>
							<input type="text" name="phone" id="contact-phone" class="contct-frm-input-txt" placeholder="Enter your Number...">
						<div id="err-msg-reachus-phone" style="color:red; "></div>
					
                                                </div>

						<div class="contct-inpt">
							<label class="contct-inpt-lbl">
								Message 
								<span class="red_font">*</span>
							</label>
							<textarea cols="" rows="7" name="message" id="contact-message" class="contct-frm-input-txtaria" placeholder="Enter your message / feedback ..."></textarea>
						</div>
						<div class="contct-inpt Capecha">
							<label class="contct-inpt-lbl">
								Captcha 
								<span class="red_font">*</span>
							</label>
                                                     
						<?php 
                                                //echo ReCaptcha::widget([ 'name' => 'captcha', ]);?>
                                                    <!--div class="capecha-img"> 
                                                    <input type="text" name="" id="" class="contct-frm-input-txt" placeholder="">
                                                    </input>	
                                                    <div class="capecha-img">
                                                    <img src="images/capecha.jpg">
                                                    -->
                                                     <div class="g-recaptcha" data-sitekey="6LfvQgkUAAAAAAR_PErYgSDIDnvK-dfwnOPQaV61"></div>
                                                    
						</div>
                                                    <input type="button" onclick="return send_contact_mail()" value="Send" class="contct-btn">
			
						<!--button type="button" class="contct-btn">Submit</button-->


						</form>
					</div>

					<div class="col-md-5">
						<div class="contct-right-social">
							<h3>Lets get Social</h3>
							<p class="para-25">Connect with us socially and stay up-to-date on all the latest Onspon happenings!</p>
							<ul class="contct-social">
								<li>
                                                                 <a href="https://www.facebook.com/Onspon" target="_blank" 
                                                                    onMouseOut="MM_swapImgRestore()" 
                                                                    onMouseOver="MM_swapImage('Image5','',
                                                                                'http://www.onspon.com/images/social_media/facebook_h.png',1)">
                                                                 <i class="fa fa-facebook"></i></a></li>
								<li>  <a href="https://twitter.com/onspon" target="_blank" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image6','','http://www.onspon.com/images/social_media/twitter_h.png',1)">

                                                                       <i class="fa fa-twitter"></i></a></li>
								<li> <a href="https://www.linkedin.com/company/onspon" target="_blank" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image7','','http://www.onspon.com/images/social_media/linkedin_h.png',1)">
                                                                 <i class="fa fa-linkedin"></i> </a></li>
                                                                 
								<li><a href="#"><i class="fa fa-youtube"></i></a></li>
							</ul>
							   </div>

                        	             
                                                        
                                                    <div class="fb-like-box" data-href="http://www.facebook.com/Onspon" data-width="294" data-height="335" data-show-faces="true" data-stream="false" data-header="true"></div></div>
                                                        
                                                         
                                                        
                                                        
                                                        
						</div>
					</div>
				</section>			
			</div>
		</div>

	</section>

<script>
    function send_contact_mail()
	{
		err=false;
		var name = $("#contact-name").val();
		var email = $("#contact-email").val();
		var msg=$("#contact-message").val();
               // var grecaptcharesponse=$("#g-recaptcha-response").val();
               var phone=$("#contact-phone").val();
                //alert(grecaptcharesponse);
		$("#err-msg-reachus-name").html("");
		$("#err-msg-reachus-email").html("");
		var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
                
		if(!name)
		{
			$("#err-msg-reachus-name").html("Please enter name");
			$('#contact-name').focus();
			return false;
		}
		if(!email)
		{
			$("#err-msg-reachus-email").html("Please enter email address");
			$('#contact-email').focus();
			return false;
		}
		if (!validateEmail(email))
		{
			$("#err-msg-reachus-email").html("Please enter valid email address");
			$('#contact-email').focus();
			return false;
		}
                if(!phone)
		{
			$("#err-msg-reachus-phone").html("Please enter contact number");
			$('#contact-phone').focus();
			return false;
		}
                if (isNaN(phone))
                {
                    $("#err-msg-reachus-phone").html("Please contact number in number format");
			$('#contact-phone').focus();
			return false;
                }
		else if(!msg)
		{
			$("#err-msg-reachus-message").html("Please enter Message");
			$('#contact-message').focus();
			return false;
		}
		else
		{
			var data='name='+name+'&email='+email+'&text='+msg+'&phone='+phone;
			$.ajax({
				type:'POST',
				data:data,
				url:'<?= Url::to(['/site/contactus']) ; ?>',
				success:function(data)
				{
					if(data == 1) {
						alert("Thank you for your enquiry. We shall revert to you soon. We hope that you have listed your event at onspon.com.");
						location.reload();
					}
					if(data == 0)  {
						alert("Error while sending message");
					}
				}
			})
		}
	}

function validateEmail(email)
	{
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}

function MM_swapImgRestore() { 

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() {

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_findObj(n, d) {

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && d.getElementById) x=d.getElementById(n); return x;

}

function MM_swapImage() {

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}
</script>
<!-- Load Facebook SDK for JavaScript --><div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
