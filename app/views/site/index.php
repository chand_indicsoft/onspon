<?php
use yii\easyii\modules\page\api\Page;
use yii\easyii\modules\text\api\Text;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\SubscribeForm;
use yii\bootstrap\ActiveForm;
use app\models\Common;
use yii\helpers\ArrayHelper;
use app\modules\eventcategories\api\EventCategories;

use app\modules\brandcategories\api\BrandCategories;
use app\modules\industry\api\Industry;

$modeleventcat=new EventCategories;
$modelCommon = new Common;
$modelbrandcat=new BrandCategories;
$modelindustry=new Industry;

$categoryids=$modeleventcat->api_items();
$brandcategoryids=$modelbrandcat->api_items();
$industryids=$modelindustry->api_items();


$asset = \app\assets\AppAsset::register($this);
$page = Page::get('page-index');
$this->title = $page->seo('title', $page->model->title);
$session = Yii::$app->session;

$this->registerJs("$(function () {
	var filterList = {
		init: function () {
			$('#portfoliolist').mixItUp({
				selectors: {
					target: '.portfolio',
					filter: '.filter'
				},
				load: {
					filter: '.box'
				}
			});
		}
	};

	filterList.init();
});

$('[data-viewall]').on('click', function() {
	$('.load-btn-cont')[($(this).data('viewall') == 'hide') ? 'hide' : 'show'](0);
});

$(window).scroll(function(){
	$('header')[($(window).scrollTop() >= 150) ? 'addClass' : 'removeClass']('fix-head');
});

var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : \"body\";
$('input[name=\"date\"], input[name=\"date1\"]').datepicker({
	format: 'mm/dd/yyyy',
	container: container,
	todayHighlight: true,
	autoclose: true,
})");
?><section class="section-banner-cont section-banner home-banner">
	<div class="section-img">
		<video width="100%" height="inherit" loop autoplay muted class="video"><source src="<?= $asset->baseUrl ?>/event.mp4" type="video/mp4"></video>
	</div>
	<div class="section-caption-cont">
		<div class="container">
			<div class="section-caption">
				<h1 class="section-lead-text"> India’s Biggest <span class="red_font list-inline">Sponsorship Marketplace</span> </h1>
				<h3 class="section-sub-lead-text"> List your event and get visibility for various brands </h3>
				<ul class="section-buttons home">
					<li><a href="<?= Url::to(['/eventcreate/create']) ; ?>" class="active">List an Event</a></li>
					<li><a href="<?= ($session['role']==1) ? (Url::to(['sponsor/index'])) : Url::to(['site/login']); ?>">I am a Sponsor</a></li>
				</ul>
			</div>
			<div class="searchfield">
				<ul class="nav nav-tabs banner-form">
					<li class="active"><a data-toggle="tab" href="#organizer">Sponsors</a></li>
					<li><a data-toggle="tab" href="#sponsor">Events</a></li>
				</ul>
				<div class="tab-content" id="avik">
					<div id="organizer" class="tab-pane fade in active">
						<?= Html::beginForm(Url::to(['/brand/brandlisting']), 'get', ['class' => 'form-inline','id'=>'sponsorForm']) ?>
						<div class="inner-addon left-addon city"> <i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>
							<input type="text" class="form-control" placeholder="Location">
						</div>
						<div class="select">
							<?= Html::dropDownList('industryid',null,ArrayHelper::map($industryids,'id','title'),['prompt' => 'Select Industry' ,'class'=> "selectpicker form-control",'data-live-search'=>"true" ]
							)?>
						</div>

						<div class="select">
							<?= Html::dropDownList('brandcatid',null,ArrayHelper::map($brandcategoryids,'id','title'),['prompt' => 'Select Brand Category' ,'class'=> "selectpicker form-control",'data-live-search'=>"true" ]
							)?>
						</div>

						<div class="inner-addon left-addon date">
							<i class="glyphicon glyphicon-calendar" aria-hidden="true"></i>
							<input class="form-control border-right" name="date" placeholder="From Date" type="text"/>
							<input type="hidden" name="search_param" value="name" />
						</div>

						<div class="inner-addon left-addon date">
							<i class="glyphicon glyphicon-calendar" aria-hidden="true"></i>
							<input class="form-control" name="date1" placeholder="To Date" type="text"/>
							<input type="hidden" name="search_param" value="name" />
						</div>

						<div class="inner-addon left-addon search-last"> <i class="glyphicon glyphicon-search"></i>
							<?= Html::textInput('brand_name', '', ['class' => '', 'placeholder' => 'Type Brand Name']) ?>
							<input type="submit" name="search" value="Go"/>
						</div>

						<?= Html::endForm() ?>
						<div class="event-sponsor">Search <span>sponsors</span></div>
					</div>

					<div id="sponsor" class="tab-pane fade">
						<?= Html::beginForm(Url::to(['/events/eventlisting']), 'get', ['class' => 'form-inline','id'=>'eventForm']) ?>

						<div class="inner-addon left-addon city"> <i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>
							<?= Html::textInput('city', '', ['class' => 'form-control', 'placeholder' => 'Type city','aria-describedby'=>"basic-addon1"]) ?>
						</div>

						<div class="select">
							<?= Html::dropDownList('catid',null,ArrayHelper::map($categoryids,'id','title'),['prompt' => 'Select Category' ,'class'=> "selectpicker form-control",'data-live-search'=>"true" ]
							)?>
						</div>

						<div class="inner-addon left-addon date">
							<i class="glyphicon glyphicon-calendar" aria-hidden="true"></i>
							<input class="form-control border-right" name="date" placeholder="From Date" type="text"/>
							<input type="hidden" name="search_param" value="name" />
						</div>

						<div class="inner-addon left-addon date">
							<i class="glyphicon glyphicon-calendar" aria-hidden="true"></i>
							<input class="form-control" name="date1" placeholder="To Date" type="text"/>
							<input type="hidden" name="search_param" value="name" />
						</div>

						<div class="inner-addon left-addon search-last"> <i class="glyphicon glyphicon-search"></i>
							<?= Html::textInput('event_name', '', ['class' => '', 'placeholder' => 'Type event name']) ?>
							<input type="submit" name="search" value="Go"/>
						</div>
						<?= Html::endForm() ?>
						<div class="event-sponsor">Search <span>Event</span></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="clearfix"></div>
	<!--banner end here-->

	<div id="wrapper">
		<div class="container">
			<div class="row">
				<div class="platform">
					<div class="section-heading">
						<h1>Some Stats About <span class="red_font">Our Platform</span></h1>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="set-size charts-container">
					<div class="pie-wrapper progress-45 style-2"> <span class="label">98,83,350 </span>
						<div class="pie">
							<div class="left-side half-circle"></div>
							<div class="right-side half-circle"></div>
						</div>
						<div class="shadow"></div>
						<div class="sponsor red"><a href="javascript:void(0)">Sponsored</a></div>
					</div>
					<div class="pie-wrapper progress-75 style-2"> <span class="label">25,385</span>
						<div class="pie">
							<div class="left-side half-circle"></div>
							<div class="right-side half-circle"></div>
						</div>
						<div class="shadow"></div>
						<div class="sponsor black"><a href="javascript:void(0)">Sponsors</a></div>
					</div>
					<div class="pie-wrapper progress-95 style-2"> <span class="label">4,83,350 </span>
						<div class="pie">
							<div class="left-side half-circle"></div>
							<div class="right-side half-circle"></div>
						</div>
						<div class="shadow"></div>
						<div class="sponsor red"><a href="javascript:void(0)">Organizers</a></div>
					</div>
					<div class="pie-wrapper progress-45 style-2"> <span class="label">15 Mins</span>
						<div class="pie">
							<div class="left-side half-circle"></div>
							<div class="right-side half-circle"></div>
						</div>
						<div class="shadow"></div>
						<div class="sponsor black"><a href="javascript:void(0)"> Spent Time</a></div>
					</div>
					<div class="pie-wrapper progress-75 style-2"> <span class="label">382</span>
						<div class="pie">
							<div class="left-side half-circle"></div>
							<div class="right-side half-circle"></div>
						</div>
						<div class="shadow"></div>
						<div class="sponsor red"><a href="javascript:void(0)">Cities Covered</a></div>
					</div>
					<div class="pie-wrapper progress-95 style-2"> <span class="label">115</span>
						<div class="pie">
							<div class="left-side half-circle"></div>
							<div class="right-side half-circle"></div>
						</div>
						<div class="shadow"></div>
						<div class="sponsor black"><a href="javascript:void(0)">Event Categories</a></div>
					</div>
				</div>
			</div>
		</div>

		<div id="event">
			<div class="section-heading">
				<h1>Events</h1>
			</div>
			<br clear="all">

			<div class="container">
				<ul id="filters" class="clearfix">
					<li><span class="filter active" data-filter=".box" data-viewall="hide">Our Recommendation</span></li>
					<?php
					foreach($eventcategoryList as $_eventcat)
					{
						?><li><span class="filter" data-filter=".<?= $_eventcat->eventcategories_id ?>abc" onclick="getEvent_View(<?=$_eventcat->eventcategories_id?>)" data-viewall="show"><?= $_eventcat->title ?></span></li><?php
					}
					?>
				</ul>

				<div id="portfoliolist">
					<?php
					$wp=1;
					foreach($eventcategoryList as  $_event_cat)
					{
						$gallerydetail=$modelCommon->getEventCategoryImg( $_event_cat->eventcategories_id);
						if($gallerydetail)
						{
							foreach($gallerydetail as $_gallerydetail)
							{
								$themename=($modelCommon->getEventtheme($_event_cat->eventcategories_id));
								?>
								<div class="portfolio <?=$_gallerydetail['event_category_id']?>abc width25" data-cat="<?=$_gallerydetail['event_category_id']?>abc" id="xyz">
									<div class="portfolio-wrapper">
										<?php if($_gallerydetail['image'])
										{
											?> <img src="<?=Yii::$app->homeUrl.$_gallerydetail['image']?>" alt="" />
											<?php
										}
										else
										{
											$defimg=$asset->baseUrl ."/comingsoon1.jpg";?><img src="<?=$defimg?>" alt="" />
											<?php
										}
										?>
									</div>

									<div class="label">
										<div class="label-text"> <a class="text-title"> <?=$_gallerydetail['event_name']?></a> <span class="text-category">October 22, 2015</span> </div>
									</div>
								</div>
								<?php
								$wp++;
							}
						}
					}

					$w=1;
					if($boxgalleryList)
					{
						foreach($boxgalleryList as $_boxgalleryList)
						{
							$width = ($w==2 || $w==7) ? 75 : 25;
							?>
							<div class="portfolio box width<?=$width?>" data-cat="box" id="box">
								<div class="portfolio-wrapper">
									<?php
									if($_boxgalleryList->image)
									{
										?><img src="<?=Yii::$app->homeUrl.$_boxgalleryList->image?>" alt="" /><?php
									}
									else
									{
										?><img src="<?= $asset->baseUrl ."/images/comingsoon1.jpg" ?>" alt="" /><?php
									}
									?><div class="label">
									<div class="label-text">
										<?php
										if($_boxgalleryList->activate_url==1)
										{
											?><a class="text-title" href="<?=$_boxgalleryList->url?>" target="_blank"><?php
										}
										else
										{
											?><a class="text-title" href="events/eventlisting?boxid=<?=$_boxgalleryList->id?>" target="_blank"><?php
										}
										?><span class="text-category"><?php
										if($_boxgalleryList->tag_id)
										{
											$boxgallery=@explode(',',$_boxgalleryList->tag_id);

											foreach($boxgallery as $_boxgallery)
											{
												echo $modelCommon->getTagname($_boxgallery).'&nbsp';
											}
										}
										?></span></a>
									</div>
									<div class="label-bg"></div>
								</div>
							</div>
						</div>
						<?php
						$w++;
					}
				}
				?>
			</div>
			<!--end of portfoliolist-->

			<div class="load-btn-cont" style="display: none;">
				<button class="load-button">View All <span class="load-btn-icons view"></span></button>
			</div>
		</div>
	</div>
	<!-- Event end here -->

	<div class="clearfix"></div>
	<div id="how-work"> <span class="timeline-overlay"></span>
		<div class="container">
			<div class="row">
				<div class="section-heading">
					<h1>How it<span class="red_font"> Works</span></h1>
				</div>

				<div id="timeline">
					<ul>
						<li>
							<div class="timeline-icon timeline-fisrt"></div>
							<div class="timeline-heading">Need Sponsorship ? Create Your Listing</div>
							<p>The first step is to create your event listing on onspon.com which gets you visible to multiple sponsors &amp; audience. You can also opt for premium listing packages for higher visibility and support.</p>
						</li>
						<li>
							<div class="timeline-icon timeline-second"></div>
							<div class="timeline-heading">Need Sponsorship ? Create Your Listing</div>
							<p>The first step is to create your event listing on onspon.com which gets you visible to multiple sponsors &amp; audience. You can also opt for premium listing packages for higher visibility and support.</p>
						</li>
						<li>
							<div class="timeline-icon timeline-third"></div>
							<div class="timeline-heading">Need Sponsorship ? Create Your Listing</div>
							<p>The first step is to create your event listing on onspon.com which gets you visible to multiple sponsors &amp; audience. You can also opt for premium listing packages for higher visibility and support.</p>
						</li>
						<li>
							<div class="timeline-icon timeline-fourth"></div>
							<div class="timeline-heading">Need Sponsorship ? Create Your Listing</div>
							<p>The first step is to create your event listing on onspon.com which gets you visible to multiple sponsors &amp; audience. You can also opt for premium listing packages for higher visibility and support.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!--how it work end here-->

	<div id="about-onspon">
		<div class="section-heading">
			<h1> What Event Say About Us </h1>
		</div>
		<div class="container">
			<div id="onspon" class="carousel slide" data-ride="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<?php $i=0;
					foreach($eventTestimonials as $_eventTestimonials)
					{
						if($i==0)  $class_1="active";

						?><li data-target="#onspon" data-slide-to="<?=$i?>" class="<?=$class_1?>"></li><?php

						$i++;
					}
					?>
				</ol>

				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
					<?php
					$k=1;
					foreach($eventTestimonials as $_eventTestimonials)
					{
						?>
						<div class="item<?= ($k==1) ? " active" : ""; ?>">
							<div class="col-md-7">
								<h3><?= $_eventTestimonials->event ?></h3>
								<img src="<?= $asset->baseUrl ?>/images/stats.jpg"/>
							</div>
							<div class="col-md-5 roshni text-center"> <img src="<?= $asset->baseUrl ?>/images/roshni.jpg">
								<div class="title">
									<?= $_eventTestimonials->title ?>
									<span><?= $_eventTestimonials->designation ?></span>
								</div>
								<p><?= $_eventTestimonials->text ?></p>
							</div>
						</div>
						<?php
						$k++;
					}
					?>
				</div>
			</div>
		</div>
	</div>

	<div class="sponsors">
		<div class="brnd-logo-left">
			<ul class="sponsor-list">
				<?php
				$featuredbrand = $modelCommon->getFeaturedBrands();
				$e=1;

				foreach($featuredbrand as $_featuredbrand)
				{
					if($e==1) $class="text-left";
					if($e==2) $class="text-center";
					if($e==3) $class="text-right";
					?><li class="text-left"><a href="javascript:void(0)"><img src="<?= Yii::$app->homeUrl.$_featuredbrand['featuredlogo'] ?>"></a></li><?php
					$e++;
					if($e==3) $e=1;
				}
				?>
			</ul>
		</div>

		<div class="brnd-logo-right">
			<div class="section-heading">
				<h1> What Sponsors say about <span><img src="<?= $asset->baseUrl ?>/images/white-logo.png"></span> </h1>
			</div>

			<div id="myCarousel" class="carousel slide" data-ride="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<?php
					for($i=0;$i<count($testmonial);$i++)
					{
						if($i==0){ $class="active";}
						?>
						<li data-target="#myCarousel" data-slide-to="<?=$i?>" class="<?=$class?>"></li>
						<?php
					}
					?>
				</ol>

				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
					<?php
					$j=0;

					foreach($testmonial as $_testmonial)
					{
						?><div class="item<?= ($j==0) ? " active" : ""; ?>">
						<div class="roshni text-center">
							<img src="<?= ($_testmonial->image=="") ? $asset->baseUrl ."/images/comingsoon.png" : Yii::$app->homeUrl.$_testmonial->image; ?>">
							<div class="hcl-title">
								<?= $_testmonial->title?>
								<span>
									<?= $_testmonial->designation ?>
								</span></div>
								<?= $_testmonial->text ?>
							</div>
						</div>
						<?php
						$j++;
					}
					?>
				</div>
			</div>
		</div>
	</div>
	<!-- brnd-logo end here-->

	<div class="container">
		<div class="row">
			<div class="engage-with-us">
				<div class="section-heading">
					<h1> Few of the brands that <span class="red_font">Engage with Us</span> </h1>
				</div>

				<div id="engage" class="carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ol class="carousel-indicators">
						<?php
						if(count($brand)>8)
						{
							?><li data-target="#engage" data-slide-to="0"  class="active"></li><?php
						}
						elseif(count($brand) < 8 && count($brand) != 17)
						{
							?><li data-target="#engage" data-slide-to="1" class=""></li><?php
						}
						?>
					</ol>

					<!-- Wrapper for slides -->
					<div class="carousel-inner" role="listbox">
						<?php
						$j=1;
						echo '<div class="item active"><ul class="engage-logo">';
						foreach($brand as $_brand)
						{
							if($j==1 || $j==5) $class="text-left";
							if($j==2 || $j==3 || $j==6 || $j==7) $class="text-center";
							if($j==4 || $j==8) $class="text-right";

							if($_brand->image=='')
							{
								$img= $asset->baseUrl ."/images/comingsoon.png";
							}
							else
							{
								$img=Yii::$app->homeUrl.$_brand->image;
							}

							?><li class="<?=$class?>"><a href="javascript:void(0)"><img src="<?=$img?>"></a></li><?php

							if($j==8)
							{
								echo '</ul></div><div class="item"><ul class="engage-logo">';
								$j=0;
							}
							$j++;
						}
						if(count($brand) && $j<8) echo '</ul></div>';
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--Engage with us container end here-->

<div class="clearfix"></div>
<div class="newsletter">
	<div class="container">
		<h3 class="pull-left">Subscribe Our Newsletter</h3>
		<div class="newsletter-right">
			<div id="err-subs-email"></div>
			<div class="input-group">
				<form name="subcribe-form"  method="post" id="subcribe-form" action="site/subcribeonspon">
					<input type="text" id="subscribe-email"  name="email" placeholder="Type your email address">
					<input type="button" onclick="return send_subscribe()" value="Submit" class="submit-button">
				</form>
			</div>
		</div>
	</div>
</div>
<!--end of newsletter-->
<div class="clearfix"></div>
<div class="write-us">
	<div class="section-heading">
		<h1> Write <span class="red_font">Us</span> </h1>
	</div>
	<div class="container">
		<div class="col-sm-6 contact-form">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi expedita perspiciatis soluta quidem, recusandae sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi expedita perspiciatis soluta quidem, recusandae sapiente.</p>
			<div class="form">
				<!-- <div id="Msgreachus" style="color:red; "></div> -->
				<form name="frmcontactus"  method="post" id="frm_reachid" action="">
					<input type="text" class="name" value="" name="name" placeholder="name" id="contact-name" maxlength="25">
					<div id="err-msg-reachus-name" style="color:red; "></div>
					<input type="text" class="name" value="" name="email" placeholder="email" id="contact-email"  maxlength="55">
					<div id="err-msg-reachus-email" style="color:red; "></div>
					<textarea rows="7" class="textaria" value="" class="masgs-deta" name="message" placeholder="Message" id="contact-message"></textarea>
					<input type="button" onclick="return send_contact_mail()" value="Send" class="send-button">
				</form>
			</div>
		</div>
		<div class="col-sm-6 map">
			<ul>
				<li>
					<div class="map-title"><i class="fa fa-phone" aria-hidden="true"></i> Call Us</div>
					<span>+91 7666003344</span>
				</li>
				<li>
					<div class="map-title"><i class="fa fa-location-arrow" aria-hidden="true"></i>&nbsp;Office location</div>
					<span>A-100, Lourem imspum lorem Mumbai - 401201</span>
				</li>
				<li>
					<div class="map-title"><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;email</div>
					<span>info@onspon.com</span> </li>
				</ul>
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3504.2737176240807!2d77.29209731507953!3d28.56154229404116!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390ce40ccdc30055%3A0x2d65301eec3e83cd!2sIndicsoft+Technologies+Pvt.+Ltd.!5e0!3m2!1sen!2sin!4v1473252283822" width="535" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</div>
	</div>
</div>
</div>
<script>
	function validateEmail(email)
	{
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}

	function send_contact_mail()
	{
		err=false;
		var name = $("#contact-name").val();
		var email = $("#contact-email").val();
		var msg=$("#contact-message").val();
		$("#err-msg-reachus-name").html("");
		$("#err-msg-reachus-email").html("");
		var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
		if(!name)
		{
			$("#err-msg-reachus-name").html("Please enter name");
			$('#contact-name').focus();
			return false;
		}
		if(!email)
		{
			$("#err-msg-reachus-email").html("Please enter email address");
			$('#contact-email').focus();
			return false;
		}
		if (!validateEmail(email))
		{
			$("#err-msg-reachus-email").html("Please enter valid email address");
			$('#contact-email').focus();
			return false;
		}
		else if(!msg)
		{
			$("#err-msg-reachus-message").html("Please enter Message");
			$('#contact-message').focus();
			return false;
		}
		else
		{
			var data='name='+name+'&email='+email+'&text='+msg;
			$.ajax({
				type:'POST',
				data:data,
				url:'<?= Url::to(['/site/contactus']) ; ?>',
				success:function(data)
				{
					if(data == 1) {
						alert("Thank you for your enquiry. We shall revert to you soon. We hope that you have listed your event at onspon.com.");
						location.reload();
					}
					if(data == 0)  {
						alert("Error while sending message");
					}
				}
			})
		}
	}

	function send_subscribe()
	{
		var semail = $("#subscribe-email").val();
		if(!semail)
		{
			$("#err-subs-email").html("Please enter email address");
			$('#subscribe-email').focus();
			return false;
		}
		if (!validateEmail(semail))
		{
			$("#err-subs-email").html("Please enter valid email address");
			$('#subscribe-email').focus();
			return false;
		}
		else
		{
			var data='semail='+semail;
			$.ajax({
				type:'POST',
				data:data,
				url:'<?= Url::to(['/site/subcribeonspon']) ; ?>',
				success:function(data)
				{
					if(data == 1)
					{
						alert("Thank you Subcribe with us. We shall revert to you soon.");
						location.reload();
					}
					if(data == 0)
					{
						alert("Error while sending message");
					}
					if(data == 2)
					{
						alert("Already subsribe");
					}
				}
			})
		}
	}
	function getEvent_View(cat_id)
	{
		$(".buttonevent").attr("href",'');
		var _href = $(".buttonevent").attr("href");
		$(".buttonevent").attr("href", _href + 'events/eventlisting?catid='+cat_id);
	}
</script>
