<?php
$asset = \app\assets\AppAsset::register($this);
	
?>

<section class="section-banner-cont section-banner sec-bnr-300">
  <div class="section-img focuspoint" data-focus-x="0" data-focus-y="0">
    <img src="<?= $asset->baseUrl ?>/images/message-banner.jpg">
  </div>

  <div class="section-caption-cont">
    <div class="container">
      <div class="section-caption">
        <h1 class="section-lead-text">
          Welcome to 
          <span class="red_font list-inline">onspon.com</span>
        </h1>
        <h3 class="section-sub-lead-text">
          Where best "Events" are discovered
        </h3>
       
      </div>
    </div>
  </div>

 
</section>

<div class="clearfix"></div>
<!--banner end here-->

<section class="section-text-cont">
  
  <div class="container">
    <div class="row">
      <div class="section-heading">
        <h1>
       About
          <span class="red_font"> Onspon</span>
        </h1>
      </div>
   <div class="clearfix"></div>
     
     <div class="about-us">
    <p class="dropCap"><span>onspon.com</span>  was founded with the objective of automating the process of making sponsorship decisions, and securing access to timely sponsorship.
  </p>
  <p>Having been on both sides of the table in respective times of their career, the team at Onspon understand how important it is for any event to secure sponsorship. We also fully understand how essential it is for a brand to find the most befitting / high ROI yielding avenues. And Onspon.com is a platform which aims to do just that!</p>
     
     <p>Onspon.com is the brain child of a bunch of senior executives with a blue-chip pedigree: premier B-schools such as IIM / INSEAD, and best in class corporates and integrated marketing organizations globally. And in it's first year of operations, Onspon.com now has over 6000 event managers registered, and a brand reach of more than 300 brands! We are sure that we will see an even greater traction in the coming year as we expand our reach and our team.</p>
     
     <p>We are always looking for feedback. Please tweet to us <a href="#" class="red_font">@onspon</a> should you have feedback.</p>
     
     </div><!--About us end here-->
     
    </div>
  </div>

</section>

