<?php
$asset = \app\assets\AppAsset::register($this);
	
?>
<section class="section-banner-cont section-banner sec-bnr-300">
  <div class="section-img focuspoint" data-focus-x="0" data-focus-y="0"> <img src="<?= $asset->baseUrl ?>/images/message-banner.jpg"> </div>
  <div class="section-caption-cont">
    <div class="container">
      <div class="section-caption">
        <h1 class="section-lead-text"> Welcome to <span class="red_font list-inline">onspon.com</span> </h1>
        <h3 class="section-sub-lead-text"> Where best "Events" are discovered </h3>
        
      </div>
    </div>
  </div>
 
</section>
<div class="clearfix"></div>
<!--banner end here-->

<section class="section-text-cont">
  <div class="container">
    <div class="row">
      <div class="section-heading">
        <h1>How it works <span class="red_font">@ Onspon</span> </h1>
      </div>
    
      <div class="clearfix"></div>
    </div>
  </div>

  <div id="how-work" class="how-work-cont">
        <div class="container">
          <div class="row">
              <div class="how-work-btn"><div>Event Owner looking for Sponsorship</div></div>
            <div id="timeline">
              <ul>
                <li>
                  <div class="timeline-icon timeline-fisrt"></div>
                  <div class="timeline-heading">Need Sponsorship ? Create Your Listing</div>
                  <p>The first step is to create your event listing on onspon.com which gets you visible to multiple sponsors &amp; audience. You can also opt for premium listing packages for higher visibility and support.</p>
                </li>
                <li>
                  <div class="timeline-icon timeline-second"></div>
                  <div class="timeline-heading">Need Sponsorship ? Create Your Listing</div>
                  <p>The first step is to create your event listing on onspon.com which gets you visible to multiple sponsors &amp; audience. You can also opt for premium listing packages for higher visibility and support.</p>
                </li>
                <li>
                  <div class="timeline-icon timeline-third"></div>
                  <div class="timeline-heading">Need Sponsorship ? Create Your Listing</div>
                  <p>The first step is to create your event listing on onspon.com which gets you visible to multiple sponsors &amp; audience. You can also opt for premium listing packages for higher visibility and support.</p>
                </li>
                <li>
                  <div class="timeline-icon timeline-fourth"></div>
                  <div class="timeline-heading">Need Sponsorship ? Create Your Listing</div>
                  <p>The first step is to create your event listing on onspon.com which gets you visible to multiple sponsors &amp; audience. You can also opt for premium listing packages for higher visibility and support.</p>
                </li>
              </ul>
            </div>
            <div class="how-work-btn"><div>Event Owner looking for Sponsorship</div></div>
          </div>
        </div>
      </div>



</section>



