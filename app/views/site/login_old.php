
<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use app\models\LoginForm;
use app\modules\role\api\Role;
//$assetHead = \app\assets\AppHeadAsset::register($this);
$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>


    <div class="container">
       
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h1><?= Html::encode($this->title) ?></h1>
                        </div>
                        <div class="panel-heading">
                             
                        </div>
                        <?php   $model = new LoginForm;?>
    
			            <?php $form = ActiveForm::begin([
			                'id' => 'login-form',
			                'action' => Url::to(['/site/login']),
			                'enableAjaxValidation' => FALSE,
			                 
			                ]); ?>
                        <div class="site-login">
							<?= $form->field($model, 'role')->dropDownList(Role::roleName(), ['prompt'=>'Choose User Type']);  ?>
							<?php $role = $model->role; ?>
							<?= yii\authclient\widgets\AuthChoice::widget([
							     'baseAuthUrl' => ['site/auth']
							]) ?>
							
							
                            <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

                            <?= $form->field($model, 'password')->passwordInput() ?>

                            <?= $form->field($model, 'rememberMe')->checkbox() ?>

                            <div style="color:#999;margin:1em 0">
                                If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.
                            </div>

                            <div class="form-group">
                                <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                               
                                <span style="float:right;margin-right: 40px;">New to OnSpon? <?= Html::a('sign up', ['site/signup']) ?><span>
                            </div>

                            <?php ActiveForm::end(); ?>




                        </div>
                    </div>
                </div>
            </div>
       
    </div>
