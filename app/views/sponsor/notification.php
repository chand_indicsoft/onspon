<?php
use yii\helpers\Url;
use app\models\Commonhelper;


?>
<?php if($inbox): foreach($inbox as $item):
$timeage = Commonhelper::getTimeago($item['date']);?>
<a href="<?= Url::to(['sponsor/inboxitem','id'=>$item['id']]);?>" 
class="ntfction-anchor-cont <?php echo $calss = ($item['status'] =='0') ? 'read': '';?>">
<div class="article-thumb-img">
              <?php if($item['logo']){ ?><img src="<?= Yii::$app->homeUrl.$item['logo'];?>"><?php  } ?>
</div>
<div class="article-thumb-text-container">
<h6 class="article-thumb-heading">
 <?= substr($item['message'],0,70).'...';?>
</h6>
<span class="notifi-evnt-time">
  <i class="fa fa-clock-o" aria-hidden="true"></i>
 <?= $timeage;?>
</span></div></a>

<?php endforeach; endif;?>


