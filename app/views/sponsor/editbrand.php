<?php

use yii\helpers\Url;
use app\models\Brandmandates;
use app\models\Common;
$commonModel = new Common;

$session = Yii::$app->session;

$asset = \app\assets\SponsorAsset::register($this);
$brandlist = Brandmandates::Select_by_user($session['user_id']);


include('header.php');

?>
<section class="section-banner-cont section-banner">
  <div class="section-img ocuspoint" data-focus-x="0" data-focus-y="0">
    <img src="<?= $asset->baseUrl ?>/images/section-banner1.jpg">
  </div>

  <div class="section-caption-cont">
    <div class="container">
      <div class="section-caption">
        <h1 class="section-lead-text">
          Welcome to 
          <span class="red_font list-inline">onspon.com</span>
        </h1>
        <h3 class="section-sub-lead-text">
          Where best "Events" are discovered
        </h3>
        <ul class="section-buttons">
          <li><a href="<?= Url::to(['inbox']) ; ?>" class="">My Inbox</a></li>
          <li><a href="<?= Url::to(['lastminutesdeals']) ; ?>">Last Minute Deals</a></li>
        </ul>
      </div>
    </div>
  </div>

  <div class="explore-events-cont red_bg">
      <div class="container">
        <p>
          More than
          <span class="font_wieght700 white_font list-inline three100"> 300</span> 
          brand managers visit <span class="font_wieght700 list-inline onspon_com"> onspon.com</span> 
          to find the best events. Let's start by 
          <a href="<?= Url::to('createbrand');?>" class="white_font font_wieght500 list-inline exp-evt-link"> creating your brand mandate (?)</a> 
          or 
          <a href="#" class="white_font font_wieght500 list-inline exp-evt-link">browse available events (?)</a>
        </p>
    </div>
  </div>
</section>

<div class="clearfix"></div>

<section class="section-text-cont">
  
  <div class="container">
    <div class="row">
      <div class="section-heading">
        <h1>
          Our
          <span class="red_font"> Mandates</span>
        </h1>
      </div>

      
      <div class="clearfix"></div>
  <?php if($brandlist):?>    
    <div class="brnd-list-cont">
	<?php foreach($brandlist as $item) : ?>
    <div class="col-lg-6 col-md-6 col-sm-6">
		<div class="listed-evnt-cont">
		  <div class="article-thumb-img focuspoint" data-focus-x="0" data-focus-y="0">
			<?php if($item['logo']){ ?><img src="<?= Yii::$app->homeUrl.$item['logo'];?>">
			<?php } else { ?>
				<img src="">
			<?php } ?>
		  </div>
		  <div class="article-thumb-text-container">
			<h3 class="article-thumb-heading"><?= $item['brand_name'];?></h3>
			<ul class="article-thumb-info">
			  <li><i class="fa fa-industry" aria-hidden="true"></i><?= $item['iname'];?></li>
			  <li><span class="listing-icon placeholder"></span><?= $item['city_id'];?></li>
			  <li> <i class="fa fa-search" aria-hidden="true"></i><?= $item['i_am_looking'];?></li>
			</ul>
		  </div>
		  <div class="evnt-edt-rmv-cont">
			<ul>
			  <li>
				<a href="<?= Url::to(['editbrand','id'=>$item['id']]) ; ?>"><i class="fa fa-pencil"></i></a>
				<span class="edt-rmv-tooltip">Edit</span>
			  </li>
			  <li>
				<a href="javascript:void(0)" onclick="DeleteBrandmandates(<?= $item['id'];?>);"><i class="fa fa-trash"></i></a>
				<span class="edt-rmv-tooltip">Delete</span>
			  </li>
			</ul>
		  </div>
		</div>
	  </div>
    <?php endforeach;?>
    

    </div>
   <?php endif;?> 
    </div>
  </div>

</section>

<script>
function DeleteBrandmandates(id)
{
	var data = 'id='+id;
	$.ajax({
		url: '<?= Url::to(['deletebrand']) ; ?>',
		data: data,
		type: 'post',
		success:function(result){
			alert(result);
			location.reload();
			}
		});
}

</script>


