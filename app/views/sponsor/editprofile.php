<?php 
use yii\widgets\ActiveForm;
use app\modules\industry\api\Industry;
use app\modules\eventgenre\api\EventGenre;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\easyii\helpers\Image;
use yii\easyii\widgets\TagsInput;

$asset = \app\assets\SponsorAsset::register($this);
if($model->intrested_area){
	$model->intrested_area = explode(",",$model->intrested_area);
	
}

include('header.php');

$this->registerJs('$(document).ready( function() {
        $("#alert-message-falsh").delay(2000).fadeOut();
      });');

?>


<section class="section-banner-cont section-banner">
  <div class="section-img ocuspoint" data-focus-x="0" data-focus-y="0">
    <img src="<?= $asset->baseUrl ?>/images/section-banner1.jpg">
  </div>

  <div class="section-caption-cont">
    <div class="container">
      <div class="section-caption">
        <h1 class="section-lead-text">
          Welcome to 
          <span class="red_font list-inline">onspon.com</span>
        </h1>
        <h3 class="section-sub-lead-text">
          Where best "Events" are discovered
        </h3>
        <ul class="section-buttons">
          <li><a href="<?= Url::to(['inbox']) ; ?>" class="">My Inbox</a></li>
          <li><a href="<?= Url::to(['lastminutesdeals']) ; ?>">Last Minute Deals</a></li>
        </ul>
      </div>
    </div>
  </div>

  <div class="explore-events-cont red_bg">
      <div class="container">
        <p>
          More than
          <span class="font_wieght700 white_font list-inline three100"> 300</span> 
          brand managers visit <span class="font_wieght700 list-inline onspon_com"> onspon.com</span> 
          to find the best events. Let's start by 
          <a href="<?= Url::to('createbrand');?>" class="white_font font_wieght500 list-inline exp-evt-link"> creating your brand mandate (?)</a> 
          or 
          <a href="#" class="white_font font_wieght500 list-inline exp-evt-link">browse available events (?)</a>
        </p>
    </div>
  </div>
</section>

<div class="clearfix"></div>

<section class="section-text-cont">
  
  <div class="container">
    <div class="row">
		
<?php if(Yii::$app->session->hasFlash('error') OR Yii::$app->session->hasFlash('success')) { ?>
	 
<div id="alert-message-falsh"  class="prfl-upd-succes">  
<?php if(Yii::$app->session->hasFlash('error')): ?>
<div class="alert alert-error">
<?php echo Yii::$app->session->getFlash('error'); ?>
</div>
<?php endif; ?>

<?php if(Yii::$app->session->hasFlash('success')): ?>
<div class="alert alert-success">
<?php echo Yii::$app->session->getFlash('success'); ?>
</div>
<?php endif; ?>
</div>

<?php } ?>	
	
      <div class="section-heading">
        <h1>
          Edit
          <span class="red_font">Profile</span>
        </h1>
      </div>

      <div class="edt-frm-container">
      <?php $form = ActiveForm::begin([
			'id' => 'brandform1',
			'enableAjaxValidation' => false,
			 'action' => Url::to(['/sponsor/editprofile']),
			'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
		]); ?> 
            <div class="usr-img-cont">
              <div class="usr-img">
				 <?php if($model->image){ ?>
					 <img src="<?= Yii::$app->homeUrl.Image::thumb($model->image, 170,180);?>" id="profileblah">
					 <?php } else {  ?>
					<img src="" id="profileblah">
					<?php } ?>
              </div>
              <?= $form->field($model, 'image')->fileInput(['onchange'=>'readURLPROFILE(this);'])->label(false); ?>
              <div class="evnt-edt-rmv-cont">
                  <ul>
					  <li>
                      <a href="javascript:void(0);" onclick="upladImage();"><i class="fa fa-camera"></i>
                      <span class="edt-rmv-tooltip">Change</span></a>
                    </li>
                    <li>
                      <a href="<?= Url::to(['sponsor/clearprofileimage', 'id' => $model->id]) ?>"><i class="fa fa-trash"></i>
                      <span class="edt-rmv-tooltip">Delete</span></a>
                    </li>
                  </ul>
              </div>
              <span class="usr-img-cam-icon">
                <span class="cam-icon"></span>
              </span>
            </div>
            <div class="usr-details">
              <div class="usr-input-cont left">
                <label class="usr-inpt-lbl">
                  <span class="usr-inpt-icon usr-icon"></span>
                  Full Name
                  <span class="red_font">*</span>
                </label>
                <?= $form->field($model, 'brand_name')->textInput(['autocomplete'=>'off','class'=>'edt-frm-input'])->label(false);?>
              </div>

              <div class="usr-input-cont right">
                <label class="usr-inpt-lbl">
                  <span class="usr-inpt-icon phone-no"></span>
                  Mobile Number
                  <span class="red_font">*</span>
                </label>
                <?= $form->field($model, 'mobile')->textInput(['autocomplete'=>'off','class'=>'edt-frm-input'])->label(false);?>
              </div>

               <div class="usr-input-cont left">
                <label class="usr-inpt-lbl">
                  <span class="usr-inpt-icon mail-icon"></span>
                  Email ID
                  <span class="red_font">*</span>
                </label>
                <?= $form->field($model, 'email')->textInput(['autocomplete'=>'off','class'=>'edt-frm-input'])->label(false);?>
              </div>

               

               <div class="usr-input-cont right">
                <label class="usr-inpt-lbl">
                  <span class="usr-inpt-icon category-icon"></span>
                  Industry
                  <span class="red_font">*</span>
                </label>
                 <?= $form->field($model, 'industry')->dropDownList(Industry::industryName(), ['class'=>'edt-frm-select'])->label('');  ?>
              </div>
              
              <div class="usr-input-cont left">
                <label class="usr-inpt-lbl">
                  <span class="usr-inpt-icon org-icon"></span>
                  Intrested Areas
                  <span class="red_font">*</span>
                </label>
                <?= $form->field($model, 'intrested_area')->label(false)->checkboxList(EventGenre::eventgenreName(),[]); ?>
              </div>
              
              <div class="usr-input-cont right">
                <label class="usr-inpt-lbl">
                  <span class="usr-inpt-icon add-icon"></span>
                  Address
                  <span class="red_font">*</span>
                </label>
					<?= $form->field($model, 'address')->textarea(['autocomplete'=>'off','class'=>'edt-frm-textarea','rows'=>'5'])->label(false); ?>
               </div>

              <div class="usr-input-cont left input-sml23 input-sml23-1">
                <label class="usr-inpt-lbl">
                  <span class="usr-inpt-icon lnd-icon"></span>
                  Landmark
                  <span class="red_font">*</span>
                </label>
                <?= $form->field($model, 'landmark')->textInput(['autocomplete'=>'off','class'=>'edt-frm-input'])->label(false);?>
              </div>

              <div class="usr-input-cont left input-sml23">
                <label class="usr-inpt-lbl">
                  <span class="usr-inpt-icon zip-icon"></span>
                  Zip Code
                  <span class="red_font">*</span>
                </label>
                <?= $form->field($model, 'pincode')->textInput(['autocomplete'=>'off','class'=>'edt-frm-input'])->label(false);?>
              </div>
            </div>
            <?= Html::submitButton('Update Profile', ['class' => 'usr-frm-submit','name' => 'login-button']) ?>
            
         <?php ActiveForm::end(); ?>
      </div>
    </div>
  </div>

</section>




<script type="text/javascript">
	
function upladImage()
{
	$('#sponsorprofile-image').trigger('click'); 
}	
	
	
	
function readURLPROFILE(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#profileblah').attr('src', e.target.result);	
		}
		reader.readAsDataURL(input.files[0]);
	}
}
</script>

