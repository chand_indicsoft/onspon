<?php
use yii\helpers\Url;
use app\models\Commonhelper;

?>
<?php if($list): foreach($list as $item):

	$date = date("d F Y", strtotime($item['date']));	
	$time = date("H:i a", strtotime($item['date']));
	
?>			
<div class="msgHeader"><div>
<ul>
<li>
	<div class="delTooltip" id="del"> Delete</div>
		<a href="javascript:void(0)" onclick="DeleteInbox(<?= $item['id'].','."'".$type."'";?>);" id="DeltoolTip" onmouseover="deleteOver();" onmouseout="deleteOut();"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
</li>
</ul>
</div>
<ul>
	<li><i class="fa fa-calendar" aria-hidden="true"></i><?= $date;?></li>
	<li><i class="fa fa-clock-o" aria-hidden="true"></i><?= $time;?></li>
</ul>
</div>
<div class="msg_Innr_Contr">
	<p>
		<?= $item['message'];?>
	</p>
	<p><?= $item['username'];?></p>
</div>
<?php endforeach; endif;?>


