<?php 
use yii\widgets\ActiveForm;
use app\modules\industry\api\Industry;
use app\modules\eventgenre\api\EventGenre;
use yii\helpers\Url;
use yii\helpers\Html;

$asset = \app\assets\SponsorAsset::register($this);

$this->registerJs('$(window).load(function() {
		document.body.className = "header_bg";
		$("#sponsorprofile-location label:eq(2)").click(function() {
			$("#toggle-search").show();
		});
		});');


?>

<header>
  <a href="#" class="top-logo"></a>
  <div class="top-menu-bar">
    <div class="hamburger-menu toggle-x">
      <span class="arrows"></span>
         <nav id="menu">
       <ul>
       <li><a href="<?= Url::home() ?>" class="active">Home</a></li>
       <li><a href="#">About</a></li>
       <li><a href="#">Contact</a></li>    
       <li><a href="#">Blog</a></li>
       <li><a href="<?= Url::home() ?>site/logout">Logout</a></li>
    </ul>
 </nav> 
    </div>
    <ul>
      <li>
        <a>
          <span class="menu-icon phone"></span>
          + 91 7666003344              
        </a>
      </li>
      
    </ul>
  </div>
</header>
<!--banner end here-->

<section class="section-text-cont">
  
  <div class="container">
    <div class="row">


      <div class="login-box-container">
        <div class="container">
          <div class="login-main-container">
            <div class="login-form-container">
             
		<?php $form = ActiveForm::begin([
			'id' => 'brandform1',
			'enableAjaxValidation' => false,
			 'action' => Url::to(['/sponsor/profile']),
			'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
		]); ?> 
            
                <div class="login-heading-text">
                  <span>Tell us more about you</span>
                  <div class="crose"></div>
                </div>
                <div class="who_r_u">
                  What's Your Industry
                </div>
                <?= $form->field($model, 'industry')->dropDownList(Industry::industryName(), ['prompt'=>'Choose Industry','class'=>'common-block'])->label('');  ?>
                
                  <div class="who_r_u">
                  Industry brand name
                </div>
               <div class="login-input-cont industry">
				  <?= $form->field($model, 'brand_name')->textInput(['autocomplete'=>'off','class'=>'industry-input'])->label(false);?>
                </div>
               
               <div class="checkbox-cont">
                <div class="who_r_u">
                  Interested Fields
                </div>
               <div class="checkbox-parent">
                    <?= $form->field($model, 'intrested_area')->label(false)->checkboxList(EventGenre::eventgenreName(),[]); ?>
               
                 </div>
               </div>
               
               <div class="checkbox-cont radio">
                <div class="who_r_u">
                  Location
                </div>
               	<?= $form->field($model, 'location')->radioList(array('Pan India'=>'Pan India','Metros'=>'Metros','Specific' =>'Specific'))->label(false); ?>
                
                 <div class="login-input-cont" id="toggle-search">
                   <span class="close-search" onclick="removetextbox();"></span>
                  <?= $form->field($model, 'location_specific')->textInput(['autocomplete'=>'off','class'=>'industry-input'])->label(false);?>
                </div>
                <?= Html::submitButton('Save', ['class' => 'login-submit-btn','name' => 'login-button']) ?>
               </div>
  
  <?php ActiveForm::end(); ?>           
  
            </div>
            <div class="login-text-area">
            
              <div class="login-onspon-logo"></div>
              <p class="login-text">
                ONSPON is India's biggest platform which helps event managers reach out to multiple sponsors and event audience at the click of a button. More than 10,000 event managers, 350+ brands and millions of event goers have made it their platform of choice.
              </p>
            </div>
          </div>

        </div>
      </div>


    </div>
  </div>

</section>
<script type="text/javascript">





	
function addtextbox()
{
	$("#toggle-search").show();
}

function removetextbox()
{
	$("#toggle-search").hide();
}
</script>

