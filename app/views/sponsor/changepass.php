<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;


$asset = \app\assets\SponsorAsset::register($this);
$session = Yii::$app->session;
$existevent = Yii::$app->getDb()->createCommand("SELECT count(*) as total FROM app_brandmandates WHERE user_id='".$session['user_id']."'")->queryone();

include('header.php');

$this->registerJs('$(window).load(function() {
document.body.className = "header_bg";    
});');

$this->registerJs('$(document).ready( function() {
        $("#alert-message-falsh").delay(2000).fadeOut();
      });');

?>






<section class="section-text-cont">
  
  <div class="container">
    <div class="row">

<?php if(Yii::$app->session->hasFlash('error') OR Yii::$app->session->hasFlash('success')) { ?>
	 
<div id="alert-message-falsh"  class="prfl-upd-succes"> 	 
<?php if(Yii::$app->session->hasFlash('error')): ?>
<div class="alert alert-error alert-success">
<?php echo Yii::$app->session->getFlash('error'); ?>
</div>
<?php endif; ?>

<?php if(Yii::$app->session->hasFlash('success')): ?>
<div class="alert alert-success">
<?php echo Yii::$app->session->getFlash('success'); ?>
</div>
<?php endif; ?>
</div>	
<?php } ?>	

      <div class="login-box-container">
        <div class="container">
          <div class="login-main-container">
            <div class="login-form-container">
           <?php $form = ActiveForm::begin([
			'id' => 'brandform1',
			'enableAjaxValidation' => false,
			 'action' => Url::to(['/sponsor/changepassword']),
			'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
		]); ?> 
               
                <div class="login-heading-text">
                 CHANGE PASSWORD
                </div>
              
                <div class="login-input-con">
					<?= $form->field($model, 'confirm_password')->textInput(['placeholder' =>'Current Password','autocomplete'=>'off','class'=>'change-pass'])->label(false);?>
                </div>
             
             	 <div class="login-input-con">
					 <?= $form->field($model, 'password')->textInput(['placeholder' =>'New Password','autocomplete'=>'off','class'=>'change-pass'])->label(false);?>
                </div>
                 <div class="login-input-con">
					 <?= $form->field($model, 'repeat_password')->textInput(['placeholder' =>'Confirm New Password','autocomplete'=>'off','class'=>'change-pass'])->label(false);?>
                </div>
             
				<?= Html::submitButton('Submit', ['class' => 'login-submit-btn register-btn','name' => 'login-button']) ?>
                
               
           <?php ActiveForm::end(); ?>
            </div>
            <div class="login-text-area">
              <div class="login-onspon-logo"></div>
              <p class="login-text">
                ONSPON is India's biggest platform which helps event managers reach out to multiple sponsors and event audience at the click of a button. More than 10,000 event managers, 350+ brands and millions of event goers have made it their platform of choice.
              </p>
            </div>
          </div>

        </div>
      </div>


    </div>
  </div>

</section>



