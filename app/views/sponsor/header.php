<?php
use yii\helpers\Url;
use app\models\Brandmandates;

$session = Yii::$app->session;

$existevent = Yii::$app->getDb()->createCommand("SELECT count(*) as total FROM app_brandmandates WHERE user_id='".$session['user_id']."'")->queryone();

		
$asset = \app\assets\SponsorAsset::register($this);
$this->registerJs('$(window).load(function() {

		$(".notification-cont").click(function(){
		  $.ajax({
				url:"'.Url::to(["inboxhome"]).'",
				dataType: "html",
				success:function(result){
				$(".notifi-sub-cont").html(result);
				$(".notification-toggle-cont").slideToggle(500);
				}
			});
      });

     $(".login").click(function(e){
     var className = $(".user-erow").attr("class");
		if(className=="user-erow"){
			$(".user-erow").addClass("open");
		}
		else
		{
			$(".user-erow").removeClass("open");
		}
     $(".login-data-cont").slideToggle(500);
		 e.stopPropagation();
		 $(".notification-toggle-cont").hide(500);
	});
	
	 $(document).click(function (event) {
			$(".notification-toggle-cont").hide(500);
			$(".user-erow").removeClass("open");
			 $(".login-data-cont").hide(500);
			 
		});
      
});');

$notification = Brandmandates::UserTotalNotification($session['user_id']);

?>

<style>
.user-erow{ margin-right:5px; float:left; font-size:14px;}
span.user-erow.open{transform: rotateX(180deg); transition: all 0.3s ease-in-out;}
</style>

<header>
  <a href="#" class="top-logo"></a>
  <div class="top-menu-bar">
    <div class="hamburger-menu toggle-x">
      <span class="arrows"></span>
         <nav id="menu">
			<?php include('sidemenu.php');?>
		</nav> 
    </div>
    <ul>
     
      <li class="login">
       <a><span class="user-erow"><i class="fa fa-chevron-down" aria-hidden="true"></i></span> <?=$session['user_tmpid1']?></a>
        <div class="login-data-cont" style="display:none;">
        	<ul class="login-list">
            <li><a href="<?= Url::to(['index']) ; ?>">My Dashboard</a></li>
             <li><a href="<?= Url::to(['brandlist']) ; ?>">My Brand Mandates</a></li>
            <li><a href="<?= Url::to(['inbox']) ; ?>">Inbox</a></li>
            <li><a href="<?= Url::to(['editprofile']) ; ?>">Edit Profile</a></li>
            <li><a href="<?= Url::to(['changepassword']) ; ?>">Change Password</a></li>
            <li><a href="<?= Url::home() ?>site/logout">Logout </a></li>
            </ul>
        </div>
      </li>
      <li><a><span class="menu-icon phone"></span>+ 91 7666003344</a></li>
      <li class="top-menu-seprate-btn"><a href="<?= Url::to('createbrand');?>"><?= ($existevent['total'] == '0') ? 'Create Brand Mandates' :'Add Brand Mandates';?></a></li>
      <li class="notification-cont">
        <a>
          <span class="bell-icon"></span>
         <?php if($notification):?> <span class="notification-counter"><?=$notification['total_notification'];?></span><?php endif;?>
        </a>
        <div class="notification-toggle-cont" style="display: none;">
          <div class="notifi-sub-cont">
          </div>
          <a href="#" class="ntfction-see-all">See All</a>
        </div>
      </li>
    </ul>
  </div>
</header>





