<?php

use yii\helpers\Url;
use app\models\Brandmandates;
use app\models\Common;
$commonModel = new Common;

$asset = \app\assets\SponsorAsset::register($this);
$currentdate = strtotime(date("d.m.Y"));
$session = Yii::$app->session;
$result = Brandmandates::Select_by_user($session['user_id']);
$cat = Brandmandates::Selcetcategory_City($session['user_id']);
$catid = '';
$cityid = '';
foreach($cat as $cat1)
{
	$catid .= ','.$cat1['event_category_id'];
	$cityid .= ','.$cat1['city_id'];
}
$eventcat= array_filter(array_unique(explode(",",$catid)));
$city= array_filter(array_unique(explode(",",$cityid)));

/******* boxes list count *******/
$totalrec = Brandmandates::TotalRecmendation($eventcat,$city,$currentdate);
$totalInbox = Brandmandates::TotalInboxlist($session['user_id']);
$totaltrending = Brandmandates::TotalTrending($currentdate);
$totallastminute = Brandmandates::TotalLastminutes($currentdate);
$totalshortlist = Brandmandates::totalShortlist($session['user_id']);

/******* end boxes list count *******/

$industryid = Yii::$app->getDb()->createCommand("SELECT industry FROM app_sponsor_profile WHERE user_id='".$session['user_id']."' ")->queryone();
$industrayname = Yii::$app->getDb()->createCommand("SELECT title FROM app_industry WHERE id='".$industryid['industry']."' ")->queryone();
$eventviewd = Brandmandates::eventvewedByOtherBrand($industryid['industry']);
$eventRecomende = Brandmandates::RecomendedEvents($eventcat,$city,$currentdate);
$shortlist = Brandmandates::Shortlistevent($session['user_id']);
$folderlist = $commonModel->UserShortlistfolder(Yii::$app->session['user_id']);
$trendingEvents = Brandmandates::trendingEventDashboard($currentdate);
$amazingEvent = Brandmandates::AmazingEventDashboard($currentdate);

include('header.php');

$this->registerJs('$(window).scroll(function() {
            if ($(this).scrollTop() > 150){
            $(".fix-brand").addClass("sticky");
            }
            else{
            $(".fix-brand").removeClass("sticky");
            }
            });');

?>

<section class="section-banner-cont section-banner sec-bnr-400">
  <div class="section-img ocuspoint" data-focus-x="0" data-focus-y="0">
    <img src="<?= $asset->baseUrl ?>/images/section-banner1.jpg">
  </div>

  <div class="section-caption-cont">
    <div class="container">
      <div class="fix-brand">
       <div class="brand-evnt-fx-txt"> <?php echo $session['user_tmpid1'] .' ( '.$industrayname['title'].' )';?></div>
      <div class="section-cap-btns">
        <ul>
          <li><a href="<?= Url::to(['inbox']) ; ?>"> Inbox <span><?php echo $totalInbox ? $totalInbox : '0';?></span></a></li>
          <li><a href="#rec-event"> Recommended Events <span><?php echo $totalrec ? $totalrec : '0';?></span></a></li>
          <li><a href="#tre-event"> Trending Events <span><?php echo $totaltrending ? $totaltrending : '0';?></span></a></li>
          <li><a href="#short-event"> Shortlisted Events <span><?php echo $totalshortlist ? $totalshortlist : '0';?></span></a></li>
          <li><a href="<?= Url::to(['lastminutesdeals']) ; ?>"> Last Minute Deals <span><?php echo $totallastminute ? $totallastminute : '0';?></span></a></li>
        </ul>
      </div>
      <div class="strip"><a href="#">Created a brand mandate and make events ‘apply’ to you</a> </div>
    </div>
    </div>
  </div>

  <div class="explore-events-cont red_bg">
      <div class="container">
        <p>
          More than
          <span class="font_wieght700 white_font list-inline three100"> 300</span> 
          brand managers visit <span class="font_wieght700 list-inline onspon_com"> onspon.com</span> 
          to find the best events. Let's start by 
          <a href="#" class="white_font font_wieght500 list-inline exp-evt-link"> creating your brand mandate (?)</a> 
          or 
          <a href="#" class="white_font font_wieght500 list-inline exp-evt-link">browse available events (?)</a>
        </p>
    </div>
  </div>
</section>

<div class="clearfix"></div>
<!--banner end here-->



<?php if($eventRecomende):?>
<section class="section-text-cont" id="rec-event">
  
  <div class="container">
    <div class="row">

      <div class="section-heading">
        <h1>
          Event
          <span class="red_font">Recommendation</span>
        </h1>
      </div>
<?php foreach($eventRecomende as $eventre):?>
	<?php $themename=($commonModel->getEventtheme($eventre->id));?>
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
        <div class="listing-card-cont details-card-cont details-card-cont premium">
          <div class="article-thumb-img">
			  <?php if($eventre->logo){ ?>
					 <img src="<?= Yii::$app->homeUrl.$eventre->logo; ?>" width="100%">
				<?php	} ?>
          </div>
          <!--span class="premium-tag">
            Premium
          </span-->
          <div class="article-thumb-text-container">
            <h3 class="article-thumb-heading"><?= $eventre->event_name;?></h3>
            <ul class="article-thumb-info">
				<?php $start_date = $eventre->event_strt_date;
					$event_start_date = date("d M Y", $start_date); ?>
              <li><span class="listing-icon calender"></span><?=$event_start_date ?></li>
              <li><span class="listing-icon like"></span> 05</li>
            <li><span class="listing-icon view"></span><?php if($eventre->views){ echo $eventre->views;} else { echo "0";}?></li>
              <li><span class="listing-icon placeholder"></span><?=ucwords($eventre->address)?></li>
            </ul>
            <a href="javascript:void(0)" class="request-to-apply" onclick="RequestToApply(<?= $eventre->id?>);"><p class="aply-brand">Request to Apply your Brand Mandate(s)</p></a>
             <ul class="article-thumb-details">
              <li class="contact-now"><a>Contact Now</a></li>
              <div class="contact-data-cont" style="display:none;" id="contactarea<?=$eventre->id?>">
                <span class="close-shortlist close-popup"></span>
                <div class="shorlist-heading">
                  <h3>Contact Listing Owner</h3>
                  <h5 class="event-nm"><?= $eventre->event_name;?></h5>
                  <span class="evnt-date-n-place"><?=$event_start_date ?> | <?= $eventre->city_id;?>, <?= $eventre->country_id;?> </span>
                </div>
					<span class="conatcterr" id="nameerr<?=$eventre->id?>" style="display:none;">Fill Your Name</span>
					<span class="conatcterr" id="phoneerr<?=$eventre->id?>" style="display:none;">Fill Your Phnone Number</span>
					<span class="conatcterr" id="validphoneerr<?=$eventre->id?>" style="display:none;">Fill Valid Phnone Number</span>
					<span class="conatcterr" id="texterr<?=$eventre->id?>" style="display:none;">Fill Your Message</span>
					<form class="contact-nw-form" id="contactnow<?=$eventre->id?>">
						<input type="text" name="name" id="name<?=$eventre->id?>" placeholder="Your Name" class="input-create-fld">
						<input type="text" name="phone" id="phone<?=$eventre->id?>" placeholder="Your Number" class="input-create-fld">
						<textarea cols="" rows="4" name="text" id="text<?=$eventre->id?>" class="input-create-fld"><?php echo Yii::$app->params['contactMessage'];?></textarea>
						<button class="shorlist-btn save" id="" onclick="contactnow(<?=$eventre->id?>); return false;">Send</button>
						<button class="shorlist-btn cancel " onclick="closepopcontact(<?=$eventre->id?>); return false;">Cancel</button>
					</form>
              </div>
              <li class="shorlist"><a>Shortlist Now</a></li>
              <div class="shorlist-data-cont" style="display:none;" id="shortlistarea<?=$eventre->id?>">
                  <span class="close-shortlist close-popup"></span>
                  <div class="shorlist-heading">
                    <h4>Shortlist &amp; save this listing</h4>
                    <p>
                      Organize your selection into folders for future references
                    </p>
                    <div id="folder-list<?=$eventre->id?>">
                    <?php if($folderlist){
						foreach($folderlist as $folder)
							{
								echo '<br><input type="checkbox" name="folder" value="'.$folder['id'].'">'.$folder['foldername'];		
							} } ?>
					</div>
                  </div>
                  <form class="id"  id="Frmshortlist">
					<div class="create-folder">
					<p>Create a new folder</p>
					<span class="conatcterr" id="shortfoldererr<?=$eventre->id?>" style="display:none;">Please Enter FolderName</span>
					<input type="text" name="shortfoldername" id="shortfoldername<?=$eventre->id?>" placeholder="e.g Apple Activation Delhi" class="input-create-fld">
					<button class="add-fld-btn" id="" onclick="shortlist_add_folder(<?=$eventre->id?>); return false;">
					<i class="fa fa-folder-open-o" aria-hidden="true"></i> Add</button>
					</div>
					<button class="shorlist-btn save" id="" onclick="shortlistevent(<?=$eventre->id?>); return false;">Save</button>
					<button class="shorlist-btn cancel" onclick="closepopshortlist(<?=$eventre->id?>); return false;">Cancel</button>
					</form>
              </div>
              <li><a href="<?= Url::home() ?>theme/<?=$themename?>?id=<?=$eventre->id?>">More Info</a></li>
            </ul>
          </div>
        </div>
      </div>
<?php endforeach;?>     
     
      <div class="load-btn-cont">
        <a href="<?= Url::to(['recommend']) ; ?>" class="load-button">
          LOAD MORE
          <span class="load-btn-icons load"></span>
        </a>
      </div>
    </div>
  </div>

</section>
<?php endif;?>

<?php if($trendingEvents) : ?>
<section class="section-text-cont" id="tre-event">
  
  <div class="container">
    <div class="row">

      <div class="section-heading">
        <h1>
          Trending
          <span class="red_font">Events</span>
        </h1>
      </div>
	<?php foreach($trendingEvents as $trending) : ?>
     <?php $themename=($commonModel->getEventtheme($trending['id']));?>
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
        <div class="listing-card-cont details-card-cont details-card-cont premium">
          <div class="article-thumb-img">
			   <?php if($trending['logo']){ ?>
					 <img src="<?= Yii::$app->homeUrl.$trending['logo']; ?>" width="100%">
				<?php	} ?>
          </div>
          <!--span class="premium-tag">
            Premium
          </span-->
          <div class="article-thumb-text-container">
            <h3 class="article-thumb-heading"><?= $trending['event_name'];?></h3>
            <ul class="article-thumb-info">
				<?php $start_date = $trending['event_strt_date'];
					$event_start_date = date("d M Y", $start_date); ?>
              <li><span class="listing-icon calender"></span><?=$event_start_date ?></li>
              <li><span class="listing-icon like"></span> 05</li>
             <li><span class="listing-icon view"></span><?php if($trending['views']){ echo $trending['views'];} else { echo "0";}?></li>
              <li><span class="listing-icon placeholder"></span><?=ucwords($trending['address'])?></li>
            </ul>
            <a href="javascript:void(0)" class="request-to-apply" onclick="RequestToApply(<?= $trending['id']?>);"><p class="aply-brand">Request to Apply your Brand Mandate(s)</p></a>
             <ul class="article-thumb-details">
              <li class="contact-now"><a>Contact Now</a></li>
              <div class="contact-data-cont" style="display:none;" id="contactarea<?=$trending['id'];?>">
                <span class="close-shortlist close-popup"></span>
                <div class="shorlist-heading">
                  <h3>Contact Listing Owner</h3>
                  <h5 class="event-nm"><?= $trending['event_name'];?></h5>
                  <span class="evnt-date-n-place"><?=$event_start_date ?> | <?= $trending['city_id'];?>, <?= $trending['country_id'];?> </span>
                </div>
					<span class="conatcterr" id="nameerr<?=$trending['id']?>" style="display:none;">Fill Your Name</span>
					<span class="conatcterr" id="phoneerr<?=$trending['id']?>" style="display:none;">Fill Your Phnone Number</span>
					<span class="conatcterr" id="validphoneerr<?=$trending['id']?>" style="display:none;">Fill Valid Phnone Number</span>
					<span class="conatcterr" id="texterr<?=$trending['id']?>" style="display:none;">Fill Your Message</span>
					<form class="contact-nw-form" id="contactnow<?=$trending['id']?>">
						<input type="text" name="name" id="name<?=$trending['id']?>" placeholder="Your Name" class="input-create-fld">
						<input type="text" name="phone" id="phone<?=$trending['id']?>" placeholder="Your Number" class="input-create-fld">
						<textarea cols="" rows="4" name="text" id="text<?=$trending['id']?>" class="input-create-fld"><?php echo Yii::$app->params['contactMessage'];?></textarea>
						<button class="shorlist-btn save" id="" onclick="contactnow(<?=$trending['id']?>); return false;">Send</button>
						<button class="shorlist-btn cancel " onclick="closepopcontact(<?=$trending['id']?>); return false;">Cancel</button>
					</form>
              </div>
              <li class="shorlist"><a>Shortlist Now</a></li>
              <div class="shorlist-data-cont" style="display:none;" id="shortlistarea<?=$trending['id']?>">
                  <span class="close-shortlist close-popup"></span>
                  <div class="shorlist-heading">
                    <h4>Shortlist &amp; save this listing</h4>
                    <p>
                      Organize your selection into folders for future references
                    </p>
                    <div id="folder-list<?=$trending['id']?>">
                    <?php if($folderlist){
						foreach($folderlist as $folder)
							{
								echo '<br><input type="checkbox" name="folder" value="'.$folder['id'].'">'.$folder['foldername'];		
							} } ?>
					</div>
                  </div>
                  <form class="id"  id="Frmshortlist">
					<div class="create-folder">
					<p>Create a new folder</p>
					<span class="conatcterr" id="shortfoldererr<?=$trending['id']?>" style="display:none;">Please Enter FolderName</span>
					<input type="text" name="shortfoldername" id="shortfoldername<?=$trending['id']?>" placeholder="e.g Apple Activation Delhi" class="input-create-fld">
					<button class="add-fld-btn" id="" onclick="shortlist_add_folder(<?=$trending['id']?>); return false;">
					<i class="fa fa-folder-open-o" aria-hidden="true"></i> Add</button>
					</div>
					<button class="shorlist-btn save" id="" onclick="shortlistevent(<?=$trending['id']?>); return false;">Send</button>
					<button class="shorlist-btn cancel" onclick="closepopshortlist(<?=$trending['id']?>); return false;">Cancel</button>
					</form>
              </div>
              <li><a href="<?= Url::home() ?>theme/<?=$themename?>?id=<?=$trending['id']?>">More Info</a></li>
            </ul>
          </div>
        </div>
      </div>
     <?php endforeach;?> 
      
      
      <div class="load-btn-cont">
          <a href="<?= Url::to(['trending']) ; ?>" class="load-button">
          LOAD MORE
          <span class="load-btn-icons load"></span>
        </a>
      </div>
    </div>
  </div>

</section>
<?php endif;?>


<?php if($shortlist):?>
<section class="section-text-cont" id="short-event">

  <div class="container">
    <div class="row">

      <div class="section-heading">
        <h1>
          Shortlisted 
          <span class="red_font">Events</span>
        </h1>
      </div>
      <?php foreach($shortlist as $short):?>
      <?php $themename=($commonModel->getEventtheme($short['id']));?>
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
        <div class="listing-card-cont details-card-cont premium">
          <div class="article-thumb-img">
			  <?php if($short['logo']){ ?>
					 <img src="<?= Yii::$app->homeUrl.$short['logo']; ?>" width="100%">
				<?php	} ?>
          </div>
         <!--span class="premium-tag">
            Premium
          </span-->
          <?php $start_date = $short['event_strt_date'];
			$event_start_date = date("d M Y", $start_date); ?>
          <div class="article-thumb-text-container">
            <h3 class="article-thumb-heading"><?=$short['event_name'];?></h3>
            <ul class="article-thumb-info">
              <li><span class="listing-icon calender"></span><?=$event_start_date;?></li>
              <li><span class="listing-icon like"></span> 05</li>
              <li><span class="listing-icon view"></span><?php if($short['views']){ echo $short['views'];} else { echo "0";}?></li>
             <li><span class="listing-icon placeholder"></span><?=$short['address'];?></li>
            </ul>
             <a href="javascript:void(0)" class="request-to-apply" onclick="RequestToApply(<?= $short['id'];?>);"><p class="aply-brand">Request to Apply your Brand Mandate(s)</p></a>
            <ul class="shortlist-details list-inline">
              <li class="contact-now"><a>Contact Now</a></li>
				 <div class="contact-data-cont" style="display:none;" id="contactarea<?=$short['id'];?>">
                <span class="close-shortlist close-popup"></span>
                <div class="shorlist-heading">
                  <h3>Contact Listing Owner</h3>
                  <h5 class="event-nm"><?= $short['event_name'];?></h5>
                  <span class="evnt-date-n-place"><?=$event_start_date ?> | <?= $short['city_id'];?>, <?= $short['country_id'];?> </span>
                </div>
					<span class="conatcterr" id="nameerr<?=$short['id'];?>" style="display:none;">Fill Your Name</span>
					<span class="conatcterr" id="phoneerr<?=$short['id'];?>" style="display:none;">Fill Your Phnone Number</span>
					<span class="conatcterr" id="validphoneerr<?=$short['id']?>" style="display:none;">Fill Valid Phnone Number</span>
					<span class="conatcterr" id="texterr<?=$short['id'];?>" style="display:none;">Fill Your Message</span>
					<form class="contact-nw-form" id="contactnow<?=$short['id'];?>">
						<input type="text" name="name" id="name<?=$short['id'];?>" placeholder="Your Name" class="input-create-fld">
						<input type="text" name="phone" id="phone<?=$short['id'];?>" placeholder="Your Number" class="input-create-fld">
						<textarea cols="" rows="4" name="text" id="text<?=$short['id']?>" class="input-create-fld"><?php echo Yii::$app->params['contactMessage'];?></textarea>
						<button class="shorlist-btn save" id="" onclick="contactnow(<?=$short['id'];?>); return false;">Send</button>
						<button class="shorlist-btn cancel " onclick="closepopcontact(<?=$short['id'];?>); return false;">Cancel</button>
					</form>
              </div>
              <li><a href="<?= Url::home() ?>theme/<?=$themename?>?id=<?=$short['id']?>">More info</a></li>
              </ul>
          </div>
        </div>
      </div>
     <?php endforeach;?>
    <div class="load-btn-cont">
        <a href="<?= Url::to(['shortlist']) ; ?>" class="load-button">
          LOAD MORE
          <span class="load-btn-icons load"></span>
        </a>
      </div>
    </div>
  </div>
</section>
<?php endif;?>


<?php if($eventviewd):?>
<section class="section-text-cont">
  <div class="container">
    <div class="row">
      <div class="section-heading">
        <div class="event-viewed">Event viewed by </div>  
        <h1>
          <span class="red_font">Other Brands in <span class="font_wieght500"><?=$industrayname['title'];?>
        </h1>
      </div>
      
      <?php foreach($eventviewd as $view):?>
       <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
        <div class="listing-card-cont details-card-cont premium">
          <div class="article-thumb-img">
            <?php if($view['logo']){ ?><img src="<?= Yii::$app->homeUrl.$view['logo'];?>" width="100%"><?php } ?>
          </div>
          <!--span class="premium-tag">
            Premium
          </span-->
         <div class="article-thumb-text-container">
            <h3 class="article-thumb-heading"><?= $view['event_name'];?></h3>
            <ul class="article-thumb-info">
				<?php $start_date = $view['event_strt_date'];
					$event_start_date = date("d M Y", $start_date); ?>
              <li><span class="listing-icon calender"></span><?=$event_start_date ?></li>
              <li><span class="listing-icon like"></span> 05</li>
             <li><span class="listing-icon view"></span><?php if($view['views']){ echo $view['views'];} else { echo "0";}?></li>
              <li><span class="listing-icon placeholder"></span><?=ucwords($view['address'])?></li>
            </ul>
            <a href="javascript:void(0)" class="request-to-apply" onclick="RequestToApply(<?= $view['id']?>);"><p class="aply-brand">Request to Apply your Brand Mandate(s)</p></a>
             <ul class="article-thumb-details">
              <li class="contact-now"><a>Contact Now</a></li>
              <div class="contact-data-cont" style="display:none;" id="contactarea<?=$view['id'];?>">
                <span class="close-shortlist close-popup"></span>
                <div class="shorlist-heading">
                  <h3>Contact Listing Owner</h3>
                  <h5 class="event-nm"><?= $view['event_name'];?></h5>
                  <span class="evnt-date-n-place"><?=$event_start_date ?> | <?= $view['city_id'];?>, <?= $view['country_id'];?> </span>
                </div>
					<span class="conatcterr" id="nameerr<?=$view['id']?>" style="display:none;">Fill Your Name</span>
					<span class="conatcterr" id="phoneerr<?=$view['id']?>" style="display:none;">Fill Your Phnone Number</span>
					<span class="conatcterr" id="validphoneerr<?=$view['id']?>" style="display:none;">Fill Valid Phnone Number</span>
					<span class="conatcterr" id="texterr<?=$view['id']?>" style="display:none;">Fill Your Message</span>
					<form class="contact-nw-form" id="contactnow<?=$view['id']?>">
						<input type="text" name="name" id="name<?=$view['id']?>" placeholder="Your Name" class="input-create-fld">
						<input type="text" name="phone" id="phone<?=$view['id']?>" placeholder="Your Number" class="input-create-fld">
						<textarea cols="" rows="4" name="text" id="text<?=$view['id']?>" class="input-create-fld"><?php echo Yii::$app->params['contactMessage'];?></textarea>
						<button class="shorlist-btn save" id="" onclick="contactnow(<?=$view['id']?>); return false;">Send</button>
						<button class="shorlist-btn cancel " onclick="closepopcontact(<?=$view['id']?>); return false;">Cancel</button>
					</form>
              </div>
              <li class="shorlist"><a>Shortlist Now</a></li>
              <div class="shorlist-data-cont" style="display:none;" id="shortlistarea<?=$view['id']?>">
                  <span class="close-shortlist close-popup"></span>
                  <div class="shorlist-heading">
                    <h4>Shortlist &amp; save this listing</h4>
                    <p>
                      Organize your selection into folders for future references
                    </p>
                    <div id="folder-list<?=$view['id']?>">
                    <?php if($folderlist){
						foreach($folderlist as $folder)
							{
								echo '<br><input type="checkbox" name="folder" value="'.$folder['id'].'">'.$folder['foldername'];		
							} } ?>
					</div>
                  </div>
                  <form class="id"  id="Frmshortlist">
					<div class="create-folder">
					<p>Create a new folder</p>
					<span class="conatcterr" id="shortfoldererr<?=$view['id']?>" style="display:none;">Please Enter FolderName</span>
					<input type="text" name="shortfoldername" id="shortfoldername<?=$view['id']?>" placeholder="e.g Apple Activation Delhi" class="input-create-fld">
					<button class="add-fld-btn" id="" onclick="shortlist_add_folder(<?=$view['id']?>); return false;">
					<i class="fa fa-folder-open-o" aria-hidden="true"></i> Add</button>
					</div>
					<button class="shorlist-btn save" id="" onclick="shortlistevent(<?=$view['id']?>); return false;">Send</button>
					<button class="shorlist-btn cancel" onclick="closepopshortlist(<?=$view['id']?>); return false;">Cancel</button>
					</form>
              </div>
              <li><a href="<?= Url::home() ?>theme/<?=$themename?>?id=<?=$view['id']?>">More Info</a></li>
            </ul>
          </div>
       </div>
      </div>
    <?php endforeach; ?>
      <div class="load-btn-cont">
        <a href="<?= Url::to(['viewedbtother']) ; ?>" class="load-button">
          VIEW MORE
          <span class="load-btn-icons view"></span>
        </a>
      </div>
    </div>
  </div>
</section>
<?php endif;?>

<?php if($amazingEvent) : ?> 
<section class="section-text-cont amazing-deal">

  <div class="container">
    <div class="row">
      <div class="section-heading">
        <h1>Amazing
          <span class="red_font"> Deals </span>
        </h1>
      </div>
     
     <?php foreach($amazingEvent as $amazing) : ?>
     <?php $themename=($commonModel->getEventtheme($amazing['id']));?>
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
        <div class="listing-card-cont details-card-cont details-card-cont premium">
          <div class="article-thumb-img">
			   <?php if($amazing['logo']){ ?>
					 <img src="<?= Yii::$app->homeUrl.$amazing['logo']; ?>" width="100%">
				<?php	} ?>
          </div>
         <!--span class="premium-tag">
            Premium
          </span-->
          <div class="article-thumb-text-container">
            <h3 class="article-thumb-heading"><?= $amazing['event_name'];?></h3>
            <ul class="article-thumb-info">
				<?php $start_date = $amazing['event_strt_date'];
					$event_start_date = date("d M Y", $start_date); ?>
              <li><span class="listing-icon calender"></span><?=$event_start_date ?></li>
              <li><span class="listing-icon like"></span> 05</li>
          <li><span class="listing-icon view"></span><?php if($amazing['views']){ echo $amazing['views'];} else { echo "0";}?></li>
              <li><span class="listing-icon placeholder"></span><?=ucwords($amazing['address'])?></li>
            </ul>
            <a href="javascript:void(0)" class="request-to-apply" onclick="RequestToApply(<?= $amazing['id']?>);"><p class="aply-brand">Request to Apply your Brand Mandate(s)</p></a>
             <ul class="article-thumb-details">
              <li class="contact-now"><a>Contact Now</a></li>
              <div class="contact-data-cont" style="display:none;" id="contactarea<?=$amazing['id'];?>">
                <span class="close-shortlist close-popup"></span>
                <div class="shorlist-heading">
                  <h3>Contact Listing Owner</h3>
                  <h5 class="event-nm"><?= $amazing['event_name'];?></h5>
                  <span class="evnt-date-n-place"><?=$event_start_date ?> | <?= $amazing['city_id'];?>, <?= $amazing['country_id'];?> </span>
                </div>
					<span class="conatcterr" id="nameerr<?=$amazing['id']?>" style="display:none;">Fill Your Name</span>
					<span class="conatcterr" id="phoneerr<?=$amazing['id']?>" style="display:none;">Fill Your Phnone Number</span>
					<span class="conatcterr" id="validphoneerr<?=$amazing['id']?>" style="display:none;">Fill Valid Phnone Number</span>
					<span class="conatcterr" id="texterr<?=$amazing['id']?>" style="display:none;">Fill Your Message</span>
					<form class="contact-nw-form" id="contactnow<?=$amazing['id']?>">
						<input type="text" name="name" id="name<?=$amazing['id']?>" placeholder="Your Name" class="input-create-fld">
						<input type="text" name="phone" id="phone<?=$amazing['id']?>" placeholder="Your Number" class="input-create-fld">
						<textarea cols="" rows="4" name="text" id="text<?=$amazing['id']?>" class="input-create-fld"><?php echo Yii::$app->params['contactMessage'];?></textarea>
						<button class="shorlist-btn save" id="" onclick="contactnow(<?=$amazing['id']?>); return false;">Send</button>
						<button class="shorlist-btn cancel " onclick="closepopcontact(<?=$amazing['id']?>); return false;">Cancel</button>
					</form>
              </div>
              <li class="shorlist"><a>Shortlist Now</a></li>
              <div class="shorlist-data-cont" style="display:none;" id="shortlistarea<?=$amazing['id']?>">
                  <span class="close-shortlist close-popup"></span>
                  <div class="shorlist-heading">
                    <h4>Shortlist &amp; save this listing</h4>
                    <p>
                      Organize your selection into folders for future references
                    </p>
                    <div id="folder-list<?=$amazing['id']?>">
                    <?php if($folderlist){
						foreach($folderlist as $folder)
							{
								echo '<br><input type="checkbox" name="folder" value="'.$folder['id'].'">'.$folder['foldername'];		
							} } ?>
					</div>
                  </div>
                  <form class="id"  id="Frmshortlist">
					<div class="create-folder">
					<p>Create a new folder</p>
					<span class="conatcterr" id="shortfoldererr<?=$amazing['id']?>" style="display:none;">Please Enter FolderName</span>
					<input type="text" name="shortfoldername" id="shortfoldername<?=$amazing['id']?>" placeholder="e.g Apple Activation Delhi" class="input-create-fld">
					<button class="add-fld-btn" id="" onclick="shortlist_add_folder(<?=$amazing['id']?>); return false;">
					<i class="fa fa-folder-open-o" aria-hidden="true"></i> Add</button>
					</div>
					<button class="shorlist-btn save" id="" onclick="shortlistevent(<?=$amazing['id']?>); return false;">Save</button>
					<button class="shorlist-btn cancel" onclick="closepopshortlist(<?=$amazing['id']?>); return false;">Cancel</button>
					</form>
              </div>
              <li><a href="<?= Url::home() ?>theme/<?=$themename?>?id=<?=$amazing['id']?>">More Info</a></li>
            </ul>
          </div>
        </div>
      </div>
     <?php endforeach;?> 
     
      <div class="load-btn-cont">
         <a href="<?= Url::to(['amazing']) ; ?>" class="load-button">
          VIEW MORE
          <span class="load-btn-icons view"></span>
        </a>
      </div>
    </div>
</section>
           
</section>
<?php endif; ?> 








