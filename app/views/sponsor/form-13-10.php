<style>
.firstpagenext{
	position: static;
    float: right;
     margin-bottom: 30px;
	}
.form-group.field-brandmandates-slug, .form-group.field-brandmandates-id, .form-group.field-brandmandates-lat, .form-group.field-brandmandates-lng{display:none;}

.BackBtn_Contnr {
    
    position: static;
   
}
</style>

<?php
use yii\easyii\widgets\DateTimePicker;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;

use app\modules\role\api\Role;
use app\modules\industry\api\Industry;
use yii\easyii\helpers\Image;
use app\modules\sponsorshipobjective\api\SponsorshipObjective;
use app\modules\eventcategories\api\EventCategories;
use app\modules\sponsorprice\api\SponsorPrice;
use app\modules\agegroup\api\AgeGroup;
use app\modules\education\api\Education;
use app\modules\income\api\Income;
use app\modules\city\api\City;


$asset = \app\assets\SponsorAsset::register($this);

if($model->specific_subjective){
	$model->specific_subjective = explode(",",$model->specific_subjective);
	
}
if($model->event_category_id){
	$model->event_category_id = explode(",",$model->event_category_id);
	
}
$session = Yii::$app -> session;

$this->registerJs('$(window).load(function() {
		document.body.className = "crtevent-header";
		$("#brandmandates-timeline label:eq(1)").click(function() {
			$("#specificrangedate").show(500);
		});
		$("#brandmandates-timeline label:eq(0)").click(function() {
			$("#specificrangedate").hide(500);
		});
		});');


?>
<style>
.brandimagepreview{margin-top:25px;}
.brandimagepreview > img{    width: 23%;height: 120px;}

div#brandmandates-event_category_id label, div#brandmandates-specific_subjective label {
    width: 100%;
}

div#brandmandates-event_category_id, div#brandmandates-specific_subjective {
    height: 105px;
    overflow-y: scroll;
    border: solid 1px #bbb;
    padding: 10px;
}



</style>



<header>
  <a href="/onspon/" class="top-logo"></a>
  <div class="top-menu-bar">
    <div class="hamburger-menu toggle-x">
      <span class="arrows"></span>
      <nav id="menu">
       <ul>
       <li><a href="/onspon/">Home</a></li>
       <li><a href="#">About</a></li>
       <li><a href="#">Contact</a></li>    
       <li><a href="#">Blog</a></li>
       <?php if(!$session['user_id']){?>
			<li><a href="<?= Url::home() ?>site/login">Login</a></li>
		<?php }	else { ?>
			<li><a href="<?= Url::home() ?>site/logout">Logout</a></li>
		<?php }?>
    </ul>
 </nav> 
    </div>
    <ul>
	  <?php if(!$session['user_id']){?>
			<li><a href="<?= Url::home() ?>site/login"><span class="menu-icon user"></span>Login</a></li>
		<?php }	else { ?>
			<li><a href="<?= Url::home() ?>site/logout"><span class="menu-icon user"></span>Logout</a></li>
		<?php }?>
      <li>
        <a>
          <span class="menu-icon phone"></span>
          + 91 7666003344              
        </a>
      </li>
    </ul>
  </div>
</header>

<div id="step1">
<main>
	<div class="progress">
      <div class="progress-bar" role="progressbar" aria-valuenow="70"
          aria-valuemin="0" aria-valuemax="100" style="width:0%">
            <span class="sr-only">0% Complete</span>
      </div>
 </div>
	<div class="container">
        <div class="row">
        	<div class="col-md-5 col-sm-5 col-xs-12">
            	<h3 class="headingCOntr">Create Brand Mandate</h3>
            	
<?php $form = ActiveForm::begin([
	'id' => 'brandform1',
    'enableAjaxValidation' => false,
    'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
]); ?>          	
                <div class="form-group">
                  <?= $form->field($model, 'brand_name')->textInput(['autocomplete'=>'off','class'=>'form-control'])?> </div>
                <div class="chkbox_MainContr">
                	<div class="checkbox">
						
					<?= $form->field($model, 'visible_to_audience')->checkbox(); ?>
                  </div>
                  	<div class="questionMark">	
                       <img src="<?= $asset->baseUrl ?>/image/questiontag.png" onmouseover="toolover();" onmouseout="toolout();" />
                    </div>
                    <label class="toolTip">Click here to hide it from audience.<div class="arrow-up"></div></label>
                </div>
                <div class="form-group">
					<?= $form->field($model, 'about_brand')->textarea(['autocomplete'=>'off','class'=>'form-control']) ?>
                </div>
                <div class="form-group">
                  <?= $form->field($model, 'industry_id')->dropDownList(Industry::industryName(), ['prompt'=>'Choose Industry','class'=>'form-control']);  ?>
                </div>
                  <div class="form-group">
			  <!--Document Upload-->
			  <label class="control-label col-sm-4" for="Sections">Image/Logo</label>
				<label class="custom-file">
				  <span class="custom-file-control">
					 <p>choose file</p>
					 <span class="btn btn-default btn-file">
						<?= $form->field($model, 'logo')->fileInput(['onchange'=>'readURLPROFILE(this);']) ?>
		 
					 </span>
				  </span>
				</label>
			  </div>
                  <div class="form-group">
					  <?= $form->field($model, 'i_am_looking')->textarea(['class'=>'form-control']) ?>
                   </div>
                   <?= $form->field($model, 'slug')->hiddenInput()->label(''); ?>
				   <?= $form->field($model, 'id')->hiddenInput()->label(''); ?> 
					
				
                 <div class="NextBtn_Contnr firstpagenext">
                    <button onclick="savebrand(); return false;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">
                        Next
                    </button>
                </div>
            </div>
<?php ActiveForm::end(); ?>
            <div class="col-md-7 col-sm-7 col-xs-12">
            	<div class="Map_Contnr">
                	<div class="Sponsership_COntnr listingRight_COntnr">
                	<p><img src="<?= $asset->baseUrl ?>/image/bell2.png"></p>
                   <br>
                    <p>Just think of ‘Brand Mandate’ like a Job Posting for events to apply. Your query gets visible to events – and the events that fit in , apply. You can compare and sort them the way you want. Its one of our attempts to make sponsorship decision making simple. Lets get started !!</p>
                </div>
                 <?php if($model->logo) { ?>
                <div class="brandimagepreview">
                	<img src="<?= Yii::$app->homeUrl.$model->logo;?>" id="profileblah">
                </div>
                <?php } else { ?>
				<div class="brandimagepreview" style="display:none;">
                	<img src="" id="profileblah">
                </div>
                <?php } ?>
              </div> 
            </div>
        </div>
    </div>
</main>
</div>

<div id="step2" style="display:none;">
<main>
	<div class="progress">
      <div class="progress-bar" role="progressbar" aria-valuenow="70"
          aria-valuemin="0" aria-valuemax="100" style="width:20%">
            <span class="sr-only">20% Complete</span>
      </div>
 </div>
	<div class="container">
        <div class="row">
        	<div class="col-md-5 col-sm-5 col-xs-12">
            	<h3 class="headingCOntr"></h3>
            	
<?php $form = ActiveForm::begin([
	'id' => 'brandform2',
    'enableAjaxValidation' => false,
    'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
]); ?>          	
			<div class="form-group">			
                <?= $form->field($model, 'event_category_id')->label("Any Specfic category of Avenue in your mind")            
				->checkboxList(EventCategories::eventcategoriesName(),
				[]); ?>
                        
			</div>
			<div class="form-group">
				<?= $form->field($model, 'specific_subjective')->label("Partnership / Sponsorship Objective")            
				 ->checkboxList(SponsorshipObjective::objectiveName(),[]); ?>
			</div>
			<div class="form-group">
				<?= $form->field($model, 'budget_range')
				->dropDownList(SponsorPrice::sponsorpriceName(), ['prompt'=>'Choose Budget Range', 'class' => 'form-control']);?>
			</div>
			
					<?= $form->field($model, 'id')->hiddenInput()->label(''); ?> 
				
<?php ActiveForm::end(); ?>
			
			<div class="BackBtn_Contnr">
                	<a href="javascript:void(0);" onclick="backbutton('step1');"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;Back</a>
               
               <div class="NextBtn_Contnr firstpagenext">
                    <button onclick="updatebrand('step2'); return false;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">
                        Next
                    </button>
                </div>
               
                </div>
            </div>
            <div class="col-md-7 col-sm-7 col-xs-12">
            	<div class="Map_Contnr">
                	<div class="Sponsership_COntnr listingRight_COntnr">
                	<p><img src="<?= $asset->baseUrl ?>/image/bell2.png"></p>
                   <br>
                    <p>Just think of ‘Brand Mandate’ like a Job Posting for events to apply. Your query gets visible to events – and the events that fit in , apply. You can compare and sort them the way you want. Its one of our attempts to make sponsorship decision making simple. Lets get started !!</p>
                </div>
               
              </div> 
            </div>
        </div>
    </div>
</main>
</div>


<div id="step3" style="display:none;">
<main>
	<div class="progress">
          <div class="progress-bar" role="progressbar" aria-valuenow="70"
              aria-valuemin="0" aria-valuemax="100" style="width:40%">
                <span class="sr-only">40% Complete</span>
          </div>
     </div>
	<div class="container">
        <div class="row">
        	<div class="col-md-5 col-sm-5 col-xs-12">
            	<h3 class="headingCOntr"></h3>
                <div class="Drop_Down_Main_Contnr">
 
 <?php $form = ActiveForm::begin([
    'enableAjaxValidation' => false,
    'id' => 'step3form',
    'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
]); ?>             
                
<div class="">
	
	<div class="form-group">
		<?= $form->field($model, 'city_id')->dropDownList(City::cityNameforBrand(), ['prompt'=>'Select City Name','class'=>'form-control']);  ?>
	</div>
	
	<div class="form-group">
		<?= $form->field($model, 'timeline')->radioList(array('Open timeline'=>'Open timeline','specific duration'=>'specific duration')); ?>
     </div>
	
	<div class="row" id="specificrangedate" style="<?php if($model->timeline=='specific duration'){ echo "";} else { echo "display:none;";}?>">
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<?= $form->field($model, 'start_date')->widget(DateTimePicker::className(), ['options'=>['format'=>Yii::$app->params['dateFormat']]]); ?>
			</div>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<?= $form->field($model, 'end_date')->widget(DateTimePicker::className(), ['options'=>['format'=>Yii::$app->params['dateFormat']]]); ?>
			</div>
		</div>
	</div>
	<?= $form->field($model, 'id')->hiddenInput()->label(''); ?> 
	<?= $form->field($model, 'lat')->hiddenInput()->label(''); ?>
	<?= $form->field($model, 'lng')->hiddenInput()->label(''); ?>
</div>
</div>  
<?php ActiveForm::end(); ?>          
                 <div class="BackBtn_Contnr">
                	<a href="javascript:void(0);" onclick="backbutton('step2');"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;Back</a>
               
                <div class="NextBtn_Contnr">
                    <button onclick="updatebrand('step3'); return false;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">
                        Next
                    </button>
                </div>
               
               </div>  
            </div>
            <div class="col-md-7 col-sm-7 col-xs-12">
            	<div class="Map_Contnr">
                	<div class="Sponsership_COntnr listingRight_COntnr">
                	<p><img src="<?= $asset->baseUrl ?>/image/bell2.png"></p>
                    <p>Just think of 'Brand Mandate' like a job Posting for events to apply. Your query gets visible to events- and the events that fit in, apply. You can compare and sort them the way you want.
                     Its one our attempts to make sponsorship decision makeing simple, Lets get started!!</p>
                </div>
                </div>
            </div>
        </div>
    </div>
</main>
</div>


<div id="step4" style="display:none;">
<main>
	<div class="progress">
          <div class="progress-bar" role="progressbar" aria-valuenow="70"
              aria-valuemin="0" aria-valuemax="100" style="width:60%">
                <span class="sr-only">60% Complete</span>
          </div>
     </div>
	<div class="container">
        <div class="row">
        	<div class="col-md-5 col-sm-5 col-xs-12">
            	<h3 class="headingCOntr">Audience Details</h3>
                <div class="Drop_Down_Main_Contnr">
					
<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => false,
    'id' => 'step4form',
    'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
]); ?>
                   
		<div class="">
			<div class="form-group">
				<?= $form->field($model, 'age_group')->dropDownList(AgeGroup::ageName(), ['class'=>'form-control']);  ?>
			</div>
			
			<div class="form-group">
				<?= $form->field($model, 'education')->dropDownList(Education::educationName(), ['class'=>'form-control']);  ?>
			</div>
			
			<div class="form-group">
				<?= $form->field($model, 'income')->dropDownList(Income::incomeName(), ['class'=>'form-control']);  ?>
			</div>
			
			<div class="form-group">
				<?= $form->field($model, 'gender')->dropDownList(["Male"=>"Male", "Female"=>"Female"], ['class'=>'form-control']);  ?>
			</div>
			<?= $form->field($model, 'id')->hiddenInput()->label(''); ?> 
		</div>
	</div>            
<?php ActiveForm::end(); ?>            
                <div class="BackBtn_Contnr">
                	<a href="javascript:void(0);" onclick="backbutton('step3');"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;Back</a>
                
                 <div class="NextBtn_Contnr">
                    <button onclick="updatebrand('step4'); return false;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">
                        Next
                    </button>
                </div>
                </div>
            </div>
            <div class="col-md-7 col-sm-7 col-xs-12">
            	<div class="Map_Contnr">
                	<div class="Sponsership_COntnr listingRight_COntnr">
                	<p><img src="<?= $asset->baseUrl ?>/image/bell2.png"></p>
                    <p>Select the targeted audience profile under the respective demographic heads as below</p>
                </div>
                </div>
               
            </div>
        </div>
    </div>
</main>
</div>

<div id="step5" style="display:none;">
<main>
	<div class="progress">
      <div class="progress-bar" role="progressbar" aria-valuenow="70"
          aria-valuemin="0" aria-valuemax="100" style="width:80%">
            <span class="sr-only">80% Complete</span>
      </div>
 </div>
	<div class="container">
        <div class="row">
        	<div class="col-md-5 col-sm-5 col-xs-12">
            	<h3 class="headingCOntr">Question for Event’s Owner</h3>
   <?php $form = ActiveForm::begin([
    'enableAjaxValidation' => false,
    'id' => 'step5form',
    'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']]); ?>
                <div class="form-group">
					<?= $form->field($model, 'question1')->textInput(['class' => 'form-control']) ?>
                 </div>
                 
                <div class="form-group">
					<?= $form->field($model, 'question2')->textInput(['class' => 'form-control']) ?>
                </div>
                
                <div class="chkbox_MainContr">
                	<div class="checkbox">
						<?= $form->field($model, 'visible_to_all_avenues')->checkbox(); ?>
                  </div>
                  <?= $form->field($model, 'id')->hiddenInput()->label(''); ?>
                </div>
  <?php ActiveForm::end(); ?> 
				
				 <div class="BackBtn_Contnr">
                	<a href="javascript:void(0);" onclick="backbutton('step4');"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;Back</a>
               
               <div class="NextBtn_Contnr">
                    <button onclick="updatebrand('step5'); return false;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">
                        Save & Exit
                    </button>
                </div>
                
                </div>
            </div>
            <div class="col-md-7 col-sm-7 col-xs-12">
            	<div class="Map_Contnr">
                	<div class="Sponsership_COntnr listingRight_COntnr">
                	  <p><img src="<?= $asset->baseUrl ?>/image/bell2.png"></p>
                   <br>
                    <p>While applying for your brand mandate, event owners would be asked to answer these questions. This saves your evalution time.</p>
                </div>
            </div>
        </div>
    </div>
    </div>
</main>

</div>

<script>
function savebrand()
{
		var formData = new FormData($("#brandform1")[0]);
		
		var name = $('#brandmandates-brand_name').val();
		var about = $('#brandmandates-about_brand').val();
		var industry = $('#brandmandates-industry_id').val();
		var looking = $('#brandmandates-i_am_looking').val();
		
		if(!name)
		{
			$('input[id="brandmandates-brand_name"]').each(function() {
            if ($.trim($(this).val()) == '') {
                $(this).css({"border": "1px solid red","background": "#FFCECE" });
            }});
            return false;
		}
		if(!about)
		{
			$('textarea[id="brandmandates-about_brand"]').each(function() {
            if ($.trim($(this).val()) == '') {
                $(this).css({"border": "1px solid red","background": "#FFCECE" });
            }});
			return false;
		}
		if(!industry)
		{
			$('select[id="brandmandates-industry_id"]').each(function() {
            if ($.trim($(this).val()) == '') {
                $(this).css({"border": "1px solid red","background": "#FFCECE" });
            }});
			return false;
		}
		if(!looking)
		{
			$('textarea[id="brandmandates-i_am_looking"]').each(function() {
            if ($.trim($(this).val()) == '') {
                $(this).css({"border": "1px solid red","background": "#FFCECE" });
            }});
			return false;
		}
		
		else {
			
			$('#brandmandates-brand_name').css({"border": "","background": ""});
			$('#brandmandates-about_brand').css({"border": "","background": ""});
			$('#brandmandates-industry_id').css({"border": "","background": ""});
			$('#brandmandates-i_am_looking').css({"border": "","background": ""});
			
			$('.page-loader').show();
			
			$.ajax({
					type: "POST",
					url: '<?= Url::to(['create']) ; ?>',
					data: formData,
					contentType: false,
					cache: false,
					processData:false,
					success: function(result){
					$('input[id="brandmandates-id"]').val(result);
					$('#step1').hide();
					$('#step2').show();
					$('#step3').hide();
					$('#step4').hide();
					$('#step5').hide();
					$('.page-loader').hide();
					}
				});
			
			}
        
}

</script>

<!--- form 2 ajax--->
<script>
function updatebrand(step)
{
	
	if(step=='step2'){
	var data = $('#brandform2').serialize() + "&step=step2";
	var TargetUrl = '<?= Url::to(['update']) ; ?>';
	}
	if(step=='step3'){
	var data = $('#step3form').serialize() + "&step=step3";
	var TargetUrl = '<?= Url::to(['updateallstep']) ; ?>';
	}
	if(step=='step4'){
	var data = $('#step4form').serialize() + "&step=step4";
	var TargetUrl = '<?= Url::to(['updateallstep']) ; ?>';
	}
	if(step=='step5'){
	var data = $('#step5form').serialize() + "&step=step5";
	var TargetUrl = '<?= Url::to(['updateallstep']) ; ?>';
	}
	
	$('.page-loader').show();
	$.ajax({
			type: "POST",
			url: TargetUrl,
			data: data,
			cache: false,
			success: function(result){
			if(result == 'step2'){
				$('#step1').hide();
				$('#step2').hide();
				$('#step3').show();
				$('#step4').hide();
				$('#step5').hide();
				$('.page-loader').hide();
			}
			if(result == 'step3'){
				$('#step1').hide();
				$('#step2').hide();
				$('#step3').hide();
				$('#step4').show();
				$('#step5').hide();
				$('.page-loader').hide();
			}
			if(result == 'step4'){
				$('#step1').hide();
				$('#step2').hide();
				$('#step3').hide();
				$('#step4').hide();
				$('#step5').show();
				$('.page-loader').hide();
			}
			if(result == 'success'){
				$('.page-loader').hide();
				alert(result);
				window.location.href = '<?= Url::to(['sponsor/index']) ; ?>';
			}
			
			}
		});  
}
</script>

<script>
function backbutton(step)
{
	if(step =='step1')
	{
		$('#step1').show();
		$('#step2').hide();
		$('#step3').hide();
		$('#step4').hide();
		$('#step5').hide();
	}
	if(step =='step2')
	{
		$('#step1').hide();
		$('#step2').show();
		$('#step3').hide();
		$('#step4').hide();
		$('#step5').hide();
	}
	if(step =='step3')
	{
		$('#step1').hide();
		$('#step2').hide();
		$('#step3').show();
		$('#step4').hide();
		$('#step5').hide();
	}
	if(step =='step4')
	{
		$('#step1').hide();
		$('#step2').hide();
		$('#step3').hide();
		$('#step4').show();
		$('#step5').hide();
	}
}
</script>


<!--stickey_Header-->
<script type="text/javascript">
	
	function toolover()
	{
		$('.toolTip').fadeIn(400);
	}
	
	function toolout()
	{
		$('.toolTip').fadeOut(400);
	}
</script>
<!--========-->

<script type="text/javascript">
function readURLPROFILE(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#profileblah').attr('src', e.target.result);	
			$('.brandimagepreview').show();
		}
		reader.readAsDataURL(input.files[0]);
	}
}
</script>

