<?php

use yii\helpers\Url;
use yii\easyii\helpers\Image;
use  yii\widgets\LinkPager;
use app\models\Brandmandates;
use app\models\Common;
$commonModel = new Common;

$asset = \app\assets\SponsorAsset::register($this);

$folderlist = $commonModel->UserShortlistfolder(Yii::$app->session['user_id']);

$session = Yii::$app->session;


include('header.php');

?>
<section class="section-banner-cont section-banner">
  <div class="section-img ocuspoint" data-focus-x="0" data-focus-y="0">
    <img src="<?= $asset->baseUrl ?>/images/section-banner1.jpg">
  </div>

  <div class="section-caption-cont">
    <div class="container">
      <div class="section-caption">
        <h1 class="section-lead-text">
          Welcome to 
          <span class="red_font list-inline">onspon.com</span>
        </h1>
        <h3 class="section-sub-lead-text">
          Where best "Events" are discovered
        </h3>
        <ul class="section-buttons">
          <li><a href="<?= Url::to(['inbox']) ; ?>" class="">My Inbox</a></li>
          <li><a href="<?= Url::to(['lastminutesdeals']) ; ?>">Last Minute Deals</a></li>
        </ul>
      </div>
    </div>
  </div>

  <div class="explore-events-cont red_bg">
    <div class="container">
      <p>
        More than
        <span class="font_wieght700 white_font list-inline three100"> 300</span> 
        brand managers visit <span class="font_wieght700 list-inline onspon_com"> onspon.com</span> 
        to find the best events. Let's start by 
        <a href="<?= Url::to('createbrand');?>" class="white_font font_wieght500 list-inline exp-evt-link"> creating your brand mandate (?)</a> 
        or 
        <a href="#" class="white_font font_wieght500 list-inline exp-evt-link">browse available events (?)</a>
      </p>
    </div>
  </div>
</section>

<div class="clearfix"></div>


<!--banner end here-->

<section class="section-text-cont">

  <div class="container">
    <div class="row">
      <div class="section-heading">
        <h1>
         Event
         <span class="red_font">  Recommendation</span>
       </h1>
     </div>


     <!-- Listing starts -->
     <div class="evnt-recmndt">
     <?php if($eventRecomende): ?>
      <?php foreach($eventRecomende as $item): ?>
        <?php $themename=($commonModel->getEventtheme($item->id));?>
        <div class="col-lg-6">
          <div class="listing-card-cont premium">
            <div class="article-thumb-img">
             <?php if($item->logo){ ?>
			 <img src="<?= Yii::$app->homeUrl.Image::thumb($item->logo, 225,208); ?>" width="100%">
             <?php	} ?>
           </div>
          <!--span class="premium-tag">
            Premium
          </span-->
          <?php $start_date = $item->event_strt_date;
          $event_start_date = date("d M Y", $start_date); ?>
          <div class="article-thumb-text-container">
            <h3 class="article-thumb-heading"><?= $item->event_name;?></h3>
            <ul class="article-thumb-info">
              <li><span class="listing-icon calender"></span> <?= $event_start_date;?></li>
              <li><span class="listing-icon like"></span> 05</li>
              <li><span class="listing-icon view"></span><?php if($item->views){ echo $item->views;} else { echo "0";}?></li>
              <li><span class="listing-icon placeholder"></span><?=ucwords($item->address)?></li>
            </ul>
            <a href="javascript:void(0)" class="request-to-apply" onclick="RequestToApply(<?= $item->id?>);"><p class="aply-brand">Request to Apply your Brand Mandate(s)</p></a>
            <ul class="article-thumb-details">
              <li class="contact-now"><a>Contact Now</a></li>
              <div class="contact-data-cont" style="display:none;" id="contactarea<?=$item->id?>">
                <span class="close-shortlist close-popup" onclick="closepop(); return false;"></span>
                <div class="shorlist-heading">
                  <h3>Contact Listing Owner</h3>
                  <h5 class="event-nm"><?= $item->event_name;?></h5>
                  <span class="evnt-date-n-place"><?=$event_start_date; ?> | <?= $item->city_id;?>, <?= $item->country_id;?> </span>
                </div>
                <span class="conatcterr" id="nameerr<?=$item->id?>" style="display:none;">Fill Your Name</span>
                <span class="conatcterr" id="phoneerr<?=$item->id?>" style="display:none;">Fill Your Phnone Number</span>
                <span class="conatcterr" id="validphoneerr<?=$item->id?>" style="display:none;">Fill Valid Phnone Number</span>
                <span class="conatcterr" id="texterr<?=$item->id?>" style="display:none;">Fill Your Message</span>
                <form class="contact-nw-form" id="contactnow<?=$item->id?>">
                  <input type="text" name="name" id="name<?=$item->id?>" placeholder="Your Name" class="input-create-fld">
                  <input type="text" name="phone" id="phone<?=$item->id?>" placeholder="Your Number" class="input-create-fld">
                  <textarea cols="" rows="4" name="text" id="text<?=$item->id?>" class="input-create-fld"><?php echo Yii::$app->params['contactMessage'];?></textarea>
                  <button class="shorlist-btn save" id="" onclick="contactnow(<?=$item->id?>); return false;">Send</button>
                  <button class="shorlist-btn cancel " onclick="closepopcontact(<?=$item->id?>); return false;">Cancel</button>
                </form>
              </div>
              <li class="shorlist"><a>Shortlist Now</a></li>
              <div class="shorlist-data-cont" style="display:none;" id="shortlistarea<?=$item->id?>">
                <span class="close-shortlist close-popup"></span>
                <div class="shorlist-heading">
                  <h4>Shortlist &amp; save this listing</h4>
                  <p>
                    Organize your selection into folders for future references
                  </p>
                  <div id="folder-list<?=$item->id?>">
                    <?php if($folderlist){
                      foreach($folderlist as $folder)
                      {
                        echo '<br><input type="checkbox" name="folder" value="'.$folder['id'].'">'.$folder['foldername'];		
                      } } ?>
                    </div>
                  </div>
                  <form class="id"  id="Frmshortlist">
                   <div class="create-folder">
                     <p>Create a new folder</p>
                     <span class="conatcterr" id="shortfoldererr<?=$item->id?>" style="display:none;">Please Enter FolderName</span>
                     <input type="text" name="shortfoldername" id="shortfoldername<?=$item->id?>" placeholder="e.g Apple Activation Delhi" class="input-create-fld">
                     <button class="add-fld-btn" id="" onclick="shortlist_add_folder(<?=$item->id?>); return false;">
                       <i class="fa fa-folder-open-o" aria-hidden="true"></i> Add</button>
                     </div>
                     <button class="shorlist-btn save" id="" onclick="shortlistevent(<?=$item->id?>); return false;">Save</button>
                     <button class="shorlist-btn cancel" onclick="closepopshortlist(<?=$item->id?>); return false;">Cancel</button>
                   </form>
                 </div>
                 <li><a href="<?= Url::home() ?>theme/<?=$themename?>?id=<?=$item->id?>">More Info</a></li>
               </ul>
             </div>
           </div>
         </div>
       <?php endforeach; endif;?>
        </div>
     </div>
     <div class="paginat">
      <?php
      echo LinkPager::widget(['pagination' => $pages,]);
      ?>
    </div>  
  </div>
</section>



