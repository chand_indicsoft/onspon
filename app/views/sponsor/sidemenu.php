<?php
use yii\helpers\Url;

$session = Yii::$app->session;
$this->registerJs('$("#about").click(function(){
		$("#about-sub").slideToggle();
		$("#menu ul a#about span").toggleClass("open");
	});
	$("#customer-says").click(function(){
		$("#customer-says-sub").slideToggle();
		$("#menu ul a#customer-says span").toggleClass("open");
	});

	$("#legal").click(function(){
		$("#legal-sub").slideToggle();
		$("#menu ul a#legal span").toggleClass("open");
	});
	$(".hamburger-menu.toggle-x").on("click", function(){
		$(".hamburger-menu.toggle-x").toggleClass("current");
	});
	$(".toggle-x").click(function(){
		$("#menu").toggleClass("active");
	});

	$(document).ready(function () {
		$("#nav > li > a").click(function(){
			if ($(this).attr("class") != "active"){
				$("#nav li ul").slideUp();
				$(this).next().slideToggle();
				$("#nav li a").removeClass("active");
				$(this).addClass("active");
			}
		});
	});');
?>

<ul>
	<li><a href="<?= Url::home() ?>" class="active">Home</a></li>
	<li>
		<a href="javascript:void(0);" id="about">About <span class="pull-right"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></a>
		<ul id="about-sub">
			<li><a href="<?php echo Url::to(['/site/aboutus/'], true);?>">About Onspon</a></li>
			<li><a href="<?php echo Url::to(['/site/howitworks/'], true);?>">How it work</a></li>
			<li><a href="<?php echo Url::to(['/site/faq/'], true);?>">FAQs</a></li>
		
		</ul>
	</li>
	<li>
		<a href="javascript:void(0);" id="customer-says">Customer say us <span class="pull-right"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></a>
		<ul id="customer-says-sub">
			<li><a href="<?php echo Url::to(['/site/clientele/'], true);?>">Clientele</a></li>
			<li><a href="<?php echo Url::to(['/site/testimonial/'], true);?>">testimonials</a></li>
			<li><a href="javascript:void(0);">Sponsonrs</a></li>
		</ul>
	</li>
	<li>
		<a href="javascript:void(0);" id="legal">Legal<span class="pull-right"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></a>
		<ul id="legal-sub">
			<li><a href="<?php echo Url::to(['/site/terms/'], true);?>">Terms of use</a></li>
			<li><a href="javascript:void(0);">Privacy policy</a></li>
			<li><a href="<?php echo Url::to(['/site/contact/'], true);?>">contact us</a></li>
		
		</ul>
	</li>
</ul>
