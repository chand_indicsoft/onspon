<style>
.basic > h5, .basic > h6, .basic > label {
    color: #ddd !important;
}

	div#newfieldid {
    padding-top: 20px;
    padding-bottom: 10px;
    border-top: 2px solid red;
}
</style>
<?php
use yii\easyii\widgets\DateTimePicker;
use yii\easyii\helpers\Image;
use yii\easyii\widgets\TagsInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\easyii\widgets\Redactor;
use yii\easyii\widgets\SeoForm;
use app\modules\eventcategories\api\EventCategories;
use app\modules\eventgenre\api\EventGenre;
use app\modules\agegroup\api\AgeGroup;
use app\modules\education\api\Education;
use app\modules\income\api\Income;
use app\modules\city\api\City;
use yii\easyii\widgets\Photos;
use app\modules\state\api\State;
use app\modules\country\api\Country;
use app\modules\role\api\Role;
use kartik\time\TimePicker;
use yii\easyii\models\Photo;
use app\models\Commonhelper;
use app\modules\events\models\Events;
use app\models\CreateEvents;
use app\modules\audience\api\Audience;

$asset = \app\assets\EventAppAsset::register($this);

if($model->event_genre_id){
	$model->event_genre_id = explode(",",$model->event_genre_id);	
}
if($model->slabname){
	$slabname = explode(",",$model->slabname);	
}
if($model->sponsorprice){
	$sponsorprice = explode(",",$model->sponsorprice);	
}
if($model->deliverables){
	$deliverables = explode(",",$model->deliverables);	
}


$this->registerJs('$(document).ready(function(e) {
        $(".dropDown_COntnr>div").click(function() {
			$(".dropDown_COntnr > ul").slideToggle(400);
		});
		
	});');

$this->registerJs('$(document).ready(function(e) {
        $(".Sponsership_Popup > i").click(function(){
			$(".Sponsership_Popup").hide();
		});
		
    	});');
		
		
			
		
?>

<!--====Drop_Down====-->

<main class="CreateEvent_MainCOntnr" id ="firststep">
	
	<div class="container">
    	<div class="row">
        	<div class="col-md-12">
            	<h3>List on <a href="javascript:void(0);">Onspon.com</a></h3>
                <h4>Start off by creating a listing page. Think of it as a profile page for your Event.</h4>
            </div>
        </div>
        <div class="row">
        	<div class="col-md-6 col-sm-6 col-xs-12">
            	<div class="list_COntnr">
                    <h5>STEP 1</h5>
                    <h6>Start With Basic Listing Details</h6>
                    <label>Name, Venue etc</label>
                    <a href="javacsript:void(0);" onclick="secondpage('1'); return false;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">continue</a>
                 <!--   <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">
                        Continue
                 </button>-->
                </div>
                <div class="list_COntnr basic">
                    <h5>STEP 2</h5>
                    <h6>Tell Us About Your Audience</h6>
                    <label>Audience Profile, Footfalls, Pictures, Videos</label>
                    
                </div>
                <div class="list_COntnr basic">
                    <h5>STEP 3</h5>
                    <h6>Your Sponsorship Details</h6>
                    <label>Sponsors, Sponsorship Slabs etc</label>
                </div>
                <div class="list_COntnr basic">
                    <h5>STEP 3</h5>
                    <h6>Your Event Description</h6>
                    <label>Description, Photo Gallery etc</label>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
            	<img src="<?= $asset->baseUrl ?>/image/sponsership_img.png" >
                <div class="Sponsership_COntnr">
                	<p><img src="<?= $asset->baseUrl ?>/image/bell.png"></p>
                    <p>This will also automatically create your ‘<span>event page</span>’. 
Average sponsorships on Onspon.com is :</p>
					<p><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;1,35,000</p>
                </div>
            </div>
        </div>
    </div>
</main>

<!-------------------------------------Second Step---------------------------------------->

<main id="secondstep" style="display:none">
	<div class="progress">
      <div class="progress-bar" role="progressbar" aria-valuenow="70"
          aria-valuemin="0" aria-valuemax="100" style="width:0%">
            <span class="sr-only">0% Complete</span>
      </div>
    </div>
	<div class="container"> 
        <div class="row">
        	<div class="col-md-6 col-sm-6 col-xs-12">
            <div class="left_Sec">
            <div class="leftSec_Inner_Contr">
            	<h3 class="headingCOntr">Basic Details</h3>
                
			<?php $form = ActiveForm::begin([
			'id' => 'secondstepform',
			'action' => Url::to(['/eventcreate/create']),
			'enableAjaxValidation' => false, ]); ?>
			
					
					<div class="form-group">
						
						<?= $form->field($model, 'event_name')->textInput(['class'=>'form-control','placeholder' => 'Event Name']); ?>
					</div>
					<div class="form-group">
                		<label>Add Time</label> 
                        <input type ="checkbox" id="addtime" onclick="addTime(this);">
					</div>
                    <div class="row">
                    	
                	<div class="col-md-6 col-sm-6 col-xs-12">
						
                		<div class="form-group">                         
                          <?= $form->field($model, 'event_strt_date')->widget(DateTimePicker::className(), ['options'=>['format'=>Yii::$app->params['dateFormat']]]); ?>
                        </div>
                        
                        <div class="form-group" id ='starttime' style="display:none;">
                        	
                        	<?= $form->field($model, 'event_strt_time')->widget(TimePicker::className(),['pluginOptions' => [
							        'showSeconds' => true,
							         'defaultTime' => '09:00:00 AM',
							        ]]); ?>
                        </div>
                	</div>
                      
                    <div class="col-md-6 col-sm-6 col-xs-12">
                		<div class="form-group">
                        	
                        	<?= $form->field($model, 'event_end_date')->widget(DateTimePicker::className(), ['options'=>['format'=>Yii::$app->params['dateFormat']]]); ?>
                          
                        </div>
                        <div class="form-group" id ='endtime' style="display:none;">
                          
                          <?= $form->field($model, 'event_end_time')->widget(TimePicker::className(),['pluginOptions' => [
							        'showSeconds' => true,
							         'defaultTime' => '09:00:00 PM',
							        'class' => 'form-control']]); ?>
							                         
                        </div>
                	</div>
                	</div>
                <div class="form-group">
                  <?= $form->field($model, 'contact')->textInput(['class'=>'form-control','value' => '+91-']); ?>
                	<div id="err-msg-reachus-contact" style="color:red; font-weight: 700;"></div>
                </div>
                <div class="form-group">
                    <?= $form->field($model, 'email')->textInput(['class'=>'form-control','placeholder' => 'Email Id']); ?>
					<div id="err-msg-reachus-email" style="color:red; font-weight: 700;"></div>
					<?= $form->field($model, 'id')->hiddenInput()->label(false); ?>
					
					
                </div>
  <?php ActiveForm::end(); ?>              
                
                </div>
                 <div class="BackBtn_Contnr firstpagenext">
                    <button onclick="sumbitfirstphase('second'); return false;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">
                        Next
                    </button>
                </div>
            </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
            	<div class="Map_Contnr">
                	<div class="Sponsership_COntnr listingRight_COntnr">
                	<p><img src="<?= $asset->baseUrl ?>/image/bell2.png"></p>
                    <h4>Enter Basic Details for Create Event</h4>
                    <p>Event Dates, Event Timings, Contact Number & Email Id</p>
                </div>
            </div>
        </div>
   
	</div>
</main>
 
<!--------------------------------------------------- second step end-------------------------------------->

<!--------------------------------------------------- Third step -------------------------------------->

<main id="thirdstep" style="display:none;">
	<div class="progress">
      <div class="progress-bar" role="progressbar" aria-valuenow="70"
          aria-valuemin="0" aria-valuemax="100" style="width:11%">
            <span class="sr-only">11% Complete</span>
      </div>
 </div>
	<div class="container">
        <div class="row">
        	<div class="col-md-6 col-sm-6 col-xs-12">
            <div class="left_Sec">
            <div class="leftSec_Inner_Contr">
            	<h3 class="headingCOntr">Where’s your place located?</h3>
		<?php $form = ActiveForm::begin([
		'enableAjaxValidation' => false,
		'id' => 'thirdstepform',
		'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
		]); ?>            
                
                <?= $form->field($model, 'id')->hiddenInput()->label(false); ?>
                <div class="form-group">
                  <label for="usr">Locality:</label>
                  <?= $form->field($model, 'address')->textInput(['class'=>'form-control','id'=>'event-address','onclick' => 'initialize();' ])->label(false)?>
                </div>
                <div class="form-group">
                  <label for="pwd">Address(optional):</label>
                  <?= $form->field($model, 'address1')->textInput(['class'=>'form-control','placeholder'=>'Apt., suite, building access code','id'=>'event-address1' ])->label(false)?>
                </div>
                <div class="row"> 
                <div id="cityhide" style="display:none">
                	<div class="col-md-6 col-sm-6 col-xs-12">
                		<div class="form-group">
                          <label for="usr">City:</label>
                           <?= $form->field($model, 'city_id')->textInput(['disabled' => 'disabled','class'=>'form-control','placeholder'=>'City','id'=>'event-city' ])->label(false)?>
                        </div>
                        <div class="form-group">
		                  <label for="sel1">Country</label>
		                  <?= $form->field($model, 'country_id')->textInput(['disabled' => 'disabled','class'=>'form-control','placeholder'=>'Country','id'=>'event-country' ])->label(false)?>
                		</div>
                        
                	</div> 
                    <div class="col-md-6 col-sm-6 col-xs-12">
                		<div class="form-group">
                          <label for="usr">State:</label>
                           <?= $form->field($model, 'state_id')->textInput(['disabled' => 'disabled','class'=>'form-control','placeholder'=>'State','id'=>'event-state' ])->label(false)?>
                        </div>
                        <div class="form-group">
                          <label for="usr">Zip Code:</label>
                          <?= $form->field($model, 'zipcode')->textInput(['class'=>'form-control','placeholder'=>'Enter Zip Code','id'=>'event-zipcode' ])->label(false)?>
                        	</div>
                        </div>
                	</div>
                	
		                	<?= $form->field($model, 'lat')->hiddenInput()->label(''); ?>
		
							<?= $form->field($model, 'lng')->hiddenInput()->label(''); ?>
							<?= $form->field($model, 'id')->hiddenInput()->label(false); ?>
                </div>
                 <?php ActiveForm::end(); ?>
                 </div>
                <div class="BackBtn_Contnr">
                	<a href="javascript:void(0);" onclick="stepback('thirdstep');"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;Back</a>
                    <button onclick="sumbitfirstphase('third'); return false;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">
                        Next
                    </button>
                </div>
            </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
            	<div class="Map_Contnr">
					
				
					
					
                	<div id="'map-canvas" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></div>
                	 </div>
                
            </div>
        </div> 
    </div>
</main>
<!----------------------------------------Third step end------------------------------------>

<!------------------------------------Fourth Step--------------------------------------------------->

<main id="fourthstep" style="display:none;">
	<div class="progress">
          <div class="progress-bar" role="progressbar" aria-valuenow="70"
              aria-valuemin="0" aria-valuemax="100" style="width:22%;">
                <span class="sr-only">22% Complete</span>
          </div>
     </div>
	<div class="container">
        <div class="row">
        	<div class="col-md-6 col-sm-6 col-xs-12">
            	<div class="left_Sec">
                <div class="leftSec_Inner_Contr">
                <div class="Drop_Down_Main_Contnr">
				<?php $form = ActiveForm::begin([
				'enableAjaxValidation' => false,
				'id' => 'fourthstepform',
				'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
				]); ?>    
                <h3 class="headingCOntr">What kind of Event are you listing?</h3>
                
                 <?= $form->field($model, 'id')->hiddenInput()->label(false); ?>      
				<div class="form-group">
					<?= $form->field($model, 'event_category_id')->dropDownList(EventCategories::eventcategoriesName(), ['prompt'=>'Choose Event Category']);  ?>
				</div>
				<div class="form-group">
					<?= $form->field($model, 'event_genre_id')->label("Select Genre")            
					 ->checkboxList(EventGenre::eventgenreName(),
					 ['class'=>'mdl-checkbox__input','onchange'=>'checkedboxgenre();']); ?>
				</div>
                  <?php ActiveForm::end(); ?>
                </div> 
                </div>           
                <div class="BackBtn_Contnr">
                	<a href="javascript:void(0);" onclick="stepback('fourthstep');"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;Back</a>
                    <button onclick="sumbitfirstphase('fourth'); return false;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">
                        Next
                    </button>
                </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
            	<div class="Map_Contnr">
                	<div class="Sponsership_COntnr listingRight_COntnr">
                	<p><img src="<?= $asset->baseUrl ?>/image/bell2.png"></p>
                    <h4>Entertainment</h4>
                    <p>Like Concerts, Fun Corporate Events</p>
                    <h4>Sports</h4>
                    <p>From Marathons to Cricket Events. Everything around sports as a central theme</p>
					<h4>Conference / Exhibition</h4>
                    <p>Business events ? All here</p>
                    <h4>College Festivals</h4>
                    <p>You know what it is. Don’t you ?</p>
                </div>
                </div>
            </div>
        </div>
    </div>
    </main>

<!--------------------------------------------fourth step end------------------------------------>

<!--------------------------------------------5th step start------------------------------------>

<main id="fivthstep" style="display:none;">
	<div class="progress">
          <div class="progress-bar" role="progressbar" aria-valuenow="70"
              aria-valuemin="0" aria-valuemax="100" style="width:33%">
                <span class="sr-only">33% Complete</span>
          </div>
     </div>
	<div class="container">
        <div class="row">
        	<div class="col-md-6 col-sm-6 col-xs-12">
            <div class="left_Sec">
            <div class="leftSec_Inner_Contr">
            	<h3 class="headingCOntr">Lets know more about your event</h3>
           <?php $form = ActiveForm::begin([
			'enableAjaxValidation' => false,
			'id' => 'fivthstepform',
			'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
			]); ?>   	
           
                <div class="Drop_Down_Main_Contnr">
                <div class="dropDown_COntnr">
                	<ul class="EventList_Contnr">
                    	<li>
                        	
                            	<div class="drpdwon_Icontxt">
                            	<i><img src="<?= $asset->baseUrl ?>/image/ticket.png" /></i>Is it ticketed ?</div>
                                <div class="drpdwon_Chkbox">
                                     <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="android">
                                        <?= $form->field($model, 'ticketed')->checkbox(['id' => 'android','class' => 'mdl-checkbox__input','onclick'=>'valueChanged();'])->label(false); ?>
                                      </label>
                                 </div>
                           
                        </li>
                        <li>
                        	
                            	<div class="drpdwon_Icontxt">
                            	<i><img src="<?= $asset->baseUrl ?>/image/tv.png" /></i>Is it televised ?</div>
                                <div class="drpdwon_Chkbox">
                                     <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="android1">
                                        <?= $form->field($model, 'televised')->checkbox(['id' => 'android1','class' => 'mdl-checkbox__input'])->label(false); ?>
									  </label>
                                 </div>
                           
                        </li>
                        <li>
                        	
                            	<div class="drpdwon_Icontxt">
                            	<i><img src="<?= $asset->baseUrl ?>/image/calender.png" /></i>Happens every year ?</div>
                                <div class="drpdwon_Chkbox">
                                     <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="android2">
                                        <?= $form->field($model, 'every_yr')->checkbox(['id' => 'android2','class' => 'mdl-checkbox__input'])->label(false); ?>
                                      </label>
                                 </div>
                           
                        </li>
                        <li>
                        	
                            	<div class="drpdwon_Icontxt">
                            	<i><img src="<?= $asset->baseUrl ?>/image/tv.png" /></i>Is it Multicity event ?</div>
                                <div class="drpdwon_Chkbox">
                                     <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="androidk">
                                        <?= $form->field($model, 'multicity_event')->checkbox(['id' => 'androidk','class' => 'mdl-checkbox__input'])->label(false); ?>
                                      </label>
                                 </div>
                           
                        </li>
                    </ul>
                    
                   
                </div>
                </div> 
                 <div class="form-group" id="ticketlink" style="display:none;">
                  <label for="usr">Ticket Link:</label>
                  <?= $form->field($model, 'ticket_text')->textInput(['class'=>'form-control','id'=>'ticket-link' ])->label(false)?>
                </div>
                 <?= $form->field($model, 'id')->hiddenInput()->label(false); ?>
               <?php ActiveForm::end(); ?>
               </div>             
                <div class="BackBtn_Contnr">
                	<a href="javascript:void(0);"  onclick="stepback('fivthstep');"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;Back</a>
                    <button onclick="sumbitfirstphase('fivth'); return false;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">
                        Next
                    </button>
                </div>
            </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
            	<div class="Map_Contnr">
                	<div class="Sponsership_COntnr listingRight_COntnr">
                	<p><img src="<?= $asset->baseUrl ?>/image/bell2.png"></p>
                    <h4>Ticketed</h4>
                    <p>Select if its restricted access to ticket / pass holders.</p>
                    <h4>Televised</h4>
                    <p>Select only if you have a television partner. Minor visibility in local news channels etc don’t count</p>
					<h4>Every Year</h4>
                    <p>Most events happen across every year. Its easier for you to renew on Onspon by a simple click post event
completion. Simple ?</p>
                </div>
                </div>
            </div>
        </div>
    </div>
</main>

<!--------------------------------------------5th step end------------------------------------>

<!--------------------------------------------step six start------------------------------------>

<main class="CreateEvent_MainCOntnr" id ="sixthstep" style="display:none;">
	
	<div class="container">
    	<div class="row">
        	<div class="col-md-12">
            	<h3>List on <a href="javascript:void(0);">Onspon.com</a></h3>
                <h4>Start off by creating a listing page. Think of it as a profile page for your Event.</h4>
            </div>
        </div>
        <div class="row">
        	<div class="col-md-6 col-sm-6 col-xs-12">
            	<div class="list_COntnr basic">
                    <h5>STEP 1</h5>
                    <h6>Start With Basic Listing Details</h6>
                    <label>Name, Venue etc</label>
                    <a href="javacsript:void(0);" onclick="editstep('1'); return false;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">Edit</a>
                
                   
                 <!--   <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">
                        Continue
                 </button>-->
                </div>
                <div class="list_COntnr">
                    <h5>STEP 2</h5>
                    <h6>Tell Us About Your Audience</h6>
                    <label>Audience Profile, Footfalls, Pictures, Videos</label>
                     <a href="javacsript:void(0);" onclick="secondpage('2'); return false;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">continue</a>
                </div>
                <div class="list_COntnr basic">
                    <h5>STEP 3</h5>
                    <h6>Your Sponsorship Details</h6>
                    <label>Sponsors, Sponsorship Slabs etc</label>
                </div>
                <div class="list_COntnr basic">
                    <h5>STEP 4</h5>
                     <h6>Your Event Description</h6>
                    <label>Description, Photo Gallery etc</label>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
            	<img src="<?= $asset->baseUrl ?>/image/sponsership_img.png" >
                <div class="Sponsership_COntnr">
                	<p><img src="<?= $asset->baseUrl ?>/image/bell.png"></p>
                    <p>This will also automatically create your ‘<span>event page</span>’. 
Average sponsorships on Onspon.com is :</p>
					<p><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;1,35,000</p>
                </div>
            </div>
        </div>
    </div>
</main>



<!--------------------------------------------step six end------------------------------------>

<!--------------------------------------------step seventh start------------------------------------>

<main id="seventhstep" style="display:none;">
	<div class="progress">
      <div class="progress-bar" role="progressbar" aria-valuenow="70"
          aria-valuemin="0" aria-valuemax="100" style="width:55%">
            <span class="sr-only">55% Complete</span>
      </div>
 </div>
	<div class="container">
        <div class="row">
        	<div class="col-md-6 col-sm-6 col-xs-12">
            	<div class="left_Sec">
                <div class="leftSec_Inner_Contr">
            	<h3 class="headingCOntr">Audience Profile</h3>
            	<?php $form = ActiveForm::begin([
					'enableAjaxValidation' => false,
					'id' => 'seventhstepform',
					'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
				]); ?> 
                 <div class="Drop_Down_Main_Contnr">
                <div class="dropDown_COntnr">
                	<div>
                    	Select Audience Profile<i class="fa fa-angle-down" aria-hidden="true"></i>
                    </div>
                	<ul class="sponsorsOffer_COntnr">
                    	<li>
                        	
                                <div class="drpdwon_Icontxt">
                                     	<?= $form->field($model, 'audience_profile')->label(false)            
										 ->checkboxList(Audience::audienceName(),
										 ['onchange'=>'checkedboxaudience();']); ?>
                                     
                                      </label>
                                </div>
                           
                        </li>
                    </ul>
                </div>
                </div>
                </div>
                <?= $form->field($model, 'id')->hiddenInput()->label(false); ?>
				
		 <?php ActiveForm::end(); ?>
                <div class="BackBtn_Contnr">
                	<a href="javascript:void(0);" onclick="stepback('seventhstep');"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;Back</a>
                    <button onclick="sumbitfirstphase('seventh'); return false;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">
                        Next
                    </button>
                </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
            	<div class="Map_Contnr">
                	<div class="Sponsership_COntnr listingRight_COntnr PckgesDescCOntnr">
                	<p><img src="<?= $asset->baseUrl ?>/image/bell2.png"></p>
                    <p>Every brand wants to reach to a different audience. Fill in this section for them to chose your event basis their target audience. You may skip this section but skipping this would mean losing out on appearance in search results when a sponsor searches for events matching their brand needs.</p>
                    <p><span>*</span>If your event caters to a specific age group(s), provide the audience mix below. (Age Group)</p>
                    <p><span>*</span>If your event caters to a specific educational level, provide the audience mix below. (Education)</p>
                    <p><span>*</span>If your event caters to a specific income group, provide the audience mix below. (Income)</p>
                    <p><span>*</span>If your event caters to a specific gender, provide the audience mix below. (Gender)</p>
                </div>
                
            </div>
        </div>
    </div>
</main>




<!--------------------------------------------step seventh end------------------------------------>

<!--------------------------------------------step eighth start------------------------------------>

<main id="eighthstep" style="display:none;">
	<div class="progress">
          <div class="progress-bar" role="progressbar" aria-valuenow="70"
              aria-valuemin="0" aria-valuemax="100" style="width:44%">
                <span class="sr-only">66% Complete</span>
          </div>
     </div>
	<div class="container">
        <div class="row">
        	<div class="col-md-6 col-sm-6 col-xs-12">
            <div class="left_Sec">
            <div class="leftSec_Inner_Contr">
            	<h3 class="headingCOntr">How many attendees will be visiting your event ?</h3>
            <?php $form = ActiveForm::begin([
		'enableAjaxValidation' => false,
		'id' => 'eighthstepform',
		'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
		]); ?>   	
            	
                <div class="form-group">
					 <?= $form->field($model, 'no_of_attendees')->dropDownList(["Less than 50"=>"Less than 50", "50-200"=>"50-200", "200-1000"=>"200-1000", "5000-20000"=>"5000-20000","20000+"=>"20000+"], ['class' => 'form-control','prompt'=>'Choose'])->label(false); ?>

				</div>  
				<?= $form->field($model, 'id')->hiddenInput()->label(false); ?>
				
		 <?php ActiveForm::end(); ?>
         </div>		
                <div class="BackBtn_Contnr">
                	<a href="javascript:void(0);" onclick="stepback('eighthstep');"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;Back</a>
                    <button onclick="sumbitfirstphase('eighth'); return false;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">
                        Next
                    </button>
                </div>
            </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
            	<div class="Map_Contnr">
                	<div class="Sponsership_COntnr listingRight_COntnr">
                	<p><img src="<?= $asset->baseUrl ?>/image/bell2.png"></p>
                    <h4>Audience Profle</h4>
                    <p>Sponsors like to know who comes and how their brand will speak to these people. Try to be select the right heads</p>
                </div>
                </div>
            </div>
        </div>
    </div>
</main>



<!--------------------------------------------step eighth end------------------------------------>

<!--------------------------------------------step nineth start------------------------------------>

<main class="CreateEvent_MainCOntnr" id ="ninethstep" style="display:none;">
	
	<div class="container">
    	<div class="row">
        	<div class="col-md-12">
            	<h3>List on <a href="javascript:void(0);">Onspon.com</a></h3>
                <h4>Start off by creating a listing page. Think of it as a profile page for your Event.</h4>
            </div>
        </div>
        <div class="row">
        	<div class="col-md-6 col-sm-6 col-xs-12">
            	<div class="list_COntnr basic">
                    <h5>STEP 1</h5>
                    <h6>Start With Basic Listing Details</h6>
                    <label>Name, Venue etc</label>
                   <a href="javacsript:void(0);" onclick="editstep('1'); return false;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">Edit</a>
                
                 <!--   <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">
                        Continue
                 </button>-->
                </div>
                <div class="list_COntnr basic">
                    <h5>STEP 2</h5>
                    <h6>Tell Us About Your Audience</h6>
                    <label>Audience Profile, Footfalls, Pictures, Videos</label>
                    <a href="javacsript:void(0);" onclick="editstep('2'); return false;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">Edit</a>
                
                      </div>
                <div class="list_COntnr ">
                    <h5>STEP 3</h5>
                    <h6>Your Sponsorship Details</h6>
                    <label>Sponsors, Sponsorship Slabs etc</label>
                    <a href="javacsript:void(0);" onclick="secondpage('3'); return false;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">continue</a>
               
                </div>
                <div class="list_COntnr basic">
                    <h5>STEP 4</h5>
                    <h6>Your Event Description</h6>
                    <label>Description, Photo Gallery etc</label>
                   
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
            	<img src="<?= $asset->baseUrl ?>/image/sponsership_img.png" >
                <div class="Sponsership_COntnr">
                	<p><img src="<?= $asset->baseUrl ?>/image/bell.png"></p>
                    <p>This will also automatically create your ‘<span>event page</span>’. 
Average sponsorships on Onspon.com is :</p>
					<p><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;1,35,000</p>
                </div>
            </div>
        </div>
    </div>
</main>


<!--------------------------------------------step nineth end------------------------------------>

<!--------------------------------------------step tenth start------------------------------------>

<main id="tenthstep" style="display:none;">
	<div class="progress">
      <div class="progress-bar" role="progressbar" aria-valuenow="70"
          aria-valuemin="0" aria-valuemax="100" style="width:77%">
            <span class="sr-only">77% Complete</span>
      </div>
 </div>
	<div class="container">
        <div class="row">
        	<div class="col-md-6 col-sm-6 col-xs-12">
            <div class="left_Sec">
            	<div class="leftSec_Inner_Contr">
            	<h3 class="headingCOntr">Select what of the below can you offer to sponsors</h3>
            	<?php $form = ActiveForm::begin([
					'enableAjaxValidation' => false,
					'id' => 'tenthstepform',
					'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
				]); ?>
                <div class="Drop_Down_Main_Contnr">
                <div class="dropDown_COntnr">
                	<div>
                    	Select Category<i class="fa fa-angle-down" aria-hidden="true"></i>
                    </div>
                	<ul class="sponsorsOffer_COntnr">
                    	<li>
                        	
                                <div class="drpdwon_Icontxt">
                                     <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="android3">
                                        <?= $form->field($model, 'event_passes_ticket')->checkbox(['id' => 'android3','class' => 'mdl-checkbox__input'])->label(false); ?>
                                      </label>
                                </div>
                                <div class="sponsors_Offer">
                                	<p>Event Passes / Tickets</p>
                            		<p>Please select only if it’s a ticketed event</p>
                                </div>
                           
                        </li>
                        <li>
                        	
                                <div class="drpdwon_Icontxt">
                                     <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="android4">
                                        <?= $form->field($model, 'pre_event_buzz')->checkbox(['id' => 'android4','class' => 'mdl-checkbox__input'])->label(false); ?>
                                      </label>
                                </div>
                                <div class="sponsors_Offer">
                                	<p>Pre-event buzz</p>
                            		<p>Select this if you would be sending emails / flyers to get audience</p>
                                </div>
                            
                        </li>
                        <li>
                        	
                                <div class="drpdwon_Icontxt">
                                     <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="android5">
                                        <?= $form->field($model, 'ticket_branding')->checkbox(['id' => 'android5','class' => 'mdl-checkbox__input'])->label(false); ?>
                                      </label>
                                </div>
                                <div class="sponsors_Offer">
                                	<p>Ticket Branding</p>
                            		<p>Do you have a physical / virtual ticket ? Can that be branded</p>
                                </div>
                            
                        </li>
                        <li>
                        	
                                <div class="drpdwon_Icontxt">
                                     <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="android6">
                                        <?= $form->field($model, 'database_collecttion')->checkbox(['id' => 'android6','class' => 'mdl-checkbox__input'])->label(false); ?>
                                      </label>
                                </div>
                                <div class="sponsors_Offer">
                                	<p>Database</p>
                            		<p>If you collect data of participants and can share it with brands</p>
                                </div>
                            
                        </li>
                         <li>
                        	
                                <div class="drpdwon_Icontxt">
                                     <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="androida">
                                        <?= $form->field($model, 'paid_media')->checkbox(['id' => 'androida','class' => 'mdl-checkbox__input'])->label(false); ?>
                                      </label>
                                </div>
                                <div class="sponsors_Offer">
                                	<p>Paid Media</p>
                            		<p>Do you have media commitments – please select if you have paid</p>
                                </div>
                            
                        </li>
                         <li>
                        	
                                <div class="drpdwon_Icontxt">
                                     <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="androidb">
                                        <?= $form->field($model, 'social_media_sharing')->checkbox(['id' => 'androidb','class' => 'mdl-checkbox__input'])->label(false); ?>
                                      </label>
                                </div>
                                <div class="sponsors_Offer">
                                	<p>Social Media sharing about event</p>
                            		<p>Will you be talking about the brand on your social media ?</p>
                                </div>
                            
                        </li>
                         <li>
                        	
                                <div class="drpdwon_Icontxt">
                                     <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="androidc">
                                        <?= $form->field($model, 'celebrity_tweets')->checkbox(['id' => 'androidc','class' => 'mdl-checkbox__input'])->label(false); ?>
                                      </label>
                                </div>
                                <div class="sponsors_Offer">
                                	<p>Celebrity Tweets about the event and sponsor</p>
                            		<p>Would you have celebrity visitors or supporters ? SM > 100K</p>
                                </div>
                            
                        </li>
                         <li>
                        	
                                <div class="drpdwon_Icontxt">
                                     <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="androidd">
                                        <?= $form->field($model, 'alchohol_licence')->checkbox(['id' => 'androidd','class' => 'mdl-checkbox__input'])->label(false); ?>
                                      </label>
                                </div>
                                <div class="sponsors_Offer">
                                	<p>Alcohol license</p>
                            		<p>Select if you would be serving alcohol</p>
                                </div>
                            
                        </li>
                         <li>
                        	
                                <div class="drpdwon_Icontxt">
                                     <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="androide">
                                        <?= $form->field($model, 'sampling')->checkbox(['id' => 'androide','class' => 'mdl-checkbox__input'])->label(false); ?>
                                      </label>
                                </div>
                                <div class="sponsors_Offer">
                                	<p>Sampling</p>
                            		<p>Can you arrange a brand to sample their products ?</p>
                                </div>
                            
                        </li>
                        <li>
                        	
                                <div class="drpdwon_Icontxt">
                                     <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="androidf">
                                        <?= $form->field($model, 'speaking_opportunities')->checkbox(['id' => 'androidf','class' => 'mdl-checkbox__input'])->label(false); ?>
                                      </label>
                                </div>
                                <div class="sponsors_Offer">
                                	<p>Speaking Opportunity for a sponsor representative</p>
                            		<p>Can the sponsor representative address the audience ?</p>
                                </div>
                            
                        </li>
                    </ul>
                </div>
                </div>       
                		<?= $form->field($model, 'id')->hiddenInput()->label(false); ?>
				
		 		<?php ActiveForm::end(); ?>
                </div>  
		 		<div class="BackBtn_Contnr">
                	<a href="javascript:void(0);" onclick="stepback('tenthstep');"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;Back</a>
                    <div class="NextBtn_Contnr">
                        <button onclick="sumbitfirstphase('tenth'); return false;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">
                            Next
                        </button>
                    </div>
              	</div> 
               </div>
            </div>
            
            <div class="col-md-6 col-sm-6 col-xs-12">
            	<div class="Map_Contnr">
                	<div class="Sponsership_COntnr listingRight_COntnr">
                	<p><img src="<?= $asset->baseUrl ?>/image/bell2.png"></p>
                    <h4>Why this data ?</h4>
                    <p>Providing this information clearly sets the expectations of the brand – thereby not leaving any room for confusion. If any of these are dependent on the sponsorship money given, that’s fine – choose the one corresponding to highest slab</p>
                </div>
                </div>
               
            </div>
        </div>
    </div>
</main>


<!--------------------------------------------step tenth end------------------------------------>

<!--------------------------------------------eleventh step start------------------------------------>
<main id="eleventhstep" style="display:none;">
	<div class="progress">
          <div class="progress-bar" role="progressbar" aria-valuenow="70"
              aria-valuemin="0" aria-valuemax="100" style="width:83%">
                <span class="sr-only">83% Complete</span>
          </div>
     </div>
	<div class="container">
        <div class="row">
        	<div class="col-md-6 col-sm-6 col-xs-12">
            <div class="left_Sec">
            <div class="leftSec_Inner_Contr">
            	<h3 class="headingCOntr">Has this event raised sponsorship before ?</h3>
           <?php $form = ActiveForm::begin([
			'enableAjaxValidation' => false,
			'id' => 'eleventhstepform',
			'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
			]); ?>   	
           
                <div class="Drop_Down_Main_Contnr">
                <div class="dropDown_COntnr">
                	<ul class="EventList_Contnr">
                    	<li>
                            	<div class="drpdwon_Icontxt">
                            	
                                <div class="drpdwon_Chkbox">
                                     <label class="">
                                        
                                        <?=	$form->field($model, 'earlier_sponsor')->radioList(['1' => 'I have', '0' => 'I am new to this'])->label(false);?>
                                      </label>
                                 </div>
                        </li>
                        
                        </div>
                    </ul>
                
                </div> 
                 <?= $form->field($model, 'id')->hiddenInput()->label(false); ?>
               <?php ActiveForm::end(); ?>
               </div>             
                <div class="BackBtn_Contnr">
                	<a href="javascript:void(0);"  onclick="stepback('eleventhstep');"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;Back</a>
                    <button onclick="sumbitfirstphase('eleventh'); return false;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">
                        Next
                    </button>
                </div>
            </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
            	<div class="Map_Contnr">
                	<div class="Sponsership_COntnr listingRight_COntnr">
                	<p><img src="<?= $asset->baseUrl ?>/image/bell2.png"></p>
                    
                    <p>This is important because earlier sponsorships would give you a broad idea on sponsorship expectation.</p>
                    
                    
                </div>
                </div>
            </div>
        </div>
    </div>
</main>

<!--------------------------------------------eleventh step end------------------------------------>


<!--------------------------------------------step twelveth start------------------------------------>

<main id="twelvethstep" style="display:none;">
	<div class="progress">
      <div class="progress-bar" role="progressbar" aria-valuenow="70"
          aria-valuemin="0" aria-valuemax="100" style="width:77%">
            <span class="sr-only">77% Complete</span>
      </div>
 </div>
	<div class="container">
        <div class="row">
        	<div class="col-md-6 col-sm-6 col-xs-12">
            	<div class="left_Sec">
                <div class="leftSec_Inner_Contr" id="page1">
            		<h3 class="headingCOntr">Sponsorship Levels</h3>
            <form id="eleventhform">	
                        <div class="form-group">
                          <label for="usr">Slab Title<span class="mandatory">*</span><span class="ex_Txtfeild">eg. Primary Sponsor, Presenting Sponsor, Platinum Sponsor etc</span></label>
                          <input class="form-control" name="slabname[]" placeholder="" type="text">
                        </div>
                        <div class="form-group">
                          <label for="usr">Price<span class="mandatory">*</span><span class="ex_Txtfeild">Kindly mention the amount sponsor has to pay to take this slot</span></label>
                          <input class="form-control" name="sponsorprice[]" placeholder="" type="text">
                        </div>
                        <div class="form-group">
                          <label for="usr">Deliverables<span class="mandatory">*</span></label>
                          <textarea class="form-control" rows ="5" name="deliverables[]"></textarea>
                        </div>
                        
                        <div id="addnewfeild"></div>
               </form>	         
                        <div class="form-group SponsorshipLevelsBtn_Contnr">
                            <a href="javascript:void(0);" onclick="stepback('twelvethstep');"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;Back</a>
                             <button onclick="sendtwelvethform(); return false;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">
                               Next
                            </button>
                            <button onclick="sendtwelvethform(); return false;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">
                               Skip
                            </button>
                            <button id="add_field_button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">
                               <i class="glyphicon glyphicon-plus font-12"></i> Add More Sponsorship Levels
                            </button>
                        </div>
                      </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
            	<div class="Map_Contnr">
                	<div class="Sponsership_COntnr listingRight_COntnr">
                	<p><img src="<?= $asset->baseUrl ?>/image/bell2.png"></p>
                    <p style="color:inherit;">Build your sponsorship packages to showcase winning investment opportunities for sponsors. Be sure to clearly define the benefits and value to help sponsors determine the ROI.</p>
                </div>
            </div>
        </div>
    </div>
</main>

<!--------------------------------------------step twelveth end------------------------------------>

<!--------------------------------------------step thirteenth end------------------------------------>

<main id ="thirteenthstep" style="display:none;">
	<div class="progress">
      <div class="progress-bar" role="progressbar" aria-valuenow="70"
          aria-valuemin="0" aria-valuemax="100" style="width:100%">
            <span class="sr-only">30% Complete</span>
      </div>
 </div>
	<div class="container">
        <div class="row">
        	<div class="col-md-6 col-sm-6 col-xs-12">
            	<div class="left_Sec">
            		
            		<?php $form = ActiveForm::begin([
							'enableAjaxValidation' => false,
							'id' => 'thirteenthstepform',
							'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
							]); ?>
                <div class="leftSec_Inner_Contr">
            	<h3 class="headingCOntr">What Spacess Can sponsers use ?</h3>
                <div class="Drop_Down_Main_Contnr">
                <div class="dropDown_COntnr">
                	<div>
                    	Select Category<i class="fa fa-angle-down" aria-hidden="true"></i>
                    </div>
                	<ul class="sponsorsOffer_COntnr">
                    	<li>
                        	
                                <div class="drpdwon_Icontxt">
                                     <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="androidz">
                                         <?= $form->field($model, 'basic_visibility')->checkbox(['id' => 'androidz','class' => 'mdl-checkbox__input'])->label(false); ?>
                                      </label>
                                </div>
                                <div class="sponsors_Offer">
                                	<p>Basic Visibility</p>
                            		<p>Branding across collaterals (including physical billboards, standees, social media posts)
</p>
                                </div>
                            
                        </li>
                        <li>
                        	
                                <div class="drpdwon_Icontxt">
                                     <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="androidy">
                                        <?= $form->field($model, 'experience_zone')->checkbox(['id' => 'androidy','class' => 'mdl-checkbox__input'])->label(false); ?>
                                      </label>
                                </div>
                                <div class="sponsors_Offer">
                                	<p>Stall / experience zone for brands</p>
                            		<p>Kindly select if your event offers a space for brand to showcase</p>
                                </div>
                          
                        </li>
                        <li>
                        	
                                <div class="drpdwon_Icontxt">
                                     <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="androidx">
                                       <?= $form->field($model, 'sales_counter')->checkbox(['id' => 'androidx','class' => 'mdl-checkbox__input'])->label(false); ?>
                                      </label>
                                </div>
                                <div class="sponsors_Offer">
                                	<p>Sales counter for brands</p>
                            		<p>Can the sponsor sell their products at your event ?</p>
                                </div>
                            
                        </li>
                        <li>
                        	
                                <div class="drpdwon_Icontxt">
                                     <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="androidw">
                                       <?= $form->field($model, 'wifi_enable')->checkbox(['id' => 'androidw','class' => 'mdl-checkbox__input'])->label(false); ?>
                                      </label>
                                </div>
                                <div class="sponsors_Offer">
                                	<p>Wi Fi enabled zone</p>
                                </div>
                           
                        </li>
                        
                        <li>
                        	
                                <div class="drpdwon_Icontxt">
                                     <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="androidv">
                                       <?= $form->field($model, 'automobile_display')->checkbox(['id' => 'androidv','class' => 'mdl-checkbox__input'])->label(false); ?>
                                      </label>
                                </div>
                                <div class="sponsors_Offer">
                                	<p>Automobile display</p>
                            		<p>If your event is on a level field and a car can be displayed</p>
                                </div>
                           
                        </li>
                        <li>
                        	
                                <div class="drpdwon_Icontxt">
                                     <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="androidu">
                                        <?= $form->field($model, 'parking_space')->checkbox(['id' => 'androidu','class' => 'mdl-checkbox__input'])->label(false); ?>
                                      </label>
                                </div>
                                <div class="sponsors_Offer">
                                	<p>Parking Space</p>
                                </div>
                          
                        </li>
                        <!---->
                        <li>
                        	
                                <div class="drpdwon_Icontxt">
                                     <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="androidt">
                                        <?= $form->field($model, 'common_areas')->checkbox(['id' => 'androidt','class' => 'mdl-checkbox__input'])->label(false); ?>
                                      </label>
                                </div>
                                <div class="sponsors_Offer">
                                	<p>Common areas outside actual venue</p>
                                </div>
                            
                        </li>
                        <li>
                        	
                                <div class="drpdwon_Icontxt">
                                     <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="androids">
                                        <?= $form->field($model, 'vip_box')->checkbox(['id' => 'androids','class' => 'mdl-checkbox__input'])->label(false); ?>
                                      </label>
                                </div>
                                <div class="sponsors_Offer">
                                	<p>VIP Hospitality box / lounge</p>
                            		
                                </div>
                           
                        </li>
                        <li>
                        
                                <div class="drpdwon_Icontxt">
                                     <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="androidr">
                                        <?= $form->field($model, 'post_event_party')->checkbox(['id' => 'androidr','class' => 'mdl-checkbox__input'])->label(false); ?>
                                      </label>
                                </div>
                                <div class="sponsors_Offer">
                                	<p>Post event celebratory event / party</p>
                            		
                                </div>
                          
                        </li>
                    </ul>
                </div>
                </div>
                </div>
                <?= $form->field($model, 'id')->hiddenInput()->label(false); ?>
        		<?php ActiveForm::end(); ?>	           
                <div class="BackBtn_Contnr">
                	<a href="javascript:void(0);" onclick="stepback('thirteenthstep');"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;Back</a>
                    <div class="NextBtn_Contnr">
                        <button onclick="sumbitfirstphase('thirteenth'); return false;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">
                            Next
                        </button>
                    </div>
                </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
            	<div class="Map_Contnr">
                	<div class="Sponsership_COntnr listingRight_COntnr">
                	<p><img src="<?= $asset->baseUrl ?>/image/bell2.png"></p>
                    <h4>Why this data ?</h4>
                    <p>Providing this information clearly sets the expectations of the brand – thereby not leaving any room for confusion. If any of these are dependent on the sponsorship money given, that’s fine – choose the one corresponding to highest slab</p>
                </div>
                </div>
            </div>
        </div>
    </div>
</main>

<!--------------------------------------------step thirteenth end------------------------------------>


<!--------------------------------------------step fourteenth start------------------------------------>

<main class="CreateEvent_MainCOntnr" id ="fourteenthstep" style="display:none;">
	
	<div class="container">
    	<div class="row">
        	<div class="col-md-12">
            	<h3>List on <a href="javascript:void(0);">Onspon.com</a></h3>
                <h4>Start off by creating a listing page. Think of it as a profile page for your Event.</h4>
            </div>
        </div>
        <div class="row">
        	<div class="col-md-6 col-sm-6 col-xs-12">
            	<div class="list_COntnr basic">
                    <h5>STEP 1</h5>
                    <h6>Start With Basic Listing Details</h6>
                    <label>Name, Venue etc</label>
                   <a href="javacsript:void(0);" onclick="editstep('1'); return false;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">Edit</a>
                
                </div>
                <div class="list_COntnr basic">
                    <h5>STEP 2</h5>
                    <h6>Tell Us About Your Audience</h6>
                    <label>Audience Profile, Footfalls, Pictures, Videos</label>
                    <a href="javacsript:void(0);" onclick="editstep('2'); return false;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">Edit</a>
                
                      </div>
                <div class="list_COntnr basic">
                    <h5>STEP 3</h5>
                    <h6>Your Sponsorship Details</h6>
                    <label>Sponsors, Sponsorship Slabs etc</label>
                    <a href="javacsript:void(0);" onclick="editstep('3'); return false;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">Edit</a>
                
                </div>
                <div class="list_COntnr ">
                    <h5>STEP 4</h5>
                    <h6>Your Event Description</h6>
                    <label>Description, Photo Gallery etc</label>
                    <a href="javacsript:void(0);" onclick="secondpage('4'); return false;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">continue</a>
               
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
            	<img src="<?= $asset->baseUrl ?>/image/sponsership_img.png" >
                <div class="Sponsership_COntnr">
                	<p><img src="<?= $asset->baseUrl ?>/image/bell.png"></p>
                    <p>This will also automatically create your ‘<span>event page</span>’. 
Average sponsorships on Onspon.com is :</p>
					<p><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;1,35,000</p>
                </div>
            </div>
        </div>
    </div>
</main>

<!--------------------------------------------step fourteenth end------------------------------------>

<!--------------------------------------------step fifteenth start------------------------------------>

<main id ="fifteenthstep" style="display:none;">
	<div class="progress">
          <div class="progress-bar" role="progressbar" aria-valuenow="70"
              aria-valuemin="0" aria-valuemax="100" style="width:90%">
                <span class="sr-only">90% Complete</span>
          </div>
     </div>
	<div class="container" style="width:67%;">
        <h3 class="headingCOntr">Photo</h3>
         <?php $form = ActiveForm::begin([
			'enableAjaxValidation' => false,
			'id' => 'fourteenthstepform',
			'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
			]); ?> 
        
        <div class="Description">
            <div class="Sponsership_COntnr Sponsership_Popup">
            	<i class="fa fa-times" aria-hidden="true"></i>
                <p><img src="<?= $asset->baseUrl ?>/image/bell2.png"></p>
                <p style="text-align:center;"><img src="<?= $asset->baseUrl ?>/image/fraim.png"></p>
              <p>This is important because earlier sponserships would give you a broad idea on sponsership expectation</p>
            </div>
            <div class="upload_Btn">
            	
            	<!--input type="file"  name="uploading" multiple class="fa fa-cloud-upload"-->
            	<?= $form->field($model, 'image')->fileInput(['onchange'=>'saveGALLERY();'])->label(false) ?>
            	
            	
            </div>
            <div class="row">
            	<div class="col-md-3 col-sm-3 col-xs-12">
                	<div class="photoUploadContnr">
                    	
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                	<div class="photoUploadContnr">
                    	
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                	<div class="photoUploadContnr">
                    	
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                	<div class="photoUploadContnr">
                    	
                    </div>
                </div>
            </div>
        </div>  
        <?= $form->field($model, 'id')->hiddenInput()->label(false); ?>
        	<?php ActiveForm::end(); ?>         
        <div class="BackBtn_Contnr">
            <a href="javascript:void(0);" onclick="stepback('fifteenthstep');"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;Back</a>
       </div>
        <div class="NextBtn_Contnr">
            <button onclick="secondpage('5'); return false;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">
                Skip for now
            </button>
        </div>
    </div>
</main>


<!--------------------------------------------step fifteenth end------------------------------------>

<!--------------------------------------------step sixteenth start------------------------------------>

<main id ="sixteenthstep" style="display:none;">
	<div class="progress">
          <div class="progress-bar" role="progressbar" aria-valuenow="70"
              aria-valuemin="0" aria-valuemax="100" style="width:12%">
                <span class="sr-only">12% Complete</span>
          </div>
     </div>
	<div class="container">
        <div class="row">
        	<div class="col-md-6 col-sm-6 col-xs-12">
            <div class="left_Sec">
            	<div class="leftSec_Inner_Contr">
            	<h3 class="headingCOntr">Start building your description</h3>
                <div class="Drop_Down_Main_Contnr">
                    <div class="dropDown_COntnr">
                        <ul class="EventList_Contnr EventList_Contnr2">
                            <li>
                                <p>My event is</p>
                            </li>
                            <li>
                                <ul class="shadedArea">
                                	<li>
                                        <a href="#">The best rated event in its category</a>
                                    </li>
                                    <li>
                                        <a href="#">Sponsor friendly</a>
                                    </li>
                                    <li>
                                        <a href="#">In top 10 events in the city</a>
                                    </li>
                                    <li>
                                        <a href="#">Featured in recored books</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="dropDown_COntnr">
                        <ul class="EventList_Contnr EventList_Contnr2">
                            <li>
                                <p>You will love my event because of</p>
                            </li>
                            <li>
                                <ul class="shadedArea">
                                	<li>
                                        <a href="#">Celebrties</a>
                                    </li>
                                    <li>
                                        <a href="#">The Location</a>
                                    </li>
                                    <li>
                                        <a href="#">Social Media</a>
                                    </li>
                                    <li>
                                        <a href="#">Audience Quelity</a>
                                    </li>
                                    <li>
                                        <a href="#">Extensive Media Coverages</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                </div>           
                <div class="BackBtn_Contnr">
                	<a href="javascript:void(0);" onclick="stepback('sixteenthstep');"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;Back</a>
                    <div class="NextBtn_Contnr">
                        <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">
                            Next
                        </button>
                    </div>
                </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
            	<div class="Map_Contnr">
                	<div class="Sponsership_COntnr listingRight_COntnr">
                		<p><img src="<?= $asset->baseUrl ?>/image/bell2.png"></p>
                    	<p>This description is appear at the top of your listing page. We've started you out with a few common questions guests have when looking for a place. You will be able to editand write more next.</p>
                	</div>
                </div>
                
            </div>
        </div>
    </div>
</main>

<!--------------------------------------------step sixteenth end------------------------------------>

<!--------------------------------------------step seventeenth start------------------------------------>

<main id="seventeenthstep" style="display:none">
	<div class="progress">
      <div class="progress-bar" role="progressbar" aria-valuenow="70"
          aria-valuemin="0" aria-valuemax="100" style="width:95%">
            <span class="sr-only">95% Complete</span>
      </div>
    </div>
	<div class="container"> 
        <div class="row">
        	<div class="col-md-6 col-sm-6 col-xs-12">
            <div class="left_Sec">
            <div class="leftSec_Inner_Contr">
            	<h3 class="headingCOntr">Organization Details</h3>
                
			<?php $form = ActiveForm::begin([
			'id' => 'seventeenthstepform',
			'action' => Url::to(['/eventcreate/create']),
			'enableAjaxValidation' => false, ]); ?>
			
					
					<div class="form-group">
						<?= $form->field($orgmodel, 'oraganization_name')->textInput(['class'=>'form-control','placeholder' => 'Event Name']); ?>
					</div>
                    <div class="row">
                	<div class="col-md-6 col-sm-6 col-xs-12">
						
                		<div class="form-group">                         
                          <?= $form->field($orgmodel, 'org_location')->textInput(['placeholder' => 'Oraganization Name']); ?>
                        </div>
                	</div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                		<div class="form-group">
                         <?= $form->field($orgmodel, 'established_date')->widget(DateTimePicker::className(), ['options'=>['format'=>Yii::$app->params['dateFormat']]]); ?>
                        
                        </div>
                	</div>
                	</div>
                <div class="form-group">
                  <?= $form->field($orgmodel, 'fb_page')->textInput(['class'=>'form-control','placeholder' => 'Facebook Page']); ?>
                	
                </div>
                <div class="form-group">
                    <?= $form->field($orgmodel, 'twitter_page')->textInput(['class'=>'form-control','placeholder' => 'Twitter Page']); ?>
					
                </div>
                <div class="form-group">
                  <?= $form->field($orgmodel, 'org_city')->textInput(['class'=>'form-control','placeholder' => 'City']); ?>
                	
                </div>
                <div class="form-group">
                  <?= $form->field($orgmodel, 'org_about')->textInput(['class'=>'form-control','placeholder' => 'About Your Oraganization']); ?>
                	
                </div>
                	<?= $form->field($model, 'id')->hiddenInput()->label(false); ?>
                	<?= $form->field($orgmodel, 'org_id')->hiddenInput()->label(false); ?>
  			<?php ActiveForm::end(); ?>              
                
                </div>
               
                
                <div class="BackBtn_Contnr">
                	<a href="javascript:void(0);" onclick="stepback('seventeenthstep');"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;Back</a>
                    <div class="NextBtn_Contnr">
                        <button onclick="sumbitseventeenthphase(); return false;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr">
                            Next
                        </button>
                    </div>
                </div>
            </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
            	<div class="Map_Contnr">
                	<div class="Sponsership_COntnr listingRight_COntnr">
                	<p><img src="<?= $asset->baseUrl ?>/image/bell2.png"></p>
                    <h4>Enter Organization Details for Create Event</h4>
                    <p>Oraganization Name, Location, Establishment Date, Facebook Page, Twitter Page, City, About & Email Id</p>
                </div>
            </div>
        </div>
   
	</div>
</main>
 
<!--------------------------------------------------- seventeenth step end-------------------------------------->



<script>

function checkedboxgenre()
{
	
 $("input[name='CreateEvents[event_genre_id][]']").change(function () {
        var maxAllowed = 3;
        var cnt = $("input[name='CreateEvents[event_genre_id][]']:checked").length;
        if (cnt > maxAllowed) {
            $(this).prop("checked", "");
            alert('You can select maximum ' + maxAllowed + ' Genre!!');
        }
    });


}

function checkedboxaudience()
{
	
 $("input[name='CreateEvents[audience_profile][]']").change(function () {
        var maxAllowed = 3;
        var cnt = $("input[name='CreateEvents[audience_profile][]']:checked").length;
        if (cnt > maxAllowed) {
            $(this).prop("checked", "");
            alert('You can select maximum ' + maxAllowed + ' Audience Profile!!');
        }
    });


}

function sumbitseventeenthphase()
{
	var data = $('#seventeenthstepform').serialize() + "&type=seventeenth";
		var url = '<?= Url::to('createorganization')?>';
		
		$.ajax({
				url : url,
				data: data,
				type: 'POST',
				
				success : function(msg)	
					{ if(msg == 'seventeenth')
					{
						alert('Successfully submitted. Please login to publish your Event.');
						window.location = '<?= Url::to(['/site/login']) ; ?>';
						 window.location = '<?= Url::to(['/site/index']) ; ?>';
					}else{
						alert(msg); // show response from the php script.
              			
						//location.reload();
					}
               
           		
					}
           		
	           	 
        	 });
}

function sendtwelvethform(){
	var slabname = $("input[name='slabname[]']")
              .map(function(){return $(this).val();}).get();
	
	var sponsorprice = $("input[name='sponsorprice[]']")
              .map(function(){return $(this).val();}).get();
              
    var deliverables = $("textarea[name='deliverables[]']")
              .map(function(){return $(this).val();}).get();
              
	var id = $('#createevents-id').val();
	
	var data = 'slabname='+slabname+'&sponsorprice='+sponsorprice+'&deliverables='+deliverables+'&id='+id+'&type=twelveth';


	$.ajax({
           type: "POST",
           url: "<?= Url::to(['sponsorslab'])?>",
           data: data, // serializes the form's elements.
           dataType : 'json',
           success: function(data)
           {
           			if(data.type == 'twelveth')
					{ $('input[id="createevents-id"]').val(data.id);
						$('#firststep').hide();
						$('#secondstep').hide(); 
						$('#thirdstep').hide();
						$('#fourthstep').hide();
						$('#fivthstep').hide();
						$('#sixthstep').hide();
						$('#seventhstep').hide();
						$('#eighthstep').hide();
						$('#ninethstep').hide();
						$('#tenthstep').hide();
						$('#eleventhstep').hide();
						$('#twelvethstep').hide();
						$('#thirteenthstep').show();
						$('#fourteenthstep').hide();
					}
           		
	           	 }
        	 });
	}

</script>

<script>



function saveGALLERY() { 
	
	var formData = new FormData($("#fourteenthstepform")[0]);
	
	$.ajax({
	       url : '<?= Url::to('eventgallery')?>',
	       type : 'POST',
	       data : formData,
	       processData: false,  // tell jQuery not to process the data
	       contentType: false,  // tell jQuery not to set contentType
		   cache: false,
	       success : function(data) {
	           console.log(data);
	           alert(data);
	       		}
			});
	
     }
	
function sumbitfirstphase(type)
{
	if(type=='second'){
		
		var event_name = $('#createevents-event_name').val();
		var contact = $('#createevents-contact').val();
		var email = $('#createevents-email').val();
		
		if(!event_name)
		{
			$('input[id="createevents-event_name"]').each(function() {
            if ($.trim($(this).val()) == '') {
                $(this).css({"border": "1px solid red","background": "#FFCECE" });
            }});
            return false;
		}
		if(!contact)
		{
			$('#createevents-event_name').css({"border": "","background": ""});
			$('input[id="createevents-contact"]').each(function() {
            if ($.trim($(this).val()) == '') {
                $(this).css({"border": "1px solid red","background": "#FFCECE" });
            }});
		
        	return false;
		}
		/*if (!ValidateContact(contact)){
                   $("#err-msg-reachus-contact").html("Please enter valid contact Number");
		   $('#createevents-contact').focus();
                   err=true;
                                    
        }*/else{
		
		if(!email)
		{
			$("#err-msg-reachus-contact").hide();
			$('#createevents-contact').css({"border": "","background": ""});
			$('input[id="createevents-email"]').each(function() {
            if ($.trim($(this).val()) == '') {
                $(this).css({"border": "1px solid red","background": "#FFCECE" });
            }});
			return false;
		}else if (!validateEmail(email)){
                   $("#err-msg-reachus-email").html("Please enter valid email address");
		   $('#createevents-email').focus();
                   err=true;
                                    
        }else {
		
			$('#createevents-email').css({"border": "","background": ""});
		
		var data = $('#secondstepform').serialize() + "&type=second";
		var url = '<?= Url::to('create')?>';
		}
		}
	}
	if(type=='third'){
		var location = $('#event-address').val();
		
		if(!location)
		{
			$('input[id="event-address"]').each(function() {
            if ($.trim($(this).val()) == '') {
                $(this).css({"border": "1px solid red","background": "#FFCECE" });
            }});
            return false;
		}else{
			
			$('#event-address').css({"border": "","background": ""});
		
		var data = $('#thirdstepform').serialize() + "&type=third";
		var url = '<?= Url::to('create')?>';
		}
	}
	if(type=='fourth'){
		
		var event_category = $('#createevents-event_category_id').val();
		
		if(!event_category)
		{
			$('select[id="createevents-event_category_id"]').each(function() {
            if ($.trim($(this).val()) == '') {
                $(this).css({"border": "1px solid red","background": "#FFCECE" });
            }});
            return false;
		}else{
		
			$('#createevents-event_category_id').css({"border": "","background": ""});
			var data = $('#fourthstepform').serialize() + "&type=fourth";
			var url = '<?= Url::to('updatefourthstep')?>';
		}
	}
	if(type=='fivth'){
		var data = $('#fivthstepform').serialize() + "&type=fivth";
		var url = '<?= Url::to('create')?>';
	}
	if(type=='seventh'){
		var data = $('#seventhstepform').serialize() + "&type=seventh";
		var url = '<?= Url::to('updateseventhstep')?>';
	}
	if(type=='eighth'){
		var data = $('#eighthstepform').serialize() + "&type=eighth";
		var url = '<?= Url::to('create')?>';
	}
	if(type=='tenth'){
		var data = $('#tenthstepform').serialize() + "&type=tenth";
		var url = '<?= Url::to('create')?>';
	}
	if(type=='eleventh'){
		var data = $('#eleventhstepform').serialize() + "&type=eleventh";
		var url = '<?= Url::to('create')?>';
	}
	if(type=='twelveth'){
		var data = $('#twelvethstepform').serialize() + "&type=twelveth";
		var url = '<?= Url::to('create')?>';
	}
	if(type=='thirteenth'){
		var data = $('#thirteenthstepform').serialize() + "&type=thirteenth";
		var url = '<?= Url::to('create')?>';
	}
	
	$.ajax({
				url : url,
				data: data,
				type: 'POST',
				dataType : 'json',
				success : function(data){	
				
					if(data.type == 'second')
					{ $('input[id="createevents-id"]').val(data.id);
						$('#firststep').hide();
						$('#secondstep').hide(); 
						$('#thirdstep').show();
						$('#fourthstep').hide();
						$('#fivthstep').hide();
						$('#sixthstep').hide();
						$('#seventhstep').hide();
						$('#eighthstep').hide();
						$('#ninethstep').hide();
						$('#tenthstep').hide();
						$('#eleventhstep').hide();
						$('#twelvethstep').hide();
						$('#thirteenthstep').hide();
						$('#fourteenthstep').hide();
						$('#fifteenthstep').hide();
						$('#sixteenthstep').hide();
					}
					if(data.type == 'third')
					{ 
						$('#firststep').hide();
						$('#secondstep').hide();
						$('#thirdstep').hide();
						$('#fourthstep').show();
						$('#fivthstep').hide();
						$('#sixthstep').hide();
						$('#seventhstep').hide();
						$('#eighthstep').hide();
						$('#ninethstep').hide();
						$('#tenthstep').hide();
						$('#eleventhstep').hide();
						$('#twelvethstep').hide();
						$('#thirteenthstep').hide();
						$('#fourteenthstep').hide();
						$('#fifteenthstep').hide();
						$('#sixteenthstep').hide();
					}
					if(data.type == 'fourth')
					{ 
						$('#firststep').hide();
						$('#secondstep').hide();
						$('#thirdstep').hide();
						$('#fourthstep').hide();
						$('#fivthstep').show();
						$('#sixthstep').hide();
						$('#seventhstep').hide();
						$('#eighthstep').hide();
						$('#ninethstep').hide();
						$('#tenthstep').hide();
						$('#eleventhstep').hide();
						$('#twelvethstep').hide();
						$('#thirteenthstep').hide();
						$('#fourteenthstep').hide();
						$('#fifteenthstep').hide();
						$('#sixteenthstep').hide();
					}
					if(data.type == 'fivth')
					{ 
						$('#firststep').hide();
						$('#secondstep').hide();
						$('#thirdstep').hide();
						$('#fourthstep').hide();
						$('#fivthstep').hide();
						$('#sixthstep').show();
						$('#seventhstep').hide();
						$('#eighthstep').hide();
						$('#ninethstep').hide();
						$('#tenthstep').hide();
						$('#eleventhstep').hide();
						$('#twelvethstep').hide();
						$('#thirteenthstep').hide();
						$('#fourteenthstep').hide();
						$('#fifteenthstep').hide();
						$('#sixteenthstep').hide();
					}
					if(data.type == 'seventh')
					{ 
						$('#firststep').hide();
						$('#secondstep').hide();
						$('#thirdstep').hide();
						$('#fourthstep').hide();
						$('#fivthstep').hide();
						$('#sixthstep').hide();
						$('#seventhstep').hide();
						$('#eighthstep').show();
						$('#ninethstep').hide();
						$('#tenthstep').hide();
						$('#eleventhstep').hide();
						$('#twelvethstep').hide();
						$('#thirteenthstep').hide();
						$('#fourteenthstep').hide();
						$('#fifteenthstep').hide();
						$('#sixteenthstep').hide();
					}
					if(data.type == 'eighth')
					{ 
						$('#firststep').hide();
						$('#secondstep').hide();
						$('#thirdstep').hide();
						$('#fourthstep').hide();
						$('#fivthstep').hide();
						$('#sixthstep').hide();
						$('#seventhstep').hide();
						$('#eighthstep').hide();
						$('#ninethstep').show();
						$('#tenthstep').hide();
						$('#eleventhstep').hide();
						$('#twelvethstep').hide();
						$('#thirteenthstep').hide();
						$('#fourteenthstep').hide();
						$('#fifteenthstep').hide();
						$('#sixteenthstep').hide();
					}
					
					if(data.type == 'tenth')
					{ 
						$('#firststep').hide();
						$('#secondstep').hide();
						$('#thirdstep').hide(); 
						$('#fourthstep').hide();
						$('#fivthstep').hide();
						$('#sixthstep').hide();
						$('#seventhstep').hide();
						$('#eighthstep').hide();
						$('#ninethstep').hide();
						$('#tenthstep').hide();
						$('#eleventhstep').show();
						$('#twelvethstep').hide();
						$('#thirteenthstep').hide();
						$('#fourteenthstep').hide();
						$('#fifteenthstep').hide();
						$('#sixteenthstep').hide();
					}
					if(data.type == 'eleventh')
					{ 
						$('#firststep').hide();
						$('#secondstep').hide();
						$('#thirdstep').hide(); 
						$('#fourthstep').hide();
						$('#fivthstep').hide();
						$('#sixthstep').hide();
						$('#seventhstep').hide();
						$('#eighthstep').hide();
						$('#ninethstep').hide();
						$('#tenthstep').hide();
						$('#eleventhstep').hide();
						$('#twelvethstep').show();
						$('#thirteenthstep').hide();
						$('#fourteenthstep').hide();
						$('#fifteenthstep').hide();
						$('#sixteenthstep').hide();
					}
					if(data.type == 'twelveth')
					{ 
						$('#firststep').hide();
						$('#secondstep').hide();
						$('#thirdstep').hide(); 
						$('#fourthstep').hide();
						$('#fivthstep').hide();
						$('#sixthstep').hide();
						$('#seventhstep').hide();
						$('#eighthstep').hide();
						$('#ninethstep').hide();
						$('#tenthstep').hide();
						$('#eleventhstep').hide();
						$('#twelvethstep').hide();
						$('#thirteenthstep').show();
						$('#fourteenthstep').hide();
						$('#fifteenthstep').hide();
						$('#sixteenthstep').hide();
					}
					if(data.type == 'thirteenth')
					{ 
						$('#firststep').hide();
						$('#secondstep').hide();
						$('#thirdstep').hide(); 
						$('#fourthstep').hide();
						$('#fivthstep').hide();
						$('#sixthstep').hide();
						$('#seventhstep').hide();
						$('#eighthstep').hide();
						$('#ninethstep').hide();
						$('#tenthstep').hide();
						$('#eleventhstep').hide();
						$('#twelvethstep').hide();
						$('#thirteenthstep').hide();
						$('#fourteenthstep').show();
						$('#fifteenthstep').hide();
						$('#sixteenthstep').hide();
					}
					

			}	
		});

} 
</script>

<script>

function ValidateContact(contact)  
    {  
      var phoneno = /^\d{10}$/;  
      return contact.match(phoneno);
    }  

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function stepback(key)
{
	if(key == 'thirdstep')
	{
		$('#firststep').hide();
		$('#secondstep').show();
		$('#thirdstep').hide();
		$('#fourthstep').hide();
		$('#fivthstep').hide();
		$('#sixthstep').hide();
		$('#seventhstep').hide();
		$('#eighthstep').hide();
		$('#ninethstep').hide();
		$('#tenthstep').hide();
		$('#eleventhstep').hide();
		$('#twelvethstep').hide();
		$('#thirteenthstep').hide();
		$('#fourteenthstep').hide();
		$('#fifteenthstep').hide();
		$('#sixteenthstep').hide();
	}
	if(key == 'fourthstep')
	{
		$('#firststep').hide();
		$('#secondstep').hide();
		$('#thirdstep').show();
		$('#fourthstep').hide();
		$('#fivthstep').hide();
		$('#sixthstep').hide();
		$('#seventhstep').hide();
		$('#eighthstep').hide();
		$('#ninethstep').hide();
		$('#tenthstep').hide();
		$('#eleventhstep').hide();
		$('#twelvethstep').hide();
		$('#thirteenthstep').hide();
		$('#fourteenthstep').hide();
		$('#fifteenthstep').hide();
		$('#sixteenthstep').hide();
	}
	if(key == 'fivthstep')
	{
		$('#firststep').hide();
		$('#secondstep').hide();
		$('#thirdstep').hide();
		$('#fourthstep').show();
		$('#fivthstep').hide();
		$('#sixthstep').hide();
		$('#seventhstep').hide();
		$('#eighthstep').hide();
		$('#ninethstep').hide();
		$('#tenthstep').hide();
		$('#eleventhstep').hide();
		$('#twelvethstep').hide();
		$('#thirteenthstep').hide();
		$('#fourteenthstep').hide();
		$('#fifteenthstep').hide();
		$('#sixteenthstep').hide();
	}
	if(key == 'sixthstep')
	{
		$('#firststep').hide();
		$('#secondstep').hide();
		$('#thirdstep').hide();
		$('#fourthstep').hide();
		$('#fivthstep').show();
		$('#sixthstep').hide();
		$('#seventhstep').hide();
		$('#eighthstep').hide();
		$('#ninethstep').hide();
		$('#tenthstep').hide();
		$('#eleventhstep').hide();
		$('#twelvethstep').hide();
		$('#thirteenthstep').hide();
		$('#fourteenthstep').hide();
		$('#fifteenthstep').hide();
		$('#sixteenthstep').hide();
	}
	if(key == 'seventhstep')
	{
		$('#firststep').hide();
		$('#secondstep').hide();
		$('#thirdstep').hide();
		$('#fourthstep').hide();
		$('#fivthstep').hide();
		$('#sixthstep').show();
		$('#seventhstep').hide();
		$('#eighthstep').hide();
		$('#ninethstep').hide();
		$('#tenthstep').hide();
		$('#eleventhstep').hide();
		$('#twelvethstep').hide();
		$('#thirteenthstep').hide();
		$('#fourteenthstep').hide();
		$('#fifteenthstep').hide();
		$('#sixteenthstep').hide();
	}
	if(key == 'eighthstep')
	{
		$('#firststep').hide();
		$('#secondstep').hide();
		$('#thirdstep').hide();
		$('#fourthstep').hide();
		$('#fivthstep').hide();
		$('#sixthstep').hide();
		$('#seventhstep').show();
		$('#eighthstep').hide();
		$('#ninethstep').hide();
		$('#tenthstep').hide();
		$('#eleventhstep').hide();
		$('#twelvethstep').hide();
		$('#thirteenthstep').hide();
		$('#fourteenthstep').hide();
		$('#fifteenthstep').hide();
		$('#sixteenthstep').hide();
	}
	if(key == 'ninethstep')
	{
		$('#firststep').hide();
		$('#secondstep').hide();
		$('#thirdstep').hide();
		$('#fourthstep').hide();
		$('#fivthstep').hide();
		$('#sixthstep').hide();
		$('#seventhstep').hide();
		$('#eighthstep').show();
		$('#ninethstep').hide();
		$('#tenthstep').hide();
		$('#eleventhstep').hide();
		$('#twelvethstep').hide();
		$('#thirteenthstep').hide();
		$('#fourteenthstep').hide();
		$('#fifteenthstep').hide();
		$('#sixteenthstep').hide();
	}
	if(key == 'tenthstep')
	{
		$('#firststep').hide();
		$('#secondstep').hide();
		$('#thirdstep').hide();
		$('#fourthstep').hide();
		$('#fivthstep').hide();
		$('#sixthstep').hide();
		$('#seventhstep').hide();
		$('#eighthstep').hide();
		$('#ninethstep').show();
		$('#tenthstep').hide();
		$('#eleventhstep').hide();
		$('#twelvethstep').hide();
		$('#thirteenthstep').hide();
		$('#fourteenthstep').hide();
		$('#fifteenthstep').hide();
		$('#sixteenthstep').hide();
	}
	if(key == 'eleventhstep')
	{
		$('#firststep').hide();
		$('#secondstep').hide();
		$('#thirdstep').hide();
		$('#fourthstep').hide();
		$('#fivthstep').hide();
		$('#sixthstep').hide();
		$('#seventhstep').hide();
		$('#eighthstep').hide();
		$('#ninethstep').hide();
		$('#tenthstep').show();
		$('#eleventhstep').hide();
		$('#twelvethstep').hide();
		$('#thirteenthstep').hide();
		$('#fourteenthstep').hide();
		$('#fifteenthstep').hide();
		$('#sixteenthstep').hide();
	}
	if(key == 'twelvethstep')
	{
		$('#firststep').hide();
		$('#secondstep').hide();
		$('#thirdstep').hide();
		$('#fourthstep').hide();
		$('#fivthstep').hide();
		$('#sixthstep').hide();
		$('#seventhstep').hide();
		$('#eighthstep').hide();
		$('#ninethstep').hide();
		$('#tenthstep').hide();
		$('#eleventhstep').show();
		$('#twelvethstep').hide();
		$('#thirteenthstep').hide();
		$('#fourteenthstep').hide();
		$('#fifteenthstep').hide();
		$('#sixteenthstep').hide();
	}
	if(key == 'thirteenthstep')
	{
		$('#firststep').hide();
		$('#secondstep').hide();
		$('#thirdstep').hide();
		$('#fourthstep').hide();
		$('#fivthstep').hide();
		$('#sixthstep').hide();
		$('#seventhstep').hide();
		$('#eighthstep').hide();
		$('#ninethstep').hide();
		$('#tenthstep').hide();
		$('#eleventhstep').hide();
		$('#twelvethstep').show();
		$('#thirteenthstep').hide();
		$('#fourteenthstep').hide();
		$('#fifteenthstep').hide();
		$('#sixteenthstep').hide();
	}
	if(key == 'fifteenthstep')
	{
		$('#firststep').hide();
		$('#secondstep').hide();
		$('#thirdstep').hide();
		$('#fourthstep').hide();
		$('#fivthstep').hide();
		$('#sixthstep').hide();
		$('#seventhstep').hide();
		$('#eighthstep').hide();
		$('#ninethstep').hide();
		$('#tenthstep').hide();
		$('#eleventhstep').hide();
		$('#twelvethstep').hide();
		$('#thirteenthstep').hide();
		$('#fourteenthstep').show();
		$('#fifteenthstep').hide();
		$('#sixteenthstep').hide();
	}
	if(key == 'sixteenthstep')
	{
		$('#firststep').hide();
		$('#secondstep').hide();
		$('#thirdstep').hide();
		$('#fourthstep').hide();
		$('#fivthstep').hide();
		$('#sixthstep').hide();
		$('#seventhstep').hide();
		$('#eighthstep').hide();
		$('#ninethstep').hide();
		$('#tenthstep').hide();
		$('#eleventhstep').hide();
		$('#twelvethstep').hide();
		$('#thirteenthstep').hide();
		$('#fourteenthstep').hide();
		$('#fifteenthstep').show();
		$('#sixteenthstep').hide();
	}
}


</script>





<script> 
function editstep(id)
{
	if(id == '1'){
		$('#firststep').show();
		$('#secondstep').hide();
		$('#thirdstep').hide();
		$('#fourthstep').hide();
		$('#fivthstep').hide();
		$('#sixthstep').hide();
		$('#seventhstep').hide();
		$('#eighthstep').hide();
		$('#ninethstep').hide();
		$('#tenthstep').hide();
		$('#eleventhstep').hide();
		$('#twelvethstep').hide();
		$('#thirteenthstep').hide();
		$('#fourteenthstep').hide();
		$('#fifteenthstep').hide();
		$('#sixteenthstep').hide();
	}
	if(id == '2'){
		$('#firststep').hide();
		$('#secondstep').hide();
		$('#thirdstep').hide();
		$('#fourthstep').hide();
		$('#fivthstep').hide();
		$('#sixthstep').show();
		$('#seventhstep').hide();
		$('#eighthstep').hide();
		$('#ninethstep').hide();
		$('#tenthstep').hide();
		$('#eleventhstep').hide();
		$('#twelvethstep').hide();
		$('#thirteenthstep').hide();
		$('#fourteenthstep').hide();
		$('#fifteenthstep').hide();
		$('#sixteenthstep').hide();
	}
	if(id == '3'){
		$('#firststep').hide();
		$('#secondstep').hide();
		$('#thirdstep').hide();
		$('#fourthstep').hide();
		$('#fivthstep').hide();
		$('#sixthstep').hide();
		$('#seventhstep').hide();
		$('#eighthstep').hide();
		$('#ninethstep').show();
		$('#tenthstep').hide();
		$('#eleventhstep').hide();
		$('#twelvethstep').hide();
		$('#thirteenthstep').hide();
		$('#fourteenthstep').hide();
		$('#fifteenthstep').hide();
		$('#sixteenthstep').hide();
	}
}

function secondpage(id)
{
	if(id == '1'){
		$('#firststep').hide();
		$('#secondstep').show();
		$('#thirdstep').hide();
		$('#fourthstep').hide();
		$('#fivthstep').hide();
		$('#sixthstep').hide();
		$('#seventhstep').hide();
		$('#eighthstep').hide();
		$('#ninethstep').hide();
		$('#tenthstep').hide();
		$('#eleventhstep').hide();
		$('#twelvethstep').hide();
		$('#thirteenthstep').hide();
		$('#fourteenthstep').hide();
		$('#fifteenthstep').hide();
		$('#sixteenthstep').hide();
	}
	if(id == '2'){
		$('#firststep').hide();
		$('#secondstep').hide();
		$('#thirdstep').hide();
		$('#fourthstep').hide();
		$('#fivthstep').hide();
		$('#sixthstep').hide();
		$('#seventhstep').show();
		$('#eighthstep').hide();
		$('#ninethstep').hide();
		$('#tenthstep').hide();
		$('#eleventhstep').hide();
		$('#twelvethstep').hide();
		$('#thirteenthstep').hide();
		$('#fourteenthstep').hide();
		$('#fifteenthstep').hide();
		$('#sixteenthstep').hide();
	}
	if(id == '3'){
		$('#firststep').hide();
		$('#secondstep').hide();
		$('#thirdstep').hide();
		$('#fourthstep').hide();
		$('#fivthstep').hide();
		$('#sixthstep').hide();
		$('#seventhstep').hide();
		$('#eighthstep').hide();
		$('#ninethstep').hide();
		$('#tenthstep').show();
		$('#eleventhstep').hide();
		$('#twelvethstep').hide();
		$('#thirteenthstep').hide();
		$('#fourteenthstep').hide();
		$('#fifteenthstep').hide();
		$('#sixteenthstep').hide();
	}
	if(id == '4'){
		$('#firststep').hide();
		$('#secondstep').hide();
		$('#thirdstep').hide();
		$('#fourthstep').hide();
		$('#fivthstep').hide();
		$('#sixthstep').hide();
		$('#seventhstep').hide();
		$('#eighthstep').hide();
		$('#ninethstep').hide();
		$('#tenthstep').hide();
		$('#eleventhstep').hide();
		$('#twelvethstep').hide();
		$('#thirteenthstep').hide();
		$('#fourteenthstep').hide();
		$('#fifteenthstep').show();
		$('#sixteenthstep').hide();
	}
	if(id == '5'){
		$('#firststep').hide();
		$('#secondstep').hide();
		$('#thirdstep').hide();
		$('#fourthstep').hide();
		$('#fivthstep').hide();
		$('#sixthstep').hide();
		$('#seventhstep').hide();
		$('#eighthstep').hide();
		$('#ninethstep').hide();
		$('#tenthstep').hide();
		$('#eleventhstep').hide();
		$('#twelvethstep').hide();
		$('#thirteenthstep').hide();
		$('#fourteenthstep').hide();
		$('#fifteenthstep').hide();
		$('#sixteenthstep').hide();
		$('#seventeenthstep').show();
	}	
}
</script>


<script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places" type="text/javascript"></script>
<script>
	function initialize() {
		
			 
			  var input = document.getElementById('event-address');
			  var autocomplete = new google.maps.places.Autocomplete(input);
			  
			  google.maps.event.addListener(autocomplete, 'place_changed', function() {
				$( "#event-address" ).trigger( "click" );
				$('#edited').val('1');
						var data = $('#event-address').val();		
							geocoder = new google.maps.Geocoder();	
							var myarr = data.split(',');
							var len = myarr.length;
							
							geocoder.geocode( { 'address': data}, function(results, status) {	
							if (status == google.maps.GeocoderStatus.OK) {
								
								source=results[0].geometry.location.lat();
								lng=results[0].geometry.location.lng();	
								$('#event-city').val(myarr[len-3]);
								$('#event-country').val(myarr[len-1]);
								$('#event-state').val(myarr[len-2]);
								$('#createevents-lat').val(source);
								$('#createevents-lng').val(lng);
								$('#cityhide').show(500);
								initialize2();
								
							}else
							{
								alert('Latitude and longitude not found for your input please try different(near by) location');
								$('#events-address').val('');		
							}
							});		
					});
			  
	  }
	  
	  
	  	  
</script>



<!--script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script-->
					
<script>
function initialize2() {

var lat = $('#createevents-lat').val();
var lon = $('#createevents-lng').val();

var result  = lat+","+lon;

var myLatlng = new google.maps.LatLng(result);

var mapOptions = {
	zoom: 15,
	center: myLatlng
}
var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
var marker = new google.maps.Marker({
  position: myLatlng,
  map: map,
  title: 'abu'
});
}
google.maps.event.addDomListener(window, 'load', initialize2);
</script>
<script type="text/javascript">
function valueChanged()
{
	
    if($('#android').is(":checked"))  { 
        $("#ticketlink").show(500);
    }else{
        $("#ticketlink").hide();
       }
}
function addTime()
{
    if($('#addtime').is(":checked")) {  
        $("#starttime").show(500);
        $("#endtime").show(500);  
    }else{
        $("#starttime").hide();
        $("#endtime").hide(); 
       }
}
</script>


