

<div>
     <?php if(Yii::$app->session->hasFlash('error')): ?>
       <div class="alert alert-error">
       <?php echo Yii::$app->session->getFlash('error'); ?>
       </div>
       <?php endif; ?>
 
       <?php if(Yii::$app->session->hasFlash('success')): ?>
       <div class="alert alert-success">
       <?php echo Yii::$app->session->getFlash('success'); ?>
       </div>
       <?php endif; ?>
   </div>
<main>
	
	
	<div class="progress">
      <div class="progress-bar" role="progressbar" aria-valuenow="70"
          aria-valuemin="0" aria-valuemax="100" style="width:88%">
            <span class="sr-only">88% Complete</span>
      </div>
 </div>
	<div class="container">
        <div class="row">
        	<div class="col-md-12">
            	<h3>PACKAGES</h3>
                <table class="table table-striped table-bordered Packages_Contnr">
                	<thead>
                    	<tr>
                        	<th>&nbsp;</th>
                            <th>Basic</th>
                            <th>Silver</th>
                            <th>Gold</th>
                            <th>Platinum</th>
                            <th>Enterprise</th>
                        </tr>
                     </thead>
                     <tbody>
                       
                   <?php $i=0; $j=0; foreach ($model as $package) {
                   	?>
						<?php if($package['category']=='Default')	{?>
						<tr>	
                        	<td><?= $package['package_name']; ?></td>
                            <td><?= $package['basic']; ?></td>
                            <td><?= $package['silver']; ?></td>
                            <td><?= $package['gold']; ?></td>
                            <td><?= $package['platinum']; ?></td>
                            <td><?= $package['enterprise']; ?></td>
                        </tr>
                        
                    <?php }
                    if($package['category']=='Sponsorship Support'){ 
                    	
                    	?>
                        
                        	<?php if($i == 0){
                        		$i++;?>
                        		<tr>
                        		<td><strong><?= $package['category']; ?></strong></td>
                        		<td>&nbsp;</td>
	                            <td>&nbsp;</td>
	                            <td>&nbsp;</td>
	                            <td>&nbsp;</td>
	                            <td>&nbsp;</td>
	                            </tr>
                        		<?php  } ?>
                        		<tr>		
	                        	<td><?= $package['package_name']; ?></td>
	                            <td><?= $package['basic']; ?></td>
	                            <td><?= $package['silver']; ?></td>
	                            <td><?= $package['gold']; ?></td>
	                            <td><?= $package['platinum']; ?></td>
	                            <td><?= $package['enterprise']; ?></td>
                        		</tr>	
                    <?php  }
                    if($package['category']=='Visibility to brands and audience'){
                    	
                    	?> 
                    	
                        	<?php if($j == 0){
                        		 $j++;?>
                        		<tr>
                        		<td><strong><?= $package['category']; ?></strong></td>
                        		<td>&nbsp;</td>
	                            <td>&nbsp;</td>
	                            <td>&nbsp;</td>
	                            <td>&nbsp;</td>
	                            <td>&nbsp;</td>
	                             </tr>
                        		<?php  } ?>
                        		<tr>	
	                        	<td><?= $package['package_name']; ?></td>
	                            <td><?= $package['basic']; ?></td>
	                            <td><?= $package['silver']; ?></td>
	                            <td><?= $package['gold']; ?></td>
	                            <td><?= $package['platinum']; ?></td>
	                            <td><?= $package['enterprise']; ?></td>
                        		</tr>
                    	
                        <?php  } ?>
				<?php  } ?>
                        <tr>
                        	<td>&nbsp;</td> 
                          <td> 
                             <label class="mdl-radio mdl-js-radio" for="option1">
                                <input type="radio" id="option1" value ="Basic" name="gender" class="mdl-radio__button" checked>
                             </label>
                          </td>
                            <td> 
                             <label class="mdl-radio mdl-js-radio" for="option2">
                                <input type="radio" id="option2" value="Silver" name="gender" class="mdl-radio__button">
                             </label>
                          </td>
                            <td> 
                             <label class="mdl-radio mdl-js-radio" for="option3">
                                <input type="radio" id="option3" value="Gold" name="gender" class="mdl-radio__button" >
                             </label>
                          </td>
                          <td> 
                             <label class="mdl-radio mdl-js-radio" for="option4">
                                <input type="radio" id="option4" value="Platinum" name="gender" class="mdl-radio__button" >
                             </label>
                          </td>
                          <td> 
                             <label class="mdl-radio mdl-js-radio" for="option5">
                                <input type="radio" id="option5" value="Enterprise" name="gender" class="mdl-radio__button" >
                             </label>
                          </td>
                            
                        </tr>
                    </tbody>
                    <tfoot>
                    	<tr>
                        	<th>&nbsp;</th>
                        	<th>FREE!</th>
                            <th>4,500 INR 
4,300 
(4% Discount)</th>
                            <th>22,900 INR 
15,900 
(31% Discount)</th>
                            <th>31,000 INR 
17,900 
(42% Discount)</th>
                            <th>99,500 INR 
64,500 
(35% Discount)</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</main>
