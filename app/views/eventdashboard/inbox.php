<?php

use yii\helpers\Url;
use app\models\Brandmandates;
use app\models\Dashboard;
use app\models\Common;
use app\models\Commonhelper;
$commonModel = new Common;

$asset = \app\assets\SponsorAsset::register($this);
$session = Yii::$app->session;

$inbox = Brandmandates::Inboxlist($session['user_id']);
$sent = Brandmandates::Sentlist($session['user_id']);
$contactlist = Dashboard::ContactList($session['user_id']);
$this->registerJs('$(window).load(function() {
		$("#DeltoolTip").mouseover(function() {
			$("#del").show();
		});
		$("#DeltoolTip").mouseout(function() {
			$("#del").hide();
		});
});');

include('header.php');

?>




<section class="section-text-cont">
  
  <div class="container">
    <div class="row">
      <div class="section-heading">
        <h1>
          Messages
        </h1>
      </div>
      <!-- Listing -->
      <div class="UserMsg_COntr">
      	<ul class="tab_COntnr">
        	<li>
            	<a class="inboxtabactive" href="javascript:void(0)" onclick="InboxList();" id="inboxtabactive"><span></span>Inbox <div class="arrow-left"></div></a>
            </li>
            <li>
            	<a href="javascript:void(0)" onclick="SentBoxList();" id="sentabactive"><span></span>Sent <div class="arrow-left"></div></a>
            </li>
            <li>
            	<a href="javascript:void(0)" onclick="contactlist();" id="contactabactive"><span></span>Contact <div class="arrow-left"></div></a>
            </li>
        </ul>
        <div class="list_COntnr" id="inboxlist">
        	<ul>
			<?php if($inbox) : foreach($inbox as $list): ?>	
            	<li>
               	  <a href="javascript:void(0)" onclick="inboxDetails(<?=$list['id'];?>,'receiver');">
               	  	<ul class="<?php echo $active =  ($list['status']=='0') ? 'listActive' : '';?>">
                        <li><?php if($list['logo']){?>
							<img src="<?= Yii::$app->homeUrl.$list['logo'];?>" /> 
							<?php } else { ?><img src="" /> <?php } ?>
						</li>
                        <li>
                            <h3><?= substr($list['message'],"0","50").'...';?></h3>
                            <p><i class="fa fa-clock-o" aria-hidden="true"></i><?php echo $timeage = Commonhelper::getTimeago($list['date']);?></p>
                        </li>
                      </ul>
                  </a>
                </li>
              <?php endforeach; endif;  ?>	
            </ul>
        </div>
        
         <div class="list_COntnr" id="sentboxlist" style="display:none;">
        	<ul>
			<?php if($sent) : foreach($sent as $item): ?>	
            	<li>
               	  <a href="javascript:void(0)" onclick="inboxDetails(<?=$item['id'];?>,'sender');">
               	  	<ul class="<?php echo $active =  ($item['status']=='0') ? 'listActive' : '';?>">
                        <li><?php if($list['logo']){?>
							<img src="<?= Yii::$app->homeUrl.$item['logo'];?>" /> 
							<?php } else { ?><img src="" /> <?php } ?>
						</li>
                        <li>
                            <h3><?= substr($item['message'],"0","50").'...';?></h3>
                            <p><i class="fa fa-clock-o" aria-hidden="true"></i><?php echo $timeage = Commonhelper::getTimeago($item['date']);?></p>
                        </li>
                      </ul>
                  </a>
                </li>
              <?php endforeach; endif;  ?>	
            </ul>
        </div>
        
         <div class="list_COntnr" id="contactmessage" style="display:none;">
        	<ul>
			<?php if($contactlist) : foreach($contactlist as $row): ?>	
            	<li>
               	  <a href="javascript:void(0)" onclick="ContactDetails(<?=$row['id'];?>);">
               	  	<ul class="<?php echo $active =  ($row['status']=='0') ? 'listActive' : '';?>">
                        <li><?php if($row['logo']){?>
							<img src="<?= Yii::$app->homeUrl.$row['logo'];?>" /> 
							<?php } else { ?><img src="" /> <?php } ?>
						</li>
                        <li>
                            <h3><?= substr($row['message'],"0","50").'...';?></h3>
                            <p><i class="fa fa-clock-o" aria-hidden="true"></i><?php echo $timeage = Commonhelper::getTimeago($item['date']);?></p>
                        </li>
                      </ul>
                  </a>
                </li>
              <?php endforeach; endif;  ?>	
            </ul>
        </div>
         
        <div class="msg_Contnr" id="messagelist">
			<?php if($modelresult): foreach($modelresult as $row){
				$date = date("d F Y", strtotime($row['date']));	
				$time = date("H:i a", strtotime($row['date']));
				
				 ?>
			<div class="msgHeader"><div>
                	<ul>
                	<li><div class="delTooltip" id="del"> Delete</div>
                        	<a href="javascript:void(0)" onclick="DeleteInbox(<?=$row['id'];?>,'receiver');" id="DeltoolTip" onmouseover="deleteOver();" onmouseout="deleteOut();"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                      </li>
                    </ul>
                </div>
                <ul>
                	<li><i class="fa fa-calendar" aria-hidden="true"></i><?=$date;?></li>
                    <li><i class="fa fa-clock-o" aria-hidden="true"></i><?=$time;?></li>
                </ul>
            </div>
            <div class="msg_Innr_Contr">
           		<p>
            	<?=$row['message'];?>
            </p>
            	<p><?=$row['username'];?></p>
            </div>
          <?php } endif;?>  
            
		</div>
	  </div>
    </div>
  </div>
</section>

<script>
	
function contactlist(){
	$('#contactmessage').show();
	$('#sentboxlist').hide();
	$('#inboxlist').hide();
	$('#inboxtabactive').removeClass("inboxtabactive");
	$('#sentabactive').removeClass("inboxtabactive");
	$('#inboxtabactive').removeClass("inboxtabactive");
	$('#messagelist').html('');
} 
function InboxList(){
	$('#sentboxlist').hide();
	$('#contactmessage').hide();
	$('#inboxlist').show();
	$('#messagelist').html('');
	$('#contactabactive').removeClass("inboxtabactive");
	$('#sentabactive').removeClass("inboxtabactive");
}

function SentBoxList(){
	$('#inboxlist').hide();
	$('#contactabactive').removeClass("inboxtabactive");
	$('#inboxtabactive').removeClass("inboxtabactive");
	$('#contactmessage').hide();
	$('#sentboxlist').show();
	$('#inboxtabactive').removeClass("inboxtabactive");
	$('#messagelist').html('');
}


function ContactDetails(id,type)
{
	$('#contactabactive').addClass("inboxtabactive");
	var data = 'id='+id;
	$.ajax({
		
		url : '<?= Url::to(['contactdetails']) ; ?>',
		data: data,
		type: 'post',
		dataType: 'html',
		success: function(result)
		{
			$('#messagelist').html(result);
		}
	});
}


function inboxDetails(id,type)
{
	
	if(type=='receiver'){$('#inboxtabactive').addClass("inboxtabactive");}
	if(type=='sender'){$('#sentabactive').addClass("inboxtabactive");}
	
	var data = 'id='+id+'&type='+type;
	$.ajax({
		
		url : '<?= Url::to(['inboxdetails']) ; ?>',
		data: data,
		type: 'post',
		dataType: 'html',
		success: function(result)
		{
			$('#messagelist').html(result);
		}
	});
}


function deleteOver(){
	$("#del").show();
}
		
function deleteOut(){
		$("#del").hide();
}

function DeleteInbox(id,type)
{
	var data = 'id='+id+'&type='+type;
	$.ajax({
		url : '<?= Url::to(['deleteinbox']) ; ?>',
		data: data,
		type: 'post',
		success: function(result)
		{
			alert(result);
			location.reload();
		}
	});
}



function DeleteContact(id)
{
	var data = 'id='+id;
	$.ajax({
		url : '<?= Url::to(['deletecontact']) ; ?>',
		data: data,
		type: 'post',
		success: function(result)
		{
			alert(result);
			location.reload();
		}
	});
}
</script>
