<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;


$asset = \app\assets\SponsorAsset::register($this);
$session = Yii::$app->session;
$existevent = Yii::$app->getDb()->createCommand("SELECT count(*) as total FROM app_events WHERE user_id='".$session['user_id']."'")->queryone();

$this->registerJs('$(window).load(function() {

document.body.className = "header_bg";

		$(".notification-cont").click(function(){
      $.ajax({
			url:"'.Url::to(["inboxhome"]).'",
			dataType: "html",
			success:function(result){
			$(".notifi-sub-cont").html(result);
			$(".notification-toggle-cont").slideToggle(500);
			}
		});
      });

     $(".login").click(function(e){
     $(".login-data-cont").slideToggle(500);
		 e.stopPropagation();
		 $(".notification-toggle-cont").hide(500);
	});
	
	 $(document).click(function (event) {            
			$(".notification-toggle-cont").hide(500);
			 $(".login-data-cont").hide(500);
		});
      
});');


?>


<header>
  <a href="#" class="top-logo"></a>
  <div class="top-menu-bar">
    <div class="hamburger-menu toggle-x">
      <span class="arrows"></span>
         <nav id="menu">
       <ul>
       <li><a href="<?= Url::home() ?>" class="active">Home</a></li>
       <li><a href="#">About</a></li>
       <li><a href="#">Contact</a></li>    
       <li><a href="#">Blog</a></li>
       <li><a href="<?= Url::home() ?>site/logout">Logout</a></li>
    </ul>
 </nav> 
    </div>
    <ul>
     
      <li class="login">
       <a><?=$session['user_tmpid1']?></a>
        <div class="login-data-cont" style="display:none;">
        	<ul class="login-list">
            <li><a href="<?= Url::to(['index']) ; ?>">My Dashboard</a></li>
             <li><a href="<?= Url::to(['myevents']) ; ?>">My Events</a></li>
            <li><a href="<?= Url::to(['inbox']) ; ?>">Inbox</a></li>
            <li><a href="<?= Url::to(['editprofile']) ; ?>">Edit Profile</a></li>
            <li><a href="<?= Url::to(['changepassword']) ; ?>">Change Password</a></li>
            <li><a href="<?= Url::home() ?>site/logout">Logout </a></li>
            </ul>
        </div>
      </li>
      <li><a><span class="menu-icon phone"></span>+ 91 7666003344</a></li>
            <li class="top-menu-seprate-btn"><a href="#"><?= ($existevent['total'] == '0') ? 'Create Your Event' :'Add Your Event';?></a></li>
    </ul>
  </div>
</header>

<section class="section-text-cont">
  
  <div class="container">
    <div class="row">

	 
 <div>  
<?php if(Yii::$app->session->hasFlash('error')): ?>
<div class="alert alert-error alert-success">
<?php echo Yii::$app->session->getFlash('error'); ?>
</div>
<?php endif; ?>

<?php if(Yii::$app->session->hasFlash('success')): ?>
<div class="alert alert-success">
<?php echo Yii::$app->session->getFlash('success'); ?>
</div>
<?php endif; ?>
</div>	

      <div class="login-box-container">
        <div class="container">
          <div class="login-main-container">
            <div class="login-form-container">
           <?php $form = ActiveForm::begin([
			'id' => 'brandform1',
			'enableAjaxValidation' => false,
			 'action' => Url::to(['/sponsor/changepassword']),
			'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
		]); ?> 
               
                <div class="login-heading-text">
                 CHANGE PASSWORD
                </div>
              
                <div class="login-input-con">
					<?= $form->field($model, 'confirm_password')->textInput(['placeholder' =>'Current Password','autocomplete'=>'off','class'=>'change-pass'])->label(false);?>
                </div>
             
             	 <div class="login-input-con">
					 <?= $form->field($model, 'password')->textInput(['placeholder' =>'New Password','autocomplete'=>'off','class'=>'change-pass'])->label(false);?>
                </div>
                 <div class="login-input-con">
					 <?= $form->field($model, 'repeat_password')->textInput(['placeholder' =>'Confirm New Password','autocomplete'=>'off','class'=>'change-pass'])->label(false);?>
                </div>
             
				<?= Html::submitButton('Submit', ['class' => 'login-submit-btn register-btn','name' => 'login-button']) ?>
                
               
           <?php ActiveForm::end(); ?>
            </div>
            <div class="login-text-area">
              <div class="login-onspon-logo"></div>
              <p class="login-text">
                ONSPON is India's biggest platform which helps event managers reach out to multiple sponsors and event audience at the click of a button. More than 10,000 event managers, 350+ brands and millions of event goers have made it their platform of choice.
              </p>
            </div>
          </div>

        </div>
      </div>


    </div>
  </div>

</section>



