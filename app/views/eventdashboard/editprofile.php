<?php 
use yii\widgets\ActiveForm;

use yii\helpers\Url;
use yii\helpers\Html;

$asset = \app\assets\SponsorAsset::register($this);


include('header.php');


?>
<style>
input#seekerprofile-email {
    background: #eee;
    cursor: not-allowed;
}
div#seekerprofile-sex {
    padding-top: 14px;
}
</style>


<section class="section-text-cont">
  
  <div class="container">
    <div class="row">
		
	 
 <div>  
<?php if(Yii::$app->session->hasFlash('error')): ?>
<div class="alert alert-error">
<?php echo Yii::$app->session->getFlash('error'); ?>
</div>
<?php endif; ?>

<?php if(Yii::$app->session->hasFlash('success')): ?>
<div class="alert alert-success">
<?php echo Yii::$app->session->getFlash('success'); ?>
</div>
<?php endif; ?>
</div>	
	
      <div class="section-heading">
        <h1>
          Edit
          <span class="red_font">Profile</span>
        </h1>
      </div>

      <div class="edt-frm-container">
      <?php $form = ActiveForm::begin([
			'id' => 'brandform1',
			'enableAjaxValidation' => false,
			 'action' => Url::to(['/eventdashboard/editprofile']),
			'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
		]); ?> 
            <div class="usr-img-cont">
              <div class="usr-img">
				 <?php if($model->image){ ?>
					 <img src="<?= Yii::$app->homeUrl.$model->image;?>" id="profileblah">
					 <?php } else {  ?>
					<img src="" id="profileblah">
					<?php } ?>
              </div>
              <?= $form->field($model, 'image')->fileInput(['onchange'=>'readURLPROFILE(this);'])->label(false); ?>
              <div class="evnt-edt-rmv-cont">
                  <ul>
                    <li>
                      <a href="#"><i class="fa fa-trash"></i></a>
                      <span class="edt-rmv-tooltip">Delete</span>
                    </li>
                  </ul>
              </div>
              <span class="usr-img-cam-icon">
                <span class="cam-icon"></span>
              </span>
            </div>
            <div class="usr-details">
              <div class="usr-input-cont left">
                <label class="usr-inpt-lbl">
                  <span class="usr-inpt-icon usr-icon"></span>
                  Full Name
                  <span class="red_font">*</span>
                </label>
                <?= $form->field($model, 'username')->textInput(['autocomplete'=>'off','class'=>'edt-frm-input'])->label(false);?>
              </div>

              <div class="usr-input-cont right">
                <label class="usr-inpt-lbl">
                  <span class="usr-inpt-icon phone-no"></span>
                  Mobile Number
                  <span class="red_font">*</span>
                </label>
                <?= $form->field($model, 'contactnumber')->textInput(['autocomplete'=>'off','class'=>'edt-frm-input'])->label(false);?>
              </div>

               <div class="usr-input-cont left">
                <label class="usr-inpt-lbl">
                  <span class="usr-inpt-icon mail-icon"></span>
                  Email ID
                  <span class="red_font">*</span>
                </label>
                <?= $form->field($model, 'email')->textInput(['readOnly'=>true,'autocomplete'=>'off','class'=>'edt-frm-input'])->label(false);?>
              </div>
              
               <div class="usr-input-cont right">
                <label class="usr-inpt-lbl">
                  <span class="usr-inpt-icon category-icon"></span>
                  Landmark
                  <span class="red_font">*</span>
                </label>
					<?= $form->field($model, 'location')->textInput(['autocomplete'=>'off','class'=>'edt-frm-input'])->label(false);?>
           </div>
           
           <div class="usr-input-cont left">
                <label class="usr-inpt-lbl">
                  <span class="usr-inpt-icon org-icon"></span>
					Address
                  <span class="red_font">*</span>
                </label>
					<?= $form->field($model, 'address')->textInput(['autocomplete'=>'off','class'=>'edt-frm-input'])->label(false);?>
           </div>
               

              
              
              <div class="usr-input-cont right">
                <label class="usr-inpt-lbl">
                  <span class="usr-inpt-icon add-icon"></span>
                  About Me
                </label>
					<?= $form->field($model, 'text')->textarea(['autocomplete'=>'off','class'=>'edt-frm-textarea','rows'=>'5'])->label(false); ?>
               </div>

              <div class="usr-input-cont left input-sml23 input-sml23-1">
                <label class="usr-inpt-lbl">
                  <span class="usr-inpt-icon lnd-icon"></span>
                  Age
                </label>
                <?= $form->field($model, 'age')->textInput(['autocomplete'=>'off','class'=>'edt-frm-input'])->label(false);?>
              </div>

              <div class="usr-input-cont left input-sml23">
                <label class="usr-inpt-lbl">
                  <span class="usr-inpt-icon zip-icon"></span>
                  Gender
                  <span class="red_font">*</span>
                </label>
                	<?= $form->field($model, 'sex')->radioList(array('Male'=>'Male','Female'=>'Female'),['class'=>'edt-frm-input'])->label(false); ?>
              </div>
            </div>
            <?= Html::submitButton('Update Profile', ['class' => 'usr-frm-submit','name' => 'login-button']) ?>
            
         <?php ActiveForm::end(); ?>
      </div>
    </div>
  </div>

</section>


<script type="text/javascript">
function readURLPROFILE(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#profileblah').attr('src', e.target.result);	
		}
		reader.readAsDataURL(input.files[0]);
	}
}
</script>

