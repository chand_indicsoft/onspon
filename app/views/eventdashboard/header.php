<?php
use yii\helpers\Url;
use app\models\Brandmandates;

$session = Yii::$app->session;
$existevent = Yii::$app->getDb()->createCommand("SELECT count(*) as total FROM app_events WHERE user_id='".$session['user_id']."'")->queryone();

$asset = \app\assets\SponsorAsset::register($this);
$this->registerJs('$(window).load(function() {
		$(".notification-cont").click(function(){
      $.ajax({
			url:"'.Url::to(["inboxhome"]).'",
			dataType: "html",
			success:function(result){
			$(".notifi-sub-cont").html(result);
			$(".notification-toggle-cont").slideToggle(500);
			}
		});
      });

     $(".login").click(function(e){
     $(".login-data-cont").slideToggle(500);
		 e.stopPropagation();
		 $(".notification-toggle-cont").hide(500);
	});
	
	 $(document).click(function (event) {            
			$(".notification-toggle-cont").hide(500);
			 $(".login-data-cont").hide(500);
		});
      
});');

$notification = Brandmandates::UserTotalNotification($session['user_id']);

?>
<header>
  <a href="#" class="top-logo"></a>
  <div class="top-menu-bar">
    <div class="hamburger-menu toggle-x">
      <span class="arrows"></span>
         <nav id="menu">
       <ul>
       <li><a href="<?= Url::home() ?>" class="active">Home</a></li>
       <li><a href="#">About</a></li>
       <li><a href="#">Contact</a></li>    
       <li><a href="#">Blog</a></li>
       <li><a href="<?= Url::home() ?>site/logout">Logout</a></li>
    </ul>
 </nav> 
    </div>
    <ul>
     
      <li class="login">
       <a><?=$session['user_tmpid1']?></a>
        <div class="login-data-cont" style="display:none;">
        	<ul class="login-list">
            <li><a href="<?= Url::to(['index']) ; ?>">My Dashboard</a></li>
             <li><a href="<?= Url::to(['myevents']) ; ?>">My Events</a></li>
            <li><a href="<?= Url::to(['inbox']) ; ?>">Inbox</a></li>
            <li><a href="<?= Url::to(['editprofile']) ; ?>">Edit Profile</a></li>
            <li><a href="<?= Url::to(['changepassword']) ; ?>">Change Password</a></li>
            <li><a href="<?= Url::home() ?>site/logout">Logout </a></li>
            </ul>
        </div>
      </li>
      <li><a><span class="menu-icon phone"></span>+ 91 7666003344</a></li>
      <li class="top-menu-seprate-btn"><a href="#"><?= ($existevent['total'] == '0') ? 'Create Your Event' :'Add Your Event';?></a></li>
      <li class="notification-cont">
        <a>
          <span class="bell-icon"></span>
         <?php if($notification):?> <span class="notification-counter"><?=$notification['total_notification'];?></span><?php endif;?>
        </a>
        <div class="notification-toggle-cont" style="display: none;">
          <div class="notifi-sub-cont">
          </div>
          <a href="#" class="ntfction-see-all">See All</a>
        </div>
      </li>
    </ul>
  </div>
</header>

<section class="section-banner-cont section-banner">
	<div class="section-img ocuspoint" data-focus-x="0" data-focus-y="0">
		<img src="<?= $asset->baseUrl ?>/images/section-banner1.jpg">
	</div>

	<div class="section-caption-cont">
		<div class="container">
			<div class="section-caption">
				<h1 class="section-lead-text">
					Welcome to
					<span class="red_font list-inline">onspon.com</span>
				</h1>
				<h3 class="section-sub-lead-text">
					Where best "Events" are discovered
				</h3>
				<ul class="section-buttons">
					<li><a href="<?= Url::to(['brandlist']) ; ?>" class="">Apply Brand Mandates</a></li>
					<li><a href="#">Looking for Barter?</a></li>
				</ul>
			</div>
		</div>
	</div>

	<div class="explore-events-cont evnt-dashboard red_bg">
			<div class="container">
				<p>
					More than
					<span class="font_wieght700 white_font list-inline three100"> 7500</span>
					event managers have chosen <span class="font_wieght700 list-inline onspon_com"> onspon.com</span>
					to showcase their event(s). Let's start by
					<a href="#" class="white_font font_wieght500 list-inline exp-evt-link"> listing your event</a>
					or by
					<a href="#" class="white_font font_wieght500 list-inline exp-evt-link">checking out what brands are looking for...</a>
				</p>
		</div>
	</div>
</section>

<!--banner end here-->



