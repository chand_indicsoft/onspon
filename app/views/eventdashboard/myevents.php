<?php

use yii\helpers\Url;
use app\models\Brandmandates;
use app\models\Common;
$commonModel = new Common;

$asset = \app\assets\SponsorAsset::register($this);

$folderlist = $commonModel->UserShortlistfolder(Yii::$app->session['user_id']);


include('header.php');

?>




<section class="section-text-cont">
  
  <div class="container">
    <div class="row">

      <div class="section-heading">
        <h1>
          All
          <span class="red_font">Events</span>
        </h1>
      </div>
      <div class="usr-all-evnt-cont">
        <div class="admsn-sub-container">
            <ul class="accordion css-accordion">
              <li class="accordion-item">
                <input class="accordion-item-input" name="accordion" id="item1" type="checkbox">
                <label for="item1" class="accordion-item-hd">
                  Active Events
                  <span class="accordion-item-hd-cta"></span>
                </label>
                <div class="accordion-item-bd">
                 <?php if($activelist):  foreach($activelist as $active) : ?> 
                  <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="listed-evnt-cont">
                      <div class="article-thumb-img focuspoint" data-focus-x="0" data-focus-y="0">
                        <?php if($active['logo']){ ?><img src="<?= Yii::$app->homeUrl.$active['logo'];?>"><?php } ?>
                      </div>
                      <div class="article-thumb-text-container">
                        <h3 class="article-thumb-heading"><?= $active['event_name'];?></h3>
                        <ul class="article-thumb-info">
							<?php $start_date = $active['event_strt_date'];
							$event_start_date = date("d M Y", $start_date); ?>
                          <li><span class="listing-icon calender"></span><?=$event_start_date ?></li>
                          <li><a href="#"><span class="listing-icon link"></span> Link</a></li>
                          <li><span class="listing-icon like"></span> 05</li>
                          <li><span class="listing-icon placeholder"></span><?= $active['address'];?></li>
                         
                        </ul>
                        <ul class="lstd-evnt-spnsr-updt">
                        <li> <span class="spnsr-updt shortlisted">
									<?= ($active['toatlshort']) ? $active['toatlshort'] : '0';?>
										</span> Shortlisted by Sponsor</li>
									<li><span class="spnsr-updt shortlisted">
										<?= ($active['totalview']) ? $active['totalview'] : '0';?>
										</span>Total viewed by Sponsor</li>
                        </ul>
                        <div class="lstd-evnt-btns">
                          <span class="evnt-type-cont">
                            Premier <span class="evnt-type-value">Silver</span>
                          </span>
                          <a href="#" class="upgrade-nw-btn">
                            <span class="upgrade-nw-icon"></span>
                            Upgrade Now
                          </a>
                        </div>
                      </div>
                      <div class="evnt-edt-rmv-cont">
                        <ul>
                          <li>
                            <a href="#"><i class="fa fa-pencil"></i></a>
                            <span class="edt-rmv-tooltip">Edit</span>
                          </li>
                          <li>
                            <a href="#"><i class="fa fa-trash"></i></a>
                            <span class="edt-rmv-tooltip">Delete</span>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
				<?php endforeach; endif;?>
             
                </div>
              </li>
              <li class="accordion-item">
                <input class="accordion-item-input" name="accordion" id="item2" type="checkbox">
                <label for="item2" class="accordion-item-hd">
                  Inactive Events
                  <span class="accordion-item-hd-cta"></span>
                </label>
                <div class="accordion-item-bd">
                 <?php if($inactivelist) : foreach($inactivelist as $inactive) : ?>
                   <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="listed-evnt-cont">
                      <div class="article-thumb-img focuspoint" data-focus-x="0" data-focus-y="0">
                        <?php if($inactive['logo']){ ?><img src="<?= Yii::$app->homeUrl.$inactive['logo'];?>"><?php } ?>
                      </div>
                      <div class="article-thumb-text-container">
                        <h3 class="article-thumb-heading"><?= $inactive['event_name'];?></h3>
                        <ul class="article-thumb-info">
							<?php $start_date = $inactive['event_strt_date'];
							$event_start_date = date("d M Y", $start_date); ?>
                          <li><span class="listing-icon calender"></span><?=$event_start_date ?></li>
                          <li><a href="#"><span class="listing-icon link"></span> Link</a></li>
                          <li><span class="listing-icon like"></span> 05</li>
                          <li><span class="listing-icon placeholder"></span><?= $inactive['address'];?></li>
                         
                        </ul>
                        <ul class="lstd-evnt-spnsr-updt">
                        <li> <span class="spnsr-updt shortlisted">
									<?= ($inactive['toatlshort']) ? $inactive['toatlshort'] : '0';?>
										</span> Shortlisted by Sponsor</li>
									<li><span class="spnsr-updt shortlisted">
										<?= ($inactive['totalview']) ? $inactive['totalview'] : '0';?>
										</span>Total viewed by Sponsor</li>
                        </ul>
                        <div class="lstd-evnt-btns">
                          <span class="evnt-type-cont">
                            Premier <span class="evnt-type-value">Silver</span>
                          </span>
                          <a href="#" class="upgrade-nw-btn">
                            <span class="upgrade-nw-icon"></span>
                            Upgrade Now
                          </a>
                        </div>
                      </div>
                      <div class="evnt-edt-rmv-cont">
                        <ul>
                          <li>
                            <a href="#"><i class="fa fa-pencil"></i></a>
                            <span class="edt-rmv-tooltip">Edit</span>
                          </li>
                          <li>
                            <a href="#"><i class="fa fa-trash"></i></a>
                            <span class="edt-rmv-tooltip">Delete</span>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
				 <?php endforeach; endif; ?> 
              
                </div>
              </li>
            </ul>
        </div>
      </div>

      
    </div>
  </div>

</section>

              
