<?php
use yii\helpers\Url;
use  yii\widgets\LinkPager;
use app\models\Dashboard;
use app\models\Common;
$commonModel = new Common;

$asset = \app\assets\SponsorAsset::register($this);




	
include('header.php');
?>



<?php if($BrandMandate): ?>
<section class="section-text-cont">

	<div class="container">
		<div class="row">

			<div class="section-heading">
				<h1>
					Explore
					<span class="red_font">Brand Mandates</span>
				</h1>
			</div>

		<?php $i = 0; foreach($BrandMandate as $brand) : ?>
			<?php if($i%3==0){ echo '<div class="clear"></div>';} ?>																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																
			<div class="col-lg-4 col-md-4 col-sm-6">
				<div class="evnt-dsbrd-card">
					<div class="article-thumb-img">
					<?php if($brand['logo']) { ?><img src="<?= Yii::$app->homeUrl.$brand['logo'];?>"><?php } ?>
					</div>
					<h3 class="article-thumb-heading">
						<?= $brand['brand_name'];?>
					</h3>
					<?php if($brand['specific_subjective']){ ?>
					<div class="evnt-dsbrd-objctv-dscp">
						<span class="objctv-dscp-heading">
							Objective
						</span>
						<span class="objctv-dscp-data">
							<ul><?php $objective=@explode(',',$brand['specific_subjective']);
										foreach($objective as $_objective){?> <li> <?php
											echo $commonModel->getObjective($_objective).'&nbsp';
											?> </li>
									 <?php }?>
							</ul>
						</span>
					</div>
					 <?php }?>
					<div class="evnt-dsbrd-objctv-dscp">
						<span class="objctv-dscp-heading">
							Discription
						</span>
						<span class="objctv-dscp-data">
							<p>
								<?= $brand['about_brand'];?>
							</p>
						</span>
					</div>
					<div class="article-thumb-details">
						<ul>
							<li><a href="#">Save for leter</a></li>
							<li class="rqr-evnt">
								<a href="#">Required Event Profile..? </a>
								<div class="rqr-evnt-prfl-cont">
									<h5>Looking for events having the following Audience profile</h5>
									<ul>
										<?php if($brand['agegroup']){ ?><li><span class="rqr-evnt-icons family"></span><?= $brand['agegroup'];?></li><?php } ?>
										<?php if($brand['educationname']){ ?><li><span class="rqr-evnt-icons education"></span><?= $brand['educationname'];?></li><?php } ?>
										<?php if($brand['gender']){ ?><li><span class="rqr-evnt-icons gender"></span><?= $brand['gender'];?></li><?php } ?>
										<?php if($brand['totalimcome']){ ?><li><span class="rqr-evnt-icons money"></span><?= $brand['totalimcome'];?></li><?php } ?>
										<li><span class="rqr-evnt-icons map"></span>All India</li>
										<?php if($brand['start_date']){
											$start_date = $brand['start_date'];
											$end_date = $brand['end_date'];
											$start = date("d M", $start_date);
											$end = date("d M Y", $end_date);?>
										<li><span class="rqr-evnt-icons calendar"></span><?= $start.' To '.$end;?></li><?php } ?>
									</ul>
								</div>
							</li>
						</ul>
					</div>
					<a  onclick="applynow(<?=$brand['id']?>,<?=$brand['user_id']?>); return false;" class="aply-nw">Apply Now</a>
				</div>
			</div>
		<?php $i++; endforeach;?>
		</div>
		
		<div class="paginat">
							<?php
								echo LinkPager::widget(['pagination' => $pages,]);
							?>
						</div>  
	</div>
</section>
<?php endif; ?>


<script>
function applynow(brandid,receiver)
{
	
	data ='brandid='+brandid+'&receiver='+receiver;
	$.ajax({
		url:'<?= Url::to(['brand/brandapply']) ; ?>',
		data:data,
		type:'POST',
		success: function(result)
		{
			alert(result);	
			
		}
		
	});
}

</script>
