<?php
use yii\helpers\Url;
use app\models\Dashboard;
use app\models\Common;
$commonModel = new Common;

$asset = \app\assets\SponsorAsset::register($this);


$this->registerJs("if(jQuery().slick)
	{
		jQuery('.lstd-evnt-slider').slick({
			infinite: true,
			slidesToShow: 2,
			slidesToScroll: 1,
			autoplay: false,
			autoplaySpeed: 2000,
			dots: false,
			arrows: true,
			prevArrow: '<div class=\"pc-prev\"><i class=\"fa fa-angle-left\"></i></div>',
			nextArrow: '<div class=\"pc-next\"><i class=\"fa fa-angle-right\"></i></div>'
		});
	}", \yii\web\View::POS_LOAD);
	
$session = Yii::$app->session;
$folderlist = $commonModel->UserShortlistfolder(Yii::$app->session['user_id']);
$amazingEvent = Dashboard::AmazingEventDashboard();
$myevents = Dashboard::MyEventsList(Yii::$app->session['user_id']);
$BrandMandate = Dashboard::BrandMandateHome();


	
	
//echo "<pre>";print_r($BrandMandate);	
	
include('header.php');
?>


<section class="section-text-cont fb-imporeted-evnt">

		<div class="container">
			<div class="row">
				<div class="fb-imprtd-sub-cont">
					<h1 class="fb-imprtd-heading">
						Import your
						<span class="fb-logo"></span>
						Event
					</h1>
				</div>

				<div class="load-btn-cont">
					<button class="load-button fb-imprtd-btn">
						IMPORT
						<span class="load-btn-icons import"></span>
					</button>
				</div>
			</div>
		</div>

</section>


<?php if($myevents) : ?>
<section class="section-text-cont">

	<div class="container">
		<div class="row">

			<div class="section-heading">
				<h1>
					Your Listed
					<span class="red_font">Events</span>
				</h1>
			</div>

			<div class="lstd-evnt-slider">
			<?php foreach($myevents as $event) : ?>	
				<div class="lstd-evnt-slide">
					<div class="col-lg-6 col-md-6 col-sm-6">
						<div class="listed-evnt-cont">
							<div class="article-thumb-img focuspoint" data-focus-x="0" data-focus-y="0">
								<?php if($event['logo']) { ?>
									<img src="<?= Yii::$app->homeUrl.$event['logo'];?>">
								<?php } ?>
							</div>
							<?php $start_date = $event['event_strt_date'];
							$event_start_date = date("d M Y", $start_date); ?>
							<div class="article-thumb-text-container">
								<h3 class="article-thumb-heading"><?= $event['event_name'];?></h3>
								<ul class="article-thumb-info">
									<li><span class="listing-icon calender"></span><?=$event_start_date ?></li>
									<li><a href="#"><span class="listing-icon link"></span> Link</a></li>
									<li><span class="listing-icon like"></span> 05</li>
									<li><span class="listing-icon placeholder"></span><?= $event['address'];?><li>
									
								</ul>
								<ul class="lstd-evnt-spnsr-updt">
									<li> <span class="spnsr-updt shortlisted">
									<?= ($event['toatlshort']) ? $event['toatlshort'] : '0';?>
										</span> Shortlisted by Sponsor</li>
									<li><span class="spnsr-updt shortlisted">
										<?= ($event['totalview']) ? $event['totalview'] : '0';?>
										</span>Total viewed by Sponsor</li>
								</ul>
								<div class="lstd-evnt-btns">
									<span class="evnt-type-cont">
										Premier <span class="evnt-type-value">Silver</span>
									</span>
									<a href="#" class="upgrade-nw-btn">
										<span class="upgrade-nw-icon"></span>
										Upgrade Now
									</a>
								</div>
							</div>
							<div class="lstd-evnt-complition">
								<span class="evnt-cmlsn-heading">Event Complition</span>
								<h3 class="percentage-text">90%</h3>
								<div class="percentage-cont">
									<span class="percentage-bg" style="width: 90%;"></span>
								</div>
							</div>
							<div class="evnt-edt-rmv-cont">
								<ul>
									<li>
										<a href="#"><i class="fa fa-pencil"></i></a>
										<span class="edt-rmv-tooltip">Edit</span>
									</li>
									<li>
										<a href="#"><i class="fa fa-trash"></i></a>
										<span class="edt-rmv-tooltip">Delete</span>
									</li>
								</ul>
							</div>
							<span class="fb-imprtd"></span>
						</div>
					</div>
				</div>
			<?php endforeach;?>
			
			</div>
			<div class="load-btn-cont">
				<button class="load-button">
					ADD AN EVENTS
					<span class="load-btn-icons load"></span>
				</button>
			</div>
		</div>
	</div>
	</div>
</div>
</section>
<?php endif;?>


<?php if($BrandMandate): ?>
<section class="section-text-cont">

	<div class="container">
		<div class="row">

			<div class="section-heading">
				<h1>
					Explore
					<span class="red_font">Brand Mandates</span>
				</h1>
			</div>

		<?php $i = 0; foreach($BrandMandate as $brand) : ?>
			<?php if($i%3==0){ echo '<div class="clear"></div>';} ?>
			<div class="col-lg-4 col-md-4 col-sm-6">
				<div class="evnt-dsbrd-card">
					<div class="article-thumb-img">
					<?php if($brand['logo']) { ?><img src="<?= Yii::$app->homeUrl.$brand['logo'];?>"><?php } ?>
					</div>
					<h3 class="article-thumb-heading">
						<?= $brand['brand_name'];?>
					</h3>
					<?php if($brand['specific_subjective']){ ?>
					<div class="evnt-dsbrd-objctv-dscp">
						<span class="objctv-dscp-heading">
							Objective
						</span>
						<span class="objctv-dscp-data">
							<ul><?php $objective=@explode(',',$brand['specific_subjective']);
										foreach($objective as $_objective){?> <li> <?php
											echo $commonModel->getObjective($_objective).'&nbsp';
											?> </li>
									 <?php }?>
							</ul>
						</span>
					</div>
					 <?php }?>
					<div class="evnt-dsbrd-objctv-dscp">
						<span class="objctv-dscp-heading">
							Discription
						</span>
						<span class="objctv-dscp-data">
							<p>
								<?= $brand['about_brand'];?>
							</p>
						</span>
					</div>
					<div class="article-thumb-details">
						<ul>
							<li><a href="#">Save for leter</a></li>
							<li class="rqr-evnt">
								<a href="#">Required Event Profile..? </a>
								<div class="rqr-evnt-prfl-cont">
									<h5>Looking for events having the following Audience profile</h5>
									<ul>
										<?php if($brand['agegroup']){ ?><li><span class="rqr-evnt-icons family"></span><?= $brand['agegroup'];?></li><?php } ?>
										<?php if($brand['educationname']){ ?><li><span class="rqr-evnt-icons education"></span><?= $brand['educationname'];?></li><?php } ?>
										<?php if($brand['gender']){ ?><li><span class="rqr-evnt-icons gender"></span><?= $brand['gender'];?></li><?php } ?>
										<?php if($brand['totalimcome']){ ?><li><span class="rqr-evnt-icons money"></span><?= $brand['totalimcome'];?></li><?php } ?>
										<li><span class="rqr-evnt-icons map"></span>All India</li>
										<?php if($brand['start_date']){
											$start_date = $brand['start_date'];
											$end_date = $brand['end_date'];
											$start = date("d M", $start_date);
											$end = date("d M Y", $end_date);?>
										<li><span class="rqr-evnt-icons calendar"></span><?= $start.' To '.$end;?></li><?php } ?>
									</ul>
								</div>
							</li>
						</ul>
					</div>
					<a  onclick="applynow(<?=$brand['id']?>,<?=$brand['user_id']?>); return false;" class="aply-nw">Apply Now</a>
				</div>
			</div>
		
		<?php $i++; endforeach;?>
		
			<div class="load-btn-cont">
				 <a href="<?= Url::to(['brandlist']) ; ?>" class="load-button">LOAD MORE<span class="load-btn-icons load"></span></a>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>

<?php if($amazingEvent) : ?> 
<section class="section-text-cont amazing-deal">

  <div class="container">
    <div class="row">
      <div class="section-heading">
        <h1>Amazing
          <span class="red_font"> Deals </span>
        </h1>
      </div>
     
     <?php foreach($amazingEvent as $amazing) : ?>
     <?php $themename=($commonModel->getEventtheme($amazing['id']));?>
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
        <div class="listing-card-cont details-card-cont details-card-cont premium">
          <div class="article-thumb-img">
			  <?php
				if($amazing['logo']==''){ ?>
					 <img src="<?= $asset->baseUrl ?>/images/NoImage.png" width="100%">
				<?php	}else { ?>
					<img src="<?= Yii::$app->homeUrl."/".$amazing['logo']; ?>" width="100%">
				<?php } ?>
          </div>
          <span class="premium-tag">
            Premium
          </span>
          <div class="article-thumb-text-container">
            <h3 class="article-thumb-heading"><?= $amazing['event_name'];?></h3>
            <ul class="article-thumb-info">
				<?php $start_date = $amazing['event_strt_date'];
					$event_start_date = date("d M Y", $start_date); ?>
              <li><span class="listing-icon calender"></span><?=$event_start_date ?></li>
              <li><span class="listing-icon like"></span> 05</li>
          <li><span class="listing-icon view"></span><?php if($amazing['views']){ echo $amazing['views'];} else { echo "0";}?></li>
              <li><span class="listing-icon placeholder"></span><?=ucwords($amazing['address'])?></li>
            </ul>
             <ul class="article-thumb-details">
              <li class="contact-now"><a>Contact Now</a></li>
              <div class="contact-data-cont" style="display:none;" id="contactarea<?=$amazing['id'];?>">
                <span class="close-shortlist close-popup"></span>
                <div class="shorlist-heading">
                  <h3>Contact Listing Owner</h3>
                  <h5 class="event-nm"><?= $amazing['event_name'];?></h5>
                  <span class="evnt-date-n-place"><?=$event_start_date ?> | <?= $amazing['city_id'];?>, <?= $amazing['country_id'];?> </span>
                </div>
					<span class="conatcterr" id="nameerr<?=$amazing['id']?>" style="display:none;">Fill Your Name</span>
					<span class="conatcterr" id="phoneerr<?=$amazing['id']?>" style="display:none;">Fill Your Phnone Number</span>
					<span class="conatcterr" id="texterr<?=$amazing['id']?>" style="display:none;">Fill Your Message</span>
					<form class="contact-nw-form" id="contactnow<?=$amazing['id']?>">
						<input type="text" name="name" id="name<?=$amazing['id']?>" placeholder="Your Name" class="input-create-fld">
						<input type="text" name="phone" id="phone<?=$amazing['id']?>" placeholder="Your Number" class="input-create-fld">
						<textarea cols="" rows="4" name="text" id="text<?=$amazing['id']?>" placeholder="Your Message" class="input-create-fld"></textarea>
						<button class="shorlist-btn save" id="" onclick="contactnow(<?=$amazing['id']?>); return false;">Save</button>
						<button class="shorlist-btn cancel " onclick="closepopcontact(<?=$amazing['id']?>); return false;">Cancel</button>
					</form>
              </div>
              <li class="shorlist"><a>Shortlist Now</a></li>
              <div class="shorlist-data-cont" style="display:none;" id="shortlistarea<?=$amazing['id']?>">
                  <span class="close-shortlist close-popup"></span>
                  <div class="shorlist-heading">
                    <h4>Shortlist &amp; save this listing</h4>
                    <p>
                      Organize your selection into folders for future references
                    </p>
                    <div id="folder-list<?=$amazing['id']?>">
                    <?php if($folderlist){
						foreach($folderlist as $folder)
							{
								echo '<br><input type="checkbox" name="folder" value="'.$folder['id'].'">'.$folder['foldername'];		
							} } ?>
					</div>
                  </div>
                  <form class="id"  id="Frmshortlist">
					<div class="create-folder">
					<p>Create a new folder</p>
					<span class="conatcterr" id="shortfoldererr<?=$amazing['id']?>" style="display:none;">Please Enter FolderName</span>
					<input type="text" name="shortfoldername" id="shortfoldername<?=$amazing['id']?>" placeholder="e.g Apple Activation Delhi" class="input-create-fld">
					<button class="add-fld-btn" id="" onclick="shortlist_add_folder(<?=$amazing['id']?>); return false;">
					<i class="fa fa-folder-open-o" aria-hidden="true"></i> Add</button>
					</div>
					<button class="shorlist-btn save" id="" onclick="shortlistevent(<?=$amazing['id']?>); return false;">Save</button>
					<button class="shorlist-btn cancel" onclick="closepopshortlist(<?=$amazing['id']?>); return false;">Cancel</button>
					</form>
              </div>
              <li><a href="<?= Url::home() ?>theme/<?=$themename?>?id=<?=$amazing['id']?>">More Info</a></li>
            </ul>
          </div>
        </div>
      </div>
     <?php endforeach;?> 
     
      <div class="load-btn-cont">
           <a href="<?= Url::to(['amazing']) ; ?>" class="load-button">
          VIEW MORE
          <span class="load-btn-icons view"></span>
        </a>
      </div>
    </div>
</section>
           
</section>
<?php endif; ?> 

<script>
function applynow(brandid,receiver)
{
	
	data ='brandid='+brandid+'&receiver='+receiver;
	$.ajax({
		url:'<?= Url::to(['brand/brandapply']) ; ?>',
		data:data,
		type:'POST',
		success: function(result)
		{
			alert(result);	
			
		}
		
	});
}

</script>
