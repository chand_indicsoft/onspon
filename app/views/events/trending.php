<?php

use yii\helpers\Url;
use  yii\widgets\LinkPager;

use app\models\Common;
$commonModel = new Common;

$asset = \app\assets\SponsorAsset::register($this);

$folderlist = $commonModel->UserShortlistfolder(Yii::$app->session['user_id']);


//print_r($Eventtrending);

?>

<section class="section-banner-cont section-banner sec-bnr-300">
  <div class="section-img">
		<img src="<?= $asset->baseUrl ?>/images/section-banner1.jpg">
	</div>

	<div class="section-caption-cont">
		<div class="container">
			<div class="section-caption">
				<h1 class="section-lead-text">
					Welcome to
					<span class="red_font list-inline">onspon.com</span>
				</h1>
				<h3 class="section-sub-lead-text">
					Where best "Events" are discovered
				</h3>
			
			</div>
		</div>
	</div>


</section>

<div class="clearfix"></div>
<!--banner end here-->

<section class="section-text-cont">
  
  <div class="container">
    <div class="row">
      <div class="section-heading">
        <h1>
         Trending
          <span class="red_font">  Events</span>
        </h1>
      </div>


      <!-- Listing starts -->
      <?php if($Eventtrending): ?>
      <?php foreach($Eventtrending as $item): ?>
      <?php $themename=($commonModel->getEventtheme($item['id']));?>
      <div class="col-lg-6">
        <div class="listing-card-cont premium">
          <div class="article-thumb-img">
			  <?php
				if($item['logo']==''){ ?>
					 <img src="<?= $asset->baseUrl ?>/images/NoImage.png">
				<?php	}else { ?>
					<img src="<?= Yii::$app->homeUrl."/".$item['logo']; ?>">
				<?php } ?>
            <img src="images/listing1.jpg">
          </div>
          <span class="premium-tag">
            Premium
          </span>
          <?php $start_date = $item['event_strt_date'];
			$event_start_date = date("d M Y", $start_date); ?>
          <div class="article-thumb-text-container">
            <h3 class="article-thumb-heading"><?= $item['event_name'];?></h3>
            <ul class="article-thumb-info">
              <li><span class="listing-icon calender"></span> <?= $event_start_date;?></li>
              <li><span class="listing-icon like"></span> 05</li>
             <?php if($item['views']): ?><li><span class="listing-icon view"></span><?= $item['views'];?></li><?php endif;?>
               <li><span class="listing-icon placeholder"></span><?=ucwords($item['address'])?></li>
            </ul>
               <ul class="article-thumb-details">
              <li class="contact-now"><a>Contact Now</a></li>
              <div class="contact-data-cont" style="display:none;" id="contactarea<?= $item['id']?>">
                <span class="close-shortlist close-popup" onclick="closepop(); return false;"></span>
                <div class="shorlist-heading">
                  <h3>Contact Listing Owner</h3>
                  <h5 class="event-nm"><?= $item['event_name'];?></h5>
                  <span class="evnt-date-n-place"><?=$event_start_date; ?> | <?= $item['city_id'];?>, <?= $item['country_id'];?> </span>
               </div>
					<span class="conatcterr" id="nameerr<?= $item['id']?>" style="display:none;">Fill Your Name</span>
					<span class="conatcterr" id="phoneerr<?= $item['id']?>" style="display:none;">Fill Your Phnone Number</span>
					<span class="conatcterr" id="validphoneerr<?=$item['id']?>" style="display:none;">Fill Valid Phnone Number</span>
					<span class="conatcterr" id="texterr<?= $item['id']?>" style="display:none;">Fill Your Message</span>
					<form class="contact-nw-form" id="contactnow<?= $item['id']?>">
						<input type="text" name="name" id="name<?= $item['id']?>" placeholder="Your Name" class="input-create-fld">
						<input type="text" name="phone" id="phone<?= $item['id']?>" placeholder="Your Number" class="input-create-fld">
						<textarea cols="" rows="4" name="text" id="text<?= $item['id']?>" class="input-create-fld"><?php echo Yii::$app->params['contactMessage'];?></textarea>
						<button class="shorlist-btn save" id="" onclick="contactnow(<?= $item['id']?>); return false;">Send</button>
						<button class="shorlist-btn cancel " onclick="closepopcontact(<?= $item['id']?>); return false;">Cancel</button>
					</form>
              </div>
              <?php
									# checking if the user has logged in or not
									if(!empty(Yii::$app->session['user_id']))
									{
										$Folderalreadyshortlist=$commonModel->checkUserEventShorlist($trending['id'],Yii::$app->session['user_id']);

										$folderlist = $commonModel->UserShortlistfolder(Yii::$app->session['user_id']);

									?>
									<li class="shorlist"><a>Shortlist Now</a></li>
									<?php }else{?>
									<li class="shorlist">
									<a href="<?php echo Url::to(['/login']); ?>">Shortlist Now</a>
									</li>
									<?php }?>
 <div class="shorlist-data-cont" style="display:none;" id="shortlistarea<?= $item['id']?>">
                  <span class="close-shortlist close-popup"></span>
                  <div class="shorlist-heading">
                    <h4>Shortlist &amp; save this listing</h4>
                    <p>
                      Organize your selection into folders for future references
                    </p>
                    <div id="folder-list<?= $item['id']?>">
                    <?php if($folderlist){
						foreach($folderlist as $folder)
							{
								echo '<br><input type="checkbox" name="folder" value="'.$folder['id'].'">'.$folder['foldername'];		
							} } ?>
					</div>
                  </div>
                  <form class="id"  id="Frmshortlist">
					<div class="create-folder">
					<p>Create a new folder</p>
					<span class="conatcterr" id="shortfoldererr<?= $item['id']?>" style="display:none;">Please Enter FolderName</span>
					<input type="text" name="shortfoldername" id="shortfoldername<?= $item['id']?>" placeholder="e.g Apple Activation Delhi" class="input-create-fld">
					<button class="add-fld-btn" id="" onclick="shortlist_add_folder(<?= $item['id']?>); return false;">
					<i class="fa fa-folder-open-o" aria-hidden="true"></i> Add</button>
					</div>
					<button class="shorlist-btn save" id="" onclick="shortlistevent(<?= $item['id']?>); return false;">Save</button>
					<button class="shorlist-btn cancel" onclick="closepopshortlist(<?= $item['id']?>); return false;">Cancel</button>
					</form>
                </div>
              <li><a href="<?= Url::home() ?>theme/<?=$themename?>?id=<?= $item['id']?>">More Info</a></li>
            </ul>
          </div>
        </div>
      </div>
      <?php endforeach; endif;?>
    </div>
     <div class="paginat">
		<?php
			echo LinkPager::widget(['pagination' => $pages,]);
		?>
	</div>  
  </div>
</section>
<script>
    function contactnow(id){
//alert(id);
	var name = $('#name'+id).val();
	var phone = $('#phone'+id).val();
	var text = $('#text'+id).val();
	if(!name){ $('#nameerr'+id).show();$('#phoneerr'+id).hide();$('#texterr'+id).hide(); return false;}
	if(!phone){ $('#nameerr'+id).hide();$('#phoneerr'+id).show();$('#texterr'+id).hide(); return false;}
	if(!text){ $('#nameerr'+id).hide();$('#phoneerr'+id).hide();$('#texterr'+id).show(); return false;}
	else{

		var data = $("#contactnow"+id).serialize();
		data +='&id='+id;
		$.ajax({
			url : '<?= Url::to(['eventcontact']) ; ?>',
			type: 'POST',
			data: data,
			success: function(result)
			{
				if(result=='success'){$("#contactarea"+id).hide(500); $('#nameerr'+id).hide();$('#phoneerr'+id).hide();$('#texterr'+id).hide(); alert('Message Sent');}
				else{alert('Something is Wrong');}
			}
		});
	}
}
</script>

<script>
function shortlist_add_folder(eid)
{
	var data;
	var name = $('#shortfoldername'+eid).val();
	if(!name)
	{
		$('#shortfoldererr'+eid).show(); return false;
	}else{
		data ='flodername='+name;
		$.ajax({
			url : '<?= Url::to(['shortlistfolder']) ; ?>',
			type: 'POST',
			data: data,
			dataType:'html',
			success: function(result)
			{
				if(result){
					//alert('Saved the Folder sucessfully');
					//alert(result);
					//document.getElementById("folder-list").innerHTML(result);

					$('#folder-list'+eid).html(result);

					$('#shortfoldererr'+id).hide();
				}else if(result=='error'){
					alert('Something is Wrong');

				}
			}
		});
	}//end of !name else



}

</script>

<script>
function shortlistevent(eid)
{
	var checkboxValues = [];
	$('input[name=folder]:checked').map(function() {
			checkboxValues.push($(this).val());
	});
	//console.log( checkboxValues);alert(checkboxValues.length);
	if(checkboxValues.length >=1)
	{
            //save to database
				data ='checkboxValues='+checkboxValues+'&eventid='+eid;
			$.ajax({
				url : '<?= Url::to(['shortlistevent']) ; ?>',
				type: 'POST',
				data: data,
				success: function(result)
				{
					if(result=='success'){alert('ShortList the event sucessfully');

                        location.reload();
						$('#shortfoldererr'+eid).hide();
						$("#shortlistarea"+eid).hide(500);
					}else{
						alert('Something is Wrong');
					}
				}
			});

            //save to db
	}else{
		alert("Select any folder name to organize your selection");
	}

}


$(function ()
	{
	
		// contact 	
		$(document).on('click', '.contact-now', function(e) {
			$(this).siblings(".contact-data-cont").show(500);
			$(this).siblings('li').next(".shorlist").addClass("active");
			e.stopPropagation();
		});

		$(document).on('click', '.close-popup', function(e) {
			$(this).parent(".contact-data-cont").hide(500);
			$(this).closest('li').next(".shorlist").removeClass("active");
			e.stopPropagation();
		});
	
	// shorlist 	
		$(document).on('click', '.shorlist', function(f) {
			$(this).siblings(".shorlist-data-cont").show(500);
			$(this).siblings('li').next(".shorlist").addClass("active");
			f.stopPropagation();
		});

		$(document).on('click', '.close-popup', function(f) {
			$(this).parent(".shorlist-data-cont").hide(500);
			$(this).closest('li').next(".shorlist").removeClass("active");
			f.stopPropagation();
		});
	});
	
	function closepopcontact(id)
	{
		$("#contactarea"+id).hide(500);
		$("#contactarea"+id).closest('li').next(".shorlist").removeClass("active");
	}
	
	function closepopshortlist(id)
	{
		$("#shortlistarea"+id).hide(500);
		$("#shortlistarea"+id).closest('li').next(".shorlist").removeClass("active");
	}
	

</script>




