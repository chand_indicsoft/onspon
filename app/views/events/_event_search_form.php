<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\eventcategories\api\EventCategories;
$modeleventcat=new EventCategories;use yii\helpers\ArrayHelper;
		
$categoryids=$modeleventcat->api_items();
if(@$_GET['catid']) {$catid=$_GET['catid'];}else{  $catid="";} 

?>
<br/>
<?= Html::beginForm(Url::to(['/events/eventlisting']), 'get', ['class' => 'form-inline']) ?>
    <div class="form-group">
        <?= Html::textInput('event_name', $event_name, ['class' => 'form-control', 'placeholder' => 'Type event name']) ?>
        	<?= Html::dropDownList('catid',null,ArrayHelper::map($categoryids,'id','title')
        	  	,['options' =>'' ,'prompt' => '--- select ---' , 'multiple'=>'multiple'  ]
        	  	

 )?>
			<?= Html::textInput('city', $city, ['class' => 'form-control', 'placeholder' => 'Type city']) ?>
       
    </div>
    <?= Html::submitButton('Search', ['class' => 'btn btn-default']) ?>
<?= Html::endForm() ?>
<br/>
