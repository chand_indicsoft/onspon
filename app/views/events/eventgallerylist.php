<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Common;
use app\modules\events\models\Events;
use  yii\widgets\LinkPager;

$asset = \app\assets\EventlistingAppAsset::register($this);
$commonModel = new Common;
?>

<section class="section-banner-cont section-banner">
	<div class="section-img">
		<img src="<?= $asset->baseUrl ?>/images/section-banner1.jpg">
	</div>

	<div class="section-caption-cont">
		<div class="container">
			<div class="section-caption">
				<h1 class="section-lead-text">
					welcome to 
					<span class="red_font list-inline">onspon.com</span>
				</h1>
				<h3 class="section-sub-lead-text">
					Curated Listing Page
				</h3>
			</div>
		</div>
	</div>
</section>

<div class="clearfix"></div>
<!--banner end here-->

<section class="section-text-cont">
	<div class="container">
		<div class="row">
			<div class="section-heading">
				<h1>
					Curated
					<span class="red_font"> Events @ Onspon</span>
				</h1>
			</div>

		<?php
				//echo '<pre>';print_r($eventdetail);die;
				foreach($eventdetail as $_eventdetail)
				{
			?>
					<!-- Listing -->
					<div class="col-lg-6">
						<div class="listing-card-cont premium">
							<div class="article-thumb-img">
								<?php
								if($_eventdetail['logo']=='')
									{ ?>
										 <img src="<?= $asset->baseUrl ?>/images/NoImage.png">
								<?php	}
								else { ?>
										<img src="<?= Yii::$app->homeUrl."/".$_eventdetail['logo'] ?>">
								<?php } ?>
							</div>

							<span class="premium-tag">Premium</span>

							<div class="article-thumb-text-container">
								<h3 class="article-thumb-heading"><?=ucwords($_eventdetail['event_name'])?></h3>
								<?php	$themename=($commonModel->getEventtheme($_eventdetail['id']));
										//echo "$themename";
								?>
								<ul class="article-thumb-info">
									<?php $start_date = $_eventdetail['event_strt_date'];
										$event_start_date = date("d M Y", $start_date); ?>
									<li><span class="listing-icon calender"></span><?=$event_start_date ?></li>
									<li><span class="listing-icon like"></span><?=$_eventdetail['fb_likes']?></li>
									<li><span class="listing-icon view"></span><?=$_eventdetail['views']?></li>
									<li><span class="listing-icon placeholder"></span><?=ucwords($_eventdetail['address'])?></li>
								</ul>
								<ul class="article-thumb-details">
									<li class="contact-now"><a>Contact Now</a>
									<div class="contact-data-cont" style="display:none;">
										<span class="close-shortlist close-popup"></span>
										<div class="shorlist-heading">
											<h3>Contact Listing Owner</h3>
											<h5 class="event-nm"><?=ucwords($_eventdetail['event_name'])?></h5>
											<span class="evnt-date-n-place">
												<?=$event_start_date ?> | <?=ucwords($_eventdetail['state_id'])?>, 
												<?=ucwords($_eventdetail['country_id'])?> </span>
										</div>
										<span class="conatcterr" id="nameerr<?=$_eventdetail['id']?>" style="display:none;">Fill Your Name</span>
										<span class="conatcterr" id="phoneerr<?=$_eventdetail['id']?>" style="display:none;">Fill Your Phnone Number</span>
										<span class="conatcterr" id="texterr<?=$_eventdetail['id']?>" style="display:none;">Fill Your Message</span>
										<form class="contact-nw-form" id="contactnow<?=$_eventdetail['id']?>">
											<input type="text" name="name" id="name<?=$_eventdetail['id']?>" placeholder="Your Name" class="input-create-fld">
											<input type="text" name="phone" id="phone<?=$_eventdetail['id']?>" placeholder="Your Number" class="input-create-fld">
											<textarea cols="" rows="4" name="text" id="text<?=$_eventdetail['id']?>" placeholder="Your Message" class="input-create-fld"></textarea>
											<button class="shorlist-btn save" id="" onclick="contactnow(<?=$_eventdetail['id']?>); return false;">Save</button>
											<button class="shorlist-btn cancel " id="" >Cancel</button>
										</form>
									</div>
									</li>

									<?php $folderlist="";
									# checking if the user has logged in or not
									if(!empty(Yii::$app->session['user_id']))
									{
										$Folderalreadyshortlist=$commonModel->checkUserEventShorlist($_eventdetail['id'],Yii::$app->session['user_id']);

										$folderlist = $commonModel->UserShortlistfolder(Yii::$app->session['user_id']);

									?>
									<li class="shorlist"><a>Shortlist Now<?=$Folderalreadyshortlist?></a>
									<?php }else{?>
									<li class="shorlist">
									<a href="<?php echo Url::to(['/login']); ?>">Shortlist Now</a>
									</li>
									<?php }?>

									<div class="shorlist-data-cont" style="display:none;">
										<span class="close-shortlist close-popup"></span>
										<div class="shorlist-heading">
											<h4>Shortlist &amp; save this listing</h4>
											<p>Organize your selection into folders for future references</p>
											<div id="folder-list<?=$_eventdetail['id']?>">
												<?php
												if($folderlist)
												{
													foreach($folderlist as $_folderlist)
													{
														echo '<br><input type="checkbox" name="folder" value="'.$_folderlist['id'].'">'.$_folderlist['foldername'];
													}//end of foreach
												}
												?>
											</div>

										</div>
										<form class="id"  id="Frmshortlist">
										<div class="create-folder">


										<p>Create a new folder</p>
										<span class="conatcterr" id="shortfoldererr<?=$_eventdetail['id']?>" style="display:none;">Please Enter FolderName</span>

										<input type="text" name="shortfoldername" id="shortfoldername<?=$_eventdetail['id']?>" placeholder="e.g Apple Activation Delhi" class="input-create-fld">
										<!--button name="" class="add-fld-btn"><i class="fa fa-folder-open-o" aria-hidden="true"></i> Add</button-->
										<button class="add-fld-btn" id="" onclick="shortlist_add_folder(<?=$_eventdetail['id']?>); return false;">
										<i class="fa fa-folder-open-o" aria-hidden="true"></i> Add
										</button>
										</div>
										<button class="shorlist-btn save" id="" onclick="shortlistevent(<?=$_eventdetail['id']?>); return false;">Save</button>
										<button class="shorlist-btn cancel" id="" >Cancel</button>
										</form>
									</div>
									</li>

									<li><a href="<?= Url::home() ?>theme/<?=$themename?>?id=<?=$_eventdetail['id']?>">More Info</a></li>
								</ul>
							</div>
						</div>
					</div>
			<?php }//end of for loop ?>
		</div>
	</div>
</section>

<script type="text/javascript">
	
		$(document).on('click', '.contact-now', function(e) {
			$(this).children(".contact-data-cont").show(500);
			$(this).parent('li').next(".shorlist").addClass("active");
			e.stopPropagation();
		});

		$(document).on('click', '.close-popup', function(e) {
			$(this).parent(".contact-data-cont").hide(500);
			$(this).closest('li').next(".shorlist").removeClass("active");
			e.stopPropagation();
		});

		$(document).on('click', '.shorlist', function(f) {
			$(this).children(".shorlist-data-cont").show(500);
			$(this).parent('li').next(".shorlist").addClass("active");
			f.stopPropagation();
		});

		$(document).on('click', '.close-popup', function(f) {
			$(this).parent(".shorlist-data-cont").hide(500);
			$(this).closest('li').next(".shorlist").removeClass("active");
			f.stopPropagation();
		});
	

		jQuery(document).ready(function($) {
			$('.smobitrigger').smplmnu();
		});
		
		
		
		
		
	
function contactnow(id){
	var name = $('#name'+id).val();
	var phone = $('#phone'+id).val();
	var text = $('#text'+id).val();
	if(!name){ $('#nameerr'+id).show();$('#phoneerr'+id).hide();$('#texterr'+id).hide(); return false;}
	if(!phone){ $('#nameerr'+id).hide();$('#phoneerr'+id).show();$('#texterr'+id).hide(); return false;}
	if(!text){ $('#nameerr'+id).hide();$('#phoneerr'+id).hide();$('#texterr'+id).show(); return false;}
	else{

		var data = $("#contactnow"+id).serialize();
		data +='&id='+id;
		$.ajax({
			url : '<?= Url::to(['eventcontact']) ; ?>',
			type: 'POST',
			data: data,
			success: function(result)
			{
				if(result=='success'){$('#nameerr'+id).hide();$('#phoneerr'+id).hide();$('#texterr'+id).hide(); alert('Message Sent');}
				else{alert('Something is Wrong');}
			}
		});
	}
}

function shortlist_add_folder(eid)
{
	var data;
	var name = $('#shortfoldername'+eid).val();
	if(!name)
	{
		$('#shortfoldererr'+eid).show(); return false;
	}else{
		data ='flodername='+name;
		$.ajax({
			url : '<?= Url::to(['shortlistfolder']) ; ?>',
			type: 'POST',
			data: data,
			dataType:'html',
			success: function(result)
			{
				if(result){
					//alert('Saved the Folder sucessfully');
					//alert(result);
					//document.getElementById("folder-list").innerHTML(result);

					$('#folder-list'+eid).html(result);

					$('#shortfoldererr'+id).hide();
				}else if(result=='error'){
					alert('Something is Wrong');

				}
			}
		});
	}//end of !name else



}


function shortlistevent(eid)
{
	var checkboxValues = [];
	$('input[name=folder]:checked').map(function() {
			checkboxValues.push($(this).val());
	});
	//console.log( checkboxValues);alert(checkboxValues.length);
	if(checkboxValues.length >=1)
	{
            //save to database
	 			data ='checkboxValues='+checkboxValues+'&eventid='+eid;
			$.ajax({
				url : '<?= Url::to(['shortlistevent']) ; ?>',
				type: 'POST',
				data: data,
				success: function(result)
				{
					if(result=='success'){alert('ShortList the event sucessfully');

                        location.reload();
						$('#shortfoldererr'+id).hide();
					}else{
						alert('Something is Wrong');
					}
				}
			});

            //save to db
	}else{
		alert("Select any folder name to organize your selection");
	}

}

</script>
	
		
