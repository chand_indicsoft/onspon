<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\widgets\ActiveForm;
	use app\models\Common;
	use app\modules\events\models\Events;
	use yii\widgets\LinkPager;
    use yii\helpers\ArrayHelper;
    use app\modules\eventcategories\api\EventCategories;


$trendingEvents = Common::trendingEventDashboard();

$modeleventcat=new EventCategories;
$categoryids=$modeleventcat->api_items();

$asset = \app\assets\AppAsset::register($this);
$commonModel = new Common;
$folderlist="";$timedisplay=false;    $catdisplay=false;$citydisplay=false;$radiovalue="";$radioval =array();
                                           

$catid = (@$_REQUEST['catid'] ? @$_REQUEST['catid'] : '');
$city  = (@$_REQUEST['city'] ? @$_REQUEST['city'] : '');

$timefval="";
$timefield=array('week'=>'This week','month'=>'This month','next3month'=>'Next 3 months');
if(@$_REQUEST['datetype']=='week') {$timefval=@$_REQUEST['datetype']; $timedisplay="This weekend";}
if(@$_REQUEST['datetype']=='month') {$timefval=@$_REQUEST['datetype']; $timedisplay="This month";}
if(@$_REQUEST['datetype']=='next3month') {$timefval=@$_REQUEST['datetype']; $timedisplay="Next 3 months";}

?><section class="section-banner-cont section-banner sec-bnr-300">
  <div class="section-img focuspoint" data-focus-x="0" data-focus-y="0">
		<img src="<?= $asset->baseUrl ?>/images/section-banner1.jpg">
	</div>

	<div class="section-caption-cont">
		<div class="container">
			<div class="section-caption">
				<h1 class="section-lead-text">
					Welcome to
					<span class="red_font list-inline">onspon.com</span>
				</h1>
				<h3 class="section-sub-lead-text">
					Where best "Events" are discovered
				</h3>
			
			</div>
		</div>
	</div>


</section>

<div class="clearfix"></div>
<!--banner end here-->

<section class="section-text-cont">
	<div class="container">
				<div class="row">
			<div class="section-heading">
				<h1>
					<?php
$varheading = "";
					if(!empty($catid))
					{
							$category = $commonModel->getCategoryById($catid);
							
					} //echo  $timedisplay=($timedisplay!="") ? $timedisplay:" ";
                                          // echo  $catdisplay=($catid!="") ? '/'.$category:" ";
                                          // echo  $citydisplay=($city!="") ? '/'.$city :" ";
                                       if($timedisplay!="")
					{
						$varheading.= $timedisplay;
					} 
					if($catid!="")   
					{
						if($varheading!="")
						$varheading.= "/".$category;
						else
						$varheading.= $category;
					}
					if ($city!="")
					{
						if($varheading!="")
						$varheading.= "/".$city;
						else
						$varheading.= $city;
					}
					?>
					<span class="red_font"><?php echo $varheading; ?> Events @ Onspon</span>
					</h1>
			</div>

			<div class="col-lg-12">
                           
				<div class="filter-buttons-cont">
					<ul>
<?= Html::beginForm(Url::to(['/events/eventlisting']), 'get', ['class' => 'form-inline','id'=>'eventForm']) ?>

<li class="filter-toggle" id="filter">
        <div class="filter-caption"><span class="filter-icon filter"></span> Filter</div>
        <div class="filter-data" id="filter-data" style="display:none;">
                <span class="filter-close"></span>
                <div class="form-group">
                <label>Time</label>
                       
                   <?= Html::dropDownList('datetype',$timefval, $timefield,
                                                  ['prompt' => '--- select ---' ,'class'=> "select-group",'data-live-search'=>"true" ]
                                                                        )?>
                   <label>Category</label>
                    <?= Html::dropDownList('catid',$catid ,ArrayHelper::map($categoryids,'id','title'),
                       ['prompt' => '--- select ---' ,'class'=> "select-group",'data-live-search'=>"true" ]
                                             )?>
                    <!--label>Audience</label>
                    <select class="select-group">
                    <option>Audience</option>
                    <option>Audience</option>
                    <option>Audience</option></select>
                     <label>City</label>
                      <label-->  
                       <label>City  </label>
                    <?= Html::textInput('city', $city, ['class' => 'form-control', 'placeholder' => 'Type city','aria-describedby'=>"basic-addon1"]) ?>
                    
                      <label>
                        <input type="submit" name="search" value="Go"/> 
                        <input type="button" onclick="window.location.href ='<?= Url::to(['eventlisting']) ; ?>';" value="Reset" />
                        </label>
        </div></div>
           </li>
<?php 
$radioval = ['no_of_attendees' => 'High audience Number', 'premium' => 'Premium','event_strt_date'=>'By Date'];
if(@$_REQUEST['sortby']=='no_of_attendees')
{
$radiovalue='no_of_attendees';}else if(@$_REQUEST['sortby']=='premium'){
    $radiovalue='premium';
}else{
     $radiovalue='event_strt_date';
}

?>
                        <li class="filter-toggle short" id="short">
                            <div class="filter-caption"><span class="filter-icon sort"></span> Sort By</div>
                            <div class="filter-data" id="short-data" style="display:none;">
								<span class="filter-close"></span>
        					
                                <div class="radio filter-radio-div">
                                        <label>
                                <?php  echo Html::radioList('sortby',@$radiovalue, $radioval, [
                                'class' => 'radio filter-radio-div',
                                'itemOptions' => ['class' => 'filter-radio'],
                                ]); ?>        </label>
                                </div>

                           
							</div>
						</li>
					</ul>
 <?= Html::endForm() ?>


				</div>
			</div>
			<?php
				//echo '<pre>';print_r($eventdetail);die;
		if($eventdetail)
		{
				foreach($eventdetail as $_eventdetail)
				{
			?>
					<!-- Listing -->
					<div class="col-lg-6">
						<div class="listing-card-cont premium">
							<div class="article-thumb-img">
								<?php
								if($_eventdetail->logo=='')
									{ ?>
										 <img src="<?= $asset->baseUrl ?>/images/NoImage.png">
								<?php	}
								else { ?>
										<img src="<?= Yii::$app->homeUrl."/".$_eventdetail->logo ?>">
								<?php } ?>
							</div>

							<?php
								if($_eventdetail->premium=='1')
									{ ?>
										<span class="premium-tag">Premium</span>
							<?php } ?>

							<div class="article-thumb-text-container">
								<h3 class="article-thumb-heading"><?php if(@$_eventdetail->event_name){
								echo ucwords($_eventdetail->event_name);
								} ?></h3>
								<?php	$themename=($commonModel->getEventtheme(@$_eventdetail['id']));
										//echo "$themename";
								?>
								<ul class="article-thumb-info">
									<?php $start_date = $_eventdetail->event_strt_date;
										$event_start_date = date("d M Y", $start_date); ?>
									<li><span class="listing-icon calender"></span><?=$event_start_date ?></li>
									<li><span class="listing-icon like"></span><?=$_eventdetail->fb_likes?></li>
									<li><span class="listing-icon view"></span><?=$_eventdetail->views?></li>
									<li><span class="listing-icon placeholder"></span><?=ucwords($_eventdetail->address)?></li>
								</ul>
								<ul class="article-thumb-details">
									<li class="contact-now">
                                        <a>Contact Now</a></li>
                                        <div class="contact-data-cont" style="display:none;" id="contactarea<?=$_eventdetail->id;?>">
                                            <span class="close-shortlist close-popup"></span>
                                            <div class="shorlist-heading">
                                                <h3>Contact Listing Owner</h3>
                                                <h5 class="event-nm"><?php if(@$_eventdetail->event_name){
								echo ucwords($_eventdetail->event_name);
								} ?></h5>
                                                <span class="evnt-date-n-place"><?=$event_start_date ?> | <?=ucwords($_eventdetail->state_id)?>, <?=ucwords($_eventdetail->country_id)?> </span>
                                            </div>
                                            <span class="conatcterr" id="nameerr<?=$_eventdetail->id?>" style="display:none;">Fill Your Name</span>
                                            <span class="conatcterr" id="phoneerr<?=$_eventdetail->id?>" style="display:none;">Fill Your Phnone Number</span>
                                            <span class="conatcterr" id="validphoneerr<?=$_eventdetail->id?>" style="display:none;">Fill Valid Phnone Number</span>
											<span class="conatcterr" id="texterr<?=$_eventdetail->id?>" style="display:none;">Fill Your Message</span>
                                            <form class="contact-nw-form" id="contactnow<?=$_eventdetail->id?>">
                                                <input type="text" name="name" id="name<?=$_eventdetail->id?>" placeholder="Your Name" class="input-create-fld">
                                                <input type="text" name="phone" id="phone<?=$_eventdetail->id?>" placeholder="Your Number" class="input-create-fld">
                                                <textarea cols="" rows="4" name="text" id="text<?=$_eventdetail->id?>" class="input-create-fld"><?php echo Yii::$app->params['contactMessage'];?></textarea>
                                                <button class="shorlist-btn save" id="" onclick="contactnow(<?=$_eventdetail->id?>); return false;">Send</button>
                                                <button class="shorlist-btn cancel " onclick="closepopcontact(<?=$_eventdetail->id;?>); return false;">Cancel</button>
                                            </form>
                                        </div>
									

									<?php
									# checking if the user has logged in or not
									if(!empty(Yii::$app->session['user_id']))
									{
										$Folderalreadyshortlist=$commonModel->checkUserEventShorlist($_eventdetail->id,Yii::$app->session['user_id']);

										$folderlist = $commonModel->UserShortlistfolder(Yii::$app->session['user_id']);

									?>
									<li class="shorlist"><a>Shortlist Now</a></li>
									<?php }else{ 
										# setting referral url
										$currentUrl = Url::current();
										$session = Yii::$app->session;
										$session['referral']= $currentUrl;
										?>
									<li class="shorlist">
									<a href="<?php echo Url::to(['/login']); ?>">Shortlist Now</a>
									</li>
									<?php }?>

									<div class="shorlist-data-cont" style="display:none;" id="shortlistarea<?=$_eventdetail->id?>">
										<span class="close-shortlist close-popup"></span>
										<div class="shorlist-heading">
											<h4>Shortlist &amp; save this listing</h4>
											<p>Organize your selection into folders for future references</p>
											<div id="folder-list<?=$_eventdetail->id?>">
												<?php
												if($folderlist)
												{
													foreach($folderlist as $_folderlist)
													{
														echo '<br><input type="checkbox" name="folder" value="'.$_folderlist['id'].'">'.$_folderlist['foldername'];
													}//end of foreach
												}
												?>
											</div>

										</div>
										<form class="id"  id="Frmshortlist">
										<div class="create-folder">


										<p>Create a new folder</p>
										<span class="conatcterr" id="shortfoldererr<?=$_eventdetail->id?>" style="display:none;">Please Enter FolderName</span>

										<input type="text" name="shortfoldername" id="shortfoldername<?=$_eventdetail->id?>" placeholder="e.g Apple Activation Delhi" class="input-create-fld">
										<!--button name="" class="add-fld-btn"><i class="fa fa-folder-open-o" aria-hidden="true"></i> Add</button-->
										<button class="add-fld-btn" id="" onclick="shortlist_add_folder(<?=$_eventdetail->id?>); return false;">
										<i class="fa fa-folder-open-o" aria-hidden="true"></i> Add
										</button>
										</div>
										<button class="shorlist-btn save" id="" onclick="shortlistevent(<?=$_eventdetail->id?>); return false;">Save</button>
										<button class="shorlist-btn cancel" onclick="closepopshortlist(<?=$_eventdetail->id?>); return false;">Cancel</button>
										</form>
									</div>
									

									<li><a href="<?= Url::home() ?>theme/<?=$themename?>?id=<?=$_eventdetail['id']?>">More Info</a></li>
								</ul>
							</div>
						</div>
					</div>
			<?php }//end of for loop 
			} //end of if ?>
		
		</div>
	</div>
    <div class="container"><div class="row">
     <?php if($eventdetail)   {?>    
        <div class="paginat">
                    <?php
        echo LinkPager::widget([
        'pagination' => $pages,
        ]);
        ?>
        </div>  
 <?php }else{?>
     <div class="empty-events">
  <?php   echo "Sorry No Events";
 }?>  </div>  
    </div></div>

</section>


<!--CURATED-->
<?php
$curatedlist=$commonModel->getCuratedEvents();
//print_r($curatedlist);die;
if(count($curatedlist))
{
	?>
<section class="section-text-cont">
  <div class="container">
    <div class="row">
      <div class="section-heading">
        <h1>
          Curated
          <span class="red_font">Events</span>
        </h1>
      </div>
      <div class="curated-slider">
       <?php foreach($curatedlist as $_curatedlist)
       {?>
        <div class="curated-slide">
         
            <div class="article-thumb-img">
				
				<?php if($_curatedlist['thumb_image'])
				{?>
                     <img src="<?= Yii::$app->homeUrl."/".$_curatedlist['thumb_image']?>" width="200" height="150">
              <?php }else {  ?>
				      <img src="<?= $asset->baseUrl ?>/images/comingsoon1.jpg"  width="200" height="150">
			  <?php }
			  ?>
            </div>
            <div class="article-thumb-heading">
				<a class="text-title" href="?boxid=<?=$_curatedlist['id']?>" target="_blank">
							
              <?php
					if($_curatedlist['tag_id'])
					{
						$curatedtag=@explode(',',$_curatedlist['tag_id']);
						foreach($curatedtag as $_curatedtag){
							echo $commonModel->getTagname($_curatedtag).', &nbsp;';
						}
					}
				?></a>
            </div>
            <ul class="article-thumb-details">
				
              <li><i class="fa fa-calendar"></i> <?=($_curatedlist['date_created'])?></li>
           
            </ul>
         
        </div>
        <?php }//end of foreach?>
        
           
     
      </div>
    </div>
  </div>
</section>
<?php }//end of curated list?>
<!--CURATED-->


<section class="section-text-cont top-category">
  <div class="container">
    <div class="row">
      <div class="section-heading">
        <h1>
          Top
          <span class="red_font">Category</span>
        </h1>
      </div>
        
        <?php if($eventcategoryList){ 
            foreach($eventcategoryList as  $_event_cat)
					{?>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                  <div class="listing-card-cont">
                    <div class="article-thumb-img">
                         <img src="<?=Yii::$app->homeUrl.$_event_cat->image?>">
                       
                        <div class="top-category-heading">
							 <a href="?catid=<?=$_event_cat->eventcategories_id?>" title="<?=$_event_cat->title?>">
                    
							<?=$_event_cat->title?> </a></div>
                    </div>

                  </div>
                </div>
        <?php }

        }//end of eventcat?>


        
        
      <!--div class="load-btn-cont">
        <button class="load-button">
          LOAD MORE
          <span class="load-btn-icons load"></span>
        </button>
      </div-->
    </div>
  </div>
</section>

<div class="clearfix"></div>

<section class="section-text-cont amazing-deal">
  <div class="container">
    <div class="row">
      <div class="section-heading">
        <h1>
          Trending 
          <span class="red_font">Events</span>
        </h1>
      </div>	  
	  <?php 
         
          foreach($trendingEvents as $trending) : ?>
     <?php $themename=($commonModel->getEventtheme($trending['id']));?>
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
        <div class="listing-card-cont details-card-cont details-card-cont premium">
          <div class="article-thumb-img">
			  <?php
				if($trending['logo']==''){ ?>
					 <img src="<?= $asset->baseUrl ?>/images/NoImage.png" width="100%">
				<?php	}else { ?>
					<img src="<?= Yii::$app->homeUrl."/".$trending['logo']; ?>" width="100%">
				<?php } ?>
          </div>
          <span class="premium-tag">
            Premium
          </span>
          <div class="article-thumb-text-container">
            <h3 class="article-thumb-heading"><?= $trending['event_name'];?></h3>
            <ul class="article-thumb-info">
				<?php $start_date1 = $trending['event_strt_date'];
					$event_start_date1 = date("d M Y", $start_date1); ?>
              <li><span class="listing-icon calender"></span><?=$event_start_date1 ?></li>
              <li><span class="listing-icon like"></span> 05</li>
              <?php if($trending['views']): ?><li><span class="listing-icon view"></span><?= $trending['views'];?></li>
                  <?php endif;?>
              <li><span class="listing-icon placeholder"></span><?=ucwords($trending['address'])?></li>
            </ul>
                  <ul class="article-thumb-details">
              		<li class="contact-now">
						<a>Contact Now</a></li>
						<div class="contact-data-cont" style="display:none;" id="contactarea<?=$trending['id'];?>">
							<span class="close-shortlist close-popup"></span>
							<div class="shorlist-heading">
								<h3>Contact Listing Owner</h3>
								<h5 class="event-nm"><?=ucwords($trending['event_name'])?></h5>
								<span class="evnt-date-n-place"><?=$event_start_date1 ?> |
<?=ucwords($trending['state_id'])?>, <?=ucwords($trending['country_id'])?> </span>
							</div>
							<span class="conatcterr" id="nameerr<?=$trending['id']?>" style="display:none;">Fill Your Name</span>
							<span class="conatcterr" id="phoneerr<?=$trending['id']?>" style="display:none;">Fill Your Phnone Number</span>
							<span class="conatcterr" id="validphoneerr<?=$trending['id']?>" style="display:none;">Fill Valid Phnone Number</span>
							<span class="conatcterr" id="texterr<?=$trending['id']?>" style="display:none;">Fill Your Message</span>
							<form class="contact-nw-form" id="contactnow<?=$trending['id']?>">
								<input type="text" name="name" id="name<?=$trending['id']?>" placeholder="Your Name" class="input-create-fld">
								<input type="text" name="phone" id="phone<?=$trending['id']?>" placeholder="Your Number" class="input-create-fld">
								<textarea cols="" rows="4" name="text" id="text<?=$trending['id']?>" class="input-create-fld"><?php echo Yii::$app->params['contactMessage'];?></textarea>
								<button class="shorlist-btn save" id="" onclick="contactnow(<?=$trending['id']?>); return false;">Send</button>
								<button class="shorlist-btn cancel" onclick="closepopcontact(<?=$trending['id']?>); return false;">Cancel</button>
							</form>
						</div>
									

									<?php
									# checking if the user has logged in or not
									if(!empty(Yii::$app->session['user_id']))
									{
										$Folderalreadyshortlist=$commonModel->checkUserEventShorlist($trending['id'],Yii::$app->session['user_id']);

										$folderlist = $commonModel->UserShortlistfolder(Yii::$app->session['user_id']);

									?>
									<li class="shorlist"><a>Shortlist Now</a></li>
									<?php }else{?>
									<li class="shorlist">
									<a href="<?php echo Url::to(['/login']); ?>">Shortlist Now</a>
									</li>
									<?php }?>

									<div class="shorlist-data-cont" style="display:none;"  id="shortlistarea<?=$trending['id']?>">
										<span class="close-shortlist close-popup"></span>
										<div class="shorlist-heading">
											<h4>Shortlist &amp; save this listing</h4>
											<p>Organize your selection into folders for future references</p>
											<div id="folder-list<?=$trending['id']?>">
												<?php
												if($folderlist)
												{
													foreach($folderlist as $_folderlist)
													{
														echo '<br><input type="checkbox" name="folder" value="'.$_folderlist['id'].'">'.$_folderlist['foldername'];
													}//end of foreach
												}
												?>
											</div>

										</div>
										<form class="id"  id="Frmshortlist">
										<div class="create-folder">


										<p>Create a new folder</p>
										<span class="conatcterr" id="shortfoldererr<?=$trending['id']?>" style="display:none;">Please Enter FolderName</span>

										<input type="text" name="shortfoldername" id="shortfoldername<?=$trending['id']?>" placeholder="e.g Apple Activation Delhi" class="input-create-fld">
										<!--button name="" class="add-fld-btn"><i class="fa fa-folder-open-o" aria-hidden="true"></i> Add</button-->
										<button class="add-fld-btn" id="" onclick="shortlist_add_folder(<?=$trending['id']?>); return false;">
										<i class="fa fa-folder-open-o" aria-hidden="true"></i> Add
										</button>
										</div>
										<button class="shorlist-btn save" id="" onclick="shortlistevent(<?=$trending['id']?>); return false;">Save</button>
										<button class="shorlist-btn cancel" onclick="closepopshortlist(<?=$trending['id']?>); return false;">Cancel</button>
										</form>
									</div>
									

									<li><a href="<?= Url::home() ?>theme/<?=$themename?>?id=<?=$trending['id']?>">More Info</a></li>
            </ul>
          </div>
        </div>
      </div>
     <?php endforeach;?> 
      <div class="load-btn-cont">
        <a href="<?= Url::to(['events/trending']) ; ?>" class="load-button">
          LOAD MORE
          <span class="load-btn-icons load"></span>
        </a>
      </div>
    </div>
  </div>
</section>






<script type="text/javascript">
	jQuery(window).load(function() {
		if(jQuery().slick)
		{
			jQuery('.curated-slider').slick({
				infinite: true,
				slidesToShow: 5,
				slidesToScroll: 1,
				// variableWidth: true,
				autoplay: false,
				autoplaySpeed: 2000,
				dots: false,
				arrows: true,
				prevArrow: '<div class="pc-prev"><i class="fa fa-angle-left"></i></div>',
				nextArrow: '<div class="pc-next"><i class="fa fa-angle-right"></i></div>'
			});
		}
	});

function contactnow(id){
	var name = $('#name'+id).val();
	var phone = $('#phone'+id).val();
	var text = $('#text'+id).val();
	if(!name){ $('.conatcterr').hide(); $('#nameerr'+id).show(); return false;}
	if(!phone){$('.conatcterr').hide(); $('#phoneerr'+id).show(); return false;}
	if (isNaN(phone)) { $('.conatcterr').hide(); $('#validphoneerr'+id).show(); return false;}
	if(!text){$('.conatcterr').hide(); $('#texterr'+id).show(); return false;}
	else{
		$('.page-loader').show();
		var data = $("#contactnow"+id).serialize();
		data +='&id='+id;
		$.ajax({
			url : '<?= Url::to(['eventcontact']) ; ?>',
			type: 'POST',
			data: data,
			success: function(result)
			{
				if(result=='success'){$('.page-loader').hide();$("#contactarea"+id).hide(500); $('#nameerr'+id).hide();$('#phoneerr'+id).hide();$('#texterr'+id).hide(); alert('Message Sent');}
				else{alert('Something is Wrong');}
			}
		});
	}
}
</script>

<script>
function shortlist_add_folder(eid)
{
	var data;
	var name = $('#shortfoldername'+eid).val();
	if(!name)
	{
		$('#shortfoldererr'+eid).show(); return false;
	}else{
		data ='flodername='+name;
		$.ajax({
			url : '<?= Url::to(['shortlistfolder']) ; ?>',
			type: 'POST',
			data: data,
			dataType:'html',
			success: function(result)
			{
				if(result){
					//alert('Saved the Folder sucessfully');
					//alert(result);
					//document.getElementById("folder-list").innerHTML(result);

					$('#folder-list'+eid).html(result);

					$('#shortfoldererr'+id).hide();
				}else if(result=='error'){
					alert('Something is Wrong');

				}
			}
		});
	}//end of !name else



}

</script>

<script>
function shortlistevent(eid)
{
	var checkboxValues = [];
	$('input[name=folder]:checked').map(function() {
			checkboxValues.push($(this).val());
	});
	//console.log( checkboxValues);alert(checkboxValues.length);
	if(checkboxValues.length >=1)
	{
            //save to database
				data ='checkboxValues='+checkboxValues+'&eventid='+eid;
			$.ajax({
				url : '<?= Url::to(['shortlistevent']) ; ?>',
				type: 'POST',
				data: data,
				success: function(result)
				{
					if(result=='success'){alert('ShortList the event sucessfully');

                        location.reload();
						$('#shortfoldererr'+eid).hide();
						$("#shortlistarea"+eid).hide(500);
					}else{
						alert('Something is Wrong');
					}
				}
			});

            //save to db
	}else{
		alert("Select any folder name to organize your selection");
	}

}




</script>

<script type="text/javascript">
	$(function ()
	{
		var filterList = {
			init: function () {
				// MixItUp plugin
				// http://mixitup.io
				$('#portfoliolist').mixItUp({
  				selectors: {
    			  target: '.portfolio',
    			  filter: '.filter'
    		  },
    		  load: {
      		  filter: '.app'
				}
				});
			}
		};

		// Run the show!
		filterList.init();

		$('.filter-caption').on('click', function(e) {
			e.preventDefault();

			$('.filter-toggle').removeClass('active');
			$('.filter-data').hide(0);
			
			var $this = $(this);

			$this.parent('.filter-toggle').addClass('active');
			$this.next('.filter-data').show(250);
		});
		
		$('.filter-close').on('click', function(e) {
			e.preventDefault();

			var $this = $(this);

			$this.closest('.filter-toggle').removeClass('active');
			$this.parent('.filter-data').hide(250);
		});

		









		// contact 	
		$(document).on('click', '.contact-now', function(e) {
			$(this).siblings(".contact-data-cont").show(500);
			$(this).siblings('li').next(".shorlist").addClass("active");
			e.stopPropagation();
		});

		$(document).on('click', '.close-popup', function(e) {
			$(this).parent(".contact-data-cont").hide(500);
			$(this).closest('li').next(".shorlist").removeClass("active");
			e.stopPropagation();
		});
	
	// shorlist 	
		$(document).on('click', '.shorlist', function(f) {
			$(this).siblings(".shorlist-data-cont").show(500);
			$(this).siblings('li').next(".shorlist").addClass("active");
			f.stopPropagation();
		});

		$(document).on('click', '.close-popup', function(f) {
			$(this).parent(".shorlist-data-cont").hide(500);
			$(this).closest('li').next(".shorlist").removeClass("active");
			f.stopPropagation();
		});
	});
	
	function closepopcontact(id)
	{
		$("#contactarea"+id).hide(500);
		$("#contactarea"+id).closest('li').next(".shorlist").removeClass("active");
	}
	
	function closepopshortlist(id)
	{
		$("#shortlistarea"+id).hide(500);
		$("#shortlistarea"+id).closest('li').next(".shorlist").removeClass("active");
	}
	
</script>
<script>
  $('input[name=sortby]').change(function(){
     //alert($(this).val());
    $("#eventForm").submit();
   });
    
 </script>
