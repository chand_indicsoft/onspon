
<?php $this->beginContent('@app/views/layouts/eventbase.php'); 

use yii\helpers\Url;

$asset = \app\assets\EventAppAsset::register($this);

$session = Yii::$app -> session;
?>

<header>
  <a href="/onspon/" class="top-logo"></a>
  <div class="top-menu-bar">
    <div class="hamburger-menu toggle-x">
      <span class="arrows"></span>
      <nav id="menu">
       <ul>
       <li><a href="/onspon/">Home</a></li>
       <li><a href="#">About</a></li>
       <li><a href="#">Contact</a></li>    
       <li><a href="#">Blog</a></li>
       <?php if(!$session['user_id']){?>
			<li><a href="<?= Url::home() ?>site/login">Login</a></li>
		<?php }	else { ?>
			<li><a href="<?= Url::home() ?>site/logout">Logout</a></li>
		<?php }?>
    </ul>
 </nav> 
    </div>
    <ul>
	  <?php if(!$session['user_id']){?>
			<li><a href="<?= Url::home() ?>site/login"><span class="menu-icon user"></span>Login</a></li>
		<?php }	else { ?>
			<li><a href="<?= Url::home() ?>site/logout"><span class="menu-icon user"></span>Logout</a></li>
		<?php }?>
      <li>
        <a>
          <span class="menu-icon phone"></span>
          + 91 7666003344              
        </a>
      </li>
    </ul>
  </div>
</header>

		<?php echo $content; ?>
		
<footer>
<div class="container">
<div class="row">
<div class="col-sm-3">
<div class="footer-one">
<a href="#"><img src="<?= $asset->baseUrl ?>/images/footer-logo.png"></a>
<ul class="list-unstyled">
<li><i class="fa fa-map-marker" aria-hidden="true"></i> 2901 lourem ipusm, lourem
Mumbai-  402530 </li>
<li><i class="fa fa-phone" aria-hidden="true"></i>+91-7666003344 </li>
<li><i class="fa fa-envelope" aria-hidden="true"></i>info@onspon.com </li>
</ul>
</div>
</div>

<div class="col-sm-2 link">
<span>About Onspon</span>
<ul class="list-unstyled">
<li><a href="#">  Home</a></li>
<li><a href="#">About Us</a></li>
<li><a href="#"> Sign Up</a></li>
<li><a href="#"> Sign In</a></li>
<li><a href="#"> Management Team</a></li>
<li><a href="#"> Our Services</a></li>
</ul>
</div>



<div class="col-sm-2 link">
<span>Why Onspon</span>
<ul class="list-unstyled">
<li><a href="#">  How it Works</a></li>
<li><a href="#">Top Avenues</a></li>
<li><a href="#">Clientele</a></li>
<li><a href="#">Testimonials</a></li>
<li><a href="#">FAQs</a></li>
<li><a href="#">  Careers</a></li>
</ul>
</div>


<div class="col-sm-2 link">
<span>More Links </span>
<ul class="list-unstyled">
<li><a href="#">  Sponsors</a></li>
<li><a href="#"> Event Categories</a></li>
<li><a href="#">Oganizers</a></li>
<li><a href="#">Partner with Us</a></li>
<li><a href="#">Managment Team</a></li>
<li><a href="#"> Contact Us</a></li>
</ul>
</div>


<div class="col-sm-2 link">
<span>Twitter Feed</span>
<ul class="list-unstyled">
<li><a href="#"> <i class="fa fa-twitter" aria-hidden="true"></i>  <span class="red">#Arijitsingh</span> live in concert <span class="red">https://t.co/6zS8FeG451</span></a></li>
<li><a href="#"> <i class="fa fa-twitter" aria-hidden="true"></i>  <span class="red">#Arijitsingh</span> live in concert <span class="red">https://t.co/6zS8FeG451</span></a></li>
<li><a href="#"> <i class="fa fa-twitter" aria-hidden="true"></i>  <span class="red">#Arijitsingh</span> live in concert <span class="red">https://t.co/6zS8FeG451</span></a></li>
</ul>
</div>



</div>
</div>

</footer>
<div class="copyright">
<div class="container">
<div class="row">

<div class="copy-left">Copyright © 2016 www.onspon.com. All rights reserved. <a href="#">Terms of Use</a>   |  <a href="#"> Privacy Policy</a> </div>
<div class="copy-right">
<ul class="list-inline">
<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
<li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
</ul>
</div>

</div>
</div>

</div>		
	

<?php $this->endContent(); ?>

<script type="text/javascript">

		$(".toggle-x").click(function(){
			  $("#menu").toggleClass("active");
			
		});  
	</script> 

<script type="text/javascript">

$(document).ready(function(){
$('body').addClass('crtevent-header');

});		
</script> 



<script>
$(document).ready(function() {
    var max_fields      = 20; //maximum input boxes allowed
    var wrapper         = $("#addnewfeild"); //Fields wrapper
    var add_button      = $("#add_field_button"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div id="newfieldid"><div class="form-group"><label for="usr">Slab Title<span class="mandatory">*</span><span class="ex_Txtfeild">eg. Primary Sponsor, Presenting Sponsor, Platinum Sponsor etc</span></label><input class="form-control" name="slabname[]" placeholder="" type="text"></div><div class="form-group"> <label for="usr">Price<span class="mandatory">*</span><span class="ex_Txtfeild">Kindly mention the amount sponsor has to pay to take this slot</span></label><input class="form-control" name="sponsorprice[]" placeholder="" type="text"></div><div class="form-group"><label for="usr">Deliverables<span class="mandatory">*</span></label> <textarea class="form-control" rows ="5" name="deliverables[]"></textarea></div><br><a href="#" id="remove_field" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btnContnr"><i class="glyphicon glyphicon-remove-sign"></i></a></div>'); //add input box
        }
    });
    
    $(wrapper).on("click","#remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});
</script>








 
