﻿<?php
	use yii\easyii\modules\subscribe\api\Subscribe;
	use yii\helpers\Url;
	use yii\widgets\Breadcrumbs;
	use yii\widgets\Menu;

	$asset = \app\assets\AppAsset::register($this);
	$session = Yii::$app -> session;
	$this->beginContent('@app/views/layouts/base.php');

	$this->registerJs('$("#about").click(function(){
		$("#about-sub").slideToggle();
		$("#menu ul a#about span").toggleClass("open");
	});
	$("#customer-says").click(function(){
		$("#customer-says-sub").slideToggle();
		$("#menu ul a#customer-says span").toggleClass("open");
	});

	$("#legal").click(function(){
		$("#legal-sub").slideToggle();
		$("#menu ul a#legal span").toggleClass("open");
	});
	$(".hamburger-menu.toggle-x").on("click", function(){
		$(".hamburger-menu.toggle-x").toggleClass("current");
	});
	$(".toggle-x").click(function(){
		$("#menu").toggleClass("active");
	});

	$(document).ready(function () {
		$("#nav > li > a").click(function(){
			if ($(this).attr("class") != "active"){
				$("#nav li ul").slideUp();
				$(this).next().slideToggle();
				$("#nav li a").removeClass("active");
				$(this).addClass("active");
			}
		});
	});');
?>
<header>
	<a href="<?= Url::home() ?>" class="top-logo"></a>
	<div class="top-menu-bar">
		<div class="hamburger-menu toggle-x">
			<span class="arrows"></span>
		</div>
		<nav id="menu">
			<ul>
				<li><a href="<?= Url::home() ?>" class="active">Home</a></li>
				<li>
					<a href="javascript:void(0);" id="about">About <span class="pull-right"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></a>
					<ul id="about-sub">
						<li><a href="<?php echo Url::to(['/site/aboutus/'], true);?>">About Onspon</a></li>
						<li><a href="<?php echo Url::to(['/site/howitworks/'], true);?>">How it work</a></li>
						<li><a href="<?php echo Url::to(['/site/faq/'], true);?>">FAQs</a></li>
					
					</ul>
				</li>
				<li>
					<a href="javascript:void(0);" id="customer-says">Customer say us <span class="pull-right"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></a>
					<ul id="customer-says-sub">
						<li><a href="<?php echo Url::to(['/site/clientele/'], true);?>">Clientele</a></li>
						<li><a href="<?php echo Url::to(['/site/testimonial/'], true);?>">testimonials</a></li>
					    <li><a href="javascript:void(0);">Sponsonrs</a></li>
					</ul>
				</li>
				<li>
					<a href="javascript:void(0);" id="legal">Legal<span class="pull-right"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></a>
					<ul id="legal-sub">
						<li><a href="<?php echo Url::to(['/site/terms/'], true);?>">Terms of use</a></li>
						<li><a href="javascript:void(0);">Privacy policy</a></li>
						<li><a href="<?php echo Url::to(['/site/contact/'], true);?>">contact us</a></li>
					
					</ul>
				</li>
			</ul>
		</nav>

		<ul>
			<?php
			if(!$session['user_id'])
			{
				?><li><a href="<?= Url::to(['site/login']) ?>"><span class="menu-icon user"></span>Login</a></li><?php
			}
			else
			{
				?><li><a href="<?= Url::to(['site/logout']) ?>"><span class="menu-icon user"></span>Logout</a></li><?php
			}
			?><li><a><span class="menu-icon phone"></span> + 91 7666003344</a></li>
			<li class="top-menu-seprate-btn"><a href="<?= Url::to(['/eventcreate/create']) ; ?>">List Your Event</a></li>
			<li></li>
		</ul>
	</div>
</header>

<?= $content ?>

<span class="page-loader" style="display:none;"></span>

<footer>
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
				<div class="footer-one">
					<a href="<?= Url::home() ?>"><img src="<?= $asset->baseUrl ?>/images/footer-logo.png"></a>
					<ul class="list-unstyled">
						<li><i class="fa fa-map-marker" aria-hidden="true"></i> 2901 lourem ipusm, lourem Mumbai- 402530</li>
						<li><i class="fa fa-phone" aria-hidden="true"></i>+91-7666003344</li>
						<li><i class="fa fa-envelope" aria-hidden="true"></i>info@onspon.com</li>
					</ul>
				</div>
			</div>

			<div class="col-sm-2 link">
				<span>About Onspon</span>
				<ul class="list-unstyled">
					<li><a href="<?= Url::home() ?>">Home</a></li>
					<li><a href="<?php echo Url::to(['/site/aboutus/'], true);?>">About Us</a></li>
					<li><a href="<?php echo Url::to(['/site/signup/'], true);?>">Sign Up</a></li>
					<li><a href="<?php echo Url::to(['/site/login/'], true);?>">Sign In</a></li>
					<li><a href="javascript:void(0);">Management Team</a></li>
					<li><a href="javascript:void(0);">Our Services</a></li>
				</ul>
			</div>

			<div class="col-sm-2 link">
				<span>Why Onspon</span>
				<ul class="list-unstyled">
					<li><a href="<?php echo Url::to(['/site/howitworks/'], true);?>">How it Works</a></li>
					<li><a href="javascript:void(0);">Top Avenues</a></li>
					<li><a href="<?php echo Url::to(['/site/clientele/'], true);?>">Clientele</a></li>
					<li><a href="<?php echo Url::to(['/site/testimonial/'], true);?>">Testimonials</a></li>
					<li><a href="<?php echo Url::to(['/site/faq/'], true);?>">FAQs</a></li>
					<li><a href="javascript:void(0);">Careers</a></li>
				</ul>
			</div>

			<div class="col-sm-2 link">
				<span>More Links</span>
				<ul class="list-unstyled">
					<li><a href="javascript:void(0);">Sponsors</a></li>
					<li><a href="javascript:void(0);">Event Categories</a></li>
					<li><a href="javascript:void(0);">Oganizers</a></li>
					<li><a href="javascript:void(0);">Partner with Us</a></li>
					<li><a href="javascript:void(0);">Managment Team</a></li>
					<li><a href="<?php echo Url::to(['/site/contact/'], true);?>">Contact Us</a></li>
				</ul>
			</div>

			<div class="col-sm-3 link">
				<span>Twitter Feed</span>
				<ul class="list-unstyled">
					<li><a href="javascript:void(0);"> <i class="fa fa-twitter" aria-hidden="true"></i> <span class="red">#Arijitsingh</span> live in concert <span class="red">https://t.co/6zS8FeG451</span></a></li>
					<li><a href="javascript:void(0);"> <i class="fa fa-twitter" aria-hidden="true"></i> <span class="red">#Arijitsingh</span> live in concert <span class="red">https://t.co/6zS8FeG451</span></a></li>
					<li><a href="javascript:void(0);"> <i class="fa fa-twitter" aria-hidden="true"></i> <span class="red">#Arijitsingh</span> live in concert <span class="red">https://t.co/6zS8FeG451</span></a></li>
				</ul>
			</div>
		</div>
	</div>
</footer>

<div class="copyright">
	<div class="container">
		<div class="row">
			<div class="copy-left">Copyright &copy; 2016 www.onspon.com. All rights reserved. <a href="javascript:void(0);">Terms of Use</a> | <a href="javascript:void(0);"> Privacy Policy</a></div>
			<div class="copy-right">
				<ul class="list-inline">
					<li><a href="javascript:void(0);"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a href="javascript:void(0);"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					<li><a href="javascript:void(0);"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
					<li><a href="javascript:void(0);"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">

</script>
<?php $this->endContent(); ?>
