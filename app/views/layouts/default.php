
<?php $this->beginContent('@app/views/layouts/basedefault.php'); 

use yii\helpers\Url;

$asset = \app\assets\DefaultAppAsset::register($this);

$session = Yii::$app -> session;
?>


		
	


		<?php echo $content; ?>

<footer id="contact">
	<div class="container">
		<div class="col-lg-6 col-md-6">
			<div class="frt-section-cont">
				<h2 class="ftr-sec-heading">Contact Info</h2>
				<p class="ftr-abt-site">
					Ut volutpat consectetur aliquam. Curabitur auctor in nis ulum ornare. Sed consequat, augue condimentum fermentum gravida, metus elit vehicula dui.
				</p>
				<ul>
					<li>
						<i class="fa fa-map-marker"></i>
						123 / 5, lorem ipsum sit amet, New Delhi, India (110006)
					</li>
					<li>
						<i class="fa fa-mobile"></i>
						+91 9876543210, +91 1234567890
					</li>
					<li>
						<i class="fa fa-envelope-o"></i>
						info@example.com
					</li>
				</ul>
			</div>
		</div>
		<div class="col-lg-6 col-md-6">
			<div class="frt-section-cont">
				<h2 class="ftr-sec-heading">Send Us a Message</h2>
				<form>
					<input type="text" name="" id="" class="form-input" placeholder="Your name...">
					<input type="text" name="" id="" class="form-input" placeholder="Your number...">
					<input type="text" name="" id="" class="form-input" placeholder="Your email...">
					<input type="text" name="" id="" class="form-input" placeholder="Your subject...">
					<textarea cols="" rows="5" id="" class="form-textarea" placeholder="Your message..."></textarea>
					<button class="form-btn">Submit Your Message</button>
				</form>
				
			</div>
		</div>
	</div>
	<div class="copyriht-container">
		<div class="container">
			<p class="copyright-text left">
				&copy; 2016, All Rights Reserved 
			</p>
			<span class="pwrd-by right">
				Powered By : 
				<a href="#" class="ftr-onspon-logo"></a>
			</span>
		</div>
	</div>
</footer>
	

<?php $this->endContent(); ?>


<script type="text/javascript">
	jQuery(window).load(function() {
		if(jQuery().slick)
		{
	
	
	// Curated Event Slider
    $('.home-slider').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      // variableWidth: true,
      autoplay: true,
      autoplaySpeed: 4000,
      dots: false,
      arrows: true,
      prevArrow: '<div class="pc-prev"><i class="fa fa-angle-left"></i></div>',
      nextArrow: '<div class="pc-next"><i class="fa fa-angle-right"></i></div>'
    });

    $('.upcmng-evnt-slider').slick({
      infinite: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      // variableWidth: true,
      autoplay: true,
      autoplaySpeed: 4000,
      dots: false,
      arrows: true,
      prevArrow: '<div class="pc-prev"><i class="fa fa-angle-left"></i></div>',
      nextArrow: '<div class="pc-next"><i class="fa fa-angle-right"></i></div>'
    });
    $('.pplr-slider').slick({
      infinite: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      // variableWidth: true,
      autoplay: true,
      autoplaySpeed: 4000,
      dots: false,
      arrows: true,
      prevArrow: '<div class="pc-prev"><i class="fa fa-angle-left"></i></div>',
      nextArrow: '<div class="pc-next"><i class="fa fa-angle-right"></i></div>'
    });
    $('.all-evnt-slider').slick({
      infinite: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      // variableWidth: true,
      autoplay: true,
      autoplaySpeed: 4000,
      dots: false,
      arrows: true,
      prevArrow: '<div class="pc-prev"><i class="fa fa-angle-left"></i></div>',
      nextArrow: '<div class="pc-next"><i class="fa fa-angle-right"></i></div>'
    });
    
    
    
}
	});

	// sticky header
	$(window).scroll(function() {
	  if ($(this).scrollTop() > 100){
	      $('header').addClass("sticky");
	    }
	    else{
	      $('header').removeClass("sticky");
	    }

	    $('[data-scroll]').each(function(index, element) {
	    	if($($(this).data('scroll')).offset().top >= $(window).scrollTop())
	    	{
	    		$('[data-scroll]').removeClass('active');
	    		$(this).addClass('active');
	    		return false;
	    	}
	    })
	});

	// Scrolling
    $(document).ready(function () {
	    // scroll positioning
	    $(document).on('click', '[data-scroll]', function() {
	    	$('[data-scroll]').removeClass('active');
	    	$(this).addClass('active');
	        var a = $($(this).attr('data-scroll')).position();
	        a = a.top;
	        $('html, body').animate({scrollTop: a-40}, 600);
	    });
	});
	
	
	
	
</script>

