<?php
use yii\easyii\modules\page\api\Page;
use yii\easyii\modules\text\api\Text;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\SubscribeForm;
use yii\bootstrap\ActiveForm;
use app\models\Common;

$asset = \app\assets\AppAsset::register($this);
$page = Page::get('page-index');
$this->title = $page->seo('title', $page->model->title);
$session = Yii::$app -> session;
$modelCommon = new Common;
?>
<section class="section-banner-cont section-banner home-banner">

  <div class="section-img">
    <video width="100%" height="inherit" loop autoplay muted class="video">
      <source src="http://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4">
      <source src="http://www.w3schools.com/html/mov_bbb.ogg" type="video/ogg">
    </video>
  </div>

  <div class="section-caption-cont">
    <div class="container">
      <div class="section-caption">
        <h1 class="section-lead-text">
         India’s Biggest <span class="red_font list-inline">Sponsorship Marketplace</span>
        </h1>
        <h3 class="section-sub-lead-text">
          List your event and get visibility for various brands
        </h3>
        <ul class="section-buttons home">
          <li><a href="<?= Url::to(['/eventcreate/create']) ; ?>" class="active">List an Event</a></li>
          <li><a href="<?= Url::to(['sponsor/index']) ; ?>">I am a Sponsor</a></li>
         
        </ul>
      </div>
      <div class="searchfield">
      <ul class="nav nav-tabs banner-form">
			<li class="active"><a data-toggle="tab" href="#organizer">Sponsors</a></li>
			<li><a data-toggle="tab" href="#sponsor">Events</a></li>
		</ul>

		<div class="tab-content" id="avik">
			<div id="organizer" class="tab-pane fade in active">
				<form class="form-inline" role="form">
                
					<div class="input-group">
						<div class="input-group-addon"> <span class="glyphicon glyphicon-map-marker"></span></div>

						<input class="form-control" placeholder="City" aria-describedby="basic-addon1" type="text"/>
</div>
					  <div class="input-group">

                    <select id="lunch" class="selectpicker form-control" data-live-search="true" title="Please select a lunch ...">
                        <option>Select one</option>
                        <option>Select one</option>
                        <option>Select one</option>
                        <option>Select one</option>
                       </select>
                </div>
 <div class="input-group">
						<div class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span></div>

						<input class="form-control border-right" id="date" name="date" placeholder="From Date" type="text"/>

						<input type="hidden" name="search_param" value="name" id=""/>

						<div class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span></div>

						<input class="form-control" id="date" name="date1" placeholder=" To Date" type="text"/>

						<input type="hidden" name="search_param" value="name" id=""/>

					</div>

					<div class="input-group search-last">
						<div class="input-group-addon"> <span class="glyphicon glyphicon-search"></span> </div>

						<input type="text" class=""  placeholder="Type Event Name"/>

						<input type="submit" name="search" value="Go"/>
					</div>
				</form>

				<div class="event-sponsor">Search <span>sponsors</span></div>
</div>

<div id="sponsor" class="tab-pane fade">
				<form class="form-inline" role="form">
                
					<div class="input-group">
						<div class="input-group-addon"> <span class="glyphicon glyphicon-map-marker"></span></div>

						<input class="form-control" placeholder="City" aria-describedby="basic-addon1" type="text"/>
</div>
					  <div class="input-group">

                    <select id="lunch" class="selectpicker form-control" data-live-search="true" title="Please select a lunch ...">
                        <option>Select one</option>
                        <option>Select one</option>
                        <option>Select one</option>
                        <option>Select one</option>
                       </select>
                </div>
 <div class="input-group">
						<div class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span></div>

						<input class="form-control border-right" id="date" name="date" placeholder="From Date" type="text"/>

						<input type="hidden" name="search_param" value="name" id=""/>

						<div class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span></div>

						<input class="form-control" id="date" name="date1" placeholder=" To Date" type="text"/>

						<input type="hidden" name="search_param" value="name" id=""/>

					</div>

					<div class="input-group search-last">
						<div class="input-group-addon"> <span class="glyphicon glyphicon-search"></span> </div>

						<input type="text" class=""  placeholder="Type Event Name"/>

						<input type="submit" name="search" value="Go"/>
					</div>
				</form>
                <div class="event-sponsor">Search <span>Event</span></div>
</div>

			</div>

			
				
			</div>


</section>













<div class="clearfix"></div>
<!--banner end here-->

<div id="wrapper">
	<div class="container">
		<div class="row">
			<div class="platform">
				
                
                <div class="section-heading">
                <h1>Some Stats About <span class="red_font">Our Platform</span></h1>
              </div>
                
			</div>

			<div class="clearfix"></div>

			<div class="set-size charts-container">
				<div class="pie-wrapper progress-45 style-2"> <span class="label">98,83,350 </span>
					<div class="pie">
						<div class="left-side half-circle"></div>
						<div class="right-side half-circle"></div>
					</div>
					<div class="shadow"></div>
					<div class="sponsor red"><a href="javascript:void(0)">Sponsored</a></div>
				</div>

				<div class="pie-wrapper progress-75 style-2"> <span class="label">25,385</span>
					<div class="pie">
						<div class="left-side half-circle"></div>
						<div class="right-side half-circle"></div>
					</div>
					<div class="shadow"></div>
					<div class="sponsor black"><a href="javascript:void(0)">Sponsors</a></div>
				</div>

				<div class="pie-wrapper progress-95 style-2"> <span class="label">4,83,350 </span>
					<div class="pie">
						<div class="left-side half-circle"></div>
						<div class="right-side half-circle"></div>
					</div>
					<div class="shadow"></div>
					<div class="sponsor red"><a href="javascript:void(0)">Organizers</a></div>
				</div>

				<div class="pie-wrapper progress-45 style-2"> <span class="label">15 Mins</span>
					<div class="pie">
						<div class="left-side half-circle"></div>
						<div class="right-side half-circle"></div>
					</div>
					<div class="shadow"></div>
					<div class="sponsor black"><a href="javascript:void(0)"> Spent Time</a></div>
				</div>

				<div class="pie-wrapper progress-75 style-2"> <span class="label">382</span>
					<div class="pie">
						<div class="left-side half-circle"></div>
						<div class="right-side half-circle"></div>
					</div>
					<div class="shadow"></div>
					<div class="sponsor red"><a href="javascript:void(0)">Cities Covered</a></div>
				</div>

				<div class="pie-wrapper progress-95 style-2"> <span class="label">115</span>
					<div class="pie">
						<div class="left-side half-circle"></div>
						<div class="right-side half-circle"></div>
					</div>
					<div class="shadow"></div>
					<div class="sponsor black"><a href="javascript:void(0)">Event Categories</a></div>
				</div>
			</div>
		</div>
	</div>
	<!--container end here-->
	<?php
	//$eventcategoryList=array('app'=>'Social Cause','logo'=>'Adventure','card'=>'Business','icon'=>'Music','food'=>'Food','mobile'=>'MobileApps');
	?>
	<div id="event">
	      <div class="section-heading">
                <h1> Events</h1>
              </div>
		<br>
		<div class="container">
			<ul id="filters" class="clearfix">
				<?php 
					foreach($eventcategoryList as  $_eventcat)
						{
				?>
						<li><span class="filter active" data-filter=".<?=$_eventcat->id?>abc" onclick="getEvent_View(<?=$_eventcat->id?>)"><?=$_eventcat->title?> </span></li>
				<?php
						}
				?>
			</ul>

			<div id="portfoliolist">
				<?php
				foreach($eventcategoryList as  $_event_cat)
					{
						$gallerydetail=$modelCommon->getEventCategoryImg( $_event_cat->id);
						//print_r($gallerydetail);
						if($gallerydetail)
							{
								foreach($gallerydetail as $_gallerydetail)
									{
										$themename=($modelCommon->getEventtheme($_event_cat->id));
						?>
										<div class="portfolio <?=$_gallerydetail['event_category_id']?>abc width20" data-cat="<?=$_gallerydetail['event_category_id']?>abc" id="xyz">
											<div class="portfolio-wrapper"> 
											<a class="text-title" target="_blank" href="<?= Url::home() ?>theme/<?=$themename?>?id=<?=$_gallerydetail['id']?>">
												<?php if($_gallerydetail['image'])
														{
												?>
															<img src="<?=Yii::$app->homeUrl.$_gallerydetail['image']?>" alt=""  width="210" height="200"/>
												<?php 
														}
													else
														{
															$defimg=$asset->baseUrl ."/images/comingsoon1.png";?>
															<img src="<?=$defimg?>" alt="" />
												<?php
														}
												?>
												<div class="label">
													<div class="label-text"> 					
														<?=$_gallerydetail['event_name']?>
														<span class="text-category">October 22, 2015<?=$themename?></span>
													</div>
													<div class="label-bg"></div>
												</div>
												</a>
											</div>
										</div>
										<?php 
									}
							}//end of gallerydetails  ?>
						<?php 
					}//end of foreach
						?>  
						
							<!--BOX--->			
		<div class="portfolio box width20" data-cat="box" id="box">
          <div class="portfolio-wrapper"> <img src="<?= $asset->baseUrl ?>/images/g1.jpg" alt="" />
            <div class="label">
              <div class="label-text"> <a class="text-title">Greenpeace Foundation</a> <span class="text-category">October 22, 2015</span> </div>
              <div class="label-bg"></div>
            </div>
          </div></div>
          <div class="portfolio box width20" data-cat="box" id="box">
          <div class="portfolio-wrapper"> <img src="<?= $asset->baseUrl ?>/images/g1.jpg" alt="" />
            <div class="label">
              <div class="label-text"> <a class="text-title">Greenpeace Foundation</a> <span class="text-category">October 22, 2015</span> </div>
              <div class="label-bg"></div>
            </div>
          </div></div>
          
          
             <div class="portfolio box width20" data-cat="box" id="box">
          <div class="portfolio-wrapper"> <img src="<?= $asset->baseUrl ?>/images/g1.jpg" alt="" />
            <div class="label">
              <div class="label-text"> <a class="text-title">Greenpeace Foundation</a> <span class="text-category">October 22, 2015</span> </div>
              <div class="label-bg"></div>
            </div>
          </div></div>
          
             <div class="portfolio box width20" data-cat="box" id="box">
          <div class="portfolio-wrapper"> <img src="<?= $asset->baseUrl ?>/images/g1.jpg" alt="" />
            <div class="label">
              <div class="label-text"> <a class="text-title">Greenpeace Foundation</a> <span class="text-category">October 22, 2015</span> </div>
              <div class="label-bg"></div>
            </div>
          </div></div>
          
          
          
          
          
          
          
			<!--BOX--->		
						
			</div>
			<!--end of portfoliolist-->
			<div class="button"> <a href="events/eventlisting" class="buttonevent">View All</a></div>
			<!-- event end here--> 
			
		</div>
	</div> <!-- Event end here -->

	<div class="clearfix"></div>

	<div id="how-work">
		<span class="timeline-overlay"></span>

		<div class="container">
			<div class="row"> 
				  <div class="section-heading">
                <h1>How it<span class="red_font"> Works</span></h1>
              </div>
                

				<div id="timeline">
					<ul>
						<li><div class="timeline-icon timeline-fisrt"></div>
							<div class="timeline-heading">Need Sponsorship ? Create Your Listing</div>
							<p>The first step is to create your event listing on onspon.com which gets you visible to multiple sponsors & audience.
							You can also opt for premium listing packages for higher visibility and support.</p>
						</li>
							
						<li><div class="timeline-icon timeline-second"></div>
							<div class="timeline-heading">Need Sponsorship ? Create Your Listing</div>
							<p>The first step is to create your event listing on onspon.com which gets you visible to multiple sponsors & audience.
								You can also opt for premium listing packages for higher visibility and support.</p>
						</li>

						<li><div class="timeline-icon timeline-third"></div>
							<div class="timeline-heading">Need Sponsorship ? Create Your Listing</div>
							<p>The first step is to create your event listing on onspon.com which gets you visible to multiple sponsors & audience.
							You can also opt for premium listing packages for higher visibility and support.</p>
						</li>
							   
						<li><div class="timeline-icon timeline-fourth"></div>
							<div class="timeline-heading">Need Sponsorship ? Create Your Listing</div>
							<p>The first step is to create your event listing on onspon.com which gets you visible to multiple sponsors & audience.
							You can also opt for premium listing packages for higher visibility and support.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div> <!--how it work end here-->

	<div id="about-onspon">
		
        <div class="section-heading">
        <h1>
          Some Stats About
          <span class="red_font"><img src="<?= $asset->baseUrl ?>/images/inverse-logo.jpg"></span>
        </h1>
      </div>

		<div class="container">      
			<div id="onspon" class="carousel slide" data-ride="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators"><?php $i=0;
					foreach($eventTestimonials as $_eventTestimonials)
						{
							if($i==0)  $class_1="active";
							?>
							<li data-target="#onspon" data-slide-to="<?=$i?>" class="<?=$class_1?>"></li>
							<?php 
							$i++;
						}	?>
				</ol>

				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
					<?php $k=1;
					foreach($eventTestimonials as $_eventTestimonials)
						{
							if($k==1) { $class_3="active"; }
							else { $class_3=""; }
							?>
							<div class="item <?=$class_3?>">
								<div class="col-md-7">
									<h3>Daikin India Annual Growth<span> Meet 2015-16</span></h3>
									<img src="<?= $asset->baseUrl ?>/images/stats.jpg"/>
                                    
								</div>

								<div class="col-md-5 roshni text-center">
									<img src="<?= $asset->baseUrl ?>/images/roshni.jpg">
									<div class="title"><?=$_eventTestimonials->title?><span><?=$_eventTestimonials->designation?></span></div>
									<p><?=$_eventTestimonials->text?></p>
								</div>
							</div>
                            
							<?php $k++; 
						}	?>
				</div>
			</div>
		</div>
	</div> 

	<div class="sponsors">
		<div class="sponsor-left">
			<ul class="sponsor-list">
				<?php 
				$featuredbrand=$modelCommon->getFeaturedBrands();
				$e=1;   
				foreach($featuredbrand as $_featuredbrand)
					{
						if($e==1) $class="text-left";
						if($e==2) $class="text-center";
						if($e==3) $class="text-right";
						?>
						<li class="text-left"><a href="javascript:void(0)"><img src="<?= Yii::$app->homeUrl.$_featuredbrand['featuredlogo'] ?>"></a></li>
						<?php 
						$e++;
							if($e==3) $e=1;
					}  ?>
			</ul>
		</div>

		<div class="sponsor-right">
	
            <div class="section-heading">
        <h1>
         What Sponsors says about <img src="<?= $asset->baseUrl ?>/images/white-logo.png">
         
        </h1>
      </div>
			<?php
			//print_r($testmonial);
			?>     
				
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<?php
					for($i=0;$i<=count($testmonial);$i++)
						{
							if($i==0){ $class="active";}
							?>
							<li data-target="#myCarousel" data-slide-to="<?=$i?>" class="<?=$class?>"></li>
							<?php
						}	?>
				</ol>

				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
					<?php $j=0;
					foreach($testmonial as $_testmonial)
						{
							if($_testmonial->image=="") { $imgtest= $asset->baseUrl ."/images/comingsoon.png"; }
							else { $imgtest=Yii::$app->homeUrl.$_testmonial->image; }

							if($j==0) { $active="active"; }
							else { $active=""; }
							?>
							<div class="item <?=$active?>">
								<div class="gaurav text-center"><img src="<?=$imgtest ?>">
									<div class="hcl-title"><?= $_testmonial->title?><span><?= $_testmonial->designation ?></span></div>
									<?= $_testmonial->text ?>
								</div>
							</div>
							<?php $j++;
						}	//end of foreach?>
				</div>
			</div>
		</div>
	</div><!-- sponsor end here-->

<div class="clearfix"></div>
	<div class="container">
		<div class="row">
			<div class="engage-with-us">
			
                <div class="section-heading">
        <h1>
        Few of the brands that
          <span class="red_font">Engage with Us</span>
        </h1>
      </div>

				<div id="engage" class="carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ol class="carousel-indicators">
						<?php if(count($brand)>8){?><li data-target="#engage" data-slide-to="0"  class="active"></li>
						<?php }elseif(count($brand)<8 && count($brand)!=17){?><li data-target="#engage" data-slide-to="1" class=""></li>
						<?php }else if(count($brand)<16 ){?> <li data-target="#engage" data-slide-to="2"  class=""></li>
						<?php }?>
					</ol>

					<!-- Wrapper for slides -->
					<div class="carousel-inner" role="listbox">
						<?php 
						$j=1;
						echo '<div class="item active"><ul class="engage-logo">';
						foreach($brand as $_brand)
							{
								if($j==1 || $j==5) $class="text-left";
								if($j==2 || $j==3 || $j==6 || $j==7) $class="text-center";
								if($j==4 || $j==8) $class="text-right";

								if($_brand->image=='') { $img= $asset->baseUrl ."/images/comingsoon.png"; }
								else { $img=Yii::$app->homeUrl.$_brand->image; } ?>
								<li class="<?=$class?>"><a href="javascript:void(0)"><img src="<?=$img?>"></a></li>
								<?php 
								if($j==8) { echo '</ul></div><div class="item"><ul class="engage-logo">'; $j=0; }
								$j++;

							}//end of forloop
						if(count($brand) && $j<8) echo '</ul></div>'; ?>
					</div>
				</div>
			</div>
		</div>
	</div><!--Engage with us container end here-->

	<div class="clearfix"></div>
<div class="newsletter">
		<div class="container">
			<h3 class="pull-left">Subscribe Our Newsletter</h3>
			<div class="newsletter-right">
				<div id="err-subs-email"></div>
				<div class="input-group">
					<form name="subcribe-form"  method="post" id="subcribe-form" action="site/subcribeonspon">
					<input type="text" id="subscribe-email"  name="email" placeholder="Type your email address">
                    <input type="button" onclick="return send_subscribe()" value="Search" class="submit-button">
                   </form>
				</div>
			</div>
		</div>
	</div><!--end of newsletter-->

	<div class="write-us">
	
        <div class="section-heading">
        <h1>
         Write
          <span class="red_font">Us</span>
        </h1>
      </div>

		<div class="container">
			<div class="col-sm-6 contact-form">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi expedita perspiciatis soluta quidem, 
					recusandae sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
						Commodi expedita perspiciatis soluta quidem, recusandae sapiente.</p>

				<div class="form">
					<!-- <div id="Msgreachus" style="color:red; "></div> -->
					<form name="frmcontactus"  method="post" id="frm_reachid" action="">
						<input type="text" class="name"value="" name="name" placeholder="name" id="contact-name"> <div id="err-msg-reachus-name" style="color:red; "></div>
						<input type="text" class="name" value="" name="email" placeholder="email" id="contact-email"><div id="err-msg-reachus-email" style="color:red; "></div>
						<textarea rows="7" class="textaria" value="" class="masgs-deta" name="message" placeholder="Message" id="contact-message"></textarea>
						<input type="button" onclick="return send_contact_mail()" value="Send" class="send-button">
					</form>
					</div>
				</div>

				<div class="col-sm-6 map">
					<ul>
						<li><div class="map-title"><i class="fa fa-phone" aria-hidden="true"></i>  Call Us</div>
							<span>+91 7666003344</span>
						</li>

						<li><div class="map-title"><i class="fa fa-location-arrow" aria-hidden="true"></i>&nbsp;Office location</div>
							<span>A-100, Lourem imspum lorem Mumbai - 401201</span>
						</li>

						<li><div class="map-title"><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;email</div>
							<span>info@onspon.com</span>
						</li>
					</ul>
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3504.2737176240807!2d77.29209731507953!3d28.56154229404116!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390ce40ccdc30055%3A0x2d65301eec3e83cd!2sIndicsoft+Technologies+Pvt.+Ltd.!5e0!3m2!1sen!2sin!4v1473252283822" width="535" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	function validateEmail(email)
		{
			var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(email);
		}

	function send_contact_mail()
		{
			err=false;
			var name = $("#contact-name").val();
			var email = $("#contact-email").val();
			var msg=$("#contact-message").val();
			$("#err-msg-reachus-name").html("");
			$("#err-msg-reachus-email").html("");
			var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
			if(!name)
				{
					$("#err-msg-reachus-name").html("Please enter name");
					$('#contact-name').focus();
					//err=true;alert("name"+err);
					return false;
				}
			if(!email)
				{
					$("#err-msg-reachus-email").html("Please enter email address");
					$('#contact-email').focus();return false;
					//err=true;alert("email"+err);	
				}
			if (!validateEmail(email))
				{
					$("#err-msg-reachus-email").html("Please enter valid email address");
					$('#contact-email').focus();
					//err=true;alert("validation"+err);
					return false;
				}
			else if(!msg)
				{            
					$("#err-msg-reachus-message").html("Please enter Message");
					$('#contact-message').focus();return false;
					//err=true;alert("message"+err);	
				}
			else
				{
					var data='name='+name+'&email='+email+'&text='+msg;
					$.ajax({
							type:'POST',
							data:data,
							url:'<?= Url::to(['/site/contactus']) ; ?>',
							success:function(data)
								{
									//alert(data);
									if(data == 1) {alert("Thank you for your enquiry. We shall revert to you soon. We hope that you have listed your event at onspon.com.");
									location.reload();}  //$("#Msgreachus").html("Thanks we will get back to you soon.");
									if(data == 0)  {alert("Error while sending message");}  // $("#Msgreachus").html("Error while sending message");
								}
						})//end of ajax
				}//end of err function
		}//end of reach function
		
		
		
		
		
		
		
		function send_subscribe()
		{
			var semail = $("#subscribe-email").val();
			if(!semail)
				{
					$("#err-subs-email").html("Please enter email address");
					$('#subscribe-email').focus();return false;
					//err=true;alert("email"+err);	
				}
			if (!validateEmail(semail))
				{
					$("#err-subs-email").html("Please enter valid email address");
					$('#subscribe-email').focus();
					//err=true;alert("validation"+err);
					return false;
				}	else
				{
					var data='semail='+semail;
					$.ajax({
							type:'POST',
							data:data,
							url:'<?= Url::to(['site/subcribeonspon']) ; ?>',
							success:function(data)
								{
									//alert(data);
									if(data == 1) {alert("Thank you Subcribe with us. We shall revert to you soon.");
									location.reload();}  //$("#Msgreachus").html("Thanks we will get back to you soon.");
									if(data == 0)  {alert("Error while sending message");}  // $("#Msgreachus").html("Error while sending message");
								}
						})//end of ajax
				}//end of err function
			
		}

		
		
		
		
</script>


<script>
function getEvent_View(cat_id)
	{
		$(".buttonevent").attr("href",'');
		var _href = $(".buttonevent").attr("href");
		$(".buttonevent").attr("href", _href + 'events/eventlisting?catid='+cat_id);
	}
</script>
