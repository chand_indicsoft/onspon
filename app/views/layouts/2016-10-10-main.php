﻿<?php
use yii\easyii\modules\subscribe\api\Subscribe;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\widgets\Menu;

$asset = \app\assets\AppAsset::register($this);
$session = Yii::$app -> session;
$this->beginContent('@app/views/layouts/base.php');

?>
<header>
	<a href="<?= Url::home() ?>" class="top-logo"></a>
	<div class="top-menu-bar">
		<div class="hamburger-menu toggle-x">
			<span class="arrows"></span></div>
			<nav id="menu">
            
            <ul>
            <li><a href="<?= Url::home() ?>" class="active">Home</a></li>
            <li><a href="#" id="about">About   <span class="pull-right"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></a>
            <ul id="about-sub">
            <li><a href="#">About Onspon</a></li>
            <li><a href="#">How it work</a></li>
            <li><a href="#">FAQs</a></li>
            </ul>
           </li>
           
           <li><a href="#" id="customer-says">Customer say us <span class="pull-right"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></a>
            <ul id="customer-says-sub">
            <li><a href="#">Clientele</a></li>
            <li><a href="#">testimonials</a></li>
            <li><a href="#">Sponsonrs</a></li>
            </ul>
           </li>
           
            <li><a href="#" id="legal">Legal<span class="pull-right"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></a>
            <ul id="legal-sub">
            <li><a href="#">Terms of use</a></li>
            <li><a href="#">Privacy policy</a></li>
            <li><a href="#">contact us</a></li>
            </ul>
           </li>
           
           
            </ul></nav>
        


        
        
        
        
        
        
        
        <ul>
        
			<?php
			if(!$session['user_id'])
			{
				?><li><a href="<?= Url::home() ?>site/login"><span class="menu-icon user"></span>Login</a></li><?php
			}
			else
			{
				?><li><a href="<?= Url::home() ?>site/logout"><span class="menu-icon user"></span>Logout</a></li><?php
			}
			?><li><a><span class="menu-icon phone"></span> + 91 7666003344</a></li>
			<li class="top-menu-seprate-btn"><a href="<?= Url::to(['/eventcreate/create']) ; ?>">List Your Event</a></li>
			<li></li>
		</ul>
	</div>
</header>
<?= $content ?>
<footer>
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
				<div class="footer-one">
					<a href="<?= Url::home() ?>"><img src="<?= $asset->baseUrl ?>/images/footer-logo.png"></a>
					<ul class="list-unstyled">
						<li><i class="fa fa-map-marker" aria-hidden="true"></i> 2901 lourem ipusm, lourem
							Mumbai-  402530 </li>
							<li><i class="fa fa-phone" aria-hidden="true"></i>+91-7666003344 </li>
							<li><i class="fa fa-envelope" aria-hidden="true"></i>info@onspon.com </li>
						</ul>
					</div>
				</div>

				<div class="col-sm-2 link">
					<span>About Onspon</span>
					<ul class="list-unstyled">
						<li><a href="<?= Url::home() ?>">  Home</a></li>
						<li><a href="#">About Us</a></li>
						<li><a href="#"> Sign Up</a></li>
						<li><a href="#"> Sign In</a></li>
						<li><a href="#"> Management Team</a></li>
						<li><a href="#"> Our Services</a></li>
					</ul>
				</div>

				<div class="col-sm-2 link">
					<span>Why Onspon</span>
					<ul class="list-unstyled">
						<li><a href="#">  How it Works</a></li>
						<li><a href="#">Top Avenues</a></li>
						<li><a href="#">Clientele</a></li>
						<li><a href="#">Testimonials</a></li>
						<li><a href="#">FAQs</a></li>
						<li><a href="#">  Careers</a></li>
					</ul>
				</div>

				<div class="col-sm-2 link">
					<span>More Links </span>
					<ul class="list-unstyled">
						<li><a href="#">Sponsors</a></li>
						<li><a href="#">Event Categories</a></li>
						<li><a href="#">Oganizers</a></li>
						<li><a href="#">Partner with Us</a></li>
						<li><a href="#">Managment Team</a></li>
						<li><a href="#">Contact Us</a></li>
					</ul>
				</div>

				<div class="col-sm-3 link">
					<span>Twitter Feed</span>
					<ul class="list-unstyled">
						<li><a href="#"> <i class="fa fa-twitter" aria-hidden="true"></i>  <span class="red">#Arijitsingh</span> live in concert <span class="red">https://t.co/6zS8FeG451</span></a></li>
						<li><a href="#"> <i class="fa fa-twitter" aria-hidden="true"></i>  <span class="red">#Arijitsingh</span> live in concert <span class="red">https://t.co/6zS8FeG451</span></a></li>
						<li><a href="#"> <i class="fa fa-twitter" aria-hidden="true"></i>  <span class="red">#Arijitsingh</span> live in concert <span class="red">https://t.co/6zS8FeG451</span></a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>

	<div class="copyright">
		<div class="container">
			<div class="row">
				<div class="copy-left">Copyright © 2016 www.onspon.com. All rights reserved. <a href="#">Terms of Use</a>   |  <a href="#"> Privacy Policy</a> </div>
				<div class="copy-right">
					<ul class="list-inline">
						<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
					</ul>
				</div>

			</div>
		</div>

	</div>


  <script type="text/javascript">
    
	$("#about").click(function(){
    $("#about-sub").slideToggle();
	$("#menu ul a#about span").toggleClass('open');
	
});
	
	
	$("#customer-says").click(function(){
    $("#customer-says-sub").slideToggle();
	$("#menu ul a#customer-says span").toggleClass('open');
	
});

$("#legal").click(function(){
    $("#legal-sub").slideToggle();
	$("#menu ul a#legal span").toggleClass('open');
	
});
	
	
	
	
	$('.hamburger-menu.toggle-x').on('click', function(){
    $('.hamburger-menu.toggle-x').toggleClass('current');
    
});
$(".toggle-x").click(function(){
		$("#menu").toggleClass("active");

	});





$(document).ready(function () {
  $('#nav > li > a').click(function(){
    if ($(this).attr('class') != 'active'){
      $('#nav li ul').slideUp();
      $(this).next().slideToggle();
      $('#nav li a').removeClass('active');
      $(this).addClass('active');
    }
  });
});

      </script>  



	<?php $this->endContent(); ?>



	
