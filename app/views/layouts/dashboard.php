
<?php $this->beginContent('@app/views/layouts/dashboardbase.php'); 

use yii\helpers\Url;

$asset = \app\assets\DashboardAsset::register($this);

$session = Yii::$app -> session;
?>

		<?php echo $content; ?>
		
<footer>
<div class="container">
<div class="row">
<div class="col-sm-3">
<div class="footer-one">
<a href="#"><img src="<?= $asset->baseUrl ?>/images/footer-logo.png"></a>
<ul class="list-unstyled">
<li><i class="fa fa-map-marker" aria-hidden="true"></i> 2901 lourem ipusm, lourem
Mumbai-  402530 </li>
<li><i class="fa fa-phone" aria-hidden="true"></i>+91-7666003344 </li>
<li><i class="fa fa-envelope" aria-hidden="true"></i>info@onspon.com </li>
</ul>
</div>
</div>

<div class="col-sm-2 link">
<span>About Onspon</span>
<ul class="list-unstyled">
<li><a href="#">  Home</a></li>
<li><a href="#">About Us</a></li>
<li><a href="#"> Sign Up</a></li>
<li><a href="#"> Sign In</a></li>
<li><a href="#"> Management Team</a></li>
<li><a href="#"> Our Services</a></li>
</ul>
</div>



<div class="col-sm-2 link">
<span>Why Onspon</span>
<ul class="list-unstyled">
<li><a href="#">  How it Works</a></li>
<li><a href="#">Top Avenues</a></li>
<li><a href="#">Clientele</a></li>
<li><a href="#">Testimonials</a></li>
<li><a href="#">FAQs</a></li>
<li><a href="#">  Careers</a></li>
</ul>
</div>


<div class="col-sm-2 link">
<span>More Links </span>
<ul class="list-unstyled">
<li><a href="#">  Sponsors</a></li>
<li><a href="#"> Event Categories</a></li>
<li><a href="#">Oganizers</a></li>
<li><a href="#">Partner with Us</a></li>
<li><a href="#">Managment Team</a></li>
<li><a href="#"> Contact Us</a></li>
</ul>
</div>


<div class="col-sm-2 link">
<span>Twitter Feed</span>
<ul class="list-unstyled">
<li><a href="#"> <i class="fa fa-twitter" aria-hidden="true"></i>  <span class="red">#Arijitsingh</span> live in concert <span class="red">https://t.co/6zS8FeG451</span></a></li>
<li><a href="#"> <i class="fa fa-twitter" aria-hidden="true"></i>  <span class="red">#Arijitsingh</span> live in concert <span class="red">https://t.co/6zS8FeG451</span></a></li>
<li><a href="#"> <i class="fa fa-twitter" aria-hidden="true"></i>  <span class="red">#Arijitsingh</span> live in concert <span class="red">https://t.co/6zS8FeG451</span></a></li>
</ul>
</div>



</div>
</div>

</footer>
<div class="copyright">
<div class="container">
<div class="row">

<div class="copy-left">Copyright © 2016 www.onspon.com. All rights reserved. <a href="#">Terms of Use</a>   |  <a href="#"> Privacy Policy</a> </div>
<div class="copy-right">
<ul class="list-inline">
<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
<li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
</ul>
</div>

</div>
</div>

</div>		
	

<?php $this->endContent(); ?>


<script type="text/javascript">
	$(function ()
	{
		<!--right-navigation-->
		$(".toggle-x").click(function(){
			var className = $('#menu').attr('class');
			if(className =='active'){
			  $("#menu").removeClass("active");
		  }else { $("#menu").addClass("active"); } 
			
		});  
		
		// contact 	
		$(document).on('click', '.contact-now', function(e) {
			$(this).siblings(".contact-data-cont").show(500);
			$(this).siblings('li').next(".shorlist").addClass("active");
			e.stopPropagation();
		});

		$(document).on('click', '.close-popup', function(e) {
			$(this).parent(".contact-data-cont").hide(500);
			$(this).closest('li').next(".shorlist").removeClass("active");
			e.stopPropagation();
		});
	
	// shorlist 	
		$(document).on('click', '.shorlist', function(f) {
			$(this).siblings(".shorlist-data-cont").show(500);
			$(this).siblings('li').next(".shorlist").addClass("active");
			f.stopPropagation();
		});

		$(document).on('click', '.close-popup', function(f) {
			$(this).parent(".shorlist-data-cont").hide(500);
			$(this).closest('li').next(".shorlist").removeClass("active");
			f.stopPropagation();
		});
		
	});
	
	function closepopcontact(id)
	{
		$("#contactarea"+id).hide(500);
		$("#contactarea"+id).closest('li').next(".shorlist").removeClass("active");
	}
	
	function closepopshortlist(id)
	{
		$("#shortlistarea"+id).hide(500);
		$("#shortlistarea"+id).closest('li').next(".shorlist").removeClass("active");
	}
	
	// Notification 
    
    
</script>
<!-- Scrollfix header-->
<script type="text/javascript">
$(window).scroll(function(){
if ($(window).scrollTop() >= 150) {
   $('header').addClass('fix-head');
}
else {
   $('header').removeClass('fix-head');
}
});
</script>


<script>
function shortlistevent(eid)
{
	var checkboxValues = [];
	$('input[name=folder]:checked').map(function() {
			checkboxValues.push($(this).val());
	});
	if(checkboxValues.length >=1)
	{
            //save to database
				data ='checkboxValues='+checkboxValues+'&eventid='+eid;
			$.ajax({
				url : '<?= Url::to(['events/shortlistevent']) ; ?>',
				type: 'POST',
				data: data,
				success: function(result)
				{
					if(result=='success'){alert('ShortList the event sucessfully');
						$("#shortlistarea"+eid).hide(500);
					}else{
						alert('Something is Wrong');
					}
				}
			});
            
            //save to db
	}else{
		alert("Select any folder name to organize your selection");
	}

}

</script>


<script>
function shortlist_add_folder(eid)
{
	var name = $('#shortfoldername'+eid).val();
	if(!name)
	{ 
		$('#shortfoldererr'+eid).show(); return false;
	}else{
		var data ='flodername='+name;
		$.ajax({
			url : '<?= Url::to(['events/shortlistfolder']) ; ?>',
			type: 'POST',
			data: data,
			dataType:'html',
			success: function(result)
			{
				if(result){
					$('#folder-list'+eid).html(result);
					$('#shortfoldererr'+eid).hide(); 
				}else if(result=='error'){
					alert('Something is Wrong');
					
				}
			}
		});
	}//end of !name else	
}

</script>

<script>
function contactnow(id){
	var name = $('#name'+id).val();
	var phone = $('#phone'+id).val();
	var text = $('#text'+id).val();
	if(!name){ $('#nameerr'+id).show();$('#phoneerr'+id).hide();$('#texterr'+id).hide(); return false;}
	if(!phone){ $('#nameerr'+id).hide();$('#phoneerr'+id).show();$('#texterr'+id).hide(); return false;}
	if(!text){ $('#nameerr'+id).hide();$('#phoneerr'+id).hide();$('#texterr'+id).show(); return false;}
	else{
	
		var data = $("#contactnow"+id).serialize();
		data +='&id='+id;
		$.ajax({
			url : '<?= Url::to(['eventcontact']) ; ?>',
			type: 'POST',
			data: data,
			success: function(result)
			{
				if(result=='success'){$("#contactarea"+id).hide(500); $('#nameerr'+id).hide();$('#phoneerr'+id).hide();$('#texterr'+id).hide(); alert('Message Sent');}
				else{alert('Something is Wrong');}
			}
		});
	}
}
</script>









 
