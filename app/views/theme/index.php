<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Common;
use app\modules\events\models\Events;

$asset = \app\assets\DefaultAppAsset::register($this);
$eventModel=new Events;
$eventDetail = Events::find()->where(['or', 'id=:id_event'], [':id_event' => $eventid])->status(Events::STATUS_ON)->one();
$eventgallery=$eventModel->getEventImg($eventid);
$session = Yii::$app->session;
if(($session['role'])==1)
{
	 Common::eventViewbyBrand($eventid,$session['user_id'],$session['role']);
}
//print_r( $eventgallery->image);
?>

<header>
	<div class="container">
		<a href="#" class="header-logo left">
			<img src="<?=Yii::$app->homeUrl.$eventDetail->logo;?>" width="100%" height="100%">
		</a>
		<nav class="top-menu right">
			<ul>
				<li><a href="javascript:;" class="active" data-scroll="#about">About</a></li>
				<li><a href="javascript:;" class="" data-scroll="#upcmng-evnt">Upcoming Events</a></li>
				<li><a href="javascript:;" class="" data-scroll="#popular">Popular / SHowcase Events</a></li>
				<li><a href="javascript:;" class="" data-scroll="#all-evnt">Event Gallery </a></li>
				<li><a href="javascript:;" class="" data-scroll="#contact">Contact</a></li>
			</ul>
		</nav>
	</div>
</header>
<section class="slider-container">
	<div class="home-slider">
		<div class="home-slide">
			<div class="slide-img">
				<img src="<?= $asset->baseUrl ?>/images/slide1.jpg">
				
			</div>
			<div class="container">
				<div class="slide-text">
					<p class="small-text">landing page template</p>
					<p class="big-text">PERFECT Fitness SOLUTION</p>
					<p class="medium-text">best choise for your site</p>
				</div>
			</div>
		</div>
		<div class="home-slide">
			<div class="slide-img">
				<img src="<?= $asset->baseUrl ?>/images/slide1.jpg">
				
			</div>
			<div class="container">
				<div class="slide-text">
					<p class="small-text">landing page template</p>
					<p class="big-text">PERFECT Fitness SOLUTION</p>
					<p class="medium-text">best choise for your site</p>
				</div>
			</div>
		</div>
		<div class="home-slide">
			<div class="slide-img">
				<img src="<?= $asset->baseUrl ?>/images/slide1.jpg">
				
			</div>
			<div class="container">
				<div class="slide-text">
					<p class="small-text">landing page template</p>
					<p class="big-text">PERFECT Fitness SOLUTION</p>
					<p class="medium-text">best choise for your site</p>
				</div>
			</div>
		</div>
	
	

	
	</div>
</section>

<section class="hm-section-container" id="about">
	<div class="container">
		<div class="section-heading">
			<h1>
				Amazing
				<span class="red_font">Features</span>
			</h1>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="amzing-fitr">
				<span class="amzing-fitr-icons conf"></span>
				<h2 class="article-thumb-heading">Conferences / Exhibitions, Music</h2>
				<p class="article-thumb-text">
					Maecenas mattis vitae tellus vel interdum. Quisque lacinia mauris id convallis pretium. Aliquam id odio sit amet mauris porttitor iaculis a eu dui.
				</p>
			</div>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="amzing-fitr">
				<span class="amzing-fitr-icons evnt"></span>
				<h2 class="article-thumb-heading">Conferences / Exhibitions, Music</h2>
				<p class="article-thumb-text">
					Maecenas mattis vitae tellus vel interdum. Quisque lacinia mauris id convallis pretium. Aliquam id odio sit amet mauris porttitor iaculis a eu dui.
				</p>
			</div>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="amzing-fitr">
				<span class="amzing-fitr-icons city"></span>
				<h2 class="article-thumb-heading">Conferences / Exhibitions, Music</h2>
				<p class="article-thumb-text">
					Maecenas mattis vitae tellus vel interdum. Quisque lacinia mauris id convallis pretium. Aliquam id odio sit amet mauris porttitor iaculis a eu dui.
				</p>
			</div>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="amzing-fitr">
				<span class="amzing-fitr-icons year"></span>
				<h2 class="article-thumb-heading">Conferences / Exhibitions, Music</h2>
				<p class="article-thumb-text">
					Maecenas mattis vitae tellus vel interdum. Quisque lacinia mauris id convallis pretium. Aliquam id odio sit amet mauris porttitor iaculis a eu dui.
				</p>
			</div>
		</div>
	</div>
</section>

<section class="hm-section-container upcmng-evnt" id="upcmng-evnt">
	<div class="container">
		<div class="section-heading">
			<h1>
				Upcoming
				<span class="red_font">Events</span>
			</h1>
		</div>
		<div class="evnt-slider upcmng-evnt-slider">
			<div class="evnt-slide">
				<div class="col-lg-4 col-sm-4">
					<div class="evnt-block">
						<div class="article-thumb-img">
							<img src="<?= $asset->baseUrl ?>/images/img1.jpg">
						</div>
						<h3 class="article-thumb-heading">
							Corporate Talent Championship
						</h3>
						<p class="article-thumb-text">
							consectetur adipiscing elit. Duis id euismod dui. Vivamus at pharetra nisi. Nulla vestibulum nec ipsum sed cursus. Duis id euismod dui. Vivamus at pharetra nisi.
						</p>
						<ul class="article-thumb-info">
							<li>
								<span class="info-icons calendar"></span>
								02 Sept 2016
							</li>
							<li>
								<span class="info-icons time"></span>
								12:00 pm
							</li>
							<li>
								<span class="info-icons map"></span>
								Delhi NCR, Delhi
							</li>
						</ul>
					</div>
				</div>
				<div class="col-lg-4 col-sm-4">
					<div class="evnt-block">
						<div class="article-thumb-img">
							<img src="<?= $asset->baseUrl ?>/images/img1.jpg">
						</div>
						<h3 class="article-thumb-heading">
							Corporate Talent Championship
						</h3>
						<p class="article-thumb-text">
							consectetur adipiscing elit. Duis id euismod dui. Vivamus at pharetra nisi. Nulla vestibulum nec ipsum sed cursus. Duis id euismod dui. Vivamus at pharetra nisi.
						</p>
						<ul class="article-thumb-info">
							<li>
								<span class="info-icons calendar"></span>
								02 Sept 2016
							</li>
							<li>
								<span class="info-icons time"></span>
								12:00 pm
							</li>
							<li>
								<span class="info-icons map"></span>
								Delhi NCR, Delhi
							</li>
						</ul>
					</div>
				</div>
				<div class="col-lg-4 col-sm-4">
					<div class="evnt-block">
						<div class="article-thumb-img">
							<img src="images/img1.jpg">
						</div>
						<h3 class="article-thumb-heading">
							Corporate Talent Championship
						</h3>
						<p class="article-thumb-text">
							consectetur adipiscing elit. Duis id euismod dui. Vivamus at pharetra nisi. Nulla vestibulum nec ipsum sed cursus. Duis id euismod dui. Vivamus at pharetra nisi.
						</p>
						<ul class="article-thumb-info">
							<li>
								<span class="info-icons calendar"></span>
								02 Sept 2016
							</li>
							<li>
								<span class="info-icons time"></span>
								12:00 pm
							</li>
							<li>
								<span class="info-icons map"></span>
								Delhi NCR, Delhi
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="evnt-slide">
				<div class="col-lg-4 col-sm-4">
					<div class="evnt-block">
						<div class="article-thumb-img">
							<img src="<?= $asset->baseUrl ?>/images/img1.jpg">
						</div>
						<h3 class="article-thumb-heading">
							Corporate Talent Championship
						</h3>
						<p class="article-thumb-text">
							consectetur adipiscing elit. Duis id euismod dui. Vivamus at pharetra nisi. Nulla vestibulum nec ipsum sed cursus. Duis id euismod dui. Vivamus at pharetra nisi.
						</p>
						<ul class="article-thumb-info">
							<li>
								<span class="info-icons calendar"></span>
								02 Sept 2016
							</li>
							<li>
								<span class="info-icons time"></span>
								12:00 pm
							</li>
							<li>
								<span class="info-icons map"></span>
								Delhi NCR, Delhi
							</li>
						</ul>
					</div>
				</div>
				<div class="col-lg-4 col-sm-4">
					<div class="evnt-block">
						<div class="article-thumb-img">
							<img src="images/img1.jpg">
						</div>
						<h3 class="article-thumb-heading">
							Corporate Talent Championship
						</h3>
						<p class="article-thumb-text">
							consectetur adipiscing elit. Duis id euismod dui. Vivamus at pharetra nisi. Nulla vestibulum nec ipsum sed cursus. Duis id euismod dui. Vivamus at pharetra nisi.
						</p>
						<ul class="article-thumb-info">
							<li>
								<span class="info-icons calendar"></span>
								02 Sept 2016
							</li>
							<li>
								<span class="info-icons time"></span>
								12:00 pm
							</li>
							<li>
								<span class="info-icons map"></span>
								Delhi NCR, Delhi
							</li>
						</ul>
					</div>
				</div>
				<div class="col-lg-4 col-sm-4">
					<div class="evnt-block">
						<div class="article-thumb-img">
							<img src="images/img1.jpg">
						</div>
						<h3 class="article-thumb-heading">
							Corporate Talent Championship
						</h3>
						<p class="article-thumb-text">
							consectetur adipiscing elit. Duis id euismod dui. Vivamus at pharetra nisi. Nulla vestibulum nec ipsum sed cursus. Duis id euismod dui. Vivamus at pharetra nisi.
						</p>
						<ul class="article-thumb-info">
							<li>
								<span class="info-icons calendar"></span>
								02 Sept 2016
							</li>
							<li>
								<span class="info-icons time"></span>
								12:00 pm
							</li>
							<li>
								<span class="info-icons map"></span>
								Delhi NCR, Delhi
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</section>

<section class="hm-section-container" id="popular">
	<div class="container">
		<div class="section-heading">
			<h1>
				Popular / Showcase
				<span class="red_font">Events</span>
			</h1>
		</div>
		<div class="evnt-slider pplr-slider">
			<div class="evnt-slide">
				<div class="col-lg-4 col-sm-4">
					<div class="evnt-block">
						<div class="article-thumb-img">
							<img src="images/img1.jpg">
						</div>
						<h3 class="article-thumb-heading">
							Corporate Talent Championship
						</h3>
						<p class="article-thumb-text">
							consectetur adipiscing elit. Duis id euismod dui. Vivamus at pharetra nisi. Nulla vestibulum nec ipsum sed cursus. Duis id euismod dui. Vivamus at pharetra nisi.
						</p>
						<a href="#" class="read-more">read more</a>
					</div>
				</div>
				<div class="col-lg-4 col-sm-4">
					<div class="evnt-block">
						<div class="article-thumb-img">
							<img src="images/img1.jpg">
						</div>
						<h3 class="article-thumb-heading">
							Corporate Talent Championship
						</h3>
						<p class="article-thumb-text">
							consectetur adipiscing elit. Duis id euismod dui. Vivamus at pharetra nisi. Nulla vestibulum nec ipsum sed cursus. Duis id euismod dui. Vivamus at pharetra nisi.
						</p>
						<a href="#" class="read-more">read more</a>
					</div>
				</div>
				<div class="col-lg-4 col-sm-4">
					<div class="evnt-block">
						<div class="article-thumb-img">
							<img src="images/img1.jpg">
						</div>
						<h3 class="article-thumb-heading">
							Corporate Talent Championship
						</h3>
						<p class="article-thumb-text">
							consectetur adipiscing elit. Duis id euismod dui. Vivamus at pharetra nisi. Nulla vestibulum nec ipsum sed cursus. Duis id euismod dui. Vivamus at pharetra nisi.
						</p>
						<a href="#" class="read-more">read more</a>
					</div>
				</div>
			</div>
			<div class="evnt-slide">
				<div class="col-lg-4 col-sm-4">
					<div class="evnt-block">
						<div class="article-thumb-img">
							<img src="images/img1.jpg">
						</div>
						<h3 class="article-thumb-heading">
							Corporate Talent Championship
						</h3>
						<p class="article-thumb-text">
							consectetur adipiscing elit. Duis id euismod dui. Vivamus at pharetra nisi. Nulla vestibulum nec ipsum sed cursus. Duis id euismod dui. Vivamus at pharetra nisi.
						</p>
						<a href="#" class="read-more">read more</a>
					</div>
				</div>
				<div class="col-lg-4 col-sm-4">
					<div class="evnt-block">
						<div class="article-thumb-img">
							<img src="images/img1.jpg">
						</div>
						<h3 class="article-thumb-heading">
							Corporate Talent Championship
						</h3>
						<p class="article-thumb-text">
							consectetur adipiscing elit. Duis id euismod dui. Vivamus at pharetra nisi. Nulla vestibulum nec ipsum sed cursus. Duis id euismod dui. Vivamus at pharetra nisi.
						</p>
						<a href="#" class="read-more">read more</a>
					</div>
				</div>
				<div class="col-lg-4 col-sm-4">
					<div class="evnt-block">
						<div class="article-thumb-img">
							<img src="images/img1.jpg">
						</div>
						<h3 class="article-thumb-heading">
							Corporate Talent Championship
						</h3>
						<p class="article-thumb-text">
							consectetur adipiscing elit. Duis id euismod dui. Vivamus at pharetra nisi. Nulla vestibulum nec ipsum sed cursus. Duis id euismod dui. Vivamus at pharetra nisi.
						</p>
						<a href="#" class="read-more">read more</a>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</section>

<section class="hm-section-container all-evnt" id="all-evnt">
	<div class="container">
		<div class="section-heading">
			<h1>
				Event
				<span class="red_font">Gallery</span>
			</h1>
		</div>
		
		<div class="evnt-slider all-evnt-slider">
			<?php $i=0;
			foreach( $eventgallery as  $_eventgallery)
			{
			 if($i==0) echo '<div class="evnt-slide">';
			?>	
				
					<div class="col-lg-4 col-sm-4">
						<div class="evnt-block">
							<div class="article-thumb-img">
								<img src="<?php echo Yii::$app->homeUrl.$_eventgallery->image;?>" width="100%">
							</div>
							<p class="article-thumb-text">
								<?php //echo "<pre>";print_r($_eventgallery); ?>
								<?= $_eventgallery->description; ?>
							</p>
							</div>
					</div>
				
			<?php 
			
			$i++;
			if($i==3) {echo '</div>'; $i=0;}
			}
			?>
		</div>
	</div>
</section>