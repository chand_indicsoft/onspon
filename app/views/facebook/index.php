<?php
	/* @var $this yii\web\View */
	$asset = \app\assets\AppAsset::register($this);

	use yii\helpers\Url;
	use yii\helpers\Html;

	# registering javascript
	$this->registerJs("window.fbAsyncInit = function() {
	FB.init({
		appId: '1778145049103057',
		status: true, // check login status
		cookie: true, // enable cookies to allow the server to access the session
		xfbml: true  // parse XFBML
	});

	FB.Event.subscribe('auth.authResponseChange', function(response) {
		if (response.status === 'connected') { //SUCCESS
			// document.getElementById('message').innerHTML += '<br>Connected to Facebook';
		} else if (response.status === 'not_authorized') { //FAILED
			// document.getElementById('message').innerHTML += '<br>Failed to Connect';
		} else { //UNKNOWN ERROR
			// document.getElementById('message').innerHTML += '<br>Logged Out';
		}
	});

	FB.getLoginStatus(function(response) {
		$('.fb-login, .fb-events').hide(0);

		if (response.status === 'connected') {
			$('.fb-events').show(0);

			// the user is logged in and has authenticated your
			// app, and response.authResponse supplies
			// the user's ID, a valid access token, a signed
			// request, and the time the access token
			// and signed request each expire
			var uid = response.authResponse.userID;
			var accessToken = response.authResponse.accessToken;
		} else if (response.status === 'not_authorized') {
			$('.fb-login').show(0);

			// the user is logged in to Facebook,
			// but has not authenticated your app
		} else {
			$('.fb-login').show(0);

			// the user isn't logged in to Facebook.
		}
	});
};

function getUser()
{
	FB.api('/me?fields=name,id,email,first_name,last_name,picture,gender', function(response) {
		console.log(response);
	});
}

function getPhoto()
{
	FB.api('/me/picture?type=normal', function(response) {
		var str='<br/><b>Pic</b> : <img src='+response.data.url+'/>';
		document.getElementById('status').innerHTML+=str;
	});
}

// login
$(document).on('click', '.fb-login', function() {
	FB.login(function(response) {
		if (response.authResponse) {
			$('.fb-login').hide(0);
			$('.fb-events').show(0);
		} else {
			console.log('User cancelled login or did not fully authorize.');
		}
	}, {
		scope: 'email,user_photos,user_videos,user_events'
	});
});

// logout
$(document).on('click', '.fb-login', function() {
	FB.logout(function() {
		$('.fb-events').hide(0);
		$('.fb-login').show(0);
	});
});

// fetching facebook events
$(document).on('click', '.fb-events', function() {
	FB.api('/me/events', function(response) {
		$.ajax({
			type: 'POST',
			url: '" . Url::to(['events']) . "',
			data: {
				'_csrf': '". Yii::$app->request->csrfToken . "',
				'data': JSON.stringify(response)
			},
			complete: function(response) {
				$('#events').html(response.responseText);
			}
		});
	});
});

// fetching facebook events and saving in database
$(document).on('click', '.fb-events-save', function() {
	FB.api('/me/events', function(response) {
		$.ajax({
			type: 'POST',
			url: '" . Url::to(['save-events']) . "',
			data: {
				'_csrf': '". Yii::$app->request->csrfToken . "',
				'data': JSON.stringify(response)
			},
			complete: function(response) {
				$('#events').html(response.responseText);
			}
		});
	});
});

// Load the SDK asynchronously
(function(d) {
	var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement('script'); js.id = id; js.async = true;
	js.src = '//connect.facebook.net/en_US/all.js';
	ref.parentNode.insertBefore(js, ref);
}(document));", \yii\web\View::POS_HEAD);

	// $this->registerJs("", \yii\web\View::POS_READY);
?>
<h1>facebook/index</h1>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<a href="javascript:void(0)" class="fb-login" style="display: none;">Login with facebook</a>
<a href="javascript:void(0)" class="fb-events" style="display: none;">Import events</a>

<div id="events"></div>
<p>You may change the content of this page by modifying the file <code><?= __FILE__; ?></code>.</p>