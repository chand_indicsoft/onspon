<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Common;
use app\modules\brandmandates\models\BrandMandates;
use  yii\widgets\LinkPager;
use yii\helpers\ArrayHelper;

$asset = \app\assets\EventlistingAppAsset::register($this);
$commonModel = new Common;

$timedisplay=false;
$citydisplay=false;
$radiovalue="";
$radioval =array();

$city  = (@$_REQUEST['city'] ? @$_REQUEST['city'] : '');

$timefval="";
$timefield=array('week'=>'This week','month'=>'This month','next3month'=>'Next 3 months');
if(@$_REQUEST['datetype']=='week') {$timefval=@$_REQUEST['datetype']; $timedisplay="This weekend";}
if(@$_REQUEST['datetype']=='month') {$timefval=@$_REQUEST['datetype']; $timedisplay="This month";}
if(@$_REQUEST['datetype']=='next3month') {$timefval=@$_REQUEST['datetype']; $timedisplay="Next 3 months";}
?>

<section class="section-banner-cont section-banner">
	<div class="section-img">
		<img src="<?= $asset->baseUrl ?>/images/section-banner1.jpg">
	</div>

	<div class="section-caption-cont">
		<div class="container">
			<div class="section-caption">
				<h1 class="section-lead-text">
					Welcome to 
					<span class="red_font list-inline">onspon.com</span>
				</h1>
				<h3 class="section-sub-lead-text">
					Brand Mandates
				</h3>
				<ul class="section-buttons">
					<li><a href="#" class="active">Brand Mandates Posted</a></li>
					<li><a href="#">Last Minute Deals</a></li>
				</ul>
			</div>
		</div>
	</div>

	<div class="explore-events-cont red_bg">
		<div class="container">
			<p>
				More than&nbsp;
				<span class="font_wieght700 white_font list-inline three100"> 300</span> 
				brand managers visit &nbsp;<span class="font_wieght700 list-inline onspon_com"> onspon.com</span> 
				to find the best events. Let's start by &nbsp;
				<a href="#" class="white_font font_wieght500 list-inline exp-evt-link">creating your brand mandate (?)</a> 
				or &nbsp;
				<a href="#" class="white_font font_wieght500 list-inline exp-evt-link">browse available events (?)</a>
			</p>
		</div>
	</div>
</section>

<div class="clearfix"></div>
<!--banner end here-->


<section class="section-text-cont">
    <div class="container">
      <div class="row">

        <div class="section-heading">
          <h1>
            <?php
				echo $timedisplay=($timedisplay!="") ? $timedisplay:" ";
				echo $citydisplay=($city!="") ? '/'.$city :" ";
			?>
			 <span class="red_font">Brand Mandates</span>
          </h1>
        </div>
		
		<div class="col-lg-12">
			<div class="filter-buttons-cont">
				<ul>
					<?= Html::beginForm(Url::to(['/brand/brandlisting']), 'get', ['class' => 'form-inline','id'=>'eventForm']) ?>
					<li class="filter-toggle" id="filter">
						<div class="filter-caption"><span class="filter-icon filter"></span> Filter</div>
						<div class="filter-data" id="filter-data" style="display:none;">
							<span class="filter-close"></span>
							<div class="form-group">
								<label>Time</label>
									<?= Html::dropDownList('datetype',$timefval, $timefield,
																['prompt' => '--- Select ---' ,'class'=> "select-group",'data-live-search'=>"true" ])
									?>

								<label>  
									<?= Html::textInput('city', $city, ['class' => 'form-control', 'placeholder' => 'Type city','aria-describedby'=>"basic-addon1"]) ?>
								</label>
								<input type="submit" name="search" value="Go"/> 
							</div>
						</div>
					</li>

					<?php 
						$radioval = ['start_date'=>'By Date'];
						if(@$_REQUEST['sortby']=='start_date')
						{
							$radiovalue='start_date';
						}
						/*else if(@$_REQUEST['sortby']=='premium')
						{
							$radiovalue='premium';
						}*/
						else
						{
							$radiovalue='start_date';
						}
					?>
					
					<li class="filter-toggle short" id="short">
						<div class="filter-caption"><span class="filter-icon sort"></span> Sort By</div>
						<div class="filter-data" id="short-data" style="display:none;">
							<span class="filter-close"></span>
							<div class="radio filter-radio-div">
								<label>
									<?php  echo Html::radioList('sortby',@$radiovalue, $radioval, [
												'class' => 'radio filter-radio-div',
												'itemOptions' => ['class' => 'filter-radio'],
											]); 
									?>
								</label>
							</div>
						</div>
					</li>
					<?= Html::endForm() ?>
				</ul>
			</div>
		</div>
        
		<?php
			//echo '<pre>';print_r($eventdetail);die;
			if($branddetail)
			{
				foreach($branddetail as $_branddetail)
				{
		?>
					<div class="col-lg-4 col-md-4 col-sm-6">
					  <div class="evnt-dsbrd-card">
						<div class="article-thumb-img">
						  <?php
							if($_branddetail->logo=='')
								{ ?>
									 <img src="<?= $asset->baseUrl ?>/images/NoImage.png">
							<?php	}
							else { ?>
									<img src="<?= Yii::$app->homeUrl."/".$_branddetail->logo ?>">
							<?php } ?>
						</div>
						<h3 class="article-thumb-heading">
						  <?=ucwords($_branddetail->brand_name)?>
						</h3>
						<div class="evnt-dsbrd-objctv-dscp">
						  <span class="objctv-dscp-heading">
							Objective
						  </span>
						  <span class="objctv-dscp-data">
							<ul>
								<?php
									if($_branddetail->specific_subjective)
									{
										$objective=@explode(',',$_branddetail->specific_subjective);
										foreach($objective as $_objective)
										{
											?> <li> <?php
											echo $commonModel->getObjective($_objective).'&nbsp';
											?> </li> <?php
										}
									}
							   ?>
							</ul>
						  </span>
						</div>
						<div class="evnt-dsbrd-objctv-dscp">
						  <span class="objctv-dscp-heading">
							Description
						  </span>
						  <span class="objctv-dscp-data">
							<p>
							   <?= $_branddetail->about_brand?>
							</p>
						  </span>
						</div>
						<div class="article-thumb-details">
						  <ul>
							<li><a href="#">Save for later</a></li>
							<li class="rqr-evnt">
							  <a href="#">Required Event Profile..? </a>
							  <div class="rqr-evnt-prfl-cont">
								<h5>Looking for events having the following Audience profile</h5>
								<ul>
									<?php $agegroup=($commonModel->getAgegroup($_branddetail->age_group)); ?>
								  <li><span class="rqr-evnt-icons family"></span><?=$agegroup ?></li>
								  
									<?php $education=($commonModel->getEducation($_branddetail->education)); ?>
								  <li><span class="rqr-evnt-icons education"></span><?=$education ?></li>
								  
								  <li><span class="rqr-evnt-icons gender"></span><?= $_branddetail->gender?></li>
								  
									<?php $income=($commonModel->getIncome($_branddetail->income)); ?>
								  <li><span class="rqr-evnt-icons money"></span><?=$income ?></li>

								  <li><span class="rqr-evnt-icons map"></span><?= $_branddetail->state_id?> <?= $_branddetail->country_id?></li>
								  
									<?php $start_date = $_branddetail->start_date;
										//$brand_start_date = date("d M Y", $start_date);
										 ?>
									<?php $end_date = $_branddetail->end_date;
										//$brand_end_date = date("d M Y", $end_date);
										 ?>
								  <li><span class="rqr-evnt-icons calendar"></span><?php //echo $brand_start_date ?> - <?php //echo $brand_end_date ?></li>
								</ul>
							  </div>
							</li>
							<!--<li class="rqr-evnt-views">
							  <a href="#">Views</a>
							  <div class="rqr-evnt-views-cont">03</div>
							</li>-->
						  </ul>
						</div>
						<?php 
							$session = Yii::$app->session;
							if(!empty(Yii::$app->session['user_id']) && $session['role']==2)
								{
						?>
						<a href="#" class="aply-nw"  onclick="applynow(<?=$_branddetail->id?>,<?=$_branddetail->user_id?>); return false;">Apply Now</a>
						<?php }else {
							# setting referral url
								$currentUrl = Url::current();
								$session = Yii::$app->session;
								$session['referral']= $currentUrl;
								 ?>
							<a href="<?php echo Url::to(['/login']); ?>" class="aply-nw"  >Apply Now</a>
						<?php } ?>
					  </div>
					</div>
					<?php 
				}//end of for loop 
			} //end of if ?>
      </div>
    </div>

	<div class="container">
		<div class="row">
			<?php if($branddetail)
					{
			?>    
						<div class="paginat">
							<?php
								echo LinkPager::widget(['pagination' => $pages,]);
							?>
						</div>  
			 <?php }
				else
					{
			?>
						<div class="empty-events">
							<?php echo "Sorry No Events"; ?> 
						</div>
			<?php	}
				?>
		</div>
	</div>
</section>

 
<script type="text/javascript">
	$(function ()
	{
		var filterList = {
			init: function () {
				// MixItUp plugin
				// http://mixitup.io
				$('#portfoliolist').mixItUp({
  				selectors: {
    			  target: '.portfolio',
    			  filter: '.filter'
    		  },
    		  load: {
      		  filter: '.app'
				}
				});
			}
		};

		// Run the show!
		filterList.init();

		$('.filter-caption').on('click', function(e) {
			e.preventDefault();

			$('.filter-toggle').removeClass('active');
			$('.filter-data').hide(0);
			
			var $this = $(this);

			$this.parent('.filter-toggle').addClass('active');
			$this.next('.filter-data').show(250);
		});
		
		$('.filter-close').on('click', function(e) {
			e.preventDefault();

			var $this = $(this);

			$this.closest('.filter-toggle').removeClass('active');
			$this.parent('.filter-data').hide(250);
		});
	});
</script>

<script>
function applynow(brandid,receiver)
{
	
	data ='brandid='+brandid+'&receiver='+receiver;
	$.ajax({
		url:'<?= Url::to(['brandapply']) ; ?>',
		data:data,
		type:'POST',
		success: function(result)
		{
			alert(result);	
			
		}
		
	});
}

</script>

