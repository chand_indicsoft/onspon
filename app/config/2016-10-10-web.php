<?php
	$params = require(__DIR__ . '/params.php');

	$basePath =  dirname(__DIR__);
	$webroot = dirname($basePath);

	$config = [
		'id' => 'app',
		'basePath' => $basePath,
		'bootstrap' => ['log'],
		'language' => 'en-US',
		'runtimePath' => $webroot . '/runtime',
		'vendorPath' => $webroot . '/vendor',
		'components' => [
			'request' => [
				// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
				'cookieValidationKey' => 'suma',
			],
			'authClientCollection' => [
				'class' => 'yii\authclient\Collection',
				'clients' => [
					'facebook' => [
						'class' => 'yii\authclient\clients\Facebook',
						'authUrl' => 'https://www.facebook.com/dialog/oauth?display=popup',
						'clientId' => '1574387206190835',
						'clientSecret' => '27548f296c15b4f91f2a6361b1420833',
					],
					'google' => [
						'class' => 'yii\authclient\clients\Google',
						'clientId' => '637590532503-4jf3q149dhtdjviomtganrcih2hgot52.apps.googleusercontent.com',
						'clientSecret' => 'yQwMNjd92NghzXSCcTw71C62',
						// 'clientId' => ' 513697536034-pbpaqan5937catfuf35jfikjj7rt7rb0.apps.googleusercontent.com',
						//'clientSecret' => '2HBsJpLM2JSeKfkzy23QZq4s',
					],
					'linkedin' => [
						'class' => 'yii\authclient\clients\LinkedIn',
						'clientId' => '818lz0v6wrmf8i',
						'clientSecret' => 'yL6y4hKmzutfukcn',
					],
					/* 'twitter' => [
						'class' => 'yii\authclient\clients\Twitter',
						'consumerKey' => 'OHynkA0U9dX5D3pCmCqM0puF5',
						'consumerSecret' => ' vMRFrEf581cmZxGcjyaVBajBhk7yiGKbOBYIBvgUYGJ76jp3Tk',
					],*/
				],
			],
			'cache' => [
				'class' => 'yii\caching\FileCache',
			],
			'errorHandler' => [
				'errorAction' => 'site/error',
			],
			'mailer' => [
				'class' => 'yii\swiftmailer\Mailer',
			],
			'urlManager' => [
				'rules' => [
					'login' => 'site/login',

					'<controller:\w+>/view/<slug:[\w-]+>' => '<controller>/view',
					'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
					'<controller:\w+>/cat/<slug:[\w-]+>' => '<controller>/cat',
				]
			],
			'assetManager' => [
				// uncomment the following line if you want to auto update your assets (unix hosting only)
				//'linkAssets' => true,
				'bundles' => [
					'yii\web\JqueryAsset' => [
						'js' => [YII_DEBUG ? 'jquery.js' : 'jquery.min.js'],
					],
					'yii\bootstrap\BootstrapAsset' => [
						'css' => [YII_DEBUG ? 'css/bootstrap.css' : 'css/bootstrap.min.css'],
					],
					'yii\bootstrap\BootstrapPluginAsset' => [
						'js' => [YII_DEBUG ? 'js/bootstrap.js' : 'js/bootstrap.min.js'],
					]
				]
			],
			'log' => [
				'traceLevel' => YII_DEBUG ? 3 : 0,
				'targets' => [
					[
						'class' => 'yii\log\FileTarget',
						'levels' => ['error', 'warning'],
						/*
						'class' => 'yii\log\EmailTarget',
						'mailer' => 'mailer',
						'levels' => ['error', 'warning'],
						'message' => [
							'from' => ['log@example.com'],
							'to' => ['amir@indicsoft.com'],
							'subject' => 'Log message'
						],*/
					]
				]
			],
			'db' => require(__DIR__ . '/db.php')
		],
		'params' => $params
	];

	if (YII_ENV_DEV) {
		 // configuration adjustments for 'dev' environment
		 $config['bootstrap'][] = 'debug';
		 $config['modules']['debug'] = 'yii\debug\Module';

		 $config['bootstrap'][] = 'gii';
		 $config['modules']['gii'] = 'yii\gii\Module';

		 $config['components']['db']['enableSchemaCache'] = false;
	}

	return array_merge_recursive($config, require($webroot . '/vendor/noumo/easyii/config/easyii.php'));
