<?php

return [
	'adminEmail' => 'admin@onspon.com',
    'supportEmail' => 'support@onspon.com',
	'user.passwordResetTokenExpire' => 3600,
	'dateFormat' => 'MM/DD/YYYY',
	'contactMessage' => 'Hello',
];
