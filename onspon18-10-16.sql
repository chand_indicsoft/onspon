-- MySQL dump 10.13  Distrib 5.6.33, for Linux (x86_64)
--
-- Host: localhost    Database: onspon_DB
-- ------------------------------------------------------
-- Server version	5.6.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `app_agegroup`
--

DROP TABLE IF EXISTS `app_agegroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_agegroup` (
  `agegroup_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `text` text,
  `slug` varchar(128) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`agegroup_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_agegroup`
--

LOCK TABLES `app_agegroup` WRITE;
/*!40000 ALTER TABLE `app_agegroup` DISABLE KEYS */;
INSERT INTO `app_agegroup` VALUES (1,'All','2016-08-19 11:03:21','2016-09-16 12:34:44','','all',1),(2,'Children (00-14 years)','2016-09-16 12:32:38','0000-00-00 00:00:00',NULL,'children-00-14-years',1),(3,'Youth (15-24 years)','2016-09-16 12:32:55','0000-00-00 00:00:00',NULL,'youth-15-24-years',1),(4,'Adults (25-64 years)','2016-09-16 12:33:11','0000-00-00 00:00:00',NULL,'adults-25-64-years',1),(5,'Seniors (65 years and Over)','2016-09-16 12:34:28','0000-00-00 00:00:00',NULL,'seniors-65-years-and-over',1);
/*!40000 ALTER TABLE `app_agegroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_amazing`
--

DROP TABLE IF EXISTS `app_amazing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_amazing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `event_id` int(11) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_amazing`
--

LOCK TABLES `app_amazing` WRITE;
/*!40000 ALTER TABLE `app_amazing` DISABLE KEYS */;
INSERT INTO `app_amazing` VALUES (2,'IIFA',2,'2016-10-03 11:09:55',NULL,1),(3,'Tech Fest',5,'2016-10-03 11:12:02',NULL,1),(4,'IPL',6,'2016-10-03 09:49:30',NULL,1),(5,'Digital India',10,'2016-10-03 09:49:38','2016-10-03 09:49:41',1),(6,'International Conference',8,'2016-10-03 09:57:12',NULL,1);
/*!40000 ALTER TABLE `app_amazing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_audience`
--

DROP TABLE IF EXISTS `app_audience`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_audience` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `image` varchar(128) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_audience`
--

LOCK TABLES `app_audience` WRITE;
/*!40000 ALTER TABLE `app_audience` DISABLE KEYS */;
INSERT INTO `app_audience` VALUES (1,'Youth',NULL,'2016-10-04 12:39:40','2016-10-04 12:41:33','youth',1),(2,'High Income Participants',NULL,'2016-10-04 01:00:38',NULL,'high-income-participants',1),(3,'Family',NULL,'2016-10-04 01:00:50',NULL,'family',1),(4,'Kids',NULL,'2016-10-04 01:00:57',NULL,'kids',1),(5,'test1111',NULL,'2016-10-14 12:20:13','2016-10-14 12:21:29','test1111',1);
/*!40000 ALTER TABLE `app_audience` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_box_tag`
--

DROP TABLE IF EXISTS `app_box_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_box_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `boxid` int(11) DEFAULT NULL,
  `tag_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_box_tag`
--

LOCK TABLES `app_box_tag` WRITE;
/*!40000 ALTER TABLE `app_box_tag` DISABLE KEYS */;
INSERT INTO `app_box_tag` VALUES (114,3,'6'),(115,3,'9'),(116,4,'1'),(117,4,'4'),(118,4,'14'),(119,5,'2'),(120,5,'3'),(121,6,'2'),(123,8,'10'),(124,8,'12'),(131,7,'1'),(132,2,'2'),(136,1,'2'),(137,1,'3'),(138,1,'14');
/*!40000 ALTER TABLE `app_box_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_boxgallery`
--

DROP TABLE IF EXISTS `app_boxgallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_boxgallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `image` varchar(128) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `text` text,
  `slug` varchar(128) DEFAULT NULL,
  `views` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  `tag_id` varchar(45) DEFAULT NULL,
  `boxname` varchar(45) DEFAULT NULL,
  `thumb_image` varchar(45) DEFAULT NULL COMMENT '																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																',
  `activate_url` tinyint(11) DEFAULT NULL,
  `url` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_boxgallery`
--

LOCK TABLES `app_boxgallery` WRITE;
/*!40000 ALTER TABLE `app_boxgallery` DISABLE KEYS */;
INSERT INTO `app_boxgallery` VALUES (1,'Navrang','/uploads/boxgallery/screen-shot-2016-10-12-at-65126--4bdb756f50.png','2016-09-23 04:14:06','2016-10-14 01:47:06',NULL,'navrang',0,1,'2,3,14','box1','',1,'http://www.onspon.com'),(2,'Our Recommendation Box 2','/uploads/boxgallery/g2-31d25d9d0b.jpg','2016-09-23 12:55:38','2016-10-14 11:39:15',NULL,'our-recommendation-box-2',0,1,'2','box2','/uploads/boxgallery/g2-e3f4b9d58f.jpg',0,''),(3,'Our Recommendation Box 3','/uploads/boxgallery/g3-c0fb896831.jpg','2016-09-23 01:07:11','2016-10-06 01:21:56',NULL,'our-recommendation-box-3',0,1,'6,9','box3','/uploads/boxgallery/g3-98e05d7e01.jpg',0,''),(4,'Our Recommendation Box 4','/uploads/boxgallery/g4-a8c51ae99d.jpg','2016-09-23 01:15:24','2016-10-06 01:22:01',NULL,'our-recommendation-box-4',0,1,'1,4,14','box4','/uploads/boxgallery/g4-3adc8e0c1b.jpg',0,''),(5,'Our Recommendation Box 5','/uploads/boxgallery/g5-493d80c752.jpg','2016-09-26 12:40:43','2016-10-06 01:22:07',NULL,'our-recommendation-box-5',0,1,'2,3','box5','/uploads/boxgallery/g5-71389ec247.jpg',0,''),(6,'Our Recommendation Box 6','/uploads/boxgallery/g6-d2ce30f414.jpg','2016-09-26 12:41:02','2016-10-06 01:22:22',NULL,'our-recommendation-box-6',0,1,'2','box6','/uploads/boxgallery/g6-e4daaff70f.jpg',0,''),(7,'Our Recommendation Box 7','/uploads/boxgallery/g7-4529cec830.jpg','2016-09-26 12:41:21','2016-10-14 11:38:52',NULL,'our-recommendation-box-7',0,1,'1','box7','/uploads/boxgallery/g7-5b7c0f6376.jpg',0,''),(8,'Our Recommendation Box 8','/uploads/boxgallery/g8-6094d4ba88.jpg','2016-09-26 12:41:52','2016-10-06 01:23:01',NULL,'our-recommendation-box-8',0,1,'10,12','box8','/uploads/boxgallery/g8-79acb4054d.jpg',0,'');
/*!40000 ALTER TABLE `app_boxgallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_brand`
--

DROP TABLE IF EXISTS `app_brand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_brand` (
  `brand_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `image` varchar(128) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `IsFeatured` int(11) DEFAULT NULL,
  `brandcategories_id` int(11) DEFAULT NULL,
  `featuredlogo` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`brand_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_brand`
--

LOCK TABLES `app_brand` WRITE;
/*!40000 ALTER TABLE `app_brand` DISABLE KEYS */;
INSERT INTO `app_brand` VALUES (1,'Universal','/uploads/brand/engage-7-ec97b62e67.png','','2016-08-19 12:42:19','2016-10-06 07:41:53','universal',1,1,NULL,'/uploads/brand/sponsor-1-be13f670b6.png'),(2,'Nike','/uploads/brand/logo-3870a1dfa7.png','','2016-08-19 12:49:41','2016-09-22 10:54:35','nike',1,0,NULL,'/uploads/brand/sponsor-2-0b2d858da9.png'),(3,'ECH Solution','/uploads/brand/engage-1-6d08614745.png','<p>ffdff</p>','2016-09-10 05:41:15','2016-10-06 07:42:01','ech-solution',1,1,NULL,'/uploads/brand/sponsor-2-c5d8a327ec.png'),(4,'Big soft','/uploads/brand/engage-2-cf8ec6ff18-8d75fdd721.png','<p>bigsoft</p>','2016-09-10 05:41:49','2016-10-06 07:42:09','big-soft',1,1,NULL,'/uploads/brand/sponsor-3-b60fffd8d2.png'),(5,'Bleubiri','/uploads/brand/engage-6-c8586ab88d.png','','2016-09-12 12:18:23','2016-10-06 07:42:17','bleubiri',1,1,NULL,'/uploads/brand/sponsor-4-ece50d62a4.png'),(6,'Sugar Berry','/uploads/brand/engage-4-ddb58555b1.png','','2016-09-12 12:20:50',NULL,'sugar-berry',1,NULL,NULL,NULL),(7,'Sarmen','/uploads/brand/engage-5-cb9468ecca.png','','2016-09-12 12:23:17','2016-10-06 07:42:25','sarmen',1,1,NULL,'/uploads/brand/sponsor-5-cfaa410db2.png'),(8,'CO','/uploads/brand/engage-3-b69e16c2bc.png','','2016-09-12 12:24:24',NULL,'co',1,NULL,NULL,NULL),(9,'Art Solutions','/uploads/brand/engage-8-61cbd766d8.png','','2016-09-12 12:25:49','2016-10-06 07:42:32','art-solutions',1,1,NULL,'/uploads/brand/sponsor-1-b91d7c5002.png'),(13,'Flipkart','/uploads/brand/flipkart-fc0a8b08d9.jpg','','2016-09-19 09:26:06','2016-09-22 10:53:52','flipkart',1,0,13,'/uploads/brand/sponsor-2-f04725c6f9.png'),(12,'Knorr','/uploads/brand/knorr-e516c428b2.jpg','','2016-09-19 09:25:02','2016-10-06 07:42:39','knorr',1,1,10,'/uploads/brand/sponsor-2-3967272da8.png'),(14,'Larsen & Tourbo','/uploads/brand/larsenandtoubro-659992119a.jpg','','2016-09-19 09:27:21','2016-10-06 07:42:46','larsen-tourbo',1,1,1,'/uploads/brand/sponsor-3-3a07dde925.png'),(15,'Line','/uploads/brand/line-7aaa908349.jpg','','2016-09-19 09:27:53','2016-10-06 07:42:54','line',1,1,2,'/uploads/brand/sponsor-4-4ce7aff59d.png'),(16,'VU','/uploads/brand/vu-d74393ef20.jpg','','2016-09-27 06:57:16',NULL,'vu',1,0,9,''),(17,'VLCC','/uploads/brand/vlcc-9f6acaaaef.jpg','','2016-09-27 06:57:47',NULL,'vlcc',1,0,10,''),(18,'Verisign','/uploads/brand/verisign-d7c2fc0dcb.jpg','','2016-09-27 06:58:11',NULL,'verisign',1,0,9,''),(19,'Varda Group','/uploads/brand/vardagroup-dda6d91ce7.jpg','','2016-09-27 06:58:51',NULL,'varda-group',1,0,12,''),(20,'Tata Motors','/uploads/brand/tatamotors-5c9b9bcd3f.jpg','','2016-09-27 06:59:12',NULL,'tata-motors',1,0,1,''),(21,'Tata Capital','/uploads/brand/tatacapital-83a928474a.jpg','','2016-09-27 06:59:58',NULL,'tata-capital',1,0,1,''),(23,'Fuji Film','/uploads/brand/fujifilm-a26e43d89a.jpg','','2016-09-27 07:01:03',NULL,'fuji-film',1,0,7,''),(24,'Fruit Shoot','/uploads/brand/fruitshoot-418d4c5ffa.jpg','','2016-09-27 07:01:37',NULL,'fruit-shoot',1,0,10,''),(25,'Lakme','/uploads/brand/lakme-1e845c5bf0.jpg','','2016-09-27 07:02:09',NULL,'lakme',1,0,10,''),(28,'Mahindra','/uploads/brand/mahindra-c62868df88.jpg','<p>Mahindra Brand</p>','2016-10-01 06:22:00',NULL,'mahindra',1,0,1,'');
/*!40000 ALTER TABLE `app_brand` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_brand_category_specific`
--

DROP TABLE IF EXISTS `app_brand_category_specific`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_brand_category_specific` (
  `brand_mandates_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_brand_category_specific`
--

LOCK TABLES `app_brand_category_specific` WRITE;
/*!40000 ALTER TABLE `app_brand_category_specific` DISABLE KEYS */;
INSERT INTO `app_brand_category_specific` VALUES (36,2),(36,3),(40,2),(41,1),(41,2),(41,3),(43,2),(35,2),(44,4),(46,2),(46,3),(48,1),(49,2),(49,3),(51,1),(54,1),(54,3),(54,4),(55,1),(55,2),(47,1),(34,1),(56,1),(56,2),(57,1),(57,2),(57,3),(58,1),(58,2);
/*!40000 ALTER TABLE `app_brand_category_specific` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_brand_sponsorship_objective`
--

DROP TABLE IF EXISTS `app_brand_sponsorship_objective`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_brand_sponsorship_objective` (
  `brand_mandates_id` int(11) NOT NULL,
  `sponsor_objective_id` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_brand_sponsorship_objective`
--

LOCK TABLES `app_brand_sponsorship_objective` WRITE;
/*!40000 ALTER TABLE `app_brand_sponsorship_objective` DISABLE KEYS */;
INSERT INTO `app_brand_sponsorship_objective` VALUES (36,'5'),(37,'3'),(38,'3'),(39,'4'),(40,'3'),(41,'1'),(41,'2'),(41,'3'),(41,'4'),(43,'6'),(35,'2'),(44,'2'),(46,'1'),(46,'2'),(46,'3'),(46,'4'),(48,'2'),(49,'2'),(49,'3'),(51,'1'),(54,'1'),(54,'2'),(54,'4'),(55,'1'),(55,'2'),(55,'3'),(47,'1'),(34,'1'),(56,'1'),(56,'2'),(57,'2'),(58,'1'),(58,'2');
/*!40000 ALTER TABLE `app_brand_sponsorship_objective` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_brandcategories`
--

DROP TABLE IF EXISTS `app_brandcategories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_brandcategories` (
  `brandcategories_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `image` varchar(128) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `text` text,
  `slug` varchar(128) DEFAULT NULL,
  `views` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`brandcategories_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_brandcategories`
--

LOCK TABLES `app_brandcategories` WRITE;
/*!40000 ALTER TABLE `app_brandcategories` DISABLE KEYS */;
INSERT INTO `app_brandcategories` VALUES (1,'Automobiles','/uploads/brandcategories/automobile-01-a78dc58951.jpg','2016-09-13 12:02:40',NULL,NULL,'automobiles',0,1),(2,'Mobiles','/uploads/brandcategories/mobiles-9b849dd633.png','2016-09-13 12:29:18','2016-09-19 11:07:15',NULL,'mobiles',0,1),(3,'Apparel and Accessories','/uploads/brandcategories/apparel-and-accessories-64f1baf274.jpg','2016-09-14 06:18:16','2016-09-14 06:18:47',NULL,'apparel-and-accessories',0,1),(4,'Banks & Financial Institution','/uploads/brandcategories/banks-financial-institution-f68c75a695.jpg','2016-09-16 12:36:49',NULL,NULL,'banks-financial-institution',0,1),(5,'Home Decor','/uploads/brandcategories/home-decor-0e1f0fbb35.jpg','2016-09-16 12:37:11',NULL,NULL,'home-decor',0,1),(6,'Education','/uploads/brandcategories/education-28578839c3.jpg','2016-09-16 12:37:27',NULL,NULL,'education',0,1),(7,'Entertainment','/uploads/brandcategories/entertainment3-5663750aa1.jpg','2016-09-16 12:37:52',NULL,NULL,'entertainment',0,1),(17,'nike2','/uploads/brandcategories/121-7c7ea56d15.png','2016-10-14 11:40:41',NULL,NULL,'nike2',0,1),(9,'Real Estate','/uploads/brandcategories/banks-financial-institution-ad1d298472.jpg','2016-09-16 12:39:14',NULL,NULL,'real-estate',0,1),(10,'Health Care','/uploads/brandcategories/healthcare-9c64355d86.jpg','2016-09-16 12:49:49',NULL,NULL,'health-care',0,1),(11,'Technology Products','/uploads/brandcategories/free-vector-digital-technology-p-f7b227b1dc.jpg','2016-09-16 12:50:03',NULL,NULL,'technology-products',0,1),(12,'Hospitality','/uploads/brandcategories/hospitality-65d956d559.jpg','2016-09-16 12:50:18',NULL,NULL,'hospitality',0,1),(13,'Retail and Etail','/uploads/brandcategories/retail-vs-etail-store-4140c77a52.jpg','2016-09-16 12:50:35',NULL,NULL,'retail-and-etail',0,1),(14,'Mobile App','/uploads/brandcategories/gofrugal-mobile-apps-40cc2d009d.png','2016-09-16 12:50:47',NULL,NULL,'mobile-app',0,1),(15,'Telecom','/uploads/brandcategories/telecomindustry600x450-8be98de4bc.jpg','2016-09-16 12:51:42',NULL,NULL,'telecom',0,1),(16,'Travel','/uploads/brandcategories/medical-tourism-in-asia-adcfc229da.jpg','2016-09-16 12:51:59',NULL,NULL,'travel',0,1);
/*!40000 ALTER TABLE `app_brandcategories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_brandmandates`
--

DROP TABLE IF EXISTS `app_brandmandates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_brandmandates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(256) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_type` int(11) DEFAULT NULL,
  `about_brand` text,
  `industry_id` int(11) DEFAULT NULL,
  `logo` varchar(256) DEFAULT NULL,
  `i_am_looking` text,
  `country_id` varchar(255) DEFAULT NULL,
  `city_id` varchar(255) DEFAULT NULL,
  `state_id` varchar(255) DEFAULT NULL,
  `age_group` int(11) DEFAULT '0',
  `education` int(11) DEFAULT NULL,
  `income` int(11) DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `question1` text,
  `question2` text,
  `visible_to_all_avenues` int(11) DEFAULT NULL,
  `lat` varchar(256) DEFAULT NULL,
  `lng` varchar(256) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `event_category_id` varchar(45) DEFAULT NULL,
  `specific_subjective` varchar(45) DEFAULT NULL,
  `visible_to_audience` int(11) DEFAULT NULL,
  `timeline` varchar(255) DEFAULT NULL,
  `start_date` varchar(45) DEFAULT NULL,
  `end_date` varchar(45) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `budget_range` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_brandmandates`
--

LOCK TABLES `app_brandmandates` WRITE;
/*!40000 ALTER TABLE `app_brandmandates` DISABLE KEYS */;
INSERT INTO `app_brandmandates` VALUES (34,'Testing by rashmee',8,1,'Testing Brand',1,'','qwertyuiop',' India','Delhi',' Delhi',1,2,2,'Female','qwertyuiop','asdfghjkl',0,'28.56028','77.29133150000007','2016-10-03 11:43:23','2016-10-14 11:21:32',1,'testing-by-rashmee','1','1',0,'specific duration','1476248261','1476248261','Okhla, New Delhi, Delhi, India',1),(35,'Gima',15,1,'Gima Brand',2,'/uploads/brandmandates/pic-3-9348762fc7.jpg','asdfghjkl',' India',' New Delhi',' Delhi',3,4,3,'Male','lkjhgfds','poiutrew',0,'28.5503314','77.25018929999999','2016-10-03 11:44:41','2016-10-06 07:14:42',1,'gima','2','2',0,'Open timeline','','','Nehru Place, New Delhi, Delhi, India',1),(36,'Mahindra',8,1,'Mahindra Brand',1,'/uploads/brandmandates/pic-2-234ae2468c.jpg','zxcvbnm',' United States','Fargo',' ND',4,5,3,'Female','poiutrew','sdfghjk',0,'46.8771863','-96.78980339999998','2016-10-03 11:45:57','2016-10-03 11:46:33',1,'mahindra','5','2,3',0,NULL,'1476143040','1476488640','Fargo, ND, United States',1),(37,'ABC',8,1,'Testing',2,'/uploads/brandmandates/pic-3-c3e47f1313.png','Testing',' India',' Gurgaon',' Haryana',NULL,NULL,NULL,'','','',0,'28.4669448','77.06651999999997','2016-10-05 10:13:51','2016-10-05 10:15:38',1,'abc','3','',0,NULL,'1475791920','1475878320','Sector 29, Gurgaon, Haryana, India',NULL),(38,'XYZ',8,1,'qwertyuio',2,'/uploads/brandmandates/pic-d35b59886b.jpg','qwertyuio',' United States','Seattle',' WA',NULL,NULL,NULL,'','','',0,'47.6062095','-122.3320708','2016-10-05 10:16:54',NULL,1,'xyz','3','',0,NULL,'1476396960','1477088160','Seattle, WA, United States',NULL),(39,'DEF',8,1,'Testing',4,'/uploads/brandmandates/pic-3-ff07372f9e.jpg','qwertyuio',' India',' Noida',' Uttar Pradesh',NULL,NULL,NULL,'','','',0,'28.6207641','77.36392920000003','2016-10-05 10:19:39',NULL,1,'def','4','',0,NULL,'1475662736','1475662736','Sector 62, Noida, Uttar Pradesh, India',NULL),(40,'PQR',8,1,'qwertyuiop',2,'/uploads/brandmandates/pic-2-0f9d01bf27.jpg','poiuytrew',' United States','Detroit',' MI',NULL,NULL,NULL,'','','',0,'42.331427','-83.0457538','2016-10-05 10:20:57',NULL,1,'pqr','3','2',0,NULL,'1476224340','1476483540','Detroit, MI, United States',NULL),(41,'Venus ',22,1,'ddvjkjjdj sdckndcndnc',1,'uploads/brandmandates/1475734410-1.jpg.jpg','sdakjhasdfhj sdkbKDFBKbdfbd AJDSBCKbdkcbkjBD',' India','','Delhi',1,1,1,'Male','DWCc','asASC',0,'28.7040592','77.10249019999992','2016-10-06 06:13:30','2016-10-06 06:14:19',1,'venus','1,2,3,4','1,2,3',0,NULL,'1475734363','1475734363','Delhi, India',1),(42,'puma2',22,1,'its a  dummy brand',2,'','testing ',NULL,'',NULL,1,1,1,'Male','','',0,'','','2016-10-06 06:13:45','2016-10-06 06:48:03',1,'puma2','','',1,'','','',NULL,NULL),(43,'puma2',22,1,'its a  dummy brand',2,'uploads/brandmandates/1475734430-1.jpg','testing ',' India',' Noida',' Uttar Pradesh',1,2,4,'Male','','',0,'28.6207641','77.36392920000003','2016-10-06 06:13:50','2016-10-06 06:15:25',1,'puma2-2','6','2',1,NULL,'1475734326','1475734326','Sector 62, Noida, Uttar Pradesh, India',1),(44,'Testing',8,1,'qwertyuiop',2,'','mnbvcx','','','',NULL,NULL,NULL,'','','',0,'','','2016-10-06 09:33:27',NULL,1,'testing','2','4',0,'','','','',NULL),(45,'abcewfiu23',22,1,'awehfhwefh efhwefh',2,'uploads/brandmandates/1475759820-current-dashboard-top.png',';890p0p70p70p70p',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-06 01:17:00',NULL,1,'abcewfiu23',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL),(46,'abcewfiu23',22,1,'awehfhwefh efhwefh',2,'uploads/brandmandates/1475759821-current-dashboard-top.png',';890p0p70p70p70p',NULL,'Delhi',NULL,1,1,1,'Male','','',0,'','','2016-10-06 01:17:01','2016-10-06 01:17:29',1,'abcewfiu23-2','1,2,3,4','2,3',0,'Open timeline','','',NULL,1),(47,'dammy6thoct',22,1,'dghgsd',1,'','dsgdfgfdgdg',NULL,'Delhi',NULL,1,2,2,'Male','','',0,'','','2016-10-06 01:21:33','2016-10-14 10:55:09',1,'dammy6thoct','1','1',0,'Open timeline','','',NULL,1),(48,'imagecheak',22,1,'gsafghdsagfghldshgf',1,'uploads/brandmandates/1475760261-Cl0TWK.jpg','asfd',NULL,'',NULL,1,1,1,'Male','','',0,'','','2016-10-06 01:24:21','2016-10-06 01:24:33',1,'imagecheak','2','1',0,'','','',NULL,1),(50,'onspon2',22,1,'dummy brand',1,'uploads/brandmandates/1475908248-Market.jpg','fearwell party on ',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-08 06:30:48',NULL,1,'onspon2',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(51,'onspon2',22,1,'dummy test of brand mandate create',1,'uploads/brandmandates/1475908329-Market.jpg','hgfjhgjgdm',NULL,'',NULL,1,1,1,'Male','','',0,'','','2016-10-08 06:32:09','2016-10-08 06:35:51',1,'onspon2-2','1','1',0,'','','',NULL,NULL),(52,'trsfd',22,1,'sdfzbv',1,'','dfhgf',NULL,'',NULL,1,1,1,'Male','','',0,'','','2016-10-10 06:14:57','2016-10-10 06:16:17',1,'trsfd','','',0,'','','',NULL,NULL),(53,'fghfgjfghk',8,1,'fghfgjgh',2,'','fghfgj',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-14 05:51:08',NULL,1,'fghfgjfghk',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL),(54,'sadjbjb',22,1,'djdjkabjbkjb asdjnajsdnkjdjf',1,'uploads/brandmandates/1476430194-current-dashboard-top.png','sadcdsc;m asdlcasdcanjnjj sdakDNKJN',NULL,'Delhi',NULL,1,1,1,'Male','dwdfmklm wefwfnk','sadncjwdnjkndj',1,'','','2016-10-14 07:29:54','2016-10-14 08:00:48',1,'sadjbjb','1,2,4','1,3,4',1,'Open timeline','','',NULL,1),(55,'test14oct',22,1,'dfffffffffffffffs',1,'uploads/brandmandates/1476442250-9-SEO-marketing-hd-wallpaper-1920x1080.jpg','sggggggggg',NULL,'Delhi',NULL,1,2,4,'Male','','',0,'','','2016-10-14 10:50:50','2016-10-14 10:51:43',1,'test14oct','1,2,3','1,2',0,'Open timeline','','',NULL,1),(56,'test14iioct',22,1,'asdfjjklasjfsakd',2,'/uploads/brandmandates/337b2ca6749e78b8b8d418b96d6fc01b-e7656fa154.png','fasdddddd','india','delhi','delhi',1,2,4,'Male','ggfdgfdh','fhgfgf',1,'','','2016-10-14 11:24:09',NULL,1,'test14iioct','1,2','1,2',1,'Open timeline','','','asfsdafadsfasfdsa',1),(57,'fdvc ',22,1,'xzvc',5,'uploads/brandmandates/1476538576-9-SEO-marketing-hd-wallpaper-1920x1080.jpg','xc',NULL,'Delhi',NULL,NULL,NULL,NULL,'','','',0,'','','2016-10-15 01:34:25','2016-10-15 01:36:22',1,'fdvc','2','1,2,3',0,'Open timeline','','',NULL,1),(58,'test 15 oct',22,1,'dummy brand',2,'uploads/brandmandates/1476710069-ffa85475a2b44cc793c1e752985b808e_small.png','dfhfhfhfhfhfhfhfhfhg',NULL,'Delhi',NULL,1,7,4,'Male','','',0,'','','2016-10-17 01:14:29','2016-10-17 01:15:03',1,'test-15-oct','1,2','1,2',1,'Open timeline','','',NULL,1);
/*!40000 ALTER TABLE `app_brandmandates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_city`
--

DROP TABLE IF EXISTS `app_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_city` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `text` text,
  `slug` varchar(128) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`city_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_city`
--

LOCK TABLES `app_city` WRITE;
/*!40000 ALTER TABLE `app_city` DISABLE KEYS */;
INSERT INTO `app_city` VALUES (1,'Delhi',0,0,'2016-08-18 09:33:51','2016-08-18 09:36:28','','delhi',1);
/*!40000 ALTER TABLE `app_city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_country`
--

DROP TABLE IF EXISTS `app_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_country` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `text` text,
  `slug` varchar(128) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`country_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_country`
--

LOCK TABLES `app_country` WRITE;
/*!40000 ALTER TABLE `app_country` DISABLE KEYS */;
INSERT INTO `app_country` VALUES (1,'India','2016-08-18 12:02:30','0000-00-00 00:00:00','','india',1);
/*!40000 ALTER TABLE `app_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_education`
--

DROP TABLE IF EXISTS `app_education`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_education` (
  `education_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `text` text,
  `slug` varchar(128) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`education_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_education`
--

LOCK TABLES `app_education` WRITE;
/*!40000 ALTER TABLE `app_education` DISABLE KEYS */;
INSERT INTO `app_education` VALUES (1,'B.A','2016-08-19 12:00:40','0000-00-00 00:00:00','','ba',1),(2,'B.Tech','2016-09-12 09:54:18',NULL,NULL,'btech',1),(3,'B.Pharma','2016-09-12 09:54:37',NULL,NULL,'bpharma',1),(4,'MBA','2016-09-12 09:54:48',NULL,NULL,'mba',1),(5,'BCA','2016-09-16 12:54:29',NULL,NULL,'bca',1),(6,'MCA','2016-09-16 12:54:38',NULL,NULL,'mca',1),(7,'All','2016-09-16 01:01:52',NULL,NULL,'all',1);
/*!40000 ALTER TABLE `app_education` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_event_tag`
--

DROP TABLE IF EXISTS `app_event_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_event_tag` (
  `event_id` int(11) NOT NULL,
  `tag_id` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_event_tag`
--

LOCK TABLES `app_event_tag` WRITE;
/*!40000 ALTER TABLE `app_event_tag` DISABLE KEYS */;
INSERT INTO `app_event_tag` VALUES (10,'7'),(10,'8'),(10,'9'),(9,'1'),(9,'3'),(9,'7'),(8,'9'),(8,'13'),(4,'1'),(4,'4'),(4,'12'),(6,'2'),(6,'13'),(7,'2'),(5,'1'),(5,'2'),(5,'4'),(5,'6'),(5,'12'),(5,'13'),(3,'1'),(3,'4'),(3,'13'),(2,'1'),(2,'4'),(2,'13');
/*!40000 ALTER TABLE `app_event_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_event_user_shortlist`
--

DROP TABLE IF EXISTS `app_event_user_shortlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_event_user_shortlist` (
  `app_event_user_shortlist_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `shortlist_title` varchar(128) NOT NULL,
  `event_id` int(11) NOT NULL,
  `date_created` date NOT NULL,
  `status` tinyint(4) NOT NULL,
  `views` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_event_user_shortlist`
--

LOCK TABLES `app_event_user_shortlist` WRITE;
/*!40000 ALTER TABLE `app_event_user_shortlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `app_event_user_shortlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_event_viewby_brand`
--

DROP TABLE IF EXISTS `app_event_viewby_brand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_event_viewby_brand` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `usertype` varchar(45) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_event_viewby_brand`
--

LOCK TABLES `app_event_viewby_brand` WRITE;
/*!40000 ALTER TABLE `app_event_viewby_brand` DISABLE KEYS */;
INSERT INTO `app_event_viewby_brand` VALUES (1,2,8,'1','2016-10-01 06:23:02'),(3,3,8,'1','2016-10-01 06:36:11'),(4,4,8,'1','2016-10-01 06:37:32'),(5,5,8,'1','2016-10-01 07:20:43'),(6,9,22,'1','2016-10-01 11:51:54'),(7,251,22,'1','2016-10-03 07:15:27'),(8,257,22,'1','2016-10-03 07:16:02'),(9,78,8,'1','2016-10-04 07:31:29'),(10,2,31,'1','2016-10-05 10:00:48'),(11,4,31,'1','2016-10-05 10:01:50'),(12,3,22,'1','2016-10-06 06:08:55'),(13,7,22,'1','2016-10-06 06:24:52'),(14,5,22,'1','2016-10-06 06:25:48'),(15,4,22,'1','2016-10-08 06:54:52'),(16,651,8,'1','2016-10-13 01:11:08');
/*!40000 ALTER TABLE `app_event_viewby_brand` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_eventcategories`
--

DROP TABLE IF EXISTS `app_eventcategories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_eventcategories` (
  `eventcategories_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `image` varchar(128) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `text` text,
  `slug` varchar(128) DEFAULT NULL,
  `views` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  `onhome` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`eventcategories_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_eventcategories`
--

LOCK TABLES `app_eventcategories` WRITE;
/*!40000 ALTER TABLE `app_eventcategories` DISABLE KEYS */;
INSERT INTO `app_eventcategories` VALUES (1,'Sports','/uploads/eventcategories/sports-f5598f90ae.png','2016-08-17 12:11:08','2016-10-03 12:35:54','','sports',0,1,1),(2,'Awards','/uploads/eventcategories/awards-1d1d241c91.jpg','2016-09-09 10:46:25','2016-10-03 12:36:15',NULL,'awards',0,1,1),(3,'College Fest','/uploads/eventcategories/college-fest-e518e9932f.jpg','2016-09-09 10:46:44','2016-10-03 12:36:30',NULL,'college-fest',0,1,1),(4,'Conference / Exhibition','/uploads/eventcategories/conference3-a6d1fcee9d.jpg','2016-09-09 10:46:51','2016-10-14 02:09:26',NULL,'conference-exhibition',0,1,1),(5,'Digital','/uploads/eventcategories/digital-867bd8d97e.jpg','2016-09-12 05:21:24','2016-10-04 08:45:23',NULL,'digital',0,1,0),(6,'Entertainment','/uploads/eventcategories/entertainment-1-69efa38262.jpg','2016-09-12 05:22:14','2016-10-03 12:36:44',NULL,'entertainment',0,1,1);
/*!40000 ALTER TABLE `app_eventcategories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_eventgenre`
--

DROP TABLE IF EXISTS `app_eventgenre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_eventgenre` (
  `eventgenre_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `image` varchar(128) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `text` text,
  `slug` varchar(128) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`eventgenre_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_eventgenre`
--

LOCK TABLES `app_eventgenre` WRITE;
/*!40000 ALTER TABLE `app_eventgenre` DISABLE KEYS */;
INSERT INTO `app_eventgenre` VALUES (1,'Health and Wellness','','2016-09-23 09:04:51',NULL,NULL,'health-and-wellness',1),(2,'Music','','2016-09-23 09:05:26',NULL,NULL,'music',1),(3,'Fashion','','2016-09-23 09:05:41',NULL,NULL,'fashion',1),(4,'Sports','','2016-09-23 09:05:54',NULL,NULL,'sports',1),(5,'Leisure (Non Music)','','2016-09-23 09:06:21',NULL,NULL,'leisure-non-music',1),(6,'Startups','','2016-09-24 12:56:11',NULL,NULL,'startups',1),(7,'Luxury','','2016-09-24 12:56:27',NULL,NULL,'luxury',1),(8,'Industry specific','','2016-09-24 12:57:19',NULL,NULL,'industry-specific',1),(9,'Digital','','2016-09-24 12:59:06',NULL,NULL,'digital',1),(10,'Religion and Spiritual','','2016-09-24 12:59:52',NULL,NULL,'religion-and-spiritual',1),(11,'Literature and Quizzing','','2016-09-24 01:00:23',NULL,NULL,'literature-and-quizzing',1),(12,'Others','','2016-09-24 01:00:41',NULL,NULL,'others',1),(13,'rakesh rajput','/uploads/eventgenre/11582954-ritratto-di-gruppo-mult-ea48fb2cac.jpg','2016-10-14 11:45:24',NULL,NULL,'rakesh-rajput',1),(14,'singh','/uploads/eventgenre/430d19c15a87c9914bd562468570a45d-be2b572376.png','2016-10-14 11:52:28',NULL,NULL,'singh',1);
/*!40000 ALTER TABLE `app_eventgenre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_events`
--

DROP TABLE IF EXISTS `app_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_name` varchar(256) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_type` int(11) DEFAULT NULL,
  `event_category_id` int(11) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `ref_no` int(11) DEFAULT NULL,
  `logo` varchar(256) DEFAULT NULL,
  `image` varchar(256) DEFAULT NULL,
  `sprice_from` decimal(10,2) DEFAULT NULL,
  `sprice_to` decimal(10,2) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `event_date` datetime DEFAULT NULL,
  `incorporation_yr` int(11) DEFAULT NULL,
  `venue_name` varchar(256) DEFAULT NULL,
  `address` varchar(256) DEFAULT NULL,
  `city_id` varchar(256) DEFAULT NULL,
  `state_id` varchar(256) DEFAULT NULL,
  `country_id` varchar(256) DEFAULT NULL,
  `lat` varchar(256) DEFAULT NULL,
  `lng` varchar(256) DEFAULT NULL,
  `event_strt_date` int(11) DEFAULT NULL,
  `event_end_date` int(11) DEFAULT NULL,
  `event_strt_time` varchar(256) DEFAULT NULL,
  `event_end_time` varchar(256) DEFAULT NULL,
  `website` varchar(256) DEFAULT NULL,
  `fb_page` varchar(256) DEFAULT NULL,
  `fb_likes` int(11) DEFAULT NULL,
  `pitch_pdf` varchar(256) DEFAULT NULL,
  `tag_id` varchar(256) DEFAULT NULL,
  `url` varchar(256) DEFAULT NULL,
  `no_of_attendees` varchar(256) DEFAULT NULL,
  `every_yr` tinyint(4) DEFAULT NULL,
  `televised` tinyint(4) DEFAULT NULL,
  `ticketed` tinyint(4) DEFAULT NULL,
  `videos_link` varchar(256) DEFAULT NULL,
  `earlier_sponsor` varchar(256) DEFAULT NULL,
  `ticket_type` varchar(256) DEFAULT NULL,
  `ticket_text` varchar(256) DEFAULT NULL,
  `premium` varchar(45) DEFAULT NULL,
  `age_group_rest` int(11) DEFAULT NULL,
  `education_most` int(11) DEFAULT NULL,
  `education_rest` int(11) DEFAULT NULL,
  `income_most` int(11) DEFAULT NULL,
  `income_rest` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `gender_most` varchar(45) DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `completed` tinyint(4) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `short` varchar(1024) DEFAULT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `views` int(11) DEFAULT '0',
  `event_genre_id` varchar(45) DEFAULT NULL,
  `banner` varchar(45) DEFAULT NULL,
  `why_sponsor` text,
  `age_group_most` int(11) DEFAULT NULL,
  `address1` varchar(512) DEFAULT NULL,
  `zipcode` varchar(45) DEFAULT NULL,
  `contact` varchar(15) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `print_media` varchar(128) DEFAULT NULL,
  `ooh_media` varchar(128) DEFAULT NULL,
  `radio` varchar(128) DEFAULT NULL,
  `television` varchar(128) DEFAULT NULL,
  `emailers` varchar(128) DEFAULT NULL,
  `in_premise_posters` varchar(128) DEFAULT NULL,
  `digital_outreach` varchar(128) DEFAULT NULL,
  `slabname` varchar(245) DEFAULT NULL,
  `sponsorprice` varchar(245) DEFAULT NULL,
  `deliverables` varchar(245) DEFAULT NULL,
  `verified` tinyint(4) DEFAULT '0',
  `ticket_branding` tinyint(4) DEFAULT NULL,
  `database_collecttion` tinyint(4) DEFAULT NULL,
  `pre_event_buzz` tinyint(4) DEFAULT NULL,
  `event_passes_ticket` tinyint(4) DEFAULT NULL,
  `audience_profile` varchar(245) DEFAULT NULL,
  `basic_visibility` tinyint(4) DEFAULT NULL,
  `experience_zone` tinyint(4) DEFAULT NULL,
  `sales_counter` tinyint(4) DEFAULT NULL,
  `wifi_enable` tinyint(4) DEFAULT NULL,
  `automobile_display` tinyint(4) DEFAULT NULL,
  `parking_space` tinyint(4) DEFAULT NULL,
  `common_areas` tinyint(4) DEFAULT NULL,
  `vip_box` tinyint(4) DEFAULT NULL,
  `post_event_party` tinyint(4) DEFAULT NULL,
  `paid_media` tinyint(4) DEFAULT NULL,
  `social_media_sharing` tinyint(4) DEFAULT NULL,
  `celebrity_tweets` tinyint(4) DEFAULT NULL,
  `alchohol_licence` tinyint(4) DEFAULT NULL,
  `sampling` tinyint(4) DEFAULT NULL,
  `speaking_opportunities` tinyint(4) DEFAULT NULL,
  `org_id` int(11) DEFAULT NULL,
  `multicity_event` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=732 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_events`
--

LOCK TABLES `app_events` WRITE;
/*!40000 ALTER TABLE `app_events` DISABLE KEYS */;
INSERT INTO `app_events` VALUES (2,'IIFA',8,2,2,'<p>IIFA Awards</p>',1234567890,'/uploads/events/iifa-3-7b56ffa979.jpg','/uploads/events/iifa-2013iifaawards2013ebuzztoda-0d8b201853.jpg',1000000.00,5000000.00,1473670893,NULL,2016,'','Mayur Vihar, New Delhi, Delhi, India',' New Delhi',' Delhi',' India','28.6146243','77.31215750000001',1474094280,1474699116,'02:30:30 PM','07:15:30 PM','abc','abc',100,'','1,4,13','http://demosoft.indicsoft.com/iifa','800',1,1,0,NULL,'Nike',NULL,'','0',3,2,1,2,1,'2016-09-12 09:01:33','','2016-10-10 05:38:01',1,1,NULL,'iifa',760,'1','/uploads/events/iifa-banner-c1b2cfc460.png','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text <br>ever since the 1500s, when an unknown printer took a galley.',NULL,NULL,NULL,NULL,NULL,'All','0-10000','10001-25000','25001-100000','100000+','All','0-10000',NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'Navrang',9,2,2,'<p>Navrang Awards Description</p>',NULL,'/uploads/events/navrang-aaa6502aeb.jpg','/uploads/events/navrang1-51f0b99d07.jpg',100000.00,500000.00,1473671149,NULL,2015,'','Pragati Maidan, New Delhi, Delhi, India',' New Delhi',' Delhi',' India','28.6159804','77.24438239999995',1473800580,1474664580,'08:30:00 AM','10:30:15 PM','','',876,'','1,4,13','http://demosoft.indicsoft.com/navrang','200',1,1,0,NULL,'ABC, DEF',NULL,'','0',1,1,3,1,3,'2016-09-12 09:05:49','Male','2016-09-30 09:27:30',0,1,NULL,'navrang',974,'1','/uploads/events/awards-ce2a72effe.jpg','ABCDEFGHIJKLMNOPQRSTUVWXYZ',1,NULL,NULL,NULL,NULL,'All','0-10000','10001-25000','25001-100000','100000+','0-10000','All',NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,'Esperanza',8,2,3,'',NULL,'/uploads/events/college-fest-7-50ea20b1f0.jpg','/uploads/events/college-fest-4-ce16883dde.jpg',200000.00,500000.00,1473672052,NULL,NULL,'','India Gate, Rajpath, New Delhi, Delhi, India',' New Delhi',' Delhi',' India','28.612912','77.2295097',1474319940,1474579140,'08:30:45 AM','09:30:45 PM','','',564,'','1,4,12','http://demosoft.indicsoft.com/esperanza','',1,1,1,NULL,'',NULL,'','0',1,1,1,1,2,'2016-09-12 09:20:52','','2016-09-30 09:10:23',0,1,NULL,'esperanza',343,NULL,'/uploads/events/esperanza-7e53dce0ee.jpg','wertyuio',NULL,NULL,NULL,NULL,NULL,'All','0-10000','10001-25000','25001-100000','100000+','All','0-10000',NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,'Tech Fest',9,2,3,'<p>College Fest - Tech Fest</p>',NULL,'/uploads/events/techfest-18f8b5e2ad.jpg','/uploads/events/college-fest-3-f728a3a53a.jpg',2345678.00,9876543.00,1473672301,NULL,NULL,'','Okhla Phase II, New Delhi, Delhi, India',' New Delhi',' Delhi',' India','28.5357628','77.27643290000003',1473456120,1474233780,'09:45:00 AM','11:00:00 PM','','',765,'','1,2,4,6,12,13','http://demosoft.indicsoft.com/tech-fest','',0,0,0,NULL,'',NULL,'','0',1,1,1,2,1,'2016-09-12 09:25:01','','2016-09-30 09:20:58',0,1,NULL,'tech-fest',987,NULL,'/uploads/events/tech-fest21-d4e6b83401.png','',NULL,NULL,NULL,NULL,NULL,'All','0-10000','10001-25000','25001-100000','25001-100000','100000+','10001-25000',NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,'IPL',9,2,1,'',NULL,'/uploads/events/ipl-2-ab688bd43b.jpg','/uploads/events/mumbai-indians-ipl-champions-110-5524982e8e.jpg',NULL,NULL,1473673417,NULL,NULL,'','San Francisco, CA, United States','San Francisco',' CA',' United States','37.7749295','-122.41941550000001',1473708600,1473795000,'03:00:30 PM','03:00:30 PM','','',543,'','2,13','http://demosoft.indicsoft.com/ipl','',0,0,0,NULL,'',NULL,'','0',2,3,3,4,2,'2016-09-12 09:43:37','Female','2016-09-30 09:13:04',0,1,NULL,'ipl',276,NULL,'/uploads/events/ipl-1-1827b9f430.jpg','',1,NULL,NULL,NULL,NULL,'All','0-10000','10001-25000','25001-100000','100000+','0-10000','All',NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,'Kabaddi Championship',8,2,1,'',NULL,'/uploads/events/kabaddi-9654504b42.jpg','/uploads/events/1435136widefullscreen-cf27990ff0.jpg',NULL,NULL,1473674537,NULL,NULL,'','Sector 24, Haryana, India','Sector 24',' Haryana',' India','28.4945095','77.09961299999998',1474267860,1474613460,'03:15:45 PM','03:15:45 PM','','',453,'','2','http://demosoft.indicsoft.com/kabaddi-championship','',0,0,0,NULL,'',NULL,'','0',2,3,4,2,3,'2016-09-12 10:02:17','Female','2016-09-30 09:17:25',0,1,NULL,'kabaddi-championship',86,NULL,'/uploads/events/kabaddi-2-89d9b875c1.jpg','',1,NULL,NULL,NULL,NULL,'10001-25000','0-10000','100000+','10001-25000','25001-100000','0-10000','10001-25000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,'International Conference',8,2,4,'',NULL,'/uploads/events/conference6-046a10c44f.jpg','/uploads/events/evp0141-764f0aeee2.jpg',NULL,NULL,1473676119,NULL,NULL,'','Chicago, IL, United States','Chicago',' IL',' United States','41.8781136','-87.62979819999998',1473146340,1473405540,'03:45:30 PM','03:45:30 PM','','',325,'','9,13','http://demosoft.indicsoft.com/international-conference','',0,0,0,NULL,'',NULL,'','0',1,2,2,3,4,'2016-09-12 10:28:39','Female','2016-09-30 07:26:18',0,1,NULL,'international-conference',543,NULL,'/uploads/events/conference5-2c25887b1b.jpg','',1,NULL,NULL,NULL,NULL,'All','0-10000','10001-25000','25001-100000','100000+','100000+','All',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,'ABC',9,2,6,'<p>Testing</p>',123,'/uploads/events/sports-award-abab9b3f7a.jpg','/uploads/events/entertainment4-379889ec39.jpg',100000.00,500000.00,1473676584,NULL,2009,'','Sector 8, Faridabad, Haryana, India',' Faridabad',' Haryana',' India','28.3618853','77.33531399999993',1474414260,1474500660,'04:00:15 PM','04:00:15 PM','','',567,'','1,3,7','http://demosoft.indicsoft.com/abc','700',0,0,0,NULL,'Puma',NULL,'','0',1,1,3,2,3,'2016-09-12 10:36:24','Female','2016-09-30 07:25:07',0,1,NULL,'abc',43,NULL,'/uploads/events/award-bfa40ef3f0.jpg','Testing',1,NULL,NULL,NULL,NULL,'All','10001-25000','0-10000','25001-100000','100000+','10001-25000','25001-100000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(622,'efefe',NULL,2,4,'',NULL,NULL,NULL,NULL,NULL,1476336951,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476336929,1476336929,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe','5000-20000',0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 05:35:51',NULL,'2016-10-13 05:36:26',NULL,1,NULL,'efefe',0,'3',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,1,0,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,NULL),(623,'efefe',NULL,2,4,'',NULL,NULL,NULL,NULL,NULL,1476337221,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476337212,1476337212,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-2','200-1000',0,0,0,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 05:40:21',NULL,'2016-10-13 05:41:26',NULL,1,NULL,'efefe-2',0,'2',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,1,'2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,NULL),(624,'efefe',NULL,2,2,'',NULL,NULL,NULL,NULL,NULL,1476337426,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476337317,1476337317,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-3','200-1000',1,1,1,NULL,NULL,NULL,'http://gsrthemes.com/aaika/fullwidth/contact.html',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 05:43:46',NULL,'2016-10-13 06:07:06',NULL,1,NULL,'efefe-3',0,'1,3,5',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1,0,1,1,'2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,NULL),(625,'efefe',NULL,2,1,'',NULL,NULL,NULL,NULL,NULL,1476339053,NULL,NULL,NULL,'delhi',NULL,NULL,NULL,'','',1476338914,1476338914,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-4','5000-20000',0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 06:10:53',NULL,'2016-10-13 06:11:31',NULL,1,NULL,'efefe-4',0,'1',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,'2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,NULL),(626,'efefe',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476340038,NULL,NULL,NULL,'delhi',NULL,NULL,NULL,'','',1476339981,1476339981,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-5','50-200',0,1,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 06:27:18',NULL,'2016-10-13 06:28:48',NULL,1,NULL,'efefe-5',0,'1,4',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,NULL),(627,'ask',27,2,1,'',NULL,NULL,NULL,NULL,NULL,1476340836,NULL,NULL,'','Batla House, New Delhi, Delhi, India','','','','28.5691957','77.28864240000007',1476337504,1476337493,'09:00:00 AM','09:00:00 PM','','',NULL,'','','http://demosoft.indicsoft.com/ask','',0,0,1,NULL,'',NULL,'www.ticket.com','0',NULL,NULL,NULL,NULL,NULL,'2016-10-13 06:40:36','','2016-10-13 12:21:10',0,1,NULL,'ask',0,'',NULL,'',NULL,'','','8989898989','asset@gmail.com','','','','','','','','','','',0,1,0,1,1,'2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,NULL),(628,'wwww',NULL,2,1,'',NULL,NULL,NULL,NULL,NULL,1476341233,NULL,NULL,NULL,'Batla House, New Delhi, Delhi, India',NULL,NULL,NULL,'28.5691957','77.28864240000007',1476341199,1476341199,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/wwww','200-1000',0,0,1,NULL,NULL,NULL,'www.tikj.com',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 06:47:13',NULL,'2016-10-13 06:47:53',NULL,1,NULL,'wwww',0,'2',NULL,NULL,NULL,'','','8998989898','ade@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(629,'aqasa',NULL,2,2,'',NULL,NULL,NULL,NULL,NULL,1476341593,NULL,NULL,NULL,'VFS Global Services, New Delhi, Delhi, India',NULL,NULL,NULL,'28.6139391','77.20902120000005',1476341574,1476341574,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/aqasa','50-200',1,1,1,NULL,NULL,NULL,'eeeeedad.sdds',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 06:53:13',NULL,'2016-10-13 06:55:31',NULL,1,NULL,'aqasa',0,'',NULL,NULL,NULL,'','','2222222222','ss@ff.dd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','',0,1,1,1,1,'1',1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,NULL,NULL),(630,'efefe',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476341971,NULL,NULL,NULL,'Democracy Boulevard, Bethesda, MD, United States',NULL,NULL,NULL,'','',1476341931,1476341931,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-6','200-1000',0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 06:59:32',NULL,'2016-10-13 07:00:03',NULL,1,NULL,'efefe-6',0,'3',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,1,'2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,NULL),(631,'efefe',NULL,2,4,'',NULL,NULL,NULL,NULL,NULL,1476342496,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476342449,1476342449,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-7','200-1000',0,1,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 07:08:16',NULL,'2016-10-13 07:08:47',NULL,1,NULL,'efefe-7',0,'1',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,1,1,'3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,NULL),(632,'efefe',NULL,2,NULL,'',NULL,NULL,NULL,NULL,NULL,1476342871,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1476342653,1476342653,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 07:14:31',NULL,NULL,NULL,1,NULL,'efefe-8',0,NULL,NULL,NULL,NULL,NULL,NULL,'8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(633,'efefe',NULL,2,4,'',NULL,NULL,NULL,NULL,NULL,1476343006,NULL,NULL,NULL,'delhi',NULL,NULL,NULL,'','',1476342933,1476342933,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-9','',0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 07:16:46',NULL,'2016-10-13 08:15:29',NULL,1,NULL,'efefe-9',0,'2',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(634,'efefe',NULL,2,2,'',NULL,NULL,NULL,NULL,NULL,1476348005,NULL,NULL,NULL,'delhi',NULL,NULL,NULL,'','',1476347989,1476347989,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-10','50-200',0,0,0,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 08:40:05',NULL,'2016-10-13 08:41:56',NULL,1,NULL,'efefe-10',0,'',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','',0,0,0,1,1,'2',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL),(635,'efefe',NULL,2,2,'',NULL,NULL,NULL,NULL,NULL,1476348916,NULL,NULL,NULL,'delhi',NULL,NULL,NULL,'','',1476348855,1476348855,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-11','50-200',0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 08:55:16',NULL,'2016-10-13 08:55:42',NULL,1,NULL,'efefe-11',0,'',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,1,'2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,NULL),(636,'efefe',NULL,2,6,'',NULL,NULL,NULL,NULL,NULL,1476349077,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476349067,1476349067,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-12','50-200',0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 08:57:57',NULL,'2016-10-13 08:58:28',NULL,1,NULL,'efefe-12',0,'1',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,',',',',',',0,0,0,0,1,'2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,NULL),(637,'efefe',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476349439,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476349415,1476349415,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-13','200-1000',0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 09:03:59',NULL,'2016-10-13 09:04:30',NULL,1,NULL,'efefe-13',0,'1',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,',',',',',',0,0,0,0,1,'3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,NULL),(638,'efefe',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476349708,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476349690,1476349690,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-14','',0,0,0,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 09:08:28',NULL,'2016-10-13 09:08:59',NULL,1,NULL,'efefe-14',0,'',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','',0,0,0,0,0,'',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL),(639,'efefe',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476350125,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476350095,1476350095,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-15','200-1000',0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 09:15:25',NULL,'2016-10-13 09:16:19',NULL,1,NULL,'efefe-15',0,'1',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,',,',',,',',,',0,0,0,0,1,'2',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL),(640,'efefe',NULL,2,4,'',NULL,NULL,NULL,NULL,NULL,1476350785,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476350768,1476350768,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-16','5000-20000',0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 09:26:25',NULL,'2016-10-13 09:33:05',NULL,1,NULL,'efefe-16',0,'2',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,',',',',',',0,0,0,0,0,'1',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL),(641,'efefe',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476351238,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476351212,1476351212,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-17','',0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 09:33:58',NULL,'2016-10-13 09:34:19',NULL,1,NULL,'efefe-17',0,'2',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(642,'efefe',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476351578,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476351564,1476351564,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-18','5000-20000',0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 09:39:38',NULL,'2016-10-13 09:45:15',NULL,1,NULL,'efefe-18',0,'1',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,',,',',,',',,',0,0,0,0,0,'2',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL),(643,'delhi',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476352260,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476352132,1476352132,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/delhi','20000+',0,1,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 09:51:00',NULL,'2016-10-13 09:51:22',NULL,1,NULL,'delhi',0,'1,3,5',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(644,'efefe',NULL,2,2,'',NULL,NULL,NULL,NULL,NULL,1476352391,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476352358,1476352358,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-19','5000-20000',0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 09:53:11',NULL,'2016-10-13 09:53:53',NULL,1,NULL,'efefe-19',0,'1,5',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,',',',',',',0,0,0,0,0,'3',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL),(645,'efefe',NULL,2,2,'',NULL,NULL,NULL,NULL,NULL,1476352512,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476352483,1476352483,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-20',NULL,0,1,0,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 09:55:12',NULL,'2016-10-13 09:55:29',NULL,1,NULL,'efefe-20',0,'1',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(646,'efefe',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476352969,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476352628,1476352628,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-21','200-1000',0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 10:02:49',NULL,'2016-10-13 10:08:32',NULL,1,NULL,'efefe-21',0,'2',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,'3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,NULL),(647,'delhi',NULL,2,4,'',NULL,NULL,NULL,NULL,NULL,1476353951,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476353931,1476353931,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/delhi-2','200-1000',0,1,0,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 10:19:11',NULL,'2016-10-13 10:20:11',NULL,1,NULL,'delhi-2',0,'2',NULL,NULL,NULL,'','','8888888888','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,'2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,NULL),(648,'efefe',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476354605,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476354575,1476354575,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-22','20000+',0,1,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 10:30:05',NULL,'2016-10-13 10:30:30',NULL,1,NULL,'efefe-22',0,'1',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,NULL),(649,'efefe',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476354840,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476354722,1476354722,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-23','50-200',0,1,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 10:34:00',NULL,'2016-10-13 10:42:57',NULL,1,NULL,'efefe-23',0,'1,5',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','',0,0,0,0,0,'2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,NULL),(650,'chek',NULL,2,4,'',NULL,NULL,NULL,NULL,NULL,1476362581,NULL,NULL,NULL,'Baton Rouge, LA, United States',NULL,NULL,NULL,'30.4582829','-91.1403196',1476362532,1476362532,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/chek','200-1000',0,0,1,NULL,NULL,NULL,'www.ghgh.com',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 12:43:01',NULL,'2016-10-13 12:44:10',NULL,1,NULL,'chek',0,'',NULL,NULL,NULL,'','','4545454545','qwerty@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','',0,0,1,0,0,'2',0,0,0,0,0,0,1,0,0,1,0,0,0,0,0,NULL,1),(651,'kjjk',NULL,2,NULL,'',NULL,NULL,NULL,NULL,NULL,1476363486,NULL,NULL,NULL,'Batla House, New Delhi, Delhi, India',NULL,NULL,NULL,'28.5691957','77.28864240000007',1476363432,1476363432,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/kjjk',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 12:58:06',NULL,'2016-10-13 12:58:18',NULL,1,NULL,'kjjk',0,NULL,NULL,NULL,NULL,'gh','','9090909090','asw@jdjk.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(652,'efefe',NULL,2,2,'',NULL,NULL,NULL,NULL,NULL,1476364774,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',NULL,NULL,'','',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-24','',0,1,0,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 01:19:34',NULL,'2016-10-13 01:20:07',NULL,1,NULL,'efefe-24',0,'',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','',0,0,0,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,0),(653,'efefe',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476364840,NULL,NULL,NULL,'DMV Pasadena, South Rosemead Boulevard, Pasadena, CA, United States',NULL,NULL,NULL,'34.1384028','-118.0732395',1476364822,1476364822,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-25','5000-20000',0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 01:20:40',NULL,'2016-10-13 01:21:05',NULL,1,NULL,'efefe-25',0,'',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,'2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,0),(654,'delhi',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476365190,NULL,NULL,NULL,'delhi',NULL,NULL,NULL,'','',1476365168,1476365168,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/delhi-3',NULL,0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 01:26:30',NULL,'2016-10-13 01:26:42',NULL,1,NULL,'delhi-3',0,'',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),(655,'efefe',NULL,2,2,'',NULL,NULL,NULL,NULL,NULL,1476365318,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476365307,1476365307,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-26','200-1000',0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 01:28:38',NULL,'2016-10-13 01:29:18',NULL,1,NULL,'efefe-26',0,'',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,',',',',',',0,0,0,0,0,'2',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0),(656,'sgsg',NULL,2,2,'',NULL,NULL,NULL,NULL,NULL,1476366993,NULL,NULL,NULL,'John Jay High School, North Salem Road, Cross River, NY, United States',NULL,NULL,NULL,'41.2843313','-73.61229879999996',1476366963,1476366963,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/sgsg',NULL,0,0,0,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-13 01:56:33',NULL,'2016-10-13 01:56:51',NULL,1,NULL,'sgsg',0,'2,4,6',NULL,NULL,NULL,'','','5656565656','shgsh@hjhj.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),(657,'sads',NULL,2,NULL,'',NULL,NULL,NULL,NULL,NULL,1476423607,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1476423568,1476423568,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-14 05:40:07',NULL,NULL,NULL,1,NULL,'sads',0,NULL,NULL,NULL,NULL,NULL,NULL,'8989898989','sjkkjkk@kjkj.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(658,'sasa34',NULL,2,4,'',NULL,NULL,NULL,NULL,NULL,1476423864,NULL,NULL,NULL,'Batla House, New Delhi, Delhi, India',NULL,NULL,NULL,'28.5691957','77.28864240000007',1476423723,1476423723,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/sasa34',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-14 05:44:24',NULL,'2016-10-14 05:44:32',NULL,1,NULL,'sasa34',0,'',NULL,NULL,NULL,'','','3434343434','sss@ff.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(659,'ads m',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476425212,NULL,NULL,NULL,'Madya Pradesh Pavilion, Pragati Maidan, New Delhi, Delhi, India',NULL,NULL,NULL,'28.6158187','77.2420869',1477436400,1477872000,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/ads-m','Less than 50',0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-14 06:06:52',NULL,'2016-10-14 06:08:09',NULL,1,NULL,'ads-m',0,'5,8',NULL,NULL,NULL,'','','9350008640','shash_shukla@yahoo.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','',0,1,1,1,1,'1,2,3',1,1,1,1,0,0,0,0,0,0,0,1,0,0,0,NULL,1),(660,'efefe',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476429270,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476429244,1476429244,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-27','50-200',0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-14 07:16:05',NULL,'2016-10-14 07:19:28',NULL,1,NULL,'efefe-27',0,'',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','',0,0,0,1,1,'3',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0),(661,'delhi',NULL,2,2,'',NULL,NULL,NULL,NULL,NULL,1476429797,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476429646,1476429646,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/delhi-4','200-1000',0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-14 07:23:17',NULL,'2016-10-14 07:23:56',NULL,1,NULL,'delhi-4',0,'1',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,'2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,0),(662,'efefe',NULL,2,4,'',NULL,NULL,NULL,NULL,NULL,1476436077,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476429853,1476429853,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-28','5000-20000',0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-14 09:07:57',NULL,'2016-10-14 09:08:23',NULL,1,NULL,'efefe-28',0,'5',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,'3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,0),(663,'efefe',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476436621,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476436599,1476436599,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-29',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-14 09:17:01',NULL,'2016-10-14 09:17:07',NULL,1,NULL,'efefe-29',0,'',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(664,'efefe',NULL,2,4,'',NULL,NULL,NULL,NULL,NULL,1476439163,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476439134,1476439134,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-14 09:59:23',NULL,'2016-10-14 09:59:29',NULL,1,NULL,'efefe-30',0,'',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(665,'delhi',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476439893,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476439886,1476439886,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/delhi-5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-14 10:11:33',NULL,'2016-10-14 10:11:40',NULL,1,NULL,'delhi-5',0,'3',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(666,'delhi',NULL,2,6,'',NULL,NULL,NULL,NULL,NULL,1476439992,NULL,NULL,NULL,'Delhi Cantonment, New Delhi, Delhi, India',NULL,NULL,NULL,'28.5961279','77.15873750000003',1476439983,1476439983,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/delhi-6',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-14 10:13:12',NULL,'2016-10-14 10:13:19',NULL,1,NULL,'delhi-6',0,'1',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(667,'efefe',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476440759,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476440750,1476440750,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-31',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-14 10:25:59',NULL,'2016-10-14 10:26:05',NULL,1,NULL,'efefe-31',0,'12',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(668,'sgedgdgd',NULL,2,2,'',NULL,NULL,NULL,NULL,NULL,1476442040,NULL,NULL,NULL,'sdafaf',NULL,NULL,NULL,'','',1476442017,1476442017,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/sgedgdgd','200-1000',0,1,0,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-14 10:47:20',NULL,'2016-10-14 10:48:06',NULL,1,NULL,'sgedgdgd',0,'1,3',NULL,NULL,NULL,'dsafa','','8781245698','dfgvxs@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'sadf','sadf','faa',0,1,0,0,0,'2',1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,1),(669,'delhi',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476442959,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476442907,1476442907,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/delhi-7','50-200',0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-14 11:02:39',NULL,'2016-10-14 11:05:42',NULL,1,NULL,'delhi-7',0,'10',NULL,NULL,NULL,'','','8802793958','islamnazrul8408@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,0),(670,'delhi',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476443526,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476443145,1476443145,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/delhi-8','50-200',1,1,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-14 11:12:06',NULL,'2016-10-14 11:12:45',NULL,1,NULL,'delhi-8',0,'1,2,3',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,1,1,'3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,0),(671,'delhi',NULL,2,4,'',NULL,NULL,NULL,NULL,NULL,1476443919,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476443910,1476443910,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/delhi-9','5000-20000',0,1,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-14 11:18:39',NULL,'2016-10-14 11:19:09',NULL,1,NULL,'delhi-9',0,'2,8',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,'2,3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,0),(672,'sss',NULL,2,NULL,'',NULL,NULL,NULL,NULL,NULL,1476446007,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1476445983,1476445983,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-14 11:53:27',NULL,NULL,NULL,1,NULL,'sss',0,NULL,NULL,NULL,NULL,NULL,NULL,'8787877878','sgg@h.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(673,'klklkl',NULL,2,NULL,'',NULL,NULL,NULL,NULL,NULL,1476448034,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1476448020,1476448020,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-14 12:27:14',NULL,NULL,NULL,1,NULL,'klklkl',0,NULL,NULL,NULL,NULL,NULL,NULL,'9090909090','sdf@tfg.jkjk',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(674,'dmdnm',NULL,2,NULL,'',NULL,NULL,NULL,NULL,NULL,1476448383,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1476448368,1476448368,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-14 12:33:03',NULL,NULL,NULL,1,NULL,'dmdnm',0,NULL,NULL,NULL,NULL,NULL,NULL,'8787878787','kjjskj@kjkjd.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(675,'mjkj',NULL,2,NULL,'',NULL,NULL,NULL,NULL,NULL,1476448673,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1476448485,1476448485,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-14 12:37:53',NULL,NULL,NULL,1,NULL,'mjkj',0,NULL,NULL,NULL,NULL,NULL,NULL,'9898988989','hjhjhjhj@fjhh.kjjh',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(676,'bhjh',NULL,2,2,'',NULL,NULL,NULL,NULL,NULL,1476449118,NULL,NULL,NULL,'Barcelona, Spain',NULL,NULL,NULL,'41.3850639','2.1734034999999494',1476449092,1476449092,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/bhjh',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-14 12:45:18',NULL,'2016-10-14 12:45:29',NULL,1,NULL,'bhjh',0,'',NULL,NULL,NULL,'','','9898988989','sdd@dfd.bnm',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(677,'abdfb',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476450299,NULL,NULL,NULL,'Pragati Maidan, New Delhi, Delhi, India',NULL,NULL,NULL,'28.6159804','77.24438239999995',1476831600,1477872000,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/abdfb','50-200',0,1,0,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-14 01:09:17',NULL,'2016-10-14 01:12:23',NULL,1,NULL,'abdfb',0,'4,7,9',NULL,NULL,NULL,'','','9350008640','shash_shukla@yahoo.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','',0,0,0,0,0,'3',0,0,0,0,1,0,1,0,0,1,1,0,0,0,0,NULL,0),(678,'shsh',NULL,2,NULL,'',NULL,NULL,NULL,NULL,NULL,1476450749,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1476450714,1476450714,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-14 01:12:29',NULL,NULL,NULL,1,NULL,'shsh',0,NULL,NULL,NULL,NULL,NULL,NULL,'9898989898','assa@jhj.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(679,'asss',NULL,2,NULL,'',NULL,NULL,NULL,NULL,NULL,1476452775,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1476452483,1476452483,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-14 01:46:15',NULL,NULL,NULL,1,NULL,'asss',0,NULL,NULL,NULL,NULL,NULL,NULL,'+91-9559905118','aaaa@gm.dj',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(680,'jxxhj',NULL,2,NULL,'',NULL,NULL,NULL,NULL,NULL,1476453122,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1476453092,1476453092,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-14 01:52:02',NULL,NULL,NULL,1,NULL,'jxxhj',0,NULL,NULL,NULL,NULL,NULL,NULL,'+91-9898989898','hsjshjhs@fff.ddd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(681,'djhdjhj',NULL,2,NULL,'',NULL,NULL,NULL,NULL,NULL,1476453293,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1476453255,1476453255,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-14 01:54:53',NULL,NULL,NULL,1,NULL,'djhdjhj',0,NULL,NULL,NULL,NULL,NULL,NULL,'+91-9898989898','sss@jdjjd.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(682,'ajhashj',NULL,2,2,'',NULL,NULL,NULL,NULL,NULL,1476453944,NULL,NULL,NULL,'Batla House, New Delhi, Delhi, India',NULL,NULL,NULL,'28.5691957','77.28864240000007',1476453892,1476453892,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/ajhashj','',0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-14 02:05:44',NULL,'2016-10-14 02:10:51',NULL,1,NULL,'ajhashj',0,'1,3,5',NULL,NULL,NULL,'','','+91-9898989898','aaa@edd.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','',0,0,0,0,0,'',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0),(683,'kjkkj',NULL,2,NULL,'',NULL,NULL,NULL,NULL,NULL,1476454361,NULL,NULL,NULL,'Morley, Batley, United Kingdom',NULL,NULL,NULL,'53.717028','-1.6350830000000087',1476454306,1476454306,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/kjkkj',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-14 02:12:41',NULL,'2016-10-14 02:13:00',NULL,1,NULL,'kjkkj',0,NULL,NULL,NULL,NULL,'','','+91-9898989898','cgfgf@fgfg.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(684,'efefe',NULL,2,NULL,'',NULL,NULL,NULL,NULL,NULL,1476508980,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1476508906,1476508906,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-15 05:23:00',NULL,NULL,NULL,1,NULL,'efefe-32',0,NULL,NULL,NULL,NULL,NULL,NULL,'8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(685,'efefe',NULL,2,6,'',NULL,NULL,NULL,NULL,NULL,1476510007,NULL,NULL,NULL,'delhi',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476509909,1476509909,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-33','200-1000',0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-15 05:40:07',NULL,'2016-10-15 05:42:58',NULL,1,NULL,'efefe-33',0,'1',NULL,NULL,NULL,'','','8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','',0,0,0,0,0,'4',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0),(686,'efefe',NULL,2,NULL,'',NULL,NULL,NULL,NULL,NULL,1476510010,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1476509909,1476509909,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-15 05:40:10',NULL,NULL,NULL,1,NULL,'efefe-34',0,NULL,NULL,NULL,NULL,NULL,NULL,'8802793958','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(687,'delhi',NULL,2,4,'',NULL,NULL,NULL,NULL,NULL,1476511039,NULL,NULL,NULL,'delhi',NULL,NULL,NULL,'','',1476510261,1476510261,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/delhi-10','',0,0,0,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-15 05:57:19',NULL,'2016-10-15 05:57:47',NULL,1,NULL,'delhi-10',0,'7',NULL,NULL,NULL,'','','8888888888','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,'3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,0),(688,'efefe',NULL,2,5,'',NULL,NULL,NULL,NULL,NULL,1476514514,NULL,NULL,NULL,'delhi',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476513290,1476513290,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-35','5000-20000',0,0,0,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-15 06:55:14',NULL,'2016-10-15 06:55:50',NULL,1,NULL,'efefe-35',0,'',NULL,NULL,NULL,'','','+91-345346','kjdf@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,'4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,0),(689,'delhi',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476514690,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'','',1476514577,1476514577,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/delhi-11','50-200',0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-15 06:58:10',NULL,'2016-10-15 07:04:27',NULL,1,NULL,'delhi-11',0,'12',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,'3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,0),(690,'delhi',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476516703,NULL,NULL,NULL,'delhi',NULL,NULL,NULL,'','',1476516531,1476516531,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/delhi-12','200-1000',0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-15 07:31:43',NULL,'2016-10-15 07:32:18',NULL,1,NULL,'delhi-12',0,'10',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,1,0,0,'3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,0),(691,'efefe',NULL,2,4,'',NULL,NULL,NULL,NULL,NULL,1476518914,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'','',1476518889,1476518889,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-36','20000+',0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-15 08:08:34',NULL,'2016-10-15 08:09:15',NULL,1,NULL,'efefe-36',0,'4',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,'2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,0),(692,'efefe',NULL,2,2,'',NULL,NULL,NULL,NULL,NULL,1476520075,NULL,NULL,NULL,'delhi',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476519228,1476519228,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-37',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-15 08:27:55',NULL,'2016-10-15 08:28:03',NULL,1,NULL,'efefe-37',0,'',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(693,'efefe',NULL,2,2,'',NULL,NULL,NULL,NULL,NULL,1476524926,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476524914,1476524914,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-38',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-15 09:48:46',NULL,'2016-10-15 09:48:55',NULL,1,NULL,'efefe-38',0,'',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(694,'delhi',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476525431,NULL,NULL,NULL,'delhi',NULL,NULL,NULL,'','',1476525267,1476525267,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/delhi-13',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-15 09:57:11',NULL,'2016-10-15 09:57:18',NULL,1,NULL,'delhi-13',0,'4',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(695,'efefe',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476526220,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'','',1476526124,1476526124,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-39',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-15 10:10:20',NULL,'2016-10-15 10:10:30',NULL,1,NULL,'efefe-39',0,'4',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(696,'efefe',NULL,2,2,'',NULL,NULL,NULL,NULL,NULL,1476526607,NULL,NULL,NULL,'delhi',NULL,NULL,NULL,'','',1476526497,1476526497,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-40',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-15 10:16:47',NULL,'2016-10-15 10:16:56',NULL,1,NULL,'efefe-40',0,'10',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(697,'delhi',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476528862,NULL,NULL,NULL,'delhi',NULL,NULL,NULL,'','',1476528444,1476528444,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/delhi-14',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-15 10:54:22',NULL,'2016-10-15 10:54:29',NULL,1,NULL,'delhi-14',0,'',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(698,'Maktest',NULL,2,2,'',NULL,NULL,NULL,NULL,NULL,1476539974,NULL,NULL,NULL,'OUR NCR, New Delhi, Delhi, India',NULL,NULL,NULL,'28.6139391','77.20902120000005',1476539561,1476539561,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/maktest','200-1000',1,0,0,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-15 01:59:34',NULL,'2016-10-15 02:06:19',NULL,1,NULL,'maktest',0,'9',NULL,NULL,NULL,'A-16, FF, DDA Shed, Okhla Industrial Phase -2','110020','+91-9971037773','akhtar@indicsoft.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','',0,0,1,0,0,'1,3',0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0),(699,'efefe',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476679289,NULL,NULL,NULL,'delhi',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476679448,1476679448,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-41','200-1000',0,1,0,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-17 04:44:35',NULL,'2016-10-17 04:45:47',NULL,1,NULL,'efefe-41',0,'8',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','',0,0,0,0,0,'4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,0),(700,'efefe',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476682079,NULL,NULL,NULL,'delhi',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476681768,1476681768,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-42','5000-20000',0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-17 05:27:59',NULL,'2016-10-17 05:28:29',NULL,1,NULL,'efefe-42',0,'6',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','',0,0,0,0,0,'2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,0),(701,'efefe',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476686871,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476687052,1476687052,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-43','5000-20000',0,1,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-17 06:50:58',NULL,'2016-10-17 06:51:26',NULL,1,NULL,'efefe-43',0,'6,8,10',NULL,NULL,NULL,'','','+91-363636454','islamnazrul8408@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','',0,0,0,0,0,'2,3',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,30,0),(702,'delhi',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476687177,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476687170,1476687170,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/delhi-15','200-1000',0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-17 06:52:57',NULL,'2016-10-17 06:53:37',NULL,1,NULL,'delhi-15',0,'4,6,8',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','',0,0,0,0,0,'3',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,48,0),(703,'efefe',NULL,2,2,'',NULL,NULL,NULL,NULL,NULL,1476687472,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476687464,1476687464,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-44','5000-20000',0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-17 06:57:52',NULL,'2016-10-17 06:58:26',NULL,1,NULL,'efefe-44',0,'3,5,6',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'delhi','','',0,0,0,0,1,'2',1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,49,0),(704,'ITC',NULL,2,NULL,'',NULL,NULL,NULL,NULL,NULL,1476690838,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1476729000,1476729000,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-17 07:53:58',NULL,NULL,NULL,1,NULL,'itc',0,NULL,NULL,NULL,NULL,NULL,NULL,'+91-9654774439','sme.akhtar@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(705,'efefe',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476704362,NULL,NULL,NULL,'delhi',NULL,NULL,NULL,'','',1476704349,1476704349,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-45','200-1000',0,0,0,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-17 11:39:22',NULL,'2016-10-17 11:39:45',NULL,1,NULL,'efefe-45',0,'6',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,'3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,1),(706,'efefe',NULL,2,5,'',NULL,NULL,NULL,NULL,NULL,1476704871,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'','',1476704861,1476704861,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-46','5000-20000',0,1,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-17 11:47:51',NULL,'2016-10-17 11:48:18',NULL,1,NULL,'efefe-46',0,'5,7,9',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,'4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,0,0,0,NULL,0),(707,'efefe',NULL,2,4,'',NULL,NULL,NULL,NULL,NULL,1476705657,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476705635,1476705635,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-47','200-1000',0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-17 12:00:57',NULL,'2016-10-17 12:03:14',NULL,1,NULL,'efefe-47',0,'5,7',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,'2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,0),(708,'efefe',NULL,2,4,'',NULL,NULL,NULL,NULL,NULL,1476706063,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476706034,1476706034,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-48','',0,1,0,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-17 12:07:43',NULL,'2016-10-17 12:08:06',NULL,1,NULL,'efefe-48',0,'2',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,0),(709,'efefe',NULL,2,NULL,'',NULL,NULL,NULL,NULL,NULL,1476706658,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-17 12:17:38',NULL,NULL,NULL,1,NULL,'efefe-49',0,NULL,NULL,NULL,NULL,NULL,NULL,'+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(710,'efefe',NULL,2,2,'',NULL,NULL,NULL,NULL,NULL,1476706769,NULL,NULL,NULL,'Delhi Cantonment, New Delhi, Delhi, India',NULL,NULL,NULL,'','',1476706749,1476706749,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-50','200-1000',0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-17 12:19:29',NULL,'2016-10-17 12:20:25',NULL,1,NULL,'efefe-50',0,'5',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,'4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,0),(711,'efefe',NULL,2,4,'',NULL,NULL,NULL,NULL,NULL,1476707024,NULL,NULL,NULL,'delhi',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476707080,1476707080,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-51','200-1000',0,1,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-17 12:30:17',NULL,'2016-10-17 12:30:47',NULL,1,NULL,'efefe-51',0,'5',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,1,'2,3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,0),(712,'efefe',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476707753,NULL,NULL,NULL,'Delhi Cantonment, New Delhi, Delhi, India',NULL,NULL,NULL,'28.5961279','77.15873750000003',1476707741,1476707741,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-52','50-200',0,0,0,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-17 12:35:53',NULL,'2016-10-17 12:36:19',NULL,1,NULL,'efefe-52',0,'3,5',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,'3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,1),(713,'delhi',NULL,2,4,'',NULL,NULL,NULL,NULL,NULL,1476707852,NULL,NULL,NULL,'Delhi Cantonment, New Delhi, Delhi, India',NULL,NULL,NULL,'28.5961279','77.15873750000003',1476707833,1476707833,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/delhi-16','5000-20000',0,1,0,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-17 12:37:32',NULL,'2016-10-17 12:37:57',NULL,1,NULL,'delhi-16',0,'7',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1,0,0,0,'4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,0),(714,'efefe',NULL,2,2,'',NULL,NULL,NULL,NULL,NULL,1476707986,NULL,NULL,NULL,'Delhi, LA, United States',NULL,NULL,NULL,'','',1476707964,1476707964,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-53','50-200',0,1,0,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-17 12:39:46',NULL,'2016-10-17 12:40:15',NULL,1,NULL,'efefe-53',0,'3,5',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,1,'4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,0),(715,'efefe',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476708105,NULL,NULL,NULL,'Delhi Cantonment, New Delhi, Delhi, India',NULL,NULL,NULL,'28.5961279','77.15873750000003',1476708060,1476708060,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-54','200-1000',1,0,0,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-17 12:41:45',NULL,'2016-10-17 12:43:06',NULL,1,NULL,'efefe-54',0,'7,9',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'delhi','','',0,0,0,1,0,'',0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0),(716,'efefe',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476708497,NULL,NULL,NULL,'Dlhili Bhot, Lucknow, Uttar Pradesh, India',NULL,NULL,NULL,'26.8466937','80.94616599999995',1476708196,1476708196,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-55','5000-20000',0,1,0,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-17 12:48:17',NULL,'2016-10-17 12:48:53',NULL,1,NULL,'efefe-55',0,'10',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'delhi','','',0,0,0,0,0,'4',1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,50,0),(717,'efefe',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476708747,NULL,NULL,NULL,'Delhi, CA, United States',NULL,NULL,NULL,'37.4321589','-120.77853540000001',1476708665,1476708665,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-56','200-1000',0,0,0,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-17 12:52:27',NULL,'2016-10-17 12:54:18',NULL,1,NULL,'efefe-56',0,'6',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),(718,'delhi',NULL,2,4,'',NULL,NULL,NULL,NULL,NULL,1476708939,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476708870,1476708870,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/delhi-17','5000-20000',0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-17 12:55:39',NULL,'2016-10-17 12:56:31',NULL,1,NULL,'delhi-17',0,'6',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','',0,0,0,0,1,'3',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0),(719,'delhi',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476709072,NULL,NULL,NULL,'Delhi Cantonment, New Delhi, Delhi, India',NULL,NULL,NULL,'28.5961279','77.15873750000003',1476709055,1476709055,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/delhi-18','200-1000',0,1,0,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-17 12:57:52',NULL,'2016-10-17 01:06:46',NULL,1,NULL,'delhi-18',0,'5',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'delhi','','',0,0,0,0,0,'3',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,51,0),(720,'efefe',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476710278,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476710269,1476710269,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-57','200-1000',1,0,0,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-17 01:17:58',NULL,'2016-10-17 01:21:30',NULL,1,NULL,'efefe-57',0,'10',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','',0,0,0,0,0,'3',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0),(721,'efefe',NULL,2,4,'',NULL,NULL,NULL,NULL,NULL,1476710549,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476710543,1476710543,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-58','5000-20000',0,0,0,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-17 01:22:29',NULL,'2016-10-17 01:23:02',NULL,1,NULL,'efefe-58',0,'7',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'delhi','','',0,0,0,0,1,'3',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,52,1),(722,'efefe',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476711327,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476711238,1476711238,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-59',NULL,0,1,0,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-17 01:35:27',NULL,'2016-10-17 01:35:40',NULL,1,NULL,'efefe-59',0,'5',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),(723,'efefe',NULL,2,2,'',NULL,NULL,NULL,NULL,NULL,1476711398,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476711372,1476711372,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-60','20000+',0,1,0,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-17 01:36:38',NULL,'2016-10-17 01:37:18',NULL,1,NULL,'efefe-60',0,'6,8',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'delhi','','',0,0,0,0,0,'4',1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,53,0),(724,'asasdfdear',NULL,2,1,'',NULL,NULL,NULL,NULL,NULL,1476711574,NULL,NULL,NULL,'Gurudwara Bangla Sahib, Sansad Marg Area, New Delhi, Delhi, India',NULL,NULL,NULL,'','',1476711553,1476711553,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/asasdfdear','Less than 50',0,1,0,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-17 01:39:34',NULL,'2016-10-17 01:40:36',NULL,1,NULL,'asasdfdear',0,'3,5',NULL,NULL,NULL,'','','+91-9350008640','shash_shukla@yahoo.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','',0,1,1,1,0,'2,3',0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,58,0),(725,'dummy15thoct',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476711682,NULL,NULL,NULL,'Govt Sr Sec School(Boys),SOHNA, 50, Gurgaon - Sohna Road, Sohna, Haryana, India',NULL,NULL,NULL,'28.3494692','77.06591179999998',1477378800,1477897200,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/dummy15thoct','50-200',1,1,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-17 01:41:22',NULL,'2016-10-17 01:42:45',NULL,1,NULL,'dummy15thoct',0,'3,4,7',NULL,NULL,NULL,'','','+91-7530889602','rakesh.singh7476@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','',0,1,1,1,1,'1,2,4',1,1,0,0,0,0,0,0,0,1,1,1,0,0,0,NULL,1),(726,'efefe',NULL,2,2,'',NULL,NULL,NULL,NULL,NULL,1476767821,NULL,NULL,NULL,'delhi',NULL,NULL,NULL,'','',1476767788,1476767788,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-61','20000+',0,1,0,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-18 05:17:01',NULL,'2016-10-18 05:17:41',NULL,1,NULL,'efefe-61',0,'5',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','',0,0,0,0,0,'3',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0),(727,'efefe',NULL,2,4,'',NULL,NULL,NULL,NULL,NULL,1476767986,NULL,NULL,NULL,'Delhi Cantonment, New Delhi, Delhi, India',NULL,NULL,NULL,'','',1476767909,1476767909,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-62','200-1000',0,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-18 05:19:46',NULL,'2016-10-18 05:20:32',NULL,1,NULL,'efefe-62',0,'1',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'delhi','','',0,0,0,0,0,'3',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0),(728,'delhi',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476768091,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476768081,1476768082,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/delhi-19','20000+',0,1,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-18 05:21:31',NULL,'2016-10-18 05:22:10',NULL,1,NULL,'delhi-19',0,'5,7',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'delhi','','',0,0,0,0,0,'3',1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0),(729,'efefe',NULL,2,6,'',NULL,NULL,NULL,NULL,NULL,1476768257,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476768215,1476768215,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-63','200-1000',1,0,0,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-18 05:24:17',NULL,'2016-10-18 05:24:51',NULL,1,NULL,'efefe-63',0,'3,5',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'delhi','','',0,0,0,0,0,'4',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,1),(730,'delhi',NULL,2,3,'',NULL,NULL,NULL,NULL,NULL,1476768365,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476768357,1476768357,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/delhi-20','5000-20000',0,0,0,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-18 05:26:05',NULL,'2016-10-18 05:26:28',NULL,1,NULL,'delhi-20',0,'5',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,'4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,1),(731,'efefe',NULL,2,2,'',NULL,NULL,NULL,NULL,NULL,1476768695,NULL,NULL,NULL,'Delhi, India',NULL,NULL,NULL,'28.7040592','77.10249019999992',1476768470,1476768470,'09:00:00 AM','09:00:00 PM',NULL,NULL,NULL,NULL,NULL,'http://demosoft.indicsoft.com/efefe-64','20000+',0,1,0,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2016-10-18 05:31:35',NULL,'2016-10-18 05:31:56',NULL,1,NULL,'efefe-64',0,'7',NULL,NULL,NULL,'','','+91-363636454','islamn000@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `app_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_events_a`
--

DROP TABLE IF EXISTS `app_events_a`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_events_a` (
  `news_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `image` varchar(128) DEFAULT NULL,
  `short` varchar(1024) DEFAULT NULL,
  `text` text NOT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `views` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`news_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_events_a`
--

LOCK TABLES `app_events_a` WRITE;
/*!40000 ALTER TABLE `app_events_a` DISABLE KEYS */;
/*!40000 ALTER TABLE `app_events_a` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_events_old`
--

DROP TABLE IF EXISTS `app_events_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_events_old` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_name` varchar(256) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_type` int(11) NOT NULL,
  `event_category_id` int(11) NOT NULL,
  `description` varchar(256) NOT NULL,
  `ref_no` int(11) NOT NULL,
  `logo` varchar(256) NOT NULL,
  `image` varchar(256) NOT NULL,
  `sprice_from` decimal(10,2) NOT NULL,
  `sprice_to` decimal(10,2) NOT NULL,
  `event_date` datetime NOT NULL,
  `incorporation_yr` int(11) NOT NULL,
  `venue_name` varchar(256) NOT NULL,
  `address` varchar(256) NOT NULL,
  `city_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `lat` varchar(256) NOT NULL,
  `lng` varchar(256) NOT NULL,
  `event_strt_date` datetime NOT NULL,
  `event_end_date` datetime NOT NULL,
  `event_strt_time` varchar(256) NOT NULL,
  `event_end_time` varchar(256) NOT NULL,
  `website` varchar(256) NOT NULL,
  `fb_page` varchar(256) NOT NULL,
  `fb_likes` int(11) NOT NULL,
  `pitch_pdf` varchar(256) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `url` varchar(256) NOT NULL,
  `no_of_attendees` int(11) NOT NULL,
  `every_yr` tinyint(4) NOT NULL,
  `televised` tinyint(4) NOT NULL,
  `ticketed` tinyint(4) NOT NULL,
  `videos_link` varchar(256) NOT NULL,
  `earlier_sponser` varchar(256) NOT NULL,
  `ticket_type` varchar(256) NOT NULL,
  `ticket_text` varchar(256) NOT NULL,
  `premium` varchar(45) NOT NULL,
  `age_group_most` int(11) NOT NULL,
  `age_group_rest` int(11) NOT NULL,
  `education_most` int(11) NOT NULL,
  `education_rest` int(11) NOT NULL,
  `income_most` int(11) NOT NULL,
  `income_rest` int(11) NOT NULL,
  `gender_most` varchar(45) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `completed` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_events_old`
--

LOCK TABLES `app_events_old` WRITE;
/*!40000 ALTER TABLE `app_events_old` DISABLE KEYS */;
/*!40000 ALTER TABLE `app_events_old` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_eventtestimonials`
--

DROP TABLE IF EXISTS `app_eventtestimonials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_eventtestimonials` (
  `eventtestimonials_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `designation` varchar(128) DEFAULT NULL,
  `image` varchar(128) DEFAULT NULL,
  `text` text,
  `slug` varchar(128) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `event` varchar(45) DEFAULT NULL,
  `video` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`eventtestimonials_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_eventtestimonials`
--

LOCK TABLES `app_eventtestimonials` WRITE;
/*!40000 ALTER TABLE `app_eventtestimonials` DISABLE KEYS */;
INSERT INTO `app_eventtestimonials` VALUES (4,'Testing','CEO','/uploads/eventtestimonials/gaurav-bded222236.png','<p>Sponsorship has become a very critical tool for us as we want to reach customers directly through an efficient and uncluttered communication channel. I firmly believe OnSpon has created a great service offering partnering events and sponsors.</p>','testing',1475302512,1,'Navrang','asdfghjkl'),(3,'Roshni Nadar','Daikin Airconditioning India Pvt. Ltd.','/uploads/eventtestimonials/roshni-0c9acda5e3.jpg','<p>Sponsorship has become a very critical tool for us as we want to reach  customers directly through an efficient and uncluttered communication  channel. I firmly believe OnSpon has created a great service offering  partnering events and sponsors. </p>','roshni-nadar',1474276471,1,'IIFA','');
/*!40000 ALTER TABLE `app_eventtestimonials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_facebook_event`
--

DROP TABLE IF EXISTS `app_facebook_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_facebook_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eid` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `image1` varchar(5000) DEFAULT NULL,
  `start_time` varchar(100) DEFAULT NULL,
  `end_time` varchar(100) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `creator` varchar(100) DEFAULT NULL,
  `creator_name` varchar(200) DEFAULT NULL,
  `description` text,
  `cover_image` varchar(100) DEFAULT NULL,
  `is_date_only` varchar(100) DEFAULT NULL,
  `parent_group_id` varchar(100) DEFAULT NULL,
  `privacy` varchar(100) DEFAULT NULL,
  `ticket_uri` varchar(100) DEFAULT NULL,
  `timezone` varchar(100) DEFAULT NULL,
  `v_latitude` varchar(100) DEFAULT NULL,
  `v_longitude` varchar(100) DEFAULT NULL,
  `v_city` varchar(100) DEFAULT NULL,
  `v_state` varchar(100) DEFAULT NULL,
  `v_country` varchar(100) DEFAULT NULL,
  `v_id` varchar(200) DEFAULT NULL,
  `v_name` varchar(200) DEFAULT NULL,
  `v_street` varchar(200) DEFAULT NULL,
  `v_zip` varchar(200) DEFAULT NULL,
  `category` varchar(200) DEFAULT NULL,
  `event_cat_id` varchar(100) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_facebook_event`
--

LOCK TABLES `app_facebook_event` WRITE;
/*!40000 ALTER TABLE `app_facebook_event` DISABLE KEYS */;
INSERT INTO `app_facebook_event` VALUES (1,NULL,'Test Event',NULL,NULL,'2016-10-12 16:00:00','2016-10-12 19:00:00','New Delhi',NULL,NULL,'Description',NULL,NULL,NULL,NULL,NULL,NULL,'28.6139','77.2089','New Delhi',NULL,'India','106517799384578',NULL,NULL,NULL,NULL,NULL,'attending'),(2,NULL,'Testingh',NULL,NULL,'2016-10-06 17:00:00',NULL,'New Delhi',NULL,NULL,'Testing details',NULL,NULL,NULL,NULL,NULL,NULL,'28.6139','77.2089','New Delhi',NULL,'India','106517799384578',NULL,NULL,NULL,NULL,NULL,'attending'),(3,NULL,'private',NULL,NULL,'2016-10-05 01:00:00',NULL,'New Delhi',NULL,NULL,'fffffffffffffffffffff',NULL,NULL,NULL,NULL,NULL,NULL,'28.6139','77.2089','New Delhi',NULL,'India','106517799384578',NULL,NULL,NULL,NULL,NULL,'attending'),(4,NULL,'Testing 123',NULL,NULL,'2016-10-05 01:00:00','2016-10-05 04:00:00','New Delhi',NULL,NULL,'fsdfsdfasdfasd asdfa sdfasdfasdf',NULL,NULL,NULL,NULL,NULL,NULL,'28.6139','77.2089','New Delhi',NULL,'India','106517799384578',NULL,NULL,NULL,NULL,NULL,'attending'),(5,NULL,'Testing',NULL,NULL,'2016-10-04 20:00:00','2016-10-04 23:00:00','New Delhi',NULL,NULL,'testing description',NULL,NULL,NULL,NULL,NULL,NULL,'28.6139','77.2089','New Delhi',NULL,'India','106517799384578',NULL,NULL,NULL,NULL,NULL,'attending');
/*!40000 ALTER TABLE `app_facebook_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_homebanner`
--

DROP TABLE IF EXISTS `app_homebanner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_homebanner` (
  `homebanner_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `video` varchar(128) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`homebanner_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_homebanner`
--

LOCK TABLES `app_homebanner` WRITE;
/*!40000 ALTER TABLE `app_homebanner` DISABLE KEYS */;
INSERT INTO `app_homebanner` VALUES (1,'Festivals','asdfghjkl;','2016-09-28 05:44:27',NULL,'festivals',1),(2,'homebanner','www.jsjks.com/djkjd','2016-09-28 05:45:54',NULL,'homebanner',1),(3,'SASS','www.dddd.sdddd/dddd','2016-09-28 05:47:08','2016-09-28 06:08:34','sass',1),(4,'jshjhjk','kjskjksjksjsk','2016-09-28 05:53:01',NULL,'jshjhjk',1),(5,'dfddffd','sdsdsddddsad','2016-09-28 05:57:14',NULL,'dfddffd',1),(6,'dsadsassd','asdsdadadd','2016-09-28 05:59:08',NULL,'dsadsassd',1),(7,'jsdjkj','kjwdkjkdjkdwj','2016-09-28 06:01:03',NULL,'jsdjkj',1);
/*!40000 ALTER TABLE `app_homebanner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_inbox`
--

DROP TABLE IF EXISTS `app_inbox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_inbox` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `sender_name` varchar(128) NOT NULL,
  `sender_mobile` varchar(15) NOT NULL,
  `message` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `date_created` varchar(128) NOT NULL,
  `sender_id` tinyint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_inbox`
--

LOCK TABLES `app_inbox` WRITE;
/*!40000 ALTER TABLE `app_inbox` DISABLE KEYS */;
INSERT INTO `app_inbox` VALUES (1,8,4,'rakesh','7530889602','cheak',0,'2016-10-08 06:52:46',NULL),(2,8,7,'rakesh rajput','7530889602','dgfbv ',0,'2016-10-10 06:19:37',NULL),(3,14,469,'Ammy','91234567890','Testing',0,'2016-10-10 08:47:17',44),(4,8,10,'Manny','91234567890','Testing',0,'2016-10-12 12:14:24',0),(5,27,627,'sdfsdf','234234234','sdsdd',0,'2016-10-13 12:21:44',0);
/*!40000 ALTER TABLE `app_inbox` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_income`
--

DROP TABLE IF EXISTS `app_income`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_income` (
  `income_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `text` text,
  `slug` varchar(128) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`income_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_income`
--

LOCK TABLES `app_income` WRITE;
/*!40000 ALTER TABLE `app_income` DISABLE KEYS */;
INSERT INTO `app_income` VALUES (1,'Middle Class','2016-08-19 03:22:37','0000-00-00 00:00:00','','middle-class',1),(2,'High Class','2016-09-12 09:06:06',NULL,NULL,'high-class',1),(3,'Low Class','2016-09-12 09:06:15',NULL,NULL,'low-class',1),(4,'All','2016-09-16 01:01:37',NULL,NULL,'all',1);
/*!40000 ALTER TABLE `app_income` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_industry`
--

DROP TABLE IF EXISTS `app_industry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_industry` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `image` varchar(128) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_industry`
--

LOCK TABLES `app_industry` WRITE;
/*!40000 ALTER TABLE `app_industry` DISABLE KEYS */;
INSERT INTO `app_industry` VALUES (1,'Consumer Durable',NULL,'2016-09-12 11:36:08','2016-09-12 11:36:46','consumer-durable',1),(2,'Ecommerce',NULL,'2016-09-12 11:37:08','0000-00-00 00:00:00','ecommerce',1),(3,'Technology',NULL,'2016-09-12 11:37:22','0000-00-00 00:00:00','technology',1),(4,'Media and Advertising',NULL,'2016-09-12 12:07:42',NULL,'media-and-advertising',1),(5,'make up kits',NULL,'2016-10-14 12:34:49',NULL,'make-up-kits',1);
/*!40000 ALTER TABLE `app_industry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_lastminute`
--

DROP TABLE IF EXISTS `app_lastminute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_lastminute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `event_id` int(11) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_lastminute`
--

LOCK TABLES `app_lastminute` WRITE;
/*!40000 ALTER TABLE `app_lastminute` DISABLE KEYS */;
INSERT INTO `app_lastminute` VALUES (1,'Esperanza',4,'2016-10-03 11:19:26','2016-10-03 11:19:30',1),(2,'Kabaddi Championship',7,'2016-10-03 11:25:56',NULL,1),(3,'International Conference',8,'2016-10-03 09:48:48',NULL,1),(4,'Tech Fest',5,'2016-10-03 09:49:08','2016-10-03 09:49:18',1),(5,'ABC Event',84,'2016-10-03 09:57:27',NULL,1);
/*!40000 ALTER TABLE `app_lastminute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_organization_detail`
--

DROP TABLE IF EXISTS `app_organization_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_organization_detail` (
  `org_id` int(11) NOT NULL AUTO_INCREMENT,
  `oraganization_name` varchar(256) DEFAULT NULL,
  `org_location` varchar(256) DEFAULT NULL,
  `established_date` int(11) DEFAULT NULL,
  `fb_page` varchar(256) DEFAULT NULL,
  `twitter_page` varchar(256) DEFAULT NULL,
  `org_city` varchar(256) DEFAULT NULL,
  `org_about` text,
  PRIMARY KEY (`org_id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_organization_detail`
--

LOCK TABLES `app_organization_detail` WRITE;
/*!40000 ALTER TABLE `app_organization_detail` DISABLE KEYS */;
INSERT INTO `app_organization_detail` VALUES (1,'delhi','',1476687052,'','','',''),(2,'delhi','',1476687052,'','','',''),(3,'delhi','',1476687052,'','','',''),(4,'delhi','',1476687052,'','','',''),(5,'delhi','',1476687052,'','','',''),(6,'delhi','',1476687052,'','','',''),(7,'delhi','',1476687052,'','','',''),(8,'delhi','',1476687052,'','','',''),(9,'delhi','',1476687052,'','','',''),(10,'delhi','',1476687052,'','','',''),(11,'delhi','',1476687052,'','','',''),(12,'delhi','',1476687052,'','','',''),(13,'delhi','',1476687052,'','','',''),(14,'delhi','',1476687052,'','','',''),(15,'delhi','',1476687052,'','','',''),(16,'delhi','',1476687052,'','','',''),(17,'delhi','',1476687052,'','','',''),(18,'delhi','',1476687052,'','','',''),(19,'delhi','',1476687052,'','','',''),(20,'delhi','',1476687052,'','','',''),(21,'delhi','',1476687052,'','','',''),(22,'delhi','',1476687052,'','','',''),(23,'delhi','',1476687052,'','','',''),(24,'delhi','',1476687052,'','','',''),(25,'delhi','',1476687052,'','','',''),(26,'delhi','',1476687052,'','','',''),(27,'delhi','',1476687052,'','','',''),(28,'delhi','',1476687052,'','','',''),(29,'delhi','',1476687052,'','','',''),(30,'delhi','',1476687052,'','','',''),(31,'jhwdji','',1476687170,'dbyudb','wdwd','dwd',''),(32,'jhwdji','',1476687170,'dbyudb','wdwd','dwd',''),(33,'jhwdji','',1476687170,'dbyudb','wdwd','dwd',''),(34,'jhwdji','',1476687170,'dbyudb','wdwd','dwd',''),(35,'jhwdji','',1476687170,'dbyudb','wdwd','dwd',''),(36,'jhwdji','',1476687170,'dbyudb','wdwd','dwd',''),(37,'jhwdji','',1476687170,'dbyudb','wdwd','dwd',''),(38,'jhwdji','',1476687170,'dbyudb','wdwd','dwd',''),(39,'jhwdji','',1476687170,'dbyudb','wdwd','dwd',''),(40,'jhwdji','',1476687170,'dbyudb','wdwd','dwd',''),(41,'jhwdji','',1476687170,'dbyudb','wdwd','dwd',''),(42,'jhwdji','',1476687170,'dbyudb','wdwd','dwd',''),(43,'jhwdji','',1476687170,'dbyudb','wdwd','dwd',''),(44,'jhwdji','',1476687170,'dbyudb','wdwd','dwd',''),(45,'jhwdji','',1476687170,'dbyudb','wdwd','dwd',''),(46,'jhwdji','',1476687170,'dbyudb','wdwd','dwd',''),(47,'jhwdji','',1476687170,'dbyudb','wdwd','dwd',''),(48,'jhwdji','',1476687170,'dbyudb','wdwd','dwd',''),(49,'duhfufi','',1476687464,'eigi','wdwqf','wdwd3',''),(50,'','',1476708196,'','','',''),(51,'','',1476709055,'','','',''),(52,'','',1476710543,'','','',''),(53,'','',1476711372,'','','',''),(54,'fgfd','rthh',1476711553,'rthrht','rthrh','trhrht',''),(55,'fgfd','rthh',1476711553,'rthrht','rthrh','trhrht',''),(56,'fgfd','rthh',1476711553,'rthrht','rthrh','trhrht',''),(57,'fgfd','rthh',1476711553,'rthrht','rthrh','trhrht',''),(58,'fgfd','rthh',1476711553,'rthrht','rthrh','trhrht','');
/*!40000 ALTER TABLE `app_organization_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_package`
--

DROP TABLE IF EXISTS `app_package`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_name` varchar(512) DEFAULT NULL,
  `basic` varchar(256) DEFAULT NULL,
  `silver` varchar(256) DEFAULT NULL,
  `gold` varchar(256) DEFAULT NULL,
  `platinum` varchar(256) DEFAULT NULL,
  `enterprise` varchar(256) DEFAULT NULL,
  `category` varchar(256) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_package`
--

LOCK TABLES `app_package` WRITE;
/*!40000 ALTER TABLE `app_package` DISABLE KEYS */;
INSERT INTO `app_package` VALUES (1,'Presence on Onspon (Custom URL, One smart page)','Yes','Yes','Yes','Yes','Yes','Default',1),(2,'Mention as premium event + Placement in search results + email support','0','Yes','Yes','Yes','Yes','Sponsorship Support',1),(3,'Customized Presentations for sponsorship pitch: Brand specific presentations on listers choice','0','1','5','7','unlimited','Sponsorship Support',1),(4,'E-mailers & SMS spurt to Brands / agencies registered with onspon.com','0','1 per fortnight - Shared','1 - dedicated + 1 shared per fortnight','2 dedicated per fortnight','1 dedicated per week','Sponsorship Support',1),(5,'Applications to brand mandates','2','4','7','10','unlimited','Sponsorship Support',1),(6,'On call support','0','0','0','Yes','Yes','Sponsorship Support',1),(7,'On ground support + Meeting scheduling','0','0','0','0','Yes','Sponsorship Support',1),(8,'Social Media amplification – posts through onspon and partner social media platforms','0','Low','Mid ','High','Very High','Visibility to brands and audience',1),(9,'Visibility in banners (one month, or till event date, whichever is earlier)','NA','NA','City','City + Category','City + Category+ Home Page','Visibility to brands and audience',1);
/*!40000 ALTER TABLE `app_package` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_role`
--

DROP TABLE IF EXISTS `app_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`role_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_role`
--

LOCK TABLES `app_role` WRITE;
/*!40000 ALTER TABLE `app_role` DISABLE KEYS */;
INSERT INTO `app_role` VALUES (1,'Sponsor','2016-09-12 08:50:51',NULL,'sponsor',1),(2,'Event Organizer','2016-09-12 08:51:11',NULL,'event-organizer',1),(3,'Audiences','2016-09-12 08:51:27',NULL,'audiences',1);
/*!40000 ALTER TABLE `app_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_role_sample`
--

DROP TABLE IF EXISTS `app_role_sample`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_role_sample` (
  `income_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `text` text NOT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`income_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_role_sample`
--

LOCK TABLES `app_role_sample` WRITE;
/*!40000 ALTER TABLE `app_role_sample` DISABLE KEYS */;
/*!40000 ALTER TABLE `app_role_sample` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_sponsor_apply`
--

DROP TABLE IF EXISTS `app_sponsor_apply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_sponsor_apply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) DEFAULT NULL,
  `receiver_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `event_name` varchar(255) DEFAULT NULL,
  `message` text,
  `status` tinyint(4) DEFAULT '0',
  `date` timestamp NULL DEFAULT NULL,
  `brandmandates_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_sponsor_apply`
--

LOCK TABLES `app_sponsor_apply` WRITE;
/*!40000 ALTER TABLE `app_sponsor_apply` DISABLE KEYS */;
INSERT INTO `app_sponsor_apply` VALUES (10,9,8,NULL,NULL,'Hi Rashmee, I am applying for this  \"25\" ',1,'2016-10-03 06:34:32',25),(11,9,15,NULL,NULL,'Hi SHASHANK, I am applying for this  \"35\" ',0,'2016-10-03 06:35:33',35),(12,9,8,NULL,NULL,'Hi Rashmee, I am applying for this  34 ',1,'2016-10-03 06:36:18',34),(19,9,22,NULL,NULL,'Hi rakesh, I am applying for this  ABC ',1,'2016-10-05 04:44:42',37),(20,9,8,NULL,NULL,'Hi Rashmee, I am applying for this  XYZ ',1,'2016-10-05 04:47:49',38),(21,9,8,NULL,NULL,'Hi Rashmee, I am applying for this  PQR ',1,'2016-10-05 06:23:38',40),(23,30,22,NULL,NULL,'Hi rakesh, I am applying for this  Venus  ',1,'2016-10-10 01:02:30',41),(35,8,9,3,'Navrang','Hi Rashmee Verma, You can apply these brands mandate \"Testing by rashmee, Mahindra, ABC, XYZ, DEF, PQR, Testing\" for your event',1,'2016-10-13 04:50:54',NULL),(36,22,NULL,656,'sgsg','Hi , You can apply these brands mandate \"Venus , puma2, puma2, abcewfiu23, abcewfiu23, dammy6thoct, imagecheak, onspon2, onspon2, trsfd, sadjbjb\" for your event',0,'2016-10-14 03:11:28',NULL),(37,30,8,NULL,NULL,'Hi Rashmee, I am applying for this  Testing by rashmee ',1,'2016-10-14 19:39:07',34);
/*!40000 ALTER TABLE `app_sponsor_apply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_sponsor_apply_outbox`
--

DROP TABLE IF EXISTS `app_sponsor_apply_outbox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_sponsor_apply_outbox` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) DEFAULT NULL,
  `receiver_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `event_name` varchar(255) DEFAULT NULL,
  `message` text,
  `status` tinyint(4) DEFAULT '0',
  `date` timestamp NULL DEFAULT NULL,
  `brandmandates_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_sponsor_apply_outbox`
--

LOCK TABLES `app_sponsor_apply_outbox` WRITE;
/*!40000 ALTER TABLE `app_sponsor_apply_outbox` DISABLE KEYS */;
INSERT INTO `app_sponsor_apply_outbox` VALUES (15,8,9,6,'IPL','Hi Rashmee Verma, You can apply these brands mandate \"Test, Testing, Mahindra\" for your event',0,'2016-10-05 04:37:36',NULL),(18,8,25,334,'dummy','Hi rakesh, You can apply these brands mandate \"Test, Testing, Mahindra\" for your event',0,'2016-10-05 04:38:32',NULL),(20,9,8,NULL,NULL,'Hi Rashmee, I am applying for this  XYZ ',0,'2016-10-05 04:47:49',38),(21,9,8,NULL,NULL,'Hi Rashmee, I am applying for this  PQR ',0,'2016-10-05 06:23:38',40),(22,30,8,NULL,NULL,'Hi Rashmee, I am applying for this  Mahindra ',0,'2016-10-10 01:02:20',36),(23,30,22,NULL,NULL,'Hi rakesh, I am applying for this  Venus  ',0,'2016-10-10 01:02:30',41),(24,22,9,3,'Navrang','Hi Rashmee Verma, You can apply these brands mandate \"Venus , puma2, puma2, abcewfiu23, abcewfiu23, dammy6thoct, imagecheak, 34r34, onspon2, onspon2, trsfd\" for your event',0,'2016-10-10 03:30:58',NULL),(25,22,8,2,'IIFA','Hi Rashmee, You can apply these brands mandate \"Venus , puma2, puma2, abcewfiu23, abcewfiu23, dammy6thoct, imagecheak, 34r34, onspon2, onspon2, trsfd\" for your event',0,'2016-10-10 03:31:08',NULL),(26,8,8,7,'Kabaddi Championship','Hi Rashmee, You can apply these brands mandate \"Testing by rashmee, Mahindra, ABC, XYZ, DEF, PQR, Testing\" for your event',0,'2016-10-13 04:05:48',NULL),(27,8,8,8,'International Conference','Hi Rashmee, You can apply these brands mandate \"Testing by rashmee, Mahindra, ABC, XYZ, DEF, PQR, Testing\" for your event',0,'2016-10-13 04:17:15',NULL),(29,8,9,3,'Navrang','Hi Rashmee Verma, You can apply these brands mandate \"Testing by rashmee, Mahindra, ABC, XYZ, DEF, PQR, Testing\" for your event',0,'2016-10-13 04:45:12',NULL),(30,8,9,6,'IPL','Hi Rashmee Verma, You can apply these brands mandate \"Testing by rashmee, Mahindra, ABC, XYZ, DEF, PQR, Testing\" for your event',0,'2016-10-13 04:49:12',NULL),(31,8,8,2,'IIFA','Hi Rashmee, You can apply these brands mandate \"Testing by rashmee, Mahindra, ABC, XYZ, DEF, PQR, Testing\" for your event',0,'2016-10-13 04:49:33',NULL),(32,8,9,3,'Navrang','Hi Rashmee Verma, You can apply these brands mandate \"Testing by rashmee, Mahindra, ABC, XYZ, DEF, PQR, Testing\" for your event',0,'2016-10-13 04:50:54',NULL),(34,30,8,NULL,NULL,'Hi Rashmee, I am applying for this  Testing by rashmee ',0,'2016-10-14 19:39:07',34);
/*!40000 ALTER TABLE `app_sponsor_apply_outbox` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_sponsor_profile`
--

DROP TABLE IF EXISTS `app_sponsor_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_sponsor_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `industry` int(11) DEFAULT NULL,
  `intrested_area` varchar(255) DEFAULT NULL,
  `address` text,
  `pincode` int(11) DEFAULT NULL,
  `landmark` text,
  `status` tinyint(4) DEFAULT '1',
  `date_created` timestamp NULL DEFAULT NULL,
  `image` varchar(400) DEFAULT NULL,
  `brand_name` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `location_specific` varchar(255) DEFAULT NULL,
  `mobile` int(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_sponsor_profile`
--

LOCK TABLES `app_sponsor_profile` WRITE;
/*!40000 ALTER TABLE `app_sponsor_profile` DISABLE KEYS */;
INSERT INTO `app_sponsor_profile` VALUES (4,8,'Rashmee','rashmee.indicsoft@gmail.com',1,'4,6,11,12','',NULL,'',1,'2016-10-05 04:20:14','','Testing Brand','Metros','',NULL),(5,31,'Amrita','amrita@indicsoft.com',3,'1,2,7,9',NULL,NULL,NULL,1,'2016-10-05 04:29:26',NULL,'MyApps','Specific','fdf',NULL),(6,22,'rakesh','onspon2@gmail.com',3,'1,3,4,10,14','sec 50 nirwana gurgaon hariyana ,55555',56565,'gurgaon',1,'2016-10-06 00:37:50','/uploads/sponsorprofile/1-b37a1d80a6.jpg','rakesh rajput','Metros','',2147483647),(7,49,'Abubakar Ansari','abubakar10.alameen@gmail.com',2,'1',NULL,NULL,NULL,1,'2016-10-11 23:34:11',NULL,'Abu','Pan India','',NULL),(8,51,'Suma Abey','suma.abey@gmail.com',2,'3',NULL,NULL,NULL,1,'2016-10-12 02:55:26',NULL,'hgryfffffff','Metros','',NULL),(9,50,'Shashank Shukla','shashank@onspon.com',1,'2,3,8',NULL,NULL,NULL,1,'2016-10-12 02:56:58',NULL,'abc123','Metros','',NULL),(10,52,'Salman Raza','salraza1993@gmail.com',1,'1,2',NULL,NULL,NULL,1,'2016-10-12 04:49:06',NULL,'Salman Raza','Pan India','',NULL);
/*!40000 ALTER TABLE `app_sponsor_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_sponsorprice`
--

DROP TABLE IF EXISTS `app_sponsorprice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_sponsorprice` (
  `income_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `text` text,
  `slug` varchar(128) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`income_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_sponsorprice`
--

LOCK TABLES `app_sponsorprice` WRITE;
/*!40000 ALTER TABLE `app_sponsorprice` DISABLE KEYS */;
INSERT INTO `app_sponsorprice` VALUES (1,'Rs.100000-Rs.500000','2016-09-12 12:49:45','2016-09-12 12:52:04',NULL,'rs100000-rs500000',1);
/*!40000 ALTER TABLE `app_sponsorprice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_sponsorshipobjective`
--

DROP TABLE IF EXISTS `app_sponsorshipobjective`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_sponsorshipobjective` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `image` varchar(128) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug_UNIQUE` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_sponsorshipobjective`
--

LOCK TABLES `app_sponsorshipobjective` WRITE;
/*!40000 ALTER TABLE `app_sponsorshipobjective` DISABLE KEYS */;
INSERT INTO `app_sponsorshipobjective` VALUES (1,'Brand Salience',NULL,'2016-09-12 07:25:24','2016-09-12 07:32:29','brand-salience',1),(2,'Media Amplification',NULL,'2016-09-12 07:33:46','0000-00-00 00:00:00','media-amplification',1),(3,'Networking',NULL,'2016-09-12 07:34:19','2016-09-12 07:34:31','networking',1),(4,'Sampling',NULL,'2016-09-12 12:08:20',NULL,'sampling',1);
/*!40000 ALTER TABLE `app_sponsorshipobjective` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_sponsorslab`
--

DROP TABLE IF EXISTS `app_sponsorslab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_sponsorslab` (
  `sponsorslab_id` int(11) NOT NULL COMMENT '	',
  `event_id` int(11) DEFAULT NULL,
  `slabname` varchar(245) DEFAULT NULL,
  `sponsorprice` varchar(245) DEFAULT NULL,
  `deliverables` varchar(245) DEFAULT NULL,
  PRIMARY KEY (`sponsorslab_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_sponsorslab`
--

LOCK TABLES `app_sponsorslab` WRITE;
/*!40000 ALTER TABLE `app_sponsorslab` DISABLE KEYS */;
/*!40000 ALTER TABLE `app_sponsorslab` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_state`
--

DROP TABLE IF EXISTS `app_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_state` (
  `state_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `text` text,
  `slug` varchar(128) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`state_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_state`
--

LOCK TABLES `app_state` WRITE;
/*!40000 ALTER TABLE `app_state` DISABLE KEYS */;
INSERT INTO `app_state` VALUES (1,'Haryana',0,'2016-08-18 11:18:45','2016-08-18 11:23:58','','haryana',1),(2,'Uttar Pradesh',0,'2016-08-18 11:24:12','0000-00-00 00:00:00','','uttar-pradesh',1);
/*!40000 ALTER TABLE `app_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_tagmanagement`
--

DROP TABLE IF EXISTS `app_tagmanagement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_tagmanagement` (
  `tagmanagemnet_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `image` varchar(128) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `text` text,
  `slug` varchar(128) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`tagmanagemnet_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_tagmanagement`
--

LOCK TABLES `app_tagmanagement` WRITE;
/*!40000 ALTER TABLE `app_tagmanagement` DISABLE KEYS */;
INSERT INTO `app_tagmanagement` VALUES (1,'Fashion','','2016-08-18 12:32:31','2016-09-16 12:21:19','','fashion',1),(2,'Sports','','2016-09-09 11:46:41','2016-09-16 12:21:43',NULL,'sports',1),(3,'Health and Wellness','','2016-09-16 12:21:59',NULL,NULL,'health-and-wellness',1),(4,'Music','','2016-09-16 12:23:15',NULL,NULL,'music',1),(5,'Leisure (Non Music)','','2016-09-16 12:23:30',NULL,NULL,'leisure-non-music',1),(6,'Startups','','2016-09-16 12:23:40',NULL,NULL,'startups',1),(7,'Luxury','','2016-09-16 12:23:48',NULL,NULL,'luxury',1),(8,'Industry Specific','','2016-09-16 12:23:58','2016-09-16 12:24:09',NULL,'industry-specific',1),(9,'Digital','','2016-09-16 12:24:20',NULL,NULL,'digital',1),(10,'Religion and Spiritual','','2016-09-16 12:24:31',NULL,NULL,'religion-and-spiritual',1),(11,'Literature and Quizzing','','2016-09-16 12:24:42',NULL,NULL,'literature-and-quizzing',1),(12,'College Festivals','','2016-09-16 12:25:03',NULL,NULL,'college-festivals',1),(13,'Others','','2016-09-16 12:25:11',NULL,NULL,'others',1),(14,'Entertainment','','2016-09-23 04:15:39',NULL,NULL,'entertainment',1),(15,'Film','','2016-09-23 04:15:42','2016-09-23 04:16:53',NULL,'film',1);
/*!40000 ALTER TABLE `app_tagmanagement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_testimonials`
--

DROP TABLE IF EXISTS `app_testimonials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_testimonials` (
  `testimonials_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `designation` varchar(128) DEFAULT NULL,
  `image` varchar(128) DEFAULT NULL,
  `text` text,
  `slug` varchar(128) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`testimonials_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_testimonials`
--

LOCK TABLES `app_testimonials` WRITE;
/*!40000 ALTER TABLE `app_testimonials` DISABLE KEYS */;
INSERT INTO `app_testimonials` VALUES (3,'Gaurav Malik','Daikin Airconditioning India Pvt. Ltd.','/uploads/testimonials/gauravmalik-c5ee46ced2.png','<p>Sponsorship has become a very critical tool for us as we want to reach  customers directly through an efficient and uncluttered communication  channel. I firmly believe OnSpon has created a great service offering  partnering events and sponsors. </p>','gaurav-malik',1474275091,1),(4,'Deepak Pant','Brand Manager - Livon (Marico Ltd.)','/uploads/testimonials/deepakpant-1dcb51f80c.png','<p>There has been a dearth of a centralized effective platform to engage sponsorship seekers and sponsors. Onspon has created a great platform and I shall look forward to using it to evaluate the sponsorship opportunities in a hassle-free way.</p>','deepak-pant',1474275198,1),(5,'Nishant Parashar','Director, Engage4more','/uploads/testimonials/nishant-6597e442e4.jpg','<p>My experience with Onspon w.r.t go-to-market for our flagship IP CTC (Corporate Talent Championship) has been simply electric. I think they are the only team in sponsorship space with the required intelligence, capability and commitment </p>','nishant-parashar',1474275301,1);
/*!40000 ALTER TABLE `app_testimonials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_theme`
--

DROP TABLE IF EXISTS `app_theme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_theme` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `themename` varchar(45) DEFAULT NULL,
  `theme_img` varchar(45) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_theme`
--

LOCK TABLES `app_theme` WRITE;
/*!40000 ALTER TABLE `app_theme` DISABLE KEYS */;
INSERT INTO `app_theme` VALUES (1,'sports','/media/sports-img/sportsbanner.jpg',NULL,NULL,1),(2,'default','/media/default-img/defaultbanner.jpg',NULL,NULL,NULL);
/*!40000 ALTER TABLE `app_theme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_trending`
--

DROP TABLE IF EXISTS `app_trending`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_trending` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `event_id` int(11) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_trending`
--

LOCK TABLES `app_trending` WRITE;
/*!40000 ALTER TABLE `app_trending` DISABLE KEYS */;
INSERT INTO `app_trending` VALUES (1,'Navrang',3,'2016-10-03 11:18:59','2016-10-03 11:19:05',1),(2,'Esperanza',4,'2016-10-03 09:55:14',NULL,1),(3,'Tech Fest',5,'2016-10-03 09:55:20','2016-10-03 09:55:34',1),(4,'DEF',78,'2016-10-03 09:55:44',NULL,1),(5,'IPL',6,'2016-10-03 09:56:49',NULL,1);
/*!40000 ALTER TABLE `app_trending` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_user_folder`
--

DROP TABLE IF EXISTS `app_user_folder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_user_folder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `foldername` varchar(45) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_user_folder`
--

LOCK TABLES `app_user_folder` WRITE;
/*!40000 ALTER TABLE `app_user_folder` DISABLE KEYS */;
INSERT INTO `app_user_folder` VALUES (1,8,'Testing','2016-09-30 04:41:57'),(2,8,'Test','2016-09-30 04:42:20'),(3,8,'Digital','2016-09-30 04:44:05'),(4,9,'College Events','2016-09-30 04:46:05'),(5,9,'Awards','2016-09-30 04:47:23'),(6,8,'abu','2016-09-30 06:28:46'),(7,22,'sport shortlisted','2016-10-06 06:23:53'),(8,22,'sport shortlisted','2016-10-06 06:24:08'),(9,22,'sec 50','2016-10-08 06:53:26'),(10,22,'sec 50','2016-10-08 06:53:36'),(11,22,'delhi','2016-10-08 06:54:08'),(12,22,'delhi','2016-10-08 06:54:09'),(13,22,'delhi','2016-10-08 06:54:10'),(14,22,'delhi','2016-10-08 06:54:10'),(15,22,'delhi','2016-10-08 06:54:11'),(16,22,'delhi','2016-10-08 06:54:11'),(17,22,'delhi','2016-10-08 06:54:11'),(18,22,'delhi','2016-10-08 06:54:11'),(19,22,'delhi','2016-10-08 06:54:11'),(20,22,'delhi','2016-10-08 06:54:13'),(21,22,'delhi','2016-10-08 06:54:14'),(22,22,'delhi','2016-10-08 06:54:14'),(23,22,'sponsor','2016-10-08 06:54:40'),(24,41,'Life','2016-10-09 09:52:42');
/*!40000 ALTER TABLE `app_user_folder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_user_shortlist_event`
--

DROP TABLE IF EXISTS `app_user_shortlist_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_user_shortlist_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `folderid` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_user_shortlist_event`
--

LOCK TABLES `app_user_shortlist_event` WRITE;
/*!40000 ALTER TABLE `app_user_shortlist_event` DISABLE KEYS */;
INSERT INTO `app_user_shortlist_event` VALUES (6,9,5,9,'2016-09-30 04:47:48'),(34,8,6,5,'2016-09-30 01:08:51'),(35,8,2,5,'2016-09-30 01:08:51'),(36,8,2,78,'2016-09-30 01:09:25'),(37,9,4,4,'2016-10-04 07:27:37'),(38,8,1,4,'2016-10-04 07:28:36'),(40,22,21,7,'2016-10-08 06:58:12'),(41,22,7,3,'2016-10-10 09:01:42'),(42,22,23,715,'2016-10-17 01:15:43'),(43,22,23,710,'2016-10-17 01:15:53'),(44,22,21,710,'2016-10-17 01:15:53'),(45,22,23,703,'2016-10-17 01:16:38'),(46,22,21,703,'2016-10-17 01:16:38'),(47,22,22,703,'2016-10-17 01:16:38');
/*!40000 ALTER TABLE `app_user_shortlist_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_users`
--

DROP TABLE IF EXISTS `app_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) DEFAULT NULL,
  `image` varchar(128) DEFAULT NULL,
  `short` varchar(1024) DEFAULT NULL,
  `text` text,
  `slug` varchar(128) DEFAULT NULL,
  `created_at` int(11) DEFAULT '0',
  `views` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '0',
  `age` varchar(45) DEFAULT NULL,
  `contactnumber` varchar(45) DEFAULT NULL,
  `sex` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `location` varchar(45) DEFAULT NULL,
  `auth_key` varchar(45) NOT NULL,
  `password_hash` varchar(245) NOT NULL,
  `password_reset_token` varchar(245) DEFAULT NULL,
  `email` varchar(245) NOT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `latitude` varchar(45) DEFAULT NULL,
  `longitude` varchar(45) DEFAULT NULL,
  `email_update` tinyint(4) DEFAULT NULL,
  `google_id` varchar(45) DEFAULT NULL,
  `facebook_id` varchar(45) DEFAULT NULL,
  `activatekey` varchar(245) DEFAULT NULL,
  `auth_client` varchar(45) DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  `total_notification` int(11) DEFAULT NULL,
  `linkedin_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_users`
--

LOCK TABLES `app_users` WRITE;
/*!40000 ALTER TABLE `app_users` DISABLE KEYS */;
INSERT INTO `app_users` VALUES (10,'Mahmood Akhtar',NULL,NULL,NULL,NULL,1474115399,0,1,NULL,'+919971037773',NULL,NULL,NULL,'hjfbTd1vodl6Ft7p9K268NKRATg2O7O8','$2y$13$5JnuzgeEphIb45bh215nSe.9kUjORB9Zt2QPUfnVVdIlDPnC85z1u',NULL,'akhtar@indicsoft.com',1474115737,NULL,NULL,NULL,NULL,NULL,'f1ed71b5864a8737a8251b96b161bb5cf2ea8f86',NULL,2,NULL,NULL),(8,'Rashmee',NULL,NULL,NULL,NULL,1473855711,0,1,NULL,'999999999',NULL,NULL,NULL,'BZIiO-HgBl0b4QSh6syHniX-aQpgkyb3','$2y$13$Wc.7Sk5ncL9vEIdgowkEC.WHY2d5ZKHe4e2bL.CJqvQ7Qj5eRn/yO',NULL,'rashmee.indicsoft@gmail.com',1476454763,NULL,NULL,NULL,NULL,NULL,'0137e4ec21c8397b4557b66022c1d4ad68174208',NULL,1,6,NULL),(9,'Rashmee Verma',NULL,NULL,'',NULL,1473858634,0,1,'','9876543210','Male','rrr','ff','sh9VxHRsHoygbqwx9XdJ1hJKf-SUPEcR','$2y$13$JhFluCtW7MpXnus1Y5PGoeNm3psByMjpIg4pBNBNlVW6AV08PCjBC',NULL,'rashmeeverma29@gmail.com',1474260235,NULL,NULL,NULL,NULL,NULL,'50ea0eb9ea9a7e4f637104a331b6aa6599213664',NULL,2,3,NULL),(11,'Ansh Rajput',NULL,NULL,NULL,NULL,1474277833,0,1,NULL,NULL,NULL,NULL,NULL,'Jwwv8YVGooU1inIeBJzoW5Berqgt-DCr','$2y$13$/8UhPMMVlWgr5JADR4DZauOr6Qlp5polQAngfZpdXuVxJTqAY15gi',NULL,'rakeshrajputgs@gmail.com',1474277833,NULL,NULL,NULL,NULL,'173906769716163',NULL,'facebook',NULL,NULL,NULL),(13,'Shashank',NULL,NULL,NULL,NULL,1474370130,0,1,NULL,'9350008640',NULL,NULL,NULL,'3lST_IEtXJIP8pnI-1462aCVgr_f4cr3','$2y$13$PLDwaXjFY.M62n96FMWxJeo34bWKM6/DDfJkIhhjdCoJNwaUDIJie',NULL,'shashank@onspon.com',1474370130,NULL,NULL,NULL,NULL,NULL,'90b7202be67d223f4486c266215368234eab532a',NULL,2,NULL,NULL),(14,'SHASHANK SHUKLA',NULL,NULL,NULL,NULL,1474700162,0,1,NULL,'9350008640',NULL,NULL,NULL,'0xap87dG5KW9nW0avVVv3G29QB9qPMxx','$2y$13$It7D.d46zzNL7S8kNzsWye4YJMofx7GkmWjPP5rqrOG8JJSMfak8C',NULL,'shash_shukla@yahoo.com',1475851078,NULL,NULL,NULL,NULL,NULL,'e803a55c0a8e15e543e9455aa5642bf2d07e1ef4',NULL,2,NULL,NULL),(15,'SHASHANK',NULL,NULL,NULL,NULL,1474700544,0,1,NULL,'9350008640',NULL,NULL,NULL,'eCxb6a_eUtWzjvfoeAZXYTaBPYpFoIgf','$2y$13$M93rUmkKHRdvoDMd/faVme2xpINaSUwwid4oD7opRLTErKbg2ftjm',NULL,'shraddhati2014@gmail.com',1474700544,NULL,NULL,NULL,NULL,NULL,'295eb0fabd7b2d1d76861a3ec5a6b7c4f0d29f15',NULL,1,1,NULL),(25,'rakesh',NULL,NULL,NULL,NULL,1475474693,0,1,NULL,'7530889602',NULL,NULL,NULL,'8oKJD9F-twWzt2crJ3hejtO5HgyEIr6p','$2y$13$UewKFBJFe8dLfPTP3QJHvOSZxIOlktgt4RUvoQvdFSAfgJmeyRDSy',NULL,'onspon3@gmail.com',1475474693,NULL,NULL,NULL,NULL,NULL,'6462a86efb61adf48b1308dbf361c2969d6a69aa',NULL,2,1,NULL),(49,'Abubakar Ansari',NULL,NULL,NULL,NULL,1476178904,0,1,NULL,NULL,NULL,NULL,NULL,'9XK-LlUwOIOh5Bq1sKZDzGFGCOcPsYqR','$2y$13$K0gHBHHL2JjeEgTU9jQRt.Q9Jkh/4PjqoRXbNTsv/ubcfXKU9mnuC',NULL,'abubakar10.alameen@gmail.com',1476178904,NULL,NULL,NULL,NULL,'1142977035793530',NULL,'facebook',NULL,NULL,NULL),(22,'rakesh',NULL,NULL,NULL,NULL,1475304997,0,1,NULL,'7530889602',NULL,NULL,NULL,'llXIAXQqEHDyG4ftcrR1Ip3rWjDl1j47','$2y$13$HtZiRqjZbdNRkkLYZNkQ2OxBTK7FSszT6KIEwl.biVx2fO8tsU54i',NULL,'onspon2@gmail.com',1476438052,NULL,NULL,NULL,NULL,NULL,'702773da9bf383bcb73be8549baa6688bb38bc28',NULL,1,0,NULL),(29,NULL,NULL,NULL,NULL,NULL,1475571417,0,1,NULL,'9559905118',NULL,NULL,NULL,'6DfkM-QPiz96jo5t2Qt6IPdEjsHyB5ms','$2y$13$qXwe1Bwf22ED39jnIRXMKex52AY1e1VTjBQh5wlqAeGZF4ZZSO692',NULL,'amiruddin.indicsoft@gmail.com',1475571417,NULL,NULL,NULL,NULL,NULL,'83aa36d7334612d684de3489b87c1a56df5fb4e8',NULL,2,NULL,NULL),(26,'Rakesh Rajput',NULL,NULL,NULL,NULL,1475475174,0,1,NULL,NULL,NULL,NULL,NULL,'ryaJ-9NXIvzZeRuiPMXHLKSO_uoN95oR','$2y$13$5EJRAkVncleRwtXXTSQRuO5hYC2VWGD79yWvfaTW/9vmwsziwR8qG',NULL,'rakesh.singh7476@gmail.com',1475475174,NULL,NULL,NULL,NULL,'940062212772163',NULL,'facebook',NULL,NULL,NULL),(27,'rakesh',NULL,NULL,NULL,NULL,1475477710,0,1,NULL,'7530889602',NULL,NULL,NULL,'9VI32w5LXGAjiOKdESdgYfOId7w4ELO1','$2y$13$JpEpu9OuN8VU7GEm2CuN1uv5j9.yKtFePcsYiqSynS6JFFfw/h9Fm',NULL,'rakeshrajputgsinfo@gmail.com',1475477710,NULL,NULL,NULL,NULL,NULL,'be2eb9c4cb561e63a70c2cd9317fd597d6a8b0d4',NULL,1,NULL,NULL),(28,NULL,NULL,NULL,NULL,NULL,1475558068,0,0,NULL,'7812356985',NULL,NULL,NULL,'liWlb-79W9TIv1XL6hyK74PWqJ4sL4un','$2y$13$i2hDDIvHBMXkluD36UUZCuwMAxwfc15QTOdxyK7H7jKiY7F2QTg5y',NULL,'sfd@gmail.com',1475558068,NULL,NULL,NULL,NULL,NULL,'4dab72bc870dbe1826a4549970b466e273c39383',NULL,2,NULL,NULL),(30,'rakesh',NULL,NULL,'dhf',NULL,1475657599,0,1,'','8579099300','Male','sec 50 guraon','delhi','Ju-jfldLUdICcC29lANzQ4cHJ5kLgLjt','$2y$13$iOA8UmIRp7UX5Cu3SH331.1Ns4yrbXy4tKfS0K2ZQp5xWHUuHBkjm',NULL,'rakeshrajput3320@gmail.com',1475657599,NULL,NULL,NULL,NULL,NULL,'c52b2228d6e4fc21b64daa31b33a980b3f30b8ca',NULL,2,NULL,NULL),(31,'Amrita',NULL,NULL,NULL,NULL,1475661411,0,1,NULL,'9123456789',NULL,NULL,NULL,'wDseZtGrAxEYeSf5QoV8pz8Iy-FnCFX7','$2y$13$fo2hSK4xdGi0V5t53ewO8eryDXyVh8.yl5hxaucbf5kjK4JhquZxi',NULL,'amrita@indicsoft.com',1475661411,NULL,NULL,NULL,NULL,NULL,'d44306f56082cdcd87c05eb35c24115463b811ed',NULL,1,NULL,NULL),(33,NULL,NULL,NULL,NULL,NULL,1475735581,0,0,NULL,'9845098839',NULL,NULL,NULL,'X9CFTZwQmr3i3RElRpitVhWFHlM_SztL','$2y$13$4X0Aq8cDMUz49PjvSyj.5.GzkKxwWfTSUS9lz2b3BKlqiv4WAR7Cu',NULL,'faraaz@onspon.com',1475735581,NULL,NULL,NULL,NULL,NULL,'f226bfe1a7c7448347e0176d5220766d96a38f23',NULL,2,NULL,NULL),(32,'Shabana Sheikh',NULL,NULL,NULL,NULL,1475689582,0,1,NULL,NULL,NULL,NULL,NULL,'TOxM6GwRKXrzcclgXAWwDfjUfN7WMoGj','$2y$13$AxyaH4vJdrb1illqS7XpXOh2JNmfa3/JyHgcy9fnMNYshuwn12bdy',NULL,'',1475689582,NULL,NULL,NULL,NULL,'645294355632474',NULL,'facebook',NULL,NULL,NULL),(47,'Shash Shukla',NULL,NULL,NULL,NULL,1476167519,0,1,NULL,NULL,NULL,NULL,NULL,'XKLwrPIQDZG4tNQ2TotdY_YcRxh6tlJs','$2y$13$FrBsnJH.yMWlaCP.w5aWk.RHpfoNTd36peQpZChh6gFtWHCp3FiBq',NULL,'shash_shukla@yahoo.com',1476167519,NULL,NULL,NULL,NULL,'10154601773790818',NULL,'facebook',NULL,NULL,NULL),(48,'Parminder Singh',NULL,NULL,NULL,NULL,1476177634,0,1,NULL,NULL,NULL,NULL,NULL,'Q4O1rbufzQadMnX0nCrvw7nYnJ9Suf_M','$2y$13$Tz28U3vTgURkIDMiwPvQwOiMFVRR92Aa4dSTsbe0og.AesCi/LXr2',NULL,'pammysayshello@gmail.com',1476177634,NULL,NULL,NULL,NULL,'1380150925343262',NULL,'facebook',NULL,NULL,NULL),(36,'Amiruddin Ahmad','https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50',NULL,NULL,NULL,1475836857,0,1,NULL,NULL,NULL,NULL,NULL,'_LVTqP8B2J6nr2harb1tgBw1EvsA5RVK','$2y$13$Kn8mjsTq41DCflcFGPQ9a.Oyq6OpibtNkCCO19zlmW8hjGxKcsxXa',NULL,'amir@indicsoft.com',1475836857,NULL,NULL,NULL,'106363409714438329392',NULL,NULL,'google',NULL,NULL,NULL),(46,'Parminder Singh',NULL,NULL,NULL,NULL,1476092664,0,1,NULL,NULL,NULL,NULL,NULL,'mKP8RwtLq4csi6n1pOZ7GRV7-6h-TM9V','$2y$13$F/NYNw4y3XRFPMW75vlwHuX/syhzemixRjnjJWOEFFlDxMGAcBoeO',NULL,'developing.muc@gmail.com',1476092664,NULL,NULL,NULL,NULL,'117476342051860',NULL,'facebook',1,NULL,NULL),(38,NULL,NULL,NULL,NULL,NULL,1475849801,0,0,NULL,'9999999999',NULL,NULL,NULL,'lfKUa9kkNzOzjPbYMvbl7wj_r0rrcnPa','$2y$13$c1gsGXxO9Ox2GLVTbkQGtOMR7aXko3cfhEj64HrlGjxYcIfOMXT3G',NULL,'amir@indicsoft.com',1475849801,NULL,NULL,NULL,NULL,NULL,'f29bfc809dae4cb8c440c2f799c51a99386ab581',NULL,1,NULL,NULL),(39,NULL,NULL,NULL,NULL,NULL,1475850793,0,0,NULL,'9354000989',NULL,NULL,NULL,'3lHLvMc9Ohq3BTQTAtbs_VgEFs2naqvF','$2y$13$/8dkQF0CDhM2PJJGpE.Zxetqed46IZOmNucu23F0cMqC5f0B3668u',NULL,'fdf@gmail.com',1475850793,NULL,NULL,NULL,NULL,NULL,'76efdd6a41f22e18248f4600c9cc4693e4d85fd3',NULL,2,NULL,NULL),(40,NULL,NULL,NULL,NULL,NULL,1475900077,0,0,NULL,'1251456585',NULL,NULL,NULL,'5ZiDMGaHh60ENszW7Nh5C7U44W0pshBW','$2y$13$F79lHU2EoUWqVcix9aQ0oOHMmQtW7a0XdcOWabcBFg9HmpEULq2L.',NULL,'ads@gmail.com',1475900077,NULL,NULL,NULL,NULL,NULL,'9e0914bf9cef45b5cd221aa0a34cc7f6294f9388',NULL,2,NULL,NULL),(41,'Teena Gossain',NULL,NULL,NULL,NULL,1476006669,0,1,NULL,NULL,NULL,NULL,NULL,'UX9EFDScQ0vTZMeVSzyn-lz49yKXbRTC','$2y$13$ABcWqEh2bCUzhr08wKM0BO1XCeZo8Vo5C7djFYnnFEgGWUygQOAsC',NULL,'gossain.teena@gmail.com',1476006669,NULL,NULL,NULL,NULL,'10155432823782818',NULL,'facebook',NULL,NULL,NULL),(42,'Hitesh Gossain','https://lh6.googleusercontent.com/-nifY3L9jT10/AAAAAAAAAAI/AAAAAAAAA4E/kSoXqgx-oak/photo.jpg?sz=50',NULL,NULL,NULL,1476007027,0,1,NULL,NULL,NULL,NULL,NULL,'oaQg1PK6A23OP42aAVqvt-EXK_OJG08J','$2y$13$wTkqY351WRFA4OroRm5T8eU.L081TwuLX4ospbqzHmeWWrsqaHi6u',NULL,'hitesh@onspon.com',1476007027,NULL,NULL,NULL,'117430506015607542016',NULL,NULL,'google',NULL,NULL,NULL),(43,'Hitesh Gossain',NULL,NULL,NULL,NULL,1476076882,0,1,NULL,NULL,NULL,NULL,NULL,'bHakhnP2ldpmJmAs0UdoYknuaoDWM0bF','$2y$13$YALhK3Tcv8uHlYA61SmEw.7TL5Jkj7crhmVrxG/TViXjq18l7RohK',NULL,'gossain@gmail.com',1476076882,NULL,NULL,NULL,NULL,'10154380252386609',NULL,'facebook',NULL,NULL,NULL),(50,'Shashank Shukla','https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50',NULL,NULL,NULL,1476259370,0,1,NULL,NULL,NULL,NULL,NULL,'egzg4MEoVHKkPuGMPj_iPA505fdDYIRT','$2y$13$zoe5vzB91KR2J6LEOG0JCOLRTrHSihjxA950j/QVbo7w6ppBfn8Le',NULL,'shashank@onspon.com',1476259370,NULL,NULL,NULL,'102977668685244802754',NULL,NULL,'google',1,NULL,NULL),(44,'Amrita Malhotra',NULL,NULL,NULL,NULL,1476088267,0,1,NULL,NULL,NULL,NULL,NULL,'mhoza8_PO4qouXpRWc2HVUrWJuakIgnU','$2y$13$1FpTjiwuzYwZL6oD.XoJTuCnv0HlXEyHktMSZlbpxhx3OV5vv/cAe',NULL,'amrita@indicsoft.com',1476088267,NULL,NULL,NULL,NULL,'108769112926197',NULL,'facebook',NULL,NULL,NULL),(51,'Suma Abey',NULL,NULL,NULL,NULL,1476259696,0,1,NULL,NULL,NULL,NULL,NULL,'u7Wgae5NGNvb1bI3tGj7DUT0Bwsfly0m','$2y$13$rAT.mhdGjbzgTq4UThncPO23KfIX9B7.rCi1PDBnlQ9d5.rS.LlqC',NULL,'suma.abey@gmail.com',1476259696,NULL,NULL,NULL,NULL,'1248723381825251',NULL,'facebook',NULL,NULL,NULL),(52,'Salman Raza',NULL,NULL,NULL,NULL,1476267437,0,1,NULL,NULL,NULL,NULL,NULL,'CbHEmrsTia5sl18O74BZmjoagSoBLqPy','$2y$13$IgdLEVAwsNg64ylcK4iQNuiZkTKJOs4VOlvyDx5pF2JS1K0ufiyZ2',NULL,'salraza1993@gmail.com',1476267437,NULL,NULL,NULL,NULL,'685313108300048',NULL,'facebook',1,NULL,NULL);
/*!40000 ALTER TABLE `app_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_users_sample`
--

DROP TABLE IF EXISTS `app_users_sample`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_users_sample` (
  `news_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `image` varchar(128) DEFAULT NULL,
  `short` varchar(1024) DEFAULT NULL,
  `text` text NOT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `views` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`news_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_users_sample`
--

LOCK TABLES `app_users_sample` WRITE;
/*!40000 ALTER TABLE `app_users_sample` DISABLE KEYS */;
/*!40000 ALTER TABLE `app_users_sample` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_venue`
--

DROP TABLE IF EXISTS `app_venue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_venue` (
  `venue_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `address` varchar(256) NOT NULL,
  `city_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`venue_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_venue`
--

LOCK TABLES `app_venue` WRITE;
/*!40000 ALTER TABLE `app_venue` DISABLE KEYS */;
INSERT INTO `app_venue` VALUES (1,'test','dfghjk',1,1,1,'2016-09-08 09:05:13','0000-00-00 00:00:00','test',1);
/*!40000 ALTER TABLE `app_venue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easyii_admins`
--

DROP TABLE IF EXISTS `easyii_admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easyii_admins` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `password` varchar(64) NOT NULL,
  `auth_key` varchar(128) NOT NULL,
  `access_token` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`admin_id`),
  UNIQUE KEY `access_token` (`access_token`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easyii_admins`
--

LOCK TABLES `easyii_admins` WRITE;
/*!40000 ALTER TABLE `easyii_admins` DISABLE KEYS */;
/*!40000 ALTER TABLE `easyii_admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easyii_article_categories`
--

DROP TABLE IF EXISTS `easyii_article_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easyii_article_categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `image` varchar(128) DEFAULT NULL,
  `order_num` int(11) DEFAULT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `tree` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easyii_article_categories`
--

LOCK TABLES `easyii_article_categories` WRITE;
/*!40000 ALTER TABLE `easyii_article_categories` DISABLE KEYS */;
INSERT INTO `easyii_article_categories` VALUES (1,'Articles category 1',NULL,2,'articles-category-1',1,1,2,0,1),(2,'Articles category 2',NULL,1,'articles-category-2',2,1,6,0,1),(3,'Subcategory 1',NULL,1,'subcategory-1',2,2,3,1,1),(4,'Subcategory 1',NULL,1,'subcategory-1-2',2,4,5,1,1);
/*!40000 ALTER TABLE `easyii_article_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easyii_article_items`
--

DROP TABLE IF EXISTS `easyii_article_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easyii_article_items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(128) NOT NULL,
  `image` varchar(128) DEFAULT NULL,
  `short` varchar(1024) DEFAULT NULL,
  `text` text NOT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `views` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`item_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easyii_article_items`
--

LOCK TABLES `easyii_article_items` WRITE;
/*!40000 ALTER TABLE `easyii_article_items` DISABLE KEYS */;
INSERT INTO `easyii_article_items` VALUES (1,1,'First article title','/uploads/article/article-1.jpg','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt molliti','<p><strong>Sed ut perspiciatis</strong>, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit, amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt, ut labore et dolore magnam aliquam quaerat voluptatem.&nbsp;</p><ul><li>item 1</li><li>item 2</li><li>item 3</li></ul><p>ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur?</p>','first-article-title',1463747043,0,1),(2,1,'Second article title','/uploads/article/article-2.jpg','Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip','<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p><ol> <li>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </li><li>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</li></ol>','second-article-title',1463660643,0,1),(3,1,'Third article title','/uploads/article/article-3.jpg','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt molliti','<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</p>','third-article-title',1463574243,0,1);
/*!40000 ALTER TABLE `easyii_article_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easyii_carousel`
--

DROP TABLE IF EXISTS `easyii_carousel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easyii_carousel` (
  `carousel_id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(128) NOT NULL,
  `link` varchar(255) NOT NULL,
  `title` varchar(128) DEFAULT NULL,
  `text` text,
  `order_num` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`carousel_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easyii_carousel`
--

LOCK TABLES `easyii_carousel` WRITE;
/*!40000 ALTER TABLE `easyii_carousel` DISABLE KEYS */;
INSERT INTO `easyii_carousel` VALUES (1,'/uploads/carousel/banner1-607cdbc024.jpg','','Ut enim ad minim veniam, quis nostrud exercitation','Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.',1,1),(2,'/uploads/carousel/banner2-7051d7f28f.jpg','','Sed do eiusmod tempor incididunt ut labore et','Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.',2,1),(3,'/uploads/carousel/banner3-85e742dac7.jpg','','Lorem ipsum dolor sit amet, consectetur adipiscing elit','Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.',3,1);
/*!40000 ALTER TABLE `easyii_carousel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easyii_catalog_categories`
--

DROP TABLE IF EXISTS `easyii_catalog_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easyii_catalog_categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `image` varchar(128) DEFAULT NULL,
  `fields` text NOT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `tree` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `order_num` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easyii_catalog_categories`
--

LOCK TABLES `easyii_catalog_categories` WRITE;
/*!40000 ALTER TABLE `easyii_catalog_categories` DISABLE KEYS */;
INSERT INTO `easyii_catalog_categories` VALUES (1,'Gadgets',NULL,'[{\"name\":\"brand\",\"title\":\"Brand\",\"type\":\"select\",\"options\":[\"Samsung\",\"Apple\",\"Nokia\"]},{\"name\":\"storage\",\"title\":\"Storage\",\"type\":\"string\",\"options\":\"\"},{\"name\":\"touchscreen\",\"title\":\"Touchscreen\",\"type\":\"boolean\",\"options\":\"\"},{\"name\":\"cpu\",\"title\":\"CPU cores\",\"type\":\"select\",\"options\":[\"1\",\"2\",\"4\",\"8\"]},{\"name\":\"features\",\"title\":\"Features\",\"type\":\"checkbox\",\"options\":[\"Wi-fi\",\"4G\",\"GPS\"]},{\"name\":\"color\",\"title\":\"Color\",\"type\":\"checkbox\",\"options\":[\"White\",\"Black\",\"Red\",\"Blue\"]}]','gadgets',1,1,6,0,NULL,1),(2,'Smartphones',NULL,'[{\"name\":\"brand\",\"title\":\"Brand\",\"type\":\"select\",\"options\":[\"Samsung\",\"Apple\",\"Nokia\"]},{\"name\":\"storage\",\"title\":\"Storage\",\"type\":\"string\",\"options\":\"\"},{\"name\":\"touchscreen\",\"title\":\"Touchscreen\",\"type\":\"boolean\",\"options\":\"\"},{\"name\":\"cpu\",\"title\":\"CPU cores\",\"type\":\"select\",\"options\":[\"1\",\"2\",\"4\",\"8\"]},{\"name\":\"features\",\"title\":\"Features\",\"type\":\"checkbox\",\"options\":[\"Wi-fi\",\"4G\",\"GPS\"]},{\"name\":\"color\",\"title\":\"Color\",\"type\":\"checkbox\",\"options\":[\"White\",\"Black\",\"Red\",\"Blue\"]}]','smartphones',1,2,3,1,NULL,1),(3,'Tablets',NULL,'[{\"name\":\"brand\",\"title\":\"Brand\",\"type\":\"select\",\"options\":[\"Samsung\",\"Apple\",\"Nokia\"]},{\"name\":\"storage\",\"title\":\"Storage\",\"type\":\"string\",\"options\":\"\"},{\"name\":\"touchscreen\",\"title\":\"Touchscreen\",\"type\":\"boolean\",\"options\":\"\"},{\"name\":\"cpu\",\"title\":\"CPU cores\",\"type\":\"select\",\"options\":[\"1\",\"2\",\"4\",\"8\"]},{\"name\":\"features\",\"title\":\"Features\",\"type\":\"checkbox\",\"options\":[\"Wi-fi\",\"4G\",\"GPS\"]},{\"name\":\"color\",\"title\":\"Color\",\"type\":\"checkbox\",\"options\":[\"White\",\"Black\",\"Red\",\"Blue\"]}]','tablets',1,4,5,1,NULL,1);
/*!40000 ALTER TABLE `easyii_catalog_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easyii_catalog_item_data`
--

DROP TABLE IF EXISTS `easyii_catalog_item_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easyii_catalog_item_data` (
  `data_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `value` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`data_id`),
  KEY `item_id_name` (`item_id`,`name`),
  KEY `value` (`value`(300))
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easyii_catalog_item_data`
--

LOCK TABLES `easyii_catalog_item_data` WRITE;
/*!40000 ALTER TABLE `easyii_catalog_item_data` DISABLE KEYS */;
INSERT INTO `easyii_catalog_item_data` VALUES (1,1,'brand','Nokia'),(2,1,'storage','1'),(3,1,'touchscreen','0'),(4,1,'cpu','1'),(5,1,'color','White'),(6,1,'color','Red'),(7,1,'color','Blue'),(8,2,'brand','Samsung'),(9,2,'storage','32'),(10,2,'touchscreen','1'),(11,2,'cpu','8'),(12,2,'features','Wi-fi'),(13,2,'features','GPS'),(14,3,'brand','Apple'),(15,3,'storage','64'),(16,3,'touchscreen','1'),(17,3,'cpu','4'),(18,3,'features','Wi-fi'),(19,3,'features','4G'),(20,3,'features','GPS');
/*!40000 ALTER TABLE `easyii_catalog_item_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easyii_catalog_items`
--

DROP TABLE IF EXISTS `easyii_catalog_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easyii_catalog_items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(128) NOT NULL,
  `description` text,
  `available` int(11) DEFAULT '1',
  `price` float DEFAULT '0',
  `discount` int(11) DEFAULT '0',
  `data` text NOT NULL,
  `image` varchar(128) DEFAULT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`item_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easyii_catalog_items`
--

LOCK TABLES `easyii_catalog_items` WRITE;
/*!40000 ALTER TABLE `easyii_catalog_items` DISABLE KEYS */;
INSERT INTO `easyii_catalog_items` VALUES (1,2,'Nokia 3310','<h3>The legend</h3><p>The Nokia 3310 is a GSMmobile phone announced on September 1, 2000, and released in the fourth quarter of the year, replacing the popular Nokia 3210. The phone sold extremely well, being one of the most successful phones with 126 million units sold worldwide.&nbsp;The phone has since received a cult status and is still widely acclaimed today.</p><p>The 3310 was developed at the Copenhagen Nokia site in Denmark. It is a compact and sturdy phone featuring an 84 × 48 pixel pure monochrome display. It has a lighter 115 g battery variant which has fewer features; for example the 133 g battery version has the start-up image of two hands touching while the 115 g version does not. It is a slightly rounded rectangular unit that is typically held in the palm of a hand, with the buttons operated with the thumb. The blue button is the main button for selecting options, with \"C\" button as a \"back\" or \"undo\" button. Up and down buttons are used for navigation purposes. The on/off/profile button is a stiff black button located on the top of the phone.</p>',5,100,0,'{\"brand\":\"Nokia\",\"storage\":\"1\",\"touchscreen\":\"0\",\"cpu\":1,\"color\":[\"White\",\"Red\",\"Blue\"]}','/uploads/catalog/3310.jpg','nokia-3310',1463747042,1),(2,2,'Galaxy S6','<h3>Next is beautifully crafted</h3><p>With their slim, seamless, full metal and glass construction, the sleek, ultra thin edged Galaxy S6 and unique, dual curved Galaxy S6 edge are crafted from the finest materials.</p><p>And while they may be the thinnest smartphones we`ve ever created, when it comes to cutting-edge technology and flagship Galaxy experience, these 5.1\" QHD Super AMOLED smartphones are certainly no lightweights.</p>',1,1000,10,'{\"brand\":\"Samsung\",\"storage\":\"32\",\"touchscreen\":\"1\",\"cpu\":8,\"features\":[\"Wi-fi\",\"GPS\"]}','/uploads/catalog/galaxy.jpg','galaxy-s6',1463660642,1),(3,2,'Iphone 6','<h3>Next is beautifully crafted</h3><p>With their slim, seamless, full metal and glass construction, the sleek, ultra thin edged Galaxy S6 and unique, dual curved Galaxy S6 edge are crafted from the finest materials.</p><p>And while they may be the thinnest smartphones we`ve ever created, when it comes to cutting-edge technology and flagship Galaxy experience, these 5.1\" QHD Super AMOLED smartphones are certainly no lightweights.</p>',0,1100,10,'{\"brand\":\"Apple\",\"storage\":\"64\",\"touchscreen\":\"1\",\"cpu\":4,\"features\":[\"Wi-fi\",\"4G\",\"GPS\"]}','/uploads/catalog/iphone.jpg','iphone-6',1463574242,1);
/*!40000 ALTER TABLE `easyii_catalog_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easyii_faq`
--

DROP TABLE IF EXISTS `easyii_faq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easyii_faq` (
  `faq_id` int(11) NOT NULL AUTO_INCREMENT,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `order_num` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `category` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`faq_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easyii_faq`
--

LOCK TABLES `easyii_faq` WRITE;
/*!40000 ALTER TABLE `easyii_faq` DISABLE KEYS */;
INSERT INTO `easyii_faq` VALUES (1,'What benefits do I get when I chose premium listing at Onspon.com ?','<br>Onspon helps you get a much bigger outreach to both sponsors and audiences - you can integrate your ticketing with us etc. The premium packages also entitle you to a set of high quality custom made sponsor pitches. We also promote your event on our social media platforms (to view the packages, click here)',1,1,'1'),(4,'When events apply to my brand mandate, can I contact them directly ?','Yes you can',4,1,'2'),(2,'How does Onspon make money ?','We charge a listing fee to premium events ; and we charge for customized services to brands',2,1,'1'),(5,'What is the guarantee I shall get the best price ?<br>','As you can be directly in touch with the brands - you can negotiate the \r\nprices yourself. Onspon would be happy to negotiate on your behalf too -\r\n if that\'s required - you can email us at brands@onspon.com',5,1,'2'),(3,'I want to know how Onspon can help me reach sponsors','Onspon is an online marketplace which is accessed by multiple sponsors who are looking at the best events to partner / sponsor. As soon as you list with onspon - your event is searchable and accessible to all these brands. If they find your event relevant- they can directly contact you.',3,1,'1');
/*!40000 ALTER TABLE `easyii_faq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easyii_feedback`
--

DROP TABLE IF EXISTS `easyii_feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easyii_feedback` (
  `feedback_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `title` varchar(128) DEFAULT NULL,
  `text` text NOT NULL,
  `answer_subject` varchar(128) DEFAULT NULL,
  `answer_text` text,
  `time` int(11) DEFAULT '0',
  `ip` varchar(16) NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`feedback_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easyii_feedback`
--

LOCK TABLES `easyii_feedback` WRITE;
/*!40000 ALTER TABLE `easyii_feedback` DISABLE KEYS */;
INSERT INTO `easyii_feedback` VALUES (10,'Rashmee','rashmee.indicsoft@gmail.com',NULL,NULL,'              ',NULL,NULL,1475217576,'203.92.41.26',0),(11,'rakesh','rakeshrajput3320@gmail.com','','','asfdfasdfasfdsdf',NULL,NULL,1475910743,'182.64.24.70',1),(12,'suma','suma.abey@gmail.com','','','hjhhsjdf kskejrkwejkr kjkjefkwjrkwejkr','Answer on your feedback message','Hello, suma.\r\ntesting from onspon\r\n\r\nBest regards.',1476264845,'203.92.41.26',2),(6,'Rashmee','rashmeeverma29@gmail.com','','','Testing Contact Us','Answer on your feedback message','Hello, Rashmee.\r\n\r\nAnwering Your feedback Message\r\n\r\nBest regards.',1474365716,'203.92.41.26',2),(13,'ass','suma@indicsoft.com','123',NULL,'ssds',NULL,NULL,1476445911,'203.92.41.26',0);
/*!40000 ALTER TABLE `easyii_feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easyii_files`
--

DROP TABLE IF EXISTS `easyii_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easyii_files` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `file` varchar(255) NOT NULL,
  `size` int(11) NOT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `downloads` int(11) DEFAULT '0',
  `time` int(11) DEFAULT '0',
  `order_num` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easyii_files`
--

LOCK TABLES `easyii_files` WRITE;
/*!40000 ALTER TABLE `easyii_files` DISABLE KEYS */;
INSERT INTO `easyii_files` VALUES (1,'Price list','/uploads/files/example.csv',104,'price-list',0,1463747043,1);
/*!40000 ALTER TABLE `easyii_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easyii_gallery_categories`
--

DROP TABLE IF EXISTS `easyii_gallery_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easyii_gallery_categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `image` varchar(128) DEFAULT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `tree` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `order_num` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easyii_gallery_categories`
--

LOCK TABLES `easyii_gallery_categories` WRITE;
/*!40000 ALTER TABLE `easyii_gallery_categories` DISABLE KEYS */;
INSERT INTO `easyii_gallery_categories` VALUES (1,'Food','/uploads/gallery/album-1.jpg','food',1,1,2,0,2,1),(2,'Music','/uploads/gallery/album-2.jpg','music',2,1,2,0,1,1);
/*!40000 ALTER TABLE `easyii_gallery_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easyii_guestbook`
--

DROP TABLE IF EXISTS `easyii_guestbook`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easyii_guestbook` (
  `guestbook_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `title` varchar(128) DEFAULT NULL,
  `text` text NOT NULL,
  `answer` text,
  `email` varchar(128) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `ip` varchar(16) NOT NULL,
  `new` tinyint(1) DEFAULT '0',
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`guestbook_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easyii_guestbook`
--

LOCK TABLES `easyii_guestbook` WRITE;
/*!40000 ALTER TABLE `easyii_guestbook` DISABLE KEYS */;
INSERT INTO `easyii_guestbook` VALUES (1,'First user','','Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.',NULL,NULL,1463747043,'127.0.0.1',0,1),(2,'Second user','','Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.',NULL,1463747043,'127.0.0.1',0,1),(3,'Third user','','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',NULL,NULL,1463747043,'127.0.0.1',0,1);
/*!40000 ALTER TABLE `easyii_guestbook` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easyii_loginform`
--

DROP TABLE IF EXISTS `easyii_loginform`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easyii_loginform` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `ip` varchar(16) NOT NULL,
  `user_agent` varchar(1024) NOT NULL,
  `time` int(11) DEFAULT '0',
  `success` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM AUTO_INCREMENT=304 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easyii_loginform`
--

LOCK TABLES `easyii_loginform` WRITE;
/*!40000 ALTER TABLE `easyii_loginform` DISABLE KEYS */;
INSERT INTO `easyii_loginform` VALUES (1,'root','******','127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:43.0) Gecko/20100101 Firefox/43.0',1463747042,1),(2,'root','root','127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0',1471409626,0),(3,'root','******','127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0',1471409633,1),(4,'root','******','127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1471685008,1),(5,'root','******','127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1471847880,1),(6,'root','******','127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1472194248,1),(7,'root','**','127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1472226440,1),(8,'root','******','127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1472226706,1),(9,'root','******','127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1472292810,1),(10,'root','******','127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1472472934,1),(11,'root','******','127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1472531116,1),(12,'onspon','******','127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1472553259,1),(13,'root','root','127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1472559548,0),(14,'root','******','127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1472559552,1),(15,'root','******','127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1472646907,1),(16,'root','******','127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1472651057,1),(17,'root','******','127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1472891799,1),(18,'root','******','127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1473134817,1),(19,'root','root','127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1473328482,0),(20,'root','******','127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1473328491,1),(21,'root','******','127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1473329244,1),(22,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36',1473416885,1),(23,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1473418823,1),(24,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36',1473421715,1),(25,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1473426166,1),(26,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1473485230,1),(27,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36',1473485437,1),(28,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36',1473500224,1),(29,'roor','admin01','122.161.171.170','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1473500442,0),(30,'roor','admin01','122.161.171.170','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1473500496,0),(31,'root','******','122.161.171.170','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1473500504,1),(32,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36',1473503444,1),(33,'root','******','203.92.41.26','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1473505370,1),(34,'root','******','122.161.171.170','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1473505461,1),(35,'root','******','122.161.171.170','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1473508541,1),(36,'root','******','122.161.171.170','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1473655655,1),(37,'','','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1473662466,0),(38,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1473662508,1),(39,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1473669728,1),(40,'root','******','122.161.130.135','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1473671632,1),(41,'root','******','203.92.41.26','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1473679686,1),(42,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1473680945,1),(43,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36',1473683701,1),(44,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1473684286,1),(45,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1473685731,1),(46,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1473686954,1),(47,'root','******','112.196.181.214','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36',1473700913,1),(48,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1473741253,1),(49,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1473745697,1),(50,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36',1473757791,1),(51,'root','******','122.161.38.40','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1473831746,1),(52,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1473849864,1),(53,'root','******','122.161.38.40','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1473850174,1),(54,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1473850633,1),(55,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1473851701,1),(56,'root','******','122.161.38.40','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1473851761,1),(57,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1473853343,1),(58,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1473855736,1),(59,'root','******','122.161.38.40','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1473857073,1),(60,'root','******','122.161.38.40','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1473857493,1),(61,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1473858315,1),(62,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1473914764,1),(63,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1473916028,1),(64,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0',1473919352,1),(65,'root','******','122.161.168.250','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1473922208,1),(66,'root','******','122.161.168.250','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1473930770,1),(67,'root','******','122.161.168.250','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1473930771,1),(68,'root','******','122.161.168.250','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0',1473930986,1),(69,'root','******','122.161.168.250','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0',1473934540,1),(70,'root','******','122.161.168.250','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0',1473943433,1),(71,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1474006998,1),(72,'root','******','122.161.49.170','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0',1474019317,1),(73,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1474109576,1),(74,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36',1474264159,1),(75,'admin','admin','203.92.41.26','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',1474264672,0),(76,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0',1474265063,1),(77,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1474266057,1),(78,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1474278402,1),(79,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0',1474280932,1),(80,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0',1474284641,1),(81,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',1474287179,1),(82,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0',1474287564,1),(83,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',1474287623,1),(84,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',1474291865,1),(85,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',1474292054,1),(86,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1474347844,1),(87,'root','aadmin01','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1474348095,0),(88,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1474348101,1),(89,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0',1474354960,1),(90,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0',1474355054,1),(91,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1474356628,1),(92,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0',1474365734,1),(93,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1474367926,1),(94,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36',1474432883,1),(95,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1474433780,1),(96,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1474440304,1),(97,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1474442341,1),(98,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0',1474451739,1),(99,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1474451781,1),(100,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1474464217,1),(101,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1474464589,1),(102,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1474552269,1),(103,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36',1474603531,1),(104,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1474604217,1),(105,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1474609980,1),(106,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36',1474611555,1),(107,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1474614367,1),(108,'root','admmin01','203.92.41.26','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36',1474615296,0),(109,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36',1474615302,1),(110,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1474616242,1),(111,'root','******','182.64.171.108','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1474634993,1),(112,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1474635310,1),(113,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1474636795,1),(114,'root','******','182.64.171.108','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1474637825,1),(115,'root','******','122.161.239.252','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1474638757,1),(116,'root','******','122.161.239.252','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1474639183,1),(117,'root','******','122.161.239.252','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1474639475,1),(118,'root','******','122.161.239.252','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1474639732,1),(119,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1474695122,1),(120,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0',1474695433,1),(121,'root','******','122.161.239.252','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36',1474701024,1),(122,'root','******','182.68.11.6','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0',1474701034,1),(123,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1474705626,1),(124,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0',1474708170,1),(125,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0',1474709392,1),(126,'root','******','182.64.171.108','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1474710920,1),(127,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36',1474711055,1),(128,'','','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1474712652,0),(129,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1474712655,1),(130,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1474713651,1),(131,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1474714857,1),(132,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1474715198,1),(133,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1474716517,1),(134,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.101 Safari/537.36 OPR/40.0.2308.62',1474716603,1),(135,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1474721488,1),(136,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1474865500,1),(137,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36',1474873355,1),(138,'root','******','122.161.168.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1474884325,1),(139,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1474893432,1),(140,'root','******','203.92.41.26','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1474895431,1),(141,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0',1474953782,1),(142,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1474959387,1),(143,'root','******','122.161.16.119','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1474971813,1),(144,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',1474972813,1),(145,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',1474983677,1),(146,'root','******','122.161.209.152','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1475039444,1),(147,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1475040630,1),(148,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',1475042720,1),(149,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',1475046554,1),(150,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1475047063,1),(151,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',1475047670,1),(152,'root','******','122.161.209.152','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1475052330,1),(153,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',1475053254,1),(154,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',1475053254,1),(155,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',1475054270,1),(156,'root','******','182.64.167.10','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1475056296,1),(157,'root','******','122.161.209.152','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1475056297,1),(158,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1475064494,1),(159,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36',1475066707,1),(160,'root','******','119.42.58.35','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1475161130,1),(161,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1475211125,1),(162,'root','******','122.161.121.60','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1475214209,1),(163,'admin','admin01','203.92.41.26','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1475226376,0),(164,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1475226393,1),(165,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1475226415,1),(166,'admin','root','203.92.41.26','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1475227224,0),(167,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1475227230,1),(168,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1475227358,1),(169,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1475227514,1),(170,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1475227608,1),(171,'root','******','122.161.121.60','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1475232763,1),(172,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1475233587,1),(173,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1475234114,1),(174,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1475234150,1),(175,'root','******','182.69.42.160','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1475241497,1),(176,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1475241501,1),(177,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',1475242224,1),(178,'root','******','112.196.181.214','Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:44.0) Gecko/20100101 Firefox/44.0',1475246963,1),(179,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1475247732,1),(180,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1475249265,1),(181,'root','******','112.196.181.214','Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:44.0) Gecko/20100101 Firefox/44.0',1475249611,1),(182,'root','******','122.161.101.198','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1475255473,1),(183,'root','******','112.196.181.214','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36',1475257571,1),(184,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',1475299647,1),(185,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1475299757,1),(186,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1475302363,1),(187,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1475302387,1),(188,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1475305908,1),(189,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36',1475309255,1),(190,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:44.0) Gecko/20100101 Firefox/44.0',1475309515,1),(191,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1475317361,1),(192,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1475318855,1),(193,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1475325666,1),(194,'root','******','103.225.188.69','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1475410000,1),(195,'root','******','196.207.74.21','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1475426509,1),(196,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0',1475470886,1),(197,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',1475474237,1),(198,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1475487074,1),(199,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1475487527,1),(200,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1475491969,1),(201,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1475495054,1),(202,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1475498128,1),(203,'root','******','119.42.58.196','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1475511643,1),(204,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1475558138,1),(205,'root','******','122.161.164.138','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1475566623,1),(206,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:44.0) Gecko/20100101 Firefox/44.0',1475570331,1),(207,'root','******','182.64.223.119','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1475570443,1),(208,'root','******','182.64.223.119','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1475572487,1),(209,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',1475575176,1),(210,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',1475579616,1),(211,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',1475647640,1),(212,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1475660728,1),(213,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1475660869,1),(214,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1475660939,1),(215,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36',1475667459,1),(216,'root','******','122.162.11.76','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',1475676917,1),(217,'root','******','112.196.181.214','Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:44.0) Gecko/20100101 Firefox/44.0',1475682864,1),(218,'root','******','196.207.72.64','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1475688098,1),(219,'root','******','122.162.72.8','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',1475720066,1),(220,'root','******','182.48.233.25','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/601.7.7 (KHTML, like Gecko) Version/9.1.2 Safari/601.7.7',1475720171,1),(221,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',1475729737,1),(222,'root','******','122.162.11.76','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',1475735911,1),(223,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1475738044,1),(224,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1475742983,1),(225,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1475748571,1),(226,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:44.0) Gecko/20100101 Firefox/44.0',1475753396,1),(227,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1475754410,1),(228,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',1475757617,1),(229,'root','******','182.68.103.240','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',1475758396,1),(230,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1475758764,1),(231,'root','admin','112.196.181.214','Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:44.0) Gecko/20100101 Firefox/44.0',1475770869,0),(232,'root','******','112.196.181.214','Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:44.0) Gecko/20100101 Firefox/44.0',1475770873,1),(233,'root','******','112.196.181.214','Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:44.0) Gecko/20100101 Firefox/44.0',1475772254,1),(234,'root','******','112.196.181.214','Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:44.0) Gecko/20100101 Firefox/44.0',1475773985,1),(235,'root','******','182.64.96.50','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',1475834633,1),(236,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1475841650,1),(237,'root','******','182.64.96.50','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',1475842160,1),(238,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1475846276,1),(239,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36',1475846495,1),(240,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',1475846902,1),(241,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1475847815,1),(242,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',1475904440,1),(243,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:44.0) Gecko/20100101 Firefox/44.0',1475905105,1),(244,'admin','admin01','203.92.41.26','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',1475916196,0),(245,'shahrukh','indic26','203.92.41.26','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',1475917946,0),(246,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',1475923072,1),(247,'root','******','112.196.181.214','Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:44.0) Gecko/20100101 Firefox/44.0',1475943488,1),(248,'root','******','196.207.75.28','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1476014585,1),(249,'root','******','27.255.246.252','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',1476016904,1),(250,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36',1476083236,1),(251,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0',1476087212,1),(252,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',1476095558,1),(253,'root','******','182.68.217.9','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',1476108005,1),(254,'root','******','27.255.246.252','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',1476117152,1),(255,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',1476174544,1),(256,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',1476180073,1),(257,'root','admin','203.92.41.26','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36',1476247206,0),(258,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36',1476247214,1),(259,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36',1476247611,1),(260,'root','******','182.64.174.249','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',1476249808,1),(261,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1476253826,1),(262,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',1476261713,1),(263,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:44.0) Gecko/20100101 Firefox/44.0',1476264020,1),(264,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.73 Safari/537.36',1476271028,1),(265,'admin','admin01','203.92.41.26','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/50.0.2661.102 Chrome/50.0.2661.102 Safari/537.36',1476273967,0),(266,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/50.0.2661.102 Chrome/50.0.2661.102 Safari/537.36',1476273976,1),(267,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:44.0) Gecko/20100101 Firefox/44.0',1476276745,1),(268,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:44.0) Gecko/20100101 Firefox/44.0',1476278675,1),(269,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1476279993,1),(270,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1476281066,1),(271,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:44.0) Gecko/20100101 Firefox/44.0',1476337811,1),(272,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',1476341904,1),(273,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0',1476342245,1),(274,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1476346663,1),(275,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',1476351240,1),(276,'root','******','27.255.246.252','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',1476351787,1),(277,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',1476354645,1),(278,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',1476357565,1),(279,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:44.0) Gecko/20100101 Firefox/44.0',1476361091,1),(280,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:44.0) Gecko/20100101 Firefox/44.0',1476362232,1),(281,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',1476365177,1),(282,'root','******','112.196.181.214','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36',1476372567,1),(283,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0',1476419324,1),(284,'root','admin01\\','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1476422223,0),(285,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1476422229,1),(286,'root','','203.92.41.26','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',1476439001,0),(287,'root','123456','203.92.41.26','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',1476439012,0),(288,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',1476439043,1),(289,'root','admon01','182.64.240.79','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',1476439140,0),(290,'root','admon01','182.64.240.79','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',1476439146,0),(291,'root','admon01','182.64.240.79','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',1476439156,0),(292,'root','******','182.64.240.79','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',1476440492,1),(293,'rakeshrajput3320@gmail.com','asdf1234','182.64.240.79','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0',1476443901,0),(294,'root','******','182.64.240.79','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0',1476443914,1),(295,'root','******','182.64.144.84','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1476452104,1),(296,'root','******','182.64.240.79','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/601.7.7 (KHTML, like Gecko) Version/9.1.2 Safari/601.7.7',1476452349,1),(297,'root','******','182.64.144.84','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1476452357,1),(298,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',1476454011,1),(299,'root','******','203.92.41.26','Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:44.0) Gecko/20100101 Firefox/44.0',1476514385,1),(300,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',1476524899,1),(301,'root','admin','203.92.41.26','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1476682609,0),(302,'root','******','203.92.41.26','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',1476682617,1),(303,'root','******','203.92.41.26','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.73 Safari/537.36',1476705595,1);
/*!40000 ALTER TABLE `easyii_loginform` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easyii_migration`
--

DROP TABLE IF EXISTS `easyii_migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easyii_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easyii_migration`
--

LOCK TABLES `easyii_migration` WRITE;
/*!40000 ALTER TABLE `easyii_migration` DISABLE KEYS */;
INSERT INTO `easyii_migration` VALUES ('m000000_000000_base',1463747040),('m000000_000000_install',1463747042);
/*!40000 ALTER TABLE `easyii_migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easyii_modules`
--

DROP TABLE IF EXISTS `easyii_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easyii_modules` (
  `module_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `class` varchar(128) NOT NULL,
  `title` varchar(128) NOT NULL,
  `icon` varchar(32) NOT NULL,
  `settings` text NOT NULL,
  `notice` int(11) DEFAULT '0',
  `order_num` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`module_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easyii_modules`
--

LOCK TABLES `easyii_modules` WRITE;
/*!40000 ALTER TABLE `easyii_modules` DISABLE KEYS */;
INSERT INTO `easyii_modules` VALUES (1,'article','yii\\easyii\\modules\\article\\ArticleModule','Articles','pencil','{\"categoryThumb\":true,\"articleThumb\":true,\"enablePhotos\":true,\"enableShort\":true,\"shortMaxLength\":255,\"enableTags\":true,\"itemsInFolder\":false}',0,65,0),(2,'carousel','yii\\easyii\\modules\\carousel\\CarouselModule','Carousel','picture','{\"enableTitle\":true,\"enableText\":true}',0,40,1),(3,'catalog','yii\\easyii\\modules\\catalog\\CatalogModule','Catalog','list-alt','{\"categoryThumb\":true,\"itemsInFolder\":false,\"itemThumb\":true,\"itemPhotos\":true,\"itemDescription\":true,\"itemSale\":true}',0,90,0),(4,'faq','yii\\easyii\\modules\\faq\\FaqModule','FAQ','question-sign','[]',0,45,1),(5,'feedback','yii\\easyii\\modules\\feedback\\FeedbackModule','Feedback','earphone','{\"mailAdminOnNewFeedback\":true,\"subjectOnNewFeedback\":\"New feedback\",\"templateOnNewFeedback\":\"@easyii\\/modules\\/feedback\\/mail\\/en\\/new_feedback\",\"answerTemplate\":\"@easyii\\/modules\\/feedback\\/mail\\/en\\/answer\",\"answerSubject\":\"Answer on your feedback message\",\"answerHeader\":\"Hello,\",\"answerFooter\":\"Best regards.\",\"enableTitle\":false,\"enablePhone\":true,\"enableCaptcha\":false}',2,60,1),(6,'file','yii\\easyii\\modules\\file\\FileModule','Files','floppy-disk','[]',0,30,1),(7,'gallery','yii\\easyii\\modules\\gallery\\GalleryModule','Photo Gallery','camera','{\"categoryThumb\":true,\"itemsInFolder\":false}',0,120,1),(8,'guestbook','yii\\easyii\\modules\\guestbook\\GuestbookModule','Guestbook','book','{\"enableTitle\":false,\"enableEmail\":true,\"preModerate\":false,\"enableCaptcha\":false,\"mailAdminOnNewPost\":true,\"subjectOnNewPost\":\"New message in the guestbook.\",\"templateOnNewPost\":\"@easyii\\/modules\\/guestbook\\/mail\\/en\\/new_post\",\"frontendGuestbookRoute\":\"\\/guestbook\",\"subjectNotifyUser\":\"Your post in the guestbook answered\",\"templateNotifyUser\":\"@easyii\\/modules\\/guestbook\\/mail\\/en\\/notify_user\"}',0,80,1),(9,'news','yii\\easyii\\modules\\news\\NewsModule','News','bullhorn','{\"enableThumb\":true,\"enablePhotos\":true,\"enableShort\":true,\"shortMaxLength\":256,\"enableTags\":true}',0,70,0),(10,'page','yii\\easyii\\modules\\page\\PageModule','Pages','file','[]',0,50,1),(11,'shopcart','yii\\easyii\\modules\\shopcart\\ShopcartModule','Orders','shopping-cart','{\"mailAdminOnNewOrder\":true,\"subjectOnNewOrder\":\"New order\",\"templateOnNewOrder\":\"@easyii\\/modules\\/shopcart\\/mail\\/en\\/new_order\",\"subjectNotifyUser\":\"Your order status changed\",\"templateNotifyUser\":\"@easyii\\/modules\\/shopcart\\/mail\\/en\\/notify_user\",\"frontendShopcartRoute\":\"\\/shopcart\\/order\",\"enablePhone\":true,\"enableEmail\":true}',0,100,0),(12,'subscribe','yii\\easyii\\modules\\subscribe\\SubscribeModule','E-mail subscribe','envelope','[]',0,10,1),(13,'text','yii\\easyii\\modules\\text\\TextModule','Text blocks','font','[]',0,20,1),(14,'eventcategories','app\\modules\\eventcategories\\EventcategoriesModule','Event Categories','bullhorn','{\"enableThumb\":true,\"enablePhotos\":true,\"enableShort\":true,\"shortMaxLength\":256,\"enableTags\":true}',0,121,1),(15,'city','app\\modules\\city\\CityModule','City','bullhorn','{\"enableThumb\":true,\"enablePhotos\":true,\"enableShort\":true,\"shortMaxLength\":256,\"enableTags\":true}',0,122,1),(16,'state','app\\modules\\state\\StateModule','State','bullhorn','{\"enableThumb\":true,\"enablePhotos\":true,\"enableShort\":true,\"shortMaxLength\":256,\"enableTags\":true}',0,123,1),(17,'country','app\\modules\\country\\CountryModule','Country','bullhorn','{\"enableThumb\":true,\"enablePhotos\":true,\"enableShort\":true,\"shortMaxLength\":256,\"enableTags\":true}',0,124,1),(18,'tagmanagement','app\\modules\\tagmanagement\\TagmanagementModule','Tag Management','bullhorn','{\"enableThumb\":true,\"enablePhotos\":true,\"enableShort\":true,\"shortMaxLength\":256,\"enableTags\":true}',0,125,1),(19,'agegroup','app\\modules\\agegroup\\AgegroupModule','Age Group','bullhorn','{\"enableThumb\":true,\"enablePhotos\":true,\"enableShort\":true,\"shortMaxLength\":256,\"enableTags\":true}',0,126,1),(20,'education','app\\modules\\education\\EducationModule','Education','bullhorn','{\"enableThumb\":true,\"enablePhotos\":true,\"enableShort\":true,\"shortMaxLength\":256,\"enableTags\":true}',0,127,1),(21,'brand','app\\modules\\brand\\BrandModule','Brand','bullhorn','{\"enableThumb\":true,\"enablePhotos\":true,\"enableShort\":true,\"shortMaxLength\":256,\"enableTags\":true}',0,128,1),(22,'income','app\\modules\\income\\IncomeModule','Income','bullhorn','{\"enableThumb\":true,\"enablePhotos\":true,\"enableShort\":true,\"shortMaxLength\":256,\"enableTags\":true}',0,129,1),(25,'events','app\\modules\\events\\EventsModule','Events','event','{\"enableThumb\":true,\"enablePhotos\":true,\"enableShort\":true,\"shortMaxLength\":256,\"enableTags\":true}',0,149,1),(24,'venue','app\\modules\\venue\\VenueModule','Venue','bullhorn','{\"enableThumb\":true,\"enablePhotos\":true,\"enableShort\":true,\"shortMaxLength\":256,\"enableTags\":true}',0,131,1),(26,'users','app\\modules\\users\\UsersModule','User Management','bullhorn','{\"enableThumb\":true,\"enablePhotos\":true,\"enableShort\":true,\"shortMaxLength\":256,\"enableTags\":true}',0,144,1),(27,'role','app\\modules\\role\\RoleModule','Role','bullhorn','{\"enableThumb\":true,\"enablePhotos\":true,\"enableShort\":true,\"shortMaxLength\":256,\"enableTags\":true}',0,132,1),(29,'testimonials','app\\modules\\testimonials\\TestimonialsModule','Testimonials','bullhorn','{\"enableThumb\":true,\"enablePhotos\":true,\"enableShort\":true,\"shortMaxLength\":256,\"enableTags\":true}',0,133,1),(31,'industry','app\\modules\\industry\\IndustryModule','Industry','bullhorn','{\"enableThumb\":true,\"enablePhotos\":true,\"enableShort\":true,\"shortMaxLength\":256,\"enableTags\":true}',0,134,1),(32,'sponsorshipobjective','app\\modules\\sponsorshipobjective\\SponsorshipobjectiveModule','Sponsorship Objective','bullhorn','{\"enableThumb\":true,\"enablePhotos\":true,\"enableShort\":true,\"shortMaxLength\":256,\"enableTags\":true}',0,135,1),(33,'brandmandates','app\\modules\\brandmandates\\BrandmandatesModule','Brand Mandates','bullhorn','{\"enableThumb\":true,\"enablePhotos\":true,\"enableShort\":true,\"shortMaxLength\":256,\"enableTags\":true}',0,148,1),(35,'sponsorprice','app\\modules\\sponsorprice\\SponsorpriceModule','Sponsor Price','bullhorn','{\"enableThumb\":true,\"enablePhotos\":true,\"enableShort\":true,\"shortMaxLength\":256,\"enableTags\":true}',0,136,1),(36,'eventtestimonials','app\\modules\\eventtestimonials\\EventtestimonialsModule','Event Testimonials','bullhorn','{\"enableThumb\":true,\"enablePhotos\":true,\"enableShort\":true,\"shortMaxLength\":256,\"enableTags\":true}',0,137,1),(37,'brandcategories','app\\modules\\brandcategories\\BrandcategoriesModule','Brand Categories','bullhorn','{\"enableThumb\":true,\"enablePhotos\":true,\"enableShort\":true,\"shortMaxLength\":256,\"enableTags\":true}',0,138,1),(40,'homebanner','app\\modules\\homebanner\\HomebannerModule','Home Banner','bullhorn','{\"enableThumb\":true,\"enablePhotos\":true,\"enableShort\":true,\"shortMaxLength\":256,\"enableTags\":true}',0,142,0),(38,'boxgallery','app\\modules\\boxgallery\\BoxgalleryModule','Boxgallery','bullhorn','{\"enableThumb\":true,\"enablePhotos\":true,\"enableShort\":true,\"shortMaxLength\":256,\"enableTags\":true}',0,143,1),(39,'eventgenre','app\\modules\\eventgenre\\EventgenreModule','Event Genre','bullhorn','{\"enableThumb\":true,\"enablePhotos\":true,\"enableShort\":true,\"shortMaxLength\":256,\"enableTags\":true}',0,139,1),(41,'trending','app\\modules\\trending\\TrendingModule','Trending','bullhorn','{\"enableThumb\":true,\"enablePhotos\":true,\"enableShort\":true,\"shortMaxLength\":256,\"enableTags\":true}',0,145,1),(42,'amazing','app\\modules\\amazing\\AmazingModule','Amazing','bullhorn','{\"enableThumb\":true,\"enablePhotos\":true,\"enableShort\":true,\"shortMaxLength\":256,\"enableTags\":true}',0,146,1),(43,'lastminute','app\\modules\\lastminute\\LastminuteModule','Last Minute','bullhorn','{\"enableThumb\":true,\"enablePhotos\":true,\"enableShort\":true,\"shortMaxLength\":256,\"enableTags\":true}',0,147,1),(44,'package','app\\modules\\package\\PackageModule','Package Management','bullhorn','{\"enableThumb\":true,\"enablePhotos\":true,\"enableShort\":true,\"shortMaxLength\":256,\"enableTags\":true}',0,140,1),(45,'audience','app\\modules\\audience\\AudienceModule','Audience','bullhorn','{\"enableThumb\":true,\"enablePhotos\":true,\"enableShort\":true,\"shortMaxLength\":256,\"enableTags\":true}',0,141,1);
/*!40000 ALTER TABLE `easyii_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easyii_news`
--

DROP TABLE IF EXISTS `easyii_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easyii_news` (
  `news_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `image` varchar(128) DEFAULT NULL,
  `short` varchar(1024) DEFAULT NULL,
  `text` text NOT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `views` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`news_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easyii_news`
--

LOCK TABLES `easyii_news` WRITE;
/*!40000 ALTER TABLE `easyii_news` DISABLE KEYS */;
INSERT INTO `easyii_news` VALUES (1,'First news title','/uploads/news/news-1.jpg','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt molliti','<p><strong>Sed ut perspiciatis</strong>, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit, amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt, ut labore et dolore magnam aliquam quaerat voluptatem.&nbsp;</p><ul>\r\n<li>item 1</li><li>item 2</li><li>item 3</li></ul><p>ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur?</p>','first-news-title',1474354647,0,1),(2,'Second news title','/uploads/news/news-2.jpg','Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip','<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p><ol> <li>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </li><li>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</li></ol>','second-news-title',1463660643,0,1),(3,'Third news title','/uploads/news/news-3.jpg','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt molliti','<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</p>','third-news-title',1463574243,0,1);
/*!40000 ALTER TABLE `easyii_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easyii_pages`
--

DROP TABLE IF EXISTS `easyii_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easyii_pages` (
  `page_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `text` text NOT NULL,
  `slug` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`page_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easyii_pages`
--

LOCK TABLES `easyii_pages` WRITE;
/*!40000 ALTER TABLE `easyii_pages` DISABLE KEYS */;
INSERT INTO `easyii_pages` VALUES (1,'Index','<p><strong>All elements are live-editable, switch on Live Edit button to see this feature.</strong>&nbsp;</p><p>Dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.&nbsp;Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>','page-index'),(2,'Shop','','page-shop'),(3,'Shop search','','page-shop-search'),(4,'Shopping cart','','page-shopcart'),(5,'Order created','<p>Your order successfully created. Our manager will contact you as soon as possible.</p>','page-shopcart-success'),(6,'News','','page-news'),(7,'Articles','','page-articles'),(8,'Gallery','','page-gallery'),(9,'Guestbook','','page-guestbook'),(10,'FAQ','','page-faq'),(11,'Contact','<p><strong>Address</strong>: Dominican republic, Santo Domingo, Some street 123</p><p><strong>ZIP</strong>: 123456</p><p><strong>Phone</strong>: +1 234 56-78</p><p><strong>E-mail</strong>: demo@example.com</p>','page-contact');
/*!40000 ALTER TABLE `easyii_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easyii_photos`
--

DROP TABLE IF EXISTS `easyii_photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easyii_photos` (
  `photo_id` int(11) NOT NULL AUTO_INCREMENT,
  `class` varchar(128) NOT NULL,
  `item_id` int(11) NOT NULL,
  `image` varchar(128) NOT NULL,
  `description` varchar(1024) NOT NULL,
  `order_num` int(11) NOT NULL,
  PRIMARY KEY (`photo_id`),
  KEY `model_item` (`class`,`item_id`)
) ENGINE=MyISAM AUTO_INCREMENT=104 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easyii_photos`
--

LOCK TABLES `easyii_photos` WRITE;
/*!40000 ALTER TABLE `easyii_photos` DISABLE KEYS */;
INSERT INTO `easyii_photos` VALUES (1,'yii\\easyii\\modules\\catalog\\models\\Item',1,'/uploads/photos/3310-1.jpg','',1),(2,'yii\\easyii\\modules\\catalog\\models\\Item',1,'/uploads/photos/3310-2.jpg','',2),(3,'yii\\easyii\\modules\\catalog\\models\\Item',2,'/uploads/photos/galaxy-1.jpg','',3),(4,'yii\\easyii\\modules\\catalog\\models\\Item',2,'/uploads/photos/galaxy-2.jpg','',4),(5,'yii\\easyii\\modules\\catalog\\models\\Item',2,'/uploads/photos/galaxy-3.jpg','',5),(6,'yii\\easyii\\modules\\catalog\\models\\Item',2,'/uploads/photos/galaxy-4.jpg','',6),(7,'yii\\easyii\\modules\\catalog\\models\\Item',3,'/uploads/photos/iphone-1.jpg','',7),(8,'yii\\easyii\\modules\\catalog\\models\\Item',3,'/uploads/photos/iphone-2.jpg','',8),(9,'yii\\easyii\\modules\\catalog\\models\\Item',3,'/uploads/photos/iphone-3.jpg','',9),(10,'yii\\easyii\\modules\\catalog\\models\\Item',3,'/uploads/photos/iphone-4.jpg','',10),(11,'yii\\easyii\\modules\\news\\models\\News',1,'/uploads/photos/news-1-1.jpg','',11),(12,'yii\\easyii\\modules\\news\\models\\News',1,'/uploads/photos/news-1-2.jpg','',12),(13,'yii\\easyii\\modules\\news\\models\\News',1,'/uploads/photos/news-1-3.jpg','',13),(14,'yii\\easyii\\modules\\news\\models\\News',1,'/uploads/photos/news-1-4.jpg','',14),(15,'yii\\easyii\\modules\\article\\models\\Item',1,'/uploads/photos/article-1-1.jpg','',15),(16,'yii\\easyii\\modules\\article\\models\\Item',1,'/uploads/photos/article-1-2.jpg','',16),(17,'yii\\easyii\\modules\\article\\models\\Item',1,'/uploads/photos/article-1-3.jpg','',17),(18,'yii\\easyii\\modules\\article\\models\\Item',1,'/uploads/photos/news-1-4.jpg','',18),(19,'yii\\easyii\\modules\\gallery\\models\\Category',1,'/uploads/photos/album-1-9.jpg','',19),(20,'yii\\easyii\\modules\\gallery\\models\\Category',1,'/uploads/photos/album-1-8.jpg','',20),(21,'yii\\easyii\\modules\\gallery\\models\\Category',1,'/uploads/photos/album-1-7.jpg','',21),(22,'yii\\easyii\\modules\\gallery\\models\\Category',1,'/uploads/photos/album-1-6.jpg','',22),(23,'yii\\easyii\\modules\\gallery\\models\\Category',1,'/uploads/photos/album-1-5.jpg','',23),(24,'yii\\easyii\\modules\\gallery\\models\\Category',1,'/uploads/photos/album-1-4.jpg','',24),(25,'yii\\easyii\\modules\\gallery\\models\\Category',1,'/uploads/photos/album-1-3.jpg','',25),(26,'yii\\easyii\\modules\\gallery\\models\\Category',1,'/uploads/photos/album-1-2.jpg','',26),(27,'yii\\easyii\\modules\\gallery\\models\\Category',1,'/uploads/photos/album-1-1.jpg','',27),(28,'app\\modules\\events\\models\\Events',1,'/uploads/photos/error-e4b3ef12eb.png','',28),(54,'app\\modules\\events\\models\\Events',3,'/uploads/photos/navrang-72c981b91c.png','Ut volutpat consectetur aliquam. Curabitur auctor in nis ulum ornare. Sed consequat, augue condimentum fermentum gravida, metus elit vehicula dui.',32),(55,'app\\modules\\events\\models\\Events',4,'/uploads/photos/college-fest-3-721e5a0a4a.jpg','consectetur adipiscing elit. Duis id euismod dui. Vivamus at pharetra nisi. Nulla vestibulum nec ipsum sed cursus. Duis id euismod dui. Vivamus at pharetra nisi.',33),(56,'app\\modules\\events\\models\\Events',4,'/uploads/photos/college-fest-2-8d6dbbce59.jpg','Ut volutpat consectetur aliquam. Curabitur auctor in nis ulum ornare. Sed consequat, augue condimentum fermentum gravida, metus elit vehicula dui.',34),(57,'app\\modules\\events\\models\\Events',4,'/uploads/photos/college-fest-1-f84490ac5d.jpg','consectetur adipiscing elit. Duis id euismod dui. Vivamus at pharetra nisi. Nulla vestibulum nec ipsum sed cursus. Duis id euismod dui. Vivamus at pharetra nisi.',35),(58,'app\\modules\\events\\models\\Events',5,'/uploads/photos/college-fest-9-246897163d.jpg','consectetur adipiscing elit. Duis id euismod dui. Vivamus at pharetra nisi. Nulla vestibulum nec ipsum sed cursus. Duis id euismod dui. Vivamus at pharetra nisi.',36),(59,'app\\modules\\events\\models\\Events',5,'/uploads/photos/college-fest-4-bbc0ab8bbf.jpg','consectetur adipiscing elit. Duis id euismod dui. Vivamus at pharetra nisi. Nulla vestibulum nec ipsum sed cursus. Duis id euismod dui. Vivamus at pharetra nisi.',37),(60,'app\\modules\\events\\models\\Events',5,'/uploads/photos/college-fest-5-8534bd5ace.jpg','Ut volutpat consectetur aliquam. Curabitur auctor in nis ulum ornare. Sed consequat, augue condimentum fermentum gravida, metus elit vehicula dui.',38),(61,'app\\modules\\events\\models\\Events',6,'/uploads/photos/ipl-4-31a8cd6727.jpg','consectetur adipiscing elit. Duis id euismod dui. Vivamus at pharetra nisi. Nulla vestibulum nec ipsum sed cursus. Duis id euismod dui. Vivamus at pharetra nisi.',39),(62,'app\\modules\\events\\models\\Events',6,'/uploads/photos/ipl-5-87aa5011a7.jpg','Ut volutpat consectetur aliquam. Curabitur auctor in nis ulum ornare. Sed consequat, augue condimentum fermentum gravida, metus elit vehicula dui.',40),(63,'app\\modules\\events\\models\\Events',6,'/uploads/photos/ipl-3-0068e0d87c.jpg','consectetur adipiscing elit. Duis id euismod dui. Vivamus at pharetra nisi. Nulla vestibulum nec ipsum sed cursus. Duis id euismod dui. Vivamus at pharetra nisi.',41),(64,'app\\modules\\events\\models\\Events',7,'/uploads/photos/kabaddi-1-e71845930b.jpg','Ut volutpat consectetur aliquam. Curabitur auctor in nis ulum ornare. Sed consequat, augue condimentum fermentum gravida, metus elit vehicula dui.',42),(65,'app\\modules\\events\\models\\Events',7,'/uploads/photos/kabaddi-4-6b0516d350.jpg','consectetur adipiscing elit. Duis id euismod dui. Vivamus at pharetra nisi. Nulla vestibulum nec ipsum sed cursus. Duis id euismod dui. Vivamus at pharetra nisi.',43),(66,'app\\modules\\events\\models\\Events',7,'/uploads/photos/kabaddi-3-dc2616e4a5.jpg','Ut volutpat consectetur aliquam. Curabitur auctor in nis ulum ornare. Sed consequat, augue condimentum fermentum gravida, metus elit vehicula dui.',44),(67,'app\\modules\\events\\models\\Events',7,'/uploads/photos/kabaddi-2-345063cad8.jpg','consectetur adipiscing elit. Duis id euismod dui. Vivamus at pharetra nisi. Nulla vestibulum nec ipsum sed cursus. Duis id euismod dui. Vivamus at pharetra nisi.',45),(51,'app\\modules\\events\\models\\Events',2,'/uploads/photos/iifa-1-acddca8da7.jpg','consectetur adipiscing elit. Duis id euismod dui. Vivamus at pharetra nisi. Nulla vestibulum nec ipsum sed cursus. Duis id euismod dui. Vivamus at pharetra nisi.',29),(52,'app\\modules\\events\\models\\Events',2,'/uploads/photos/iifa-4-ee183d8f1b.jpg','Ut volutpat consectetur aliquam. Curabitur auctor in nis ulum ornare. Sed consequat, augue condimentum fermentum gravida, metus elit vehicula dui.',30),(53,'app\\modules\\events\\models\\Events',2,'/uploads/photos/iifa-2-4dec07a29a.jpg','consectetur adipiscing elit. Duis id euismod dui. Vivamus at pharetra nisi. Nulla vestibulum nec ipsum sed cursus. Duis id euismod dui. Vivamus at pharetra nisi.',31),(68,'app\\modules\\events\\models\\Events',6,'/uploads/photos/ipl-1-9441098784.jpg','Ut volutpat consectetur aliquam. Curabitur auctor in nis ulum ornare. Sed consequat, augue condimentum fermentum gravida, metus elit vehicula dui.',46),(69,'app\\modules\\events\\models\\Events',8,'/uploads/photos/evp0141-a339ca1242.jpg','Ut volutpat consectetur aliquam. Curabitur auctor in nis ulum ornare. Sed consequat, augue condimentum fermentum gravida, metus elit vehicula dui.',47),(70,'app\\modules\\events\\models\\Events',8,'/uploads/photos/benifit-conf-cfd66fac76.jpg','consectetur adipiscing elit. Duis id euismod dui. Vivamus at pharetra nisi. Nulla vestibulum nec ipsum sed cursus. Duis id euismod dui. Vivamus at pharetra nisi.',48),(71,'app\\modules\\events\\models\\Events',2,'/uploads/photos/iifa-e7be57b0e8.jpg','Ut volutpat consectetur aliquam. Curabitur auctor in nis ulum ornare. Sed consequat, augue condimentum fermentum gravida, metus elit vehicula dui.',49),(72,'app\\modules\\events\\models\\Events',2,'/uploads/photos/iifa-4-db7e85aeb1.jpg','Ut volutpat consectetur aliquam. Curabitur auctor in nis ulum ornare. Sed consequat, augue condimentum fermentum gravida, metus elit vehicula dui.',50),(73,'app\\modules\\events\\models\\Events',2,'/uploads/photos/iifa-2-49cabf4382.jpg','consectetur adipiscing elit. Duis id euismod dui. Vivamus at pharetra nisi. Nulla vestibulum nec ipsum sed cursus. Duis id euismod dui. Vivamus at pharetra nisi.',51),(74,'app\\modules\\events\\models\\Events',2,'/uploads/photos/iifa-3-0b6ea5b4ac.jpg','Ut volutpat consectetur aliquam. Curabitur auctor in nis ulum ornare. Sed consequat, augue condimentum fermentum gravida, metus elit vehicula dui.',52),(75,'app\\modules\\events\\models\\Events',2,'/uploads/photos/iifa-5-87aadca971.jpg','consectetur adipiscing elit. Duis id euismod dui. Vivamus at pharetra nisi. Nulla vestibulum nec ipsum sed cursus. Duis id euismod dui. Vivamus at pharetra nisi.',53),(76,'app\\modules\\events\\models\\Events',3,'/uploads/photos/navrang1-2a29df49da.jpg','consectetur adipiscing elit. Duis id euismod dui. Vivamus at pharetra nisi. Nulla vestibulum nec ipsum sed cursus. Duis id euismod dui. Vivamus at pharetra nisi.',54),(77,'app\\modules\\events\\models\\Events',3,'/uploads/photos/navrang3-242075dcc7.jpg','Ut volutpat consectetur aliquam. Curabitur auctor in nis ulum ornare. Sed consequat, augue condimentum fermentum gravida, metus elit vehicula dui.',55),(78,'app\\modules\\events\\models\\Events',3,'/uploads/photos/navrang2-bc72de0d25.jpg','consectetur adipiscing elit. Duis id euismod dui. Vivamus at pharetra nisi. Nulla vestibulum nec ipsum sed cursus. Duis id euismod dui. Vivamus at pharetra nisi.',56),(79,'app\\modules\\events\\models\\Events',8,'/uploads/photos/conference2-209ae073ab.jpg','Ut volutpat consectetur aliquam. Curabitur auctor in nis ulum ornare. Sed consequat, augue condimentum fermentum gravida, metus elit vehicula dui.',57),(80,'app\\modules\\events\\models\\Events',8,'/uploads/photos/conference3-e8c780d185.jpg','consectetur adipiscing elit. Duis id euismod dui. Vivamus at pharetra nisi. Nulla vestibulum nec ipsum sed cursus. Duis id euismod dui. Vivamus at pharetra nisi.',58),(81,'app\\modules\\events\\models\\Events',8,'/uploads/photos/conference1-b5602d96fc.jpg','Ut volutpat consectetur aliquam. Curabitur auctor in nis ulum ornare. Sed consequat, augue condimentum fermentum gravida, metus elit vehicula dui.',59),(82,'app\\modules\\events\\models\\Events',9,'/uploads/photos/entertainment2-0b1384916a.jpg','consectetur adipiscing elit. Duis id euismod dui. Vivamus at pharetra nisi. Nulla vestibulum nec ipsum sed cursus. Duis id euismod dui. Vivamus at pharetra nisi.',60),(83,'app\\modules\\events\\models\\Events',9,'/uploads/photos/entertainment-1-23350a31b5.jpg','Ut volutpat consectetur aliquam. Curabitur auctor in nis ulum ornare. Sed consequat, augue condimentum fermentum gravida, metus elit vehicula dui.',61),(84,'app\\modules\\events\\models\\Events',9,'/uploads/photos/entertainment3-f72f72d048.jpg','consectetur adipiscing elit. Duis id euismod dui. Vivamus at pharetra nisi. Nulla vestibulum nec ipsum sed cursus. Duis id euismod dui. Vivamus at pharetra nisi.',62),(85,'app\\modules\\events\\models\\Events',10,'/uploads/photos/digital3-4a02e29af7.jpeg','consectetur adipiscing elit. Duis id euismod dui. Vivamus at pharetra nisi. Nulla vestibulum nec ipsum sed cursus. Duis id euismod dui. Vivamus at pharetra nisi.',63),(86,'app\\modules\\events\\models\\Events',11,'/uploads/photos/digital7-96900d1084.jpg','',64),(87,'app\\modules\\events\\models\\Events',10,'/uploads/photos/digital2-15943bd309.jpg','Ut volutpat consectetur aliquam. Curabitur auctor in nis ulum ornare. Sed consequat, augue condimentum fermentum gravida, metus elit vehicula dui.',65),(88,'app\\modules\\events\\models\\Events',10,'/uploads/photos/digital1-b23b068cc6.jpg','consectetur adipiscing elit. Duis id euismod dui. Vivamus at pharetra nisi. Nulla vestibulum nec ipsum sed cursus. Duis id euismod dui. Vivamus at pharetra nisi.',66),(89,'app\\modules\\events\\models\\Events',11,'/uploads/photos/digital-1d3899b999.jpg','',67),(90,'app\\modules\\events\\models\\Events',10,'/uploads/photos/digital4-93b7f55924.jpg','Ut volutpat consectetur aliquam. Curabitur auctor in nis ulum ornare. Sed consequat, augue condimentum fermentum gravida, metus elit vehicula dui.',68),(91,'app\\modules\\events\\models\\Events',11,'/uploads/photos/digital8-be5a5140b5.jpg','',69),(96,'app\\modules\\events\\models\\Events',570,'','',70),(97,'app\\modules\\events\\models\\Events',576,'','',71),(98,'app\\modules\\events\\models\\Events',585,'','',72),(99,'app\\modules\\events\\models\\Events',659,'','',73),(100,'app\\modules\\events\\models\\Events',677,'','',74),(101,'app\\modules\\events\\models\\Events',702,'','',75),(102,'app\\modules\\events\\models\\Events',724,'','',76),(103,'app\\modules\\events\\models\\Events',725,'','',77);
/*!40000 ALTER TABLE `easyii_photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easyii_seotext`
--

DROP TABLE IF EXISTS `easyii_seotext`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easyii_seotext` (
  `seotext_id` int(11) NOT NULL AUTO_INCREMENT,
  `class` varchar(128) NOT NULL,
  `item_id` int(11) NOT NULL,
  `h1` varchar(128) DEFAULT NULL,
  `title` varchar(128) DEFAULT NULL,
  `keywords` varchar(128) DEFAULT NULL,
  `description` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`seotext_id`),
  UNIQUE KEY `model_item` (`class`,`item_id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easyii_seotext`
--

LOCK TABLES `easyii_seotext` WRITE;
/*!40000 ALTER TABLE `easyii_seotext` DISABLE KEYS */;
INSERT INTO `easyii_seotext` VALUES (1,'yii\\easyii\\modules\\page\\models\\Page',1,'','Onspon','','yii2, easyii, admin'),(2,'yii\\easyii\\modules\\page\\models\\Page',2,'Shop categories','Extended shop title','',''),(3,'yii\\easyii\\modules\\page\\models\\Page',3,'Shop search results','Extended shop search title','',''),(4,'yii\\easyii\\modules\\page\\models\\Page',4,'Shopping cart H1','Extended shopping cart title','',''),(5,'yii\\easyii\\modules\\page\\models\\Page',5,'Success','Extended order success title','',''),(6,'yii\\easyii\\modules\\page\\models\\Page',6,'News H1','Extended news title','',''),(7,'yii\\easyii\\modules\\page\\models\\Page',7,'Articles H1','Extended articles title','',''),(8,'yii\\easyii\\modules\\page\\models\\Page',8,'Photo gallery','Extended gallery title','',''),(9,'yii\\easyii\\modules\\page\\models\\Page',9,'Guestbook H1','Extended guestbook title','',''),(10,'yii\\easyii\\modules\\page\\models\\Page',10,'Frequently Asked Question','Extended faq title','',''),(11,'yii\\easyii\\modules\\page\\models\\Page',11,'Contact us','Extended contact title','',''),(12,'yii\\easyii\\modules\\catalog\\models\\Category',2,'Smartphones H1','Extended smartphones title','',''),(13,'yii\\easyii\\modules\\catalog\\models\\Category',3,'Tablets H1','Extended tablets title','',''),(14,'yii\\easyii\\modules\\catalog\\models\\Item',1,'Nokia 3310','','',''),(15,'yii\\easyii\\modules\\catalog\\models\\Item',2,'Samsung Galaxy S6','','',''),(16,'yii\\easyii\\modules\\catalog\\models\\Item',3,'Apple Iphone 6','','',''),(17,'yii\\easyii\\modules\\news\\models\\News',1,'First news H1','','',''),(18,'yii\\easyii\\modules\\news\\models\\News',2,'Second news H1','','',''),(19,'yii\\easyii\\modules\\news\\models\\News',3,'Third news H1','','',''),(20,'yii\\easyii\\modules\\article\\models\\Category',1,'Articles category 1 H1','Extended category 1 title','',''),(21,'yii\\easyii\\modules\\article\\models\\Category',3,'Subcategory 1 H1','Extended subcategory 1 title','',''),(22,'yii\\easyii\\modules\\article\\models\\Category',4,'Subcategory 2 H1','Extended subcategory 2 title','',''),(23,'yii\\easyii\\modules\\article\\models\\Item',1,'First article H1','','',''),(24,'yii\\easyii\\modules\\article\\models\\Item',2,'Second article H1','','',''),(25,'yii\\easyii\\modules\\article\\models\\Item',3,'Third article H1','','',''),(26,'yii\\easyii\\modules\\gallery\\models\\Category',1,'Album 1 H1','Extended Album 1 title','',''),(27,'yii\\easyii\\modules\\gallery\\models\\Category',2,'Album 2 H1','Extended Album 2 title','','');
/*!40000 ALTER TABLE `easyii_seotext` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easyii_settings`
--

DROP TABLE IF EXISTS `easyii_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easyii_settings` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `title` varchar(128) NOT NULL,
  `value` varchar(1024) NOT NULL,
  `visibility` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`setting_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easyii_settings`
--

LOCK TABLES `easyii_settings` WRITE;
/*!40000 ALTER TABLE `easyii_settings` DISABLE KEYS */;
INSERT INTO `easyii_settings` VALUES (1,'easyii_version','EasyiiCMS version','0.9',0),(2,'recaptcha_key','ReCaptcha key','6LfvQgkUAAAAAAR_PErYgSDIDnvK-dfwnOPQaV61',1),(3,'password_salt','Password salt','x-ea_jcc9tGXW4Xaytbjm08tfFpSFvfS',0),(4,'root_auth_key','Root authorization key','m54LMX-XkCHS2dph8F6SlTz0HNNrmHPN',0),(5,'root_password','Root password','7d4247d5b7cc9967eee200b70687851452b8c4ee',1),(6,'auth_time','Auth time','86400',1),(7,'robot_email','Robot E-mail','rashmee.indicsoft@gmail.com',1),(8,'admin_email','Admin E-mail','rashmee.indicsoft@gmail.com',2),(9,'recaptcha_secret','ReCaptcha secret','6LfvQgkUAAAAAHKxaK0-FNNLBr-bEEGMO_eUk0Xo',1),(10,'toolbar_position','Frontend toolbar position (\"top\" or \"bottom\")','top',1);
/*!40000 ALTER TABLE `easyii_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easyii_shopcart_goods`
--

DROP TABLE IF EXISTS `easyii_shopcart_goods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easyii_shopcart_goods` (
  `good_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `options` varchar(255) NOT NULL,
  `price` float DEFAULT '0',
  `discount` int(11) DEFAULT '0',
  PRIMARY KEY (`good_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easyii_shopcart_goods`
--

LOCK TABLES `easyii_shopcart_goods` WRITE;
/*!40000 ALTER TABLE `easyii_shopcart_goods` DISABLE KEYS */;
/*!40000 ALTER TABLE `easyii_shopcart_goods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easyii_shopcart_orders`
--

DROP TABLE IF EXISTS `easyii_shopcart_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easyii_shopcart_orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(64) NOT NULL,
  `email` varchar(128) NOT NULL,
  `comment` varchar(1024) NOT NULL,
  `remark` varchar(1024) NOT NULL,
  `access_token` varchar(32) NOT NULL,
  `ip` varchar(16) NOT NULL,
  `time` int(11) DEFAULT '0',
  `new` tinyint(1) DEFAULT '0',
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easyii_shopcart_orders`
--

LOCK TABLES `easyii_shopcart_orders` WRITE;
/*!40000 ALTER TABLE `easyii_shopcart_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `easyii_shopcart_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easyii_subscribe_history`
--

DROP TABLE IF EXISTS `easyii_subscribe_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easyii_subscribe_history` (
  `history_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(128) NOT NULL,
  `body` text NOT NULL,
  `sent` int(11) DEFAULT '0',
  `time` int(11) DEFAULT '0',
  PRIMARY KEY (`history_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easyii_subscribe_history`
--

LOCK TABLES `easyii_subscribe_history` WRITE;
/*!40000 ALTER TABLE `easyii_subscribe_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `easyii_subscribe_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easyii_subscribe_subscribers`
--

DROP TABLE IF EXISTS `easyii_subscribe_subscribers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easyii_subscribe_subscribers` (
  `subscriber_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL,
  `ip` varchar(16) NOT NULL,
  `time` int(11) DEFAULT '0',
  PRIMARY KEY (`subscriber_id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easyii_subscribe_subscribers`
--

LOCK TABLES `easyii_subscribe_subscribers` WRITE;
/*!40000 ALTER TABLE `easyii_subscribe_subscribers` DISABLE KEYS */;
INSERT INTO `easyii_subscribe_subscribers` VALUES (2,'wefsd@vdd.com','203.92.41.26',1476443823);
/*!40000 ALTER TABLE `easyii_subscribe_subscribers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easyii_tags`
--

DROP TABLE IF EXISTS `easyii_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easyii_tags` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `frequency` int(11) DEFAULT '0',
  PRIMARY KEY (`tag_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easyii_tags`
--

LOCK TABLES `easyii_tags` WRITE;
/*!40000 ALTER TABLE `easyii_tags` DISABLE KEYS */;
INSERT INTO `easyii_tags` VALUES (1,'php',2),(2,'yii2',3),(3,'jquery',3),(4,'html',1),(5,'css',1),(6,'bootstrap',1),(7,'ajax',1),(39,'sport',1),(10,'Awards',2),(35,'College Fest',1),(40,'IIFA',1);
/*!40000 ALTER TABLE `easyii_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easyii_tags_assign`
--

DROP TABLE IF EXISTS `easyii_tags_assign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easyii_tags_assign` (
  `class` varchar(128) NOT NULL,
  `item_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  KEY `class` (`class`),
  KEY `item_tag` (`item_id`,`tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easyii_tags_assign`
--

LOCK TABLES `easyii_tags_assign` WRITE;
/*!40000 ALTER TABLE `easyii_tags_assign` DISABLE KEYS */;
INSERT INTO `easyii_tags_assign` VALUES ('yii\\easyii\\modules\\news\\models\\News',1,3),('yii\\easyii\\modules\\news\\models\\News',1,2),('yii\\easyii\\modules\\news\\models\\News',1,1),('yii\\easyii\\modules\\news\\models\\News',2,2),('yii\\easyii\\modules\\news\\models\\News',2,3),('yii\\easyii\\modules\\news\\models\\News',2,4),('yii\\easyii\\modules\\article\\models\\Item',1,1),('yii\\easyii\\modules\\article\\models\\Item',1,5),('yii\\easyii\\modules\\article\\models\\Item',1,6),('yii\\easyii\\modules\\article\\models\\Item',2,2),('yii\\easyii\\modules\\article\\models\\Item',2,3),('yii\\easyii\\modules\\article\\models\\Item',2,7),('app\\modules\\eventcategories\\models\\EventCategories',1,39),('app\\modules\\events\\models\\Events',2,40),('app\\modules\\events\\models\\Events',3,10),('app\\modules\\events\\models\\Events',4,35),('app\\modules\\events\\models\\Events',2,10);
/*!40000 ALTER TABLE `easyii_tags_assign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easyii_texts`
--

DROP TABLE IF EXISTS `easyii_texts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easyii_texts` (
  `text_id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  `slug` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`text_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easyii_texts`
--

LOCK TABLES `easyii_texts` WRITE;
/*!40000 ALTER TABLE `easyii_texts` DISABLE KEYS */;
INSERT INTO `easyii_texts` VALUES (1,'Welcome on OnSpon demo website','index-welcome-title');
/*!40000 ALTER TABLE `easyii_texts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test` (
  `test` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test`
--

LOCK TABLES `test` WRITE;
/*!40000 ALTER TABLE `test` DISABLE KEYS */;
/*!40000 ALTER TABLE `test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Parminder Singh','CsrHWBYk2grypZ-IhE0HmJg7jCCVAp4u','$2y$13$uDHY8TujW..vKDBpXdIGReeNjltIN7HZ0gD6VK5HP3iG0v3UGn5B2',NULL,'developing.muc@gmail.com',10,1475575813,1475575813),(2,'Parminder Singh Sidhu','lSDj2FkzlHOTEGSJpfMTEkpoHg3tQxDW','$2y$13$95CQQ324Wjoaw0lbg/rwHOgriEY9dnkNgmJS0l3NL03g.xNzviLsC',NULL,'pammysayshello@gmail.com',10,1475647638,1475647638),(3,'Suma Mathew','2iaRkxTH4_KeSxJl8ZSOVpsqB7wEcaYZ','$2y$13$HJnwU12ADviPkrpNiFGuZu2CJp81dX/w0BS6EDZFtKWohOk7tIL0y',NULL,'suma.abey@gmail.com',10,1475652069,1475652069);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-18 11:02:51
